"""
Scribe3

The Internet Archive bookscanning software
"""
import scribe_globals
from utils.metadata import get_sc_metadata
from utils.util import resource_path

__author__ = "Davide Semenzin"
__copyright__ = "Copyright 2017, Internet Archive"
__credits__ = ["Raj Kumar", "Giovani Damiola", "Filip Radovic", "Wester De Weerdt"]
__maintainer__ = "Davide Semenzin"
__email__ = "davide@archive.org"
__status__ = scribe_globals.release_status
__date__ = scribe_globals.release_date
__version__ = scribe_globals.release_version

# Scribe3 uses kivy to draw the UI and manage workers and threads.
#
# At the moment the main UI component and views are:
#
#      ScribeWidget: the container and screen manager
#      UploadWidget: the main view with booklist etc
# CalibrationWidget: the calibration view
#
# the widgets templates and behaviour are in the file scribe.kv
# as the screen manager status, and views.
#
#            Screen: upload_screen - managed by the UploadWidget - the main view
# CalibrationScreen: calibration_screen
#     CaptureScreen: capture_screen
#       AboutScreen: about_screen
#    SettingsScreen: settings_screen
#
# There are two threads: the 'main thread' and the 'worker thread', 
#  plus one thread per camera (1-3)
#

import os
from os.path import join

# Config must be updated before any other Kivy import for Config values to
# take effect on Window.
from kivy.config import Config

Config.set('kivy', 'window_icon', join(scribe_globals.app_working_dir,
                                       'images', 'window_icon.png'))
Config.set('kivy', 'exit_on_escape', 0)
Config.set('input', 'mouse', 'mouse,disable_multitouch')
Config.set('graphics', 'width', 1450)
Config.set('graphics', 'height', 850)

from kivy.app import App
from kivy.logger import Logger
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen
from kivy.core.audio import SoundLoader
from ia_scribe.uix.republisher_note import RepublisherNote
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from functools import partial
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from Queue import Queue
import shutil
import subprocess
import threading, thread
import time
import xml.etree.ElementTree as ET

# pip install packages
##

# ttScribe packages
##
import scribe_config
import ia_scribe.capture.capture_action_detector as capture_action
from ia_scribe.uix.behaviors.focus import FocusBehavior
from ia_scribe.uix.top_bar import TopBar
from ia_scribe.uix.message import ScribeMessage, ScribeMessageOKCancel
from optics.camera import Camera, Cameras, fake_cameras

# Set the cacert.pem for HTTPS in packaged app
os.environ['REQUESTS_CA_BUNDLE'] = 'cacert.pem'
Builder.load_file('scribe.kv')

# we're optimists
configuration_ok = True


# ScribeWidget
# this class initialize the main kivy UI container
# the layout is defined in the scribe.kv file at <ScribeWidget>:
#
# this function is use to initizlize the capture_threads to manage the capture image process
# ___________________________________________________________________________________________
class ScribeWidget(BoxLayout):
    # left_camera        = StringProperty()
    # right_camera       = StringProperty()
    # left_camera_model  = StringProperty()
    # right_camera_model = StringProperty()
    cameras = ObjectProperty()

    try:
        cameras = Cameras({'left': None, 'right': None, 'foldout': None})
    except Exception as e:
        print("Failed to load cameras. Error: {0}".format(e))

    top_bar = ObjectProperty()

    # trigger_capture() - worker thread
    # at initialization three threads of the capture_thread function
    # _____________________________________________________________________________________
    def __init__(self, **kwargs):

        super(ScribeWidget, self).__init__(**kwargs)
        self.left_queue = Queue()
        self.right_queue = Queue()
        self.foldout_queue = Queue()
        screen_manager = self.ids['_screen_manager']
        screen_manager.bind(current=self._on_current_screen)
        self._init_top_bar()

        t = threading.Thread(target=self.capture_thread, args=(self.left_queue,))
        t.daemon = True
        t.start()

        t = threading.Thread(target=self.capture_thread, args=(self.right_queue,))
        t.daemon = True
        t.start()

        t = threading.Thread(target=self.capture_thread, args=(self.foldout_queue,))
        t.daemon = True
        t.start()

        config_path = os.path.expanduser(scribe_globals.config_dir)

        try:
            if os.stat(config_path + "/" + scribe_globals.scribe_config).st_size > 0:
                Logger.info("ScribeWidget: Found config file.")
                config = scribe_config.read()
                if 's3' not in config:
                    self.begin_wizard()
            else:
                print "empty file"
                self.begin_wizard()
        except OSError:
            print "No file"
            self.begin_wizard()

    # capture_thread()
    # this function manage the capture image process
    # _____________________________________________________________________________________
    def capture_thread(self, q):
        sound = SoundLoader.load('camera_shoot.wav')
        config = scribe_config.read()
        sound_delay = config.get('sound_delay') if 'sound_delay' in config else 0

        def psound():
            try:
                print "SOUND DELAY ->", sound_delay
                try:
                    if float(sound_delay) < 0:
                        print "negative value in sound = disabled"
                        return
                except:
                    pass
                sound.seek(0)
                time.sleep(float(sound_delay))
                sound.play()
            except Exception as e:
                Logger.exception("error {0} playing sound".format(e))
                pass

        Logger.warning('{0}: Starting capture thread {0} with Queue {1} '.format(threading.current_thread().ident, q))

        myfile = None
        clog = False
        if config.get('camera_logging', True):
            myfile = open("/tmp/tts_shoot_times.csv", "a")
            clog = True

        Logger.warning('{0}: Camera logging {1} with file {2} '.format(threading.current_thread().ident, clog, myfile))

        while True:
            side, path, thumb_path, callback, img_obj, stats = q.get()
            err = None
            if side == 'left':
                angle = 90
            elif side == 'right':
                angle = -90
            else:
                angle = 0

            port = self.cameras.camera_ports[side]

            camera = Camera(port, side)

            Logger.warning(
                '{th}: Capturing side {s} with port {p}, angle {a}, '
                'path {path} and fake {fake}'.format(
                    s=side, p=port, a=angle, path=path, fake=fake_cameras, th=threading.current_thread().ident))

            camera_start = time.time()
            # move this in try catch block when debug is finished
            try:
                print "playng sound"
                thread.start_new_thread(psound, ())
                print "sound played"
                camera.shoot(path)
            except subprocess.CalledProcessError as e:
                err = 'gphoto error: {r}'.format(r=e.returncode)
            except Exception as e:
                err = 'gphoto error'
            camera_end = time.time()

            Logger.info(
                "{th}: camera took {t:.2f}s".format(t=camera_end - camera_start, th=threading.current_thread().ident))

            # having a camera powered off won't cause an exception,
            # so check to see if the image was created
            if not os.path.exists(path):
                err = 'gphoto error'

            thumb_start = time.time()

            Logger.info("{th}: Generating thumbs".format(th=threading.current_thread().ident))
            if err is None:
                try:
                    # size = (576, 384)  # (4608,3072)/8
                    # size = (1152, 768) # (4608,3072)/4
                    size = (2304, 1536)  # (4608,3072)/2
                    im = Image.open(path)
                    im.thumbnail(size)
                    im = im.rotate(angle)

                    if fake_cameras:
                        Logger.info('fake_cameras is {0} (not False) -> Drawing debug output'.format(fake_cameras))
                        black = (0, 0, 0)
                        draw = ImageDraw.Draw(im)
                        font = ImageFont.truetype(resource_path(scribe_globals.font), 144)
                        draw.text((0, 0), os.path.basename(path), black, font=font)
                        if angle == 0:
                            height = size[1]
                        else:
                            height = size[0]
                        draw.text((0, height - 200), '{s} image'.format(s=side), black, font=font)
                        draw.text((0, height - 400), 'port {p}'.format(p=port), black, font=font)

                    im.save(thumb_path, 'JPEG', quality=90)
                except Exception as e:
                    err = 'thumbnail error' + str(e)
            thumb_end = time.time()
            Logger.info("{th}: Generated thumbs. Took {t}s".format(t=thumb_start - thumb_end,
                                                                   th=threading.current_thread().ident))

            if stats is not None:
                Logger.info('{th}: Stats = {s}'.format(th=threading.current_thread().ident, s=stats))
                stats['capture_time'] = '{t:.2f}s'.format(t=camera_end - camera_start)
                stats['thumb_time'] = '{t:.2f}s'.format(t=thumb_end - thumb_start)

            print "CLOG IS ", clog
            if clog:
                ctime = '{t:.2f}s'.format(t=camera_end - camera_start)
                ttime = '{t:.2f}s'.format(t=thumb_end - thumb_start)
                Logger.info('{th}: Writing  ctime = {ctime}, ttime = {ttime} to stats file {path}'.format(
                    th=threading.current_thread().ident, stats=stats if stats is not None else '',
                    ttime=ttime, ctime=ctime, path=myfile.name))
                lineout = "{0},{1},{2},{3},{4},{5},{6},{7}\n".format(time.time(), side, port, angle, ctime, ttime, path,
                                                                     threading.current_thread().ident, )
                myfile.write(lineout)

            if callback is not None:
                Clock.schedule_once(partial(callback, thumb_path, img_obj, side, err))
            q.task_done()

    def begin_wizard(self, ):
        self.ids['_screen_manager'].transition.direction = 'right'
        self.ids['_screen_manager'].current = 'wizard_screen'

    # back_to_library()
    # this function let the screen_manager go back to the library UI view
    # _____________________________________________________________________________________
    def back_to_library(self):
        manager = self.ids['_screen_manager']
        if manager.current != 'upload_screen':
            manager.transition.direction = 'right'
            manager.current = 'upload_screen'

    def _init_top_bar(self):
        self.top_bar = top_bar = TopBar()
        config = scribe_config.read()
        meta = get_sc_metadata()
        top_bar.username = meta.get('operator', None) or ''
        top_bar.machine_id = meta.get('scanner', None) or ''
        top_bar.bind(on_option_select=self._on_top_bar_option_select)
        # TODO: Better way to check if user is logged in
        if 's3' in config:
            self.add_widget(top_bar, len(self.children))

    def _on_top_bar_option_select(self, top_bar, option):
        # TODO: Don't hardcode top bar options
        if option == 'home':
            self.back_to_library()
        elif option == 'settings':
            self.show_settings_screen()
        elif option == 'logout':
            self.show_logout_screen()
        elif option == 'calibrate_cameras':
            self.show_calibration_screen(target_screen='upload_screen')

    def _on_current_screen(self, screen_manager, current):
        self.top_bar.use_back_button = current != 'upload_screen'

    def show_settings_screen(self):
        manager = self.ids['_screen_manager']
        manager.get_screen('about_screen').setup_manager(manager)
        manager.transition.direction = 'left'
        manager.current = 'about_screen'

    def show_logout_screen(self):
        # Only using logout screen method `archive_logout`
        # which logouts the user and restarts the app
        manager = self.ids['_screen_manager']
        login_screen = manager.get_screen('login_screen')
        login_screen.archive_logout()

    def show_calibration_screen(self, target_screen='capture_screen'):
        # set target screen
        self.ids['_calibration_screen'].target_screen = target_screen
        manager = self.ids['_screen_manager']
        manager.transition.direction = 'left'
        manager.current = 'calibration_screen'


# ReScribeScreen()
# _____________________________________________________________________________________
class ReScribeScreen(Screen):
    scribe_widget = ObjectProperty(None)
    screen_manager = ObjectProperty(None)
    book = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ReScribeScreen, self).__init__(**kwargs)

    def on_enter(self):
        self.get_book_notes(self.book)
        self.ids['_title_label'].text = "ReScribing " + self.book["identifier"]
        self.ids['_button_upload'].disabled = not self.is_rescribing_complete()

        if os.path.exists(os.path.join(self.book['path'], 'reshooting', 'done')):
            msg_box = ScribeMessage()
            msg_box.text = "The book you selected is queued\nfor upload back to RePublisher."
            popup = Popup(title='Done!', content=msg_box,
                          auto_dismiss=False, size_hint=(None, None), size=(400, 300))
            msg_box.popup = popup
            msg_box.trigger_func = self.popup_dismiss_to_home
            popup.open()

    def popup_dismiss_to_home(self, popup):
        popup.dismiss()
        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = 'upload_screen'

    def on_leave(self):
        self.ids['_notes_layout'].clear_widgets()

    def get_book_notes(self, book):
        tree_path = join(book['path'], "correction_scandata.xml")

        try:
            assert os.path.isfile(tree_path) is True
            pages = [elem for event, elem in ET.iterparse(tree_path) if event == "end" and elem.tag == "page"]
        except:
            print "EXCEPTION CAUGHT: CORRECTIONS ARE NOT PRESENT"
            return

        pages = [elem for event, elem in ET.iterparse(tree_path) if event == "end" and elem.tag == "page"]

        for item in pages:
            if item.find("note") is not None:
                row = RepublisherNote(page=item.attrib["leafNum"], note=item.find("note").text, book=self.book,
                                      screen_manager=self.screen_manager, scribe_widget=self.scribe_widget)
                self.ids['_notes_layout'].add_widget(row)

    def has_rescribing_began(self):
        ret = all([x.is_done() == False for x in self.ids['_notes_layout'].children])
        print "Has rescribing began? == Has hany page been reshot? ", not ret, " Book: ", self.book
        return not ret

    def is_rescribing_complete(self):
        ret = all([x.is_done() == True for x in self.ids['_notes_layout'].children])
        print "is is_rescribing_complete? ", ret, " Book: ", self.book
        return ret

    def package_and_schedule_for_upload(self):
        if self.is_rescribing_complete():
            # popup
            msg_box = ScribeMessageOKCancel()
            msg_box.text = 'Do you want to UPLOAD these corrections\nback to RePublisher?'
            msg_box.ok_text = 'Yes, upload.'
            msg_box.trigger_func = self.trigger_upload
            popup = Popup(title='Upload corrections', content=msg_box,
                          size_hint=(None, None), size=(400, 300))
            msg_box.popup = popup
            popup.open()
        else:
            print "ERROR: The book isn't done rescribing."

    def trigger_upload(self, popup):
        # create the done file
        f = open(os.path.join(self.book['path'], 'reshooting', 'done'), 'w+')
        f.close()
        self.popup_dismiss_to_home(popup)


# this class tarts the UI and the app
# _________________________________________________________________________________________
class Scribe3(App):
    def build(self):
        self.title = 'Scribe3 v{v}'.format(v=__version__)
        self.setup_files()
        Window.bind(on_touch_up=self.on_window_touch_up)
        return ScribeWidget()

    def setup_files(self):
        config_dir = os.path.expanduser(scribe_globals.config_dir)
        app_config = scribe_globals.capture_action_bindings
        default_config = scribe_globals.default_capture_action_bindings
        print "testing if", config_dir, "exists: ", os.path.exists(config_dir)
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)
            print "Created", config_dir
        if not os.path.exists(app_config):
            print app_config, "does not exist"
            print default_config, "->", app_config
            shutil.copy(default_config, app_config)
            return
        cls = capture_action.CaptureActionDetector
        detector = cls(app_config, auto_init=False)
        version = detector.load_version()
        if version != scribe_globals.capture_action_bindings_version:
            shutil.copy(default_config, app_config)

    def on_window_touch_up(self, window, touch):
        # Because FocusBehavior is ported from Kivy 1.9.1,
        # method `_handle_post_on_touch_up` must be called to unfocus
        # widgets when touch_up happen outside of FocusBehavior widget.
        # Link: https://github.com/kivy/kivy/blob/1.9.1/kivy/core/window/
        # __init__.py#L1035
        FocusBehavior._handle_post_on_touch_up(touch)


if __name__ == '__main__':
    Scribe3().run()
