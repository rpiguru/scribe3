from kivy.app import App
from kivy.lang import Builder

import ia_scribe.uix.cover_leaf_menu_bar

kv = '''
FloatLayout:
    CoverLeafMenuBar:
        id: menu_bar
        use_tooltips: True
        size_hint: None, None
        height: menu_bar.height
        width: '400dp'
        pos_hint: {'center_x': 0.5, 'center_y': 0.5}
        on_option_select: label.text = 'Selected: ' + args[1]
    Label:
        id: label
        size_hint_y: None
        font_size: '22sp'
'''


class CoverLeafMenuBarApp(App):

    def build(self):
        root = Builder.load_string(kv)
        menu = root.children[1]
        menu.bind(on_option_select=self.on_menu_option_select)
        return root

    def on_menu_option_select(self, menu, option):
        print('Selected', option)


if __name__ == '__main__':
    CoverLeafMenuBarApp().run()
