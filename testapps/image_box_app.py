from kivy.app import App

from ia_scribe.uix.image_box import ImageBox


class ImageBoxApp(App):

    def build(self):
        return ImageBox()


if __name__ == '__main__':
    ImageBoxApp().run()
