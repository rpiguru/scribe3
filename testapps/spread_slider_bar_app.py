from kivy.app import App
from kivy.core.window import Window

from ia_scribe.uix.spread_slider_bar import SpreadSliderBar


class SpreadSliderBarApp(App):

    def build(self):
        Window.clearcolor[:] = [0.92, 0.92, 0.92, 1.0]
        root = SpreadSliderBar(use_tooltips=True)
        root.slider.max = 100
        return root

    def on_slider_value_pos(self, slider, pos):
        slider.tooltip = str(pos)


if __name__ == '__main__':
    SpreadSliderBarApp().run()
