[size=18][b] Version 1.30 [/b][/size]

Scribe3 1.30 introduces a significant UI refresh. While maintaining the field-tested book scanning workflow intact, this update introduces changes to four main areas: the app frame, the library list, the scanning UI and the settings, as well as adding a number of functional features.

[size=16][b][i]New features[/b][/i][/size]

[b]Top bar[/b] - where navigation through app screens happens, including a consolidated "back to home" button that replaces multiple controls that used to exist in disparate places in the app. On the right-hand side, a dropdown menu is available to navigate into the settings (in place of the simple gear icon); this menu show the currently logged-in user, as well as the scanner name. 

[b]Book bar[/b] - in scanning screen allows for direct edit of books' metadata, as well as quick actions.

[b]Leaf bar[/b] - Allows quick access to actions (import, export, see original) per every leaf, including calibration

[b]Book bar[/b] - Allows you to navigate inside a book and displays information when you hove on the knob: [current spread][total # spreads] | [leaf left][leaf right] | [asserted_left][asserted_right]

[b]Autoshoot timer[/b] allows for automatic picture-taking after a configurable timeout (default 4 seconds). Use with caution as it may overload the camera bus and result in overall slowness.

[b]MARC records & MARC catalogs[/b] - In the book metadata view, this system allows for querying partners' catalogs via IA's z39.50 connections and add MARCXML and MARC binaries to the item being scanned. Before doing this, please remember to set up your catalogs in the "Catalogs" settings tab.

[b]Do We Want It?[/b] This feature is designed specifically for IA internal scanning centers, and allows the user to evaluate a book (by scanning or typing its isbn) and check on IA whether we actually need to scan it or not. Actions and workflows are supported for boxing where necessary. The wizards can be accessed from library list, as well as from a book's cover view.

[b]Library list[/b] - now highlights selected books, presents more and better options interaction options and displays book identifier where present

[b]Key bindings[/b] - use the keyboard to trigger key actions like shoot, reshoot, autoshoot, delete, assert leaf. You can customize these via the ~/.scribe/capture_action_bindings.json. This feature allows you to scan an entire book without ever touching the mouse.

[b]Refreshed settings[/b] - now less crash-prone and improved, settings have been revamped and new tabs and controls added.

[b]"About your scribe"[/b] - info panel shows an overview of your metadata, camera status, books, etc at a glance

[b]Use TAB[/b] to cycle through text boxes

[b]Tooltips[/b] - new and old UI components now have tooltips 

[b]Preloaded items in offline mode[/b] - Scribe3 now allows you to load a preloaded item even when IA is not reachable and verifies later that the identifier and metadata are correct, letting you correct it in case of error.

[b]Exif tags in JP2[/b] - If exiftool is installed on the system, Scribe3 will copy EXIF data from the source jpgs to the JP2s in preimage.zip. If Exiftool is not available, a banner will be displayed in settings

[b]Import[/b] a jpg image as a leaf

[b]Export[/b] - now every picture taken by Scribe3, including the calibration images, can be seen in original and exported. 

[b]Native file chooser[/b] - Use ubuntu's default file chooser for import/export functionalities, resulting in a much faster and consistent user experience.

[b]UTF-8 support[/b] - The ui now fully supports other character sets like thai or chinese (UTF-8)

[b]Collection settings toggle[/b] - Enable this in settings to access the collections set editor, without having to go through the wizard again

[size=16][b][i]Improvements[/b][/i][/size]

- New unified back button consistenly located on top bar

- Dismiss scan cover popup with ENTER or ESC

- Logging is improved and unnecessary data dumps have mostly been removed from the Scribe3 execution log.

- Tidier, friendlier camera logs in the format: [unix timestamp], [side], [port], [angle], [camera time], [thumbs time], [path], [thread]

- Page assertions: Assert any page as foldout, and assert any foldout page (shot with LIC) as any page type.

- Rejected corrections now handled. 

[size=16][b][i]Fixes[/b][/i][/size]

- Spinners are back!

- Correct handling of the 'creator' field

[size=14][b][i]Upgrade notes[/b][/i][/size]

This version does not require significant input from the user and should work out of the box upon update. Please not that it does make some breaking changes to the settings file, which may no longer be readable by previous version, was the user to downgrade (this issue can be addressed by deleting the file)




[size=18][b] Version 1.22 [/b][/size]

[size=16][b][i]New features[/b][/i][/size]

- [b]Camera shoot sound[/b] - Scribe3 now produce a shoot sound upon pressing the "SHOOT" button.

  The delay can be controlled by the non-UI facing setting "sound_delay" in camera_setting.yml like so:
      camera_shoot = [time in milliseconds]

      for example

      sound_delay = 0.5

  A default value of 0.1 is assumed if this value is not present. To turn off the feature, set

      sound_delay = -1

  In order for this feature to work, speakers are necessary. 

[size=16][b][i]Fixes[/b][/i][/size]

- [b]Non-blocking corrections download[/b]

    In versions up to 1.22, corrections would download on app startup, blocking execution until finish. In this version
    corrections download in the background - though in the same thread. As a result, the user will not see the books in the
    list until they are downloaded (which may take a while).

    This change doesn't require user interaction to work.

[size=16][b][i]Updates[/b][/i][/size]

- All Sony cameras now use Gphoto v.2.5.10

[size=14][b][i]Upgrade notes[/b][/i][/size]

    
Required items:
    - SPEAKERS: This version requires speakers (embedded in the HDMI monitor for stations issued in 2017 or external if older).
    - Internet connection

Update as usual by clicking on the "update" button
