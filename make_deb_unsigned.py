#!/usr/bin/env python
"""This script creates a signed debian repository in the directory 'apt-repo'

"""

import os
import shutil
import subprocess

import scribe_globals

version = scribe_globals.release_version

build_number = subprocess.check_output(['git', 'describe', '--tags'])
with open('build_number', 'w+') as file:
	file.write(build_number)

if scribe_globals.fake_cameras:
    import sys
    sys.exit('fake_cameras should only be used for debugging, and not in the .deb-packaged version!')


from distutils.version import StrictVersion
v = StrictVersion(version)
major, minor, patch = v.version

deb_version  = '{major}.{minor}-{patch}'.format(major=major, minor=minor, patch=patch)
print "-> Deb version is {0}".format(deb_version)
deb_dir_path = 'ia-scribe_{v}'.format(v=deb_version)
if os.path.exists(deb_dir_path):
    shutil.rmtree(deb_dir_path)

shutil.copytree('deb_pkg', deb_dir_path)

subprocess.check_call(["pyinstaller", "scribe.spec", "--clean"])

scribe_binary = 'dist/ia-scribe'
assert os.path.exists(scribe_binary)

binary_path = os.path.join(deb_dir_path, 'usr/local/bin')
os.makedirs(binary_path)
shutil.copy(scribe_binary, binary_path)

control_file = os.path.join(deb_dir_path, 'DEBIAN/control')
assert os.path.exists(control_file)

f = open(control_file, 'a')
f.write('Version: {v}\n'.format(v=deb_version))
f.close()

print "Built version {0} with success".format(build_number)