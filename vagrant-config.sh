#!/bin/sh

## This is the provisioning script used to generate the Vagrant VBox image ttscribe/precise32
## starting from the hashicorp/precise32


## update the apt repo
sudo apt-get -y update

## upgrade all the packages
sudo  DEBIAN_FRONTEND=noninteractive apt-get --no-install-recommends -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade

## install some packages for ubuntu
sudo  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y gdm gnome-shell ubuntu-desktop gnome-panel
sudo apt-get install -y unity-place-files unity-place-applications ttf-ubuntu-font-family

## install some extra package
sudo apt-get install -y --no-install-recommends git vim 
sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11

## remove some package
sudo apt-get remove -y update-manager

## install python packages
sudo apt-get install --no-install-recommends -y build-essential python-dev python-pip 
sudo apt-get build-dep -y python-pygame && sudo apt-get install --no-install-recommends -y python-pygame
sudo apt-get install -y python-virtualenv
sudo apt-get install -y --no-install-recommends gphoto2 dpkg-sig


## disable autologin for gdm
printf '[daemon]\nAutomaticLoginEnable=true\nAutomaticLogin=vagrant\nDefaultSession=gnome\n' | sudo tee /etc/gdm/custom.conf

## add gnome-terminal
# dbus-launch --exit-with-session  gsettings set com.canonical.Unity.Launcher favorites "['nautilus-home.desktop', 'ubuntu-software-center.desktop', 'gnome-control-center.desktop','gnome-terminal.desktop']"

## mounting vagrant shared forlder
echo '/vagrant  /vagrant    vboxsf  uid=1000,gid=1000   0   0' | sudo tee -a  /etc/fstab

sudo reboot

exit 0
