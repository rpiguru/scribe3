# Scribe3 [![build status](https://git.archive.org/ia-books/scribe3/badges/master/build.svg)](https://git.archive.org/ia-books/scribe3/commits/master)
## The Internet Archive's bookscanning software

1. [Introduction](#1-introduction) 
    1. [Features](#11-features) 
2. [Requirements](#2-requirements)
4. [Environment](#4-configuration)
   1. [Running](#41-running)
   2. [Virtualenv](#42-virtualenv)
   3. [VagrantBox](#43-vagrant-box)
   4. [Libraries](#44-libraries)
   5. [Debian package](#45-debian-package)
5. [Configuration](#5-Configuration)
   1. [Initial Setup](#51-initial-setup) 
   2. [Metadata](#53-metadata) 
   3. [Collection sets](#53-collection-sets)
   4. [Catalogs](#54-catalogs)
   5. [Cameras](#55-cameras)
   6. [Other](#56-other)
6. [Links](#6-links)
7. [Contacts](#7-contacts)
8. [Acknowledgements](#8-acknowledgements)


## 1. Introduction
Scribe3 is the software the powers the [ttScribe system](https://archive.org/details/tabletopscribesystem) for book scanning in use at the Internet Archive. Founded in 1996, the [Internet Archive](http://www.archived.org) is one of the world’s largest public digital libraries, with an extensive collection of human culture: more then 2 million books, 450 billion Web pages, 3 million hours of television and more. Its purposes include offering permanent access for researchers, historians, scholars, people with disabilities, and the general public to historical collections that exist in digital format.

### 1.1 Key features
- Scan books with 1, 2 or 3 cameras (see [Cameras](#55-cameras) for supported models) and upload to Archive.org
- Report production metrics to IABDASH
- Get books metadata by ISBN or IA identifier (preloaded items)
- Print labels
- Do-we-want-it feature for internal scancenters
- Fetch MARC records from all IA partner catalogs
- Import/export images/books
- Configurable key bindings


## 2. Requirements
- Scribe3 is designed to support the [Table Top Scribe](https://archive.org/details/tabletopscribesystem) book scanner, as well as legacy Scribe2 systems (Full Frame Scribes and Folio stations) workflows for both internal and external scancenters. 
- Scribe3 is a GUI application. Its native execution environment is 32bit (i686) Ubuntu 14.04.5 LTS.
- Scribe3 uses Archive.org as a backend through its [S3-like](https://archive.org/help/abouts3.txt) API, through the IABDASH metrics system, the BTSERVER items system for corrections - as well as Cluster Republisher. 
- [gphoto2](http://www.gphoto.org/) is required to interface with cameras. It's free software [LGPL license](https://www.gnu.org/licenses/lgpl-3.0.en.html).

        
Before installing the python requirements for the ttScribe it's a good idea to use a [*virtualenv*](http://docs.python-guide.org/en/latest/dev/virtuale$
        
        # You will need --sytem-site-packages if you have Cython, 
        # python-apt and Pygame installed system-wide. 

        virtualenv --system-site-packages venv
        source venv/bin/activate

install then the requirements:

        pip install -r requirements.txt

run ttScribe executing this command in a graphic terminal:

        python scribe.py


## 4. Environment
This section describes how to install the necessary dependencies to run Scribe3. 

### 4.1 Running
The following instructions apply to a system-wide installation. 

Start from an Ubuntu 14.04.05 LTS i686 image

        sudo apt-get update
        sudo apt-get upgrade

Install the dependencies you'll need to compile Cython

        sudo apt-get install build-essential python-dev python-pip python-apt git python-virtualenv libffi-dev libssl-dev cups libcups2-dev

If you plan on installing pygame in virtualenv, please see the notes in section 5.1
before installing pygame as follows

        sudo apt-get build-dep python-pygame
        sudo apt-get install python-pygame
        
Install *Cython 0.20.1* and *numpy* :

        sudo pip install numpy
        sudo pip install Cython==0.20.1

Then follow the steps in section [3](#3)):

### 4.2 Virtualenv
If you want to run scribe from within a **virtualenv**, you have to consider that
*Cython 0.20.1* is an hard requirement for this software to execute and must be installed
system-wide. 

It is however possible to install *kivy* inside virtualenv. Instructions refer 
to those [provided by kivy](http://kivy.org/docs/installation/installation-linux.html#installation-in-a-virtual-environment-with-system-site-packages). 

        # Initialize virtualenv
        virtualenv -p python2.7 --system-site-packages venv
        
Note on ***pygame***: Please note that *Pygame* is more easily installed from apt and not included in the *pip*requirements file. This is because there seem to be some [issues](https://bitbucket.org/pygame/pygame/issue/140/pip-install-pygame-fails-on-ubuntu-1204) and hurdles with installing *pygame* within a virtualenv. Should you want to install it with *pip*, you should note that the package is not present in *pypy* and you need to install it from [bitbucket](https://bitbucket.org/pygame/pygame)) by following [these instructions](http://askubuntu.com/questions/299950/how-do-i-install-pygame-in-virtualenv/299965#299965) or use [this script](https://gist.github.com/brousch/6395214#file-install_pygame-sh-L4). 

Activate your virtualenv before running as above:

        source venv/bin/activate

### 4.3 Vagrant box

Use box-cutter/ubuntu1404-desktop as a base image. You can use the `Vagrantfile` in the repository or generate a new one using:
        
        vagrant init box-cutter/ubuntu1404-desktop

run the VM with:

        vagrant up

and then follow the instructions in the [quick start](#3-quick-start).

### 4.4 Libraries

* This project uses the [*Kivy framework*](http://kivy.org/#home). You can use this Vagrantfile to bootstrap
a development environment: https://github.com/rajbot/kivy_pyinstaller_linux_example

* A `gphoto` directory is also included that contains *libgphoto2* with Nikon J3 support,
built from the svn repo at commit r14922 (available in this [git-svn mirror](https://github.com/rajbot/gphoto))

### 4.4 Debian package
Scribe3 is distributed to scancenters through a [debian ppa](https://archive.org/download/scribe-repo-internal). In order to build the debian package, the `./make_deb.py` script is used. This process has additional dependencies (PyInstaller version 2.1, Kivy version 1.8.1, the GPG Keys for dpkg-sig, privs on IA ppa items). 

## 5. Configuration
Scribe3 requires a minimum amount of configuration to work. The following is necessary: 
* An archive.org account with privs for the collections you intend to upload to (for developers, that's `booksgrouptest`), as well as privs for `iabooks-btserver`
* Values for the following item metadata fields: `contributor`, `sponsor` and `scanningcenter` (if these are not set properly, they will cause redrows at billing time.)

### 5.1 Initial setup
The initial setup consists of three phases: 
* Login to archive.org
* Metadata setup
* Collections and collection sets setup

The `scanner` field should be in the form `[scanner_name].[scancenter].archive.org`; this is important because upon first run, Scribe3 will create an item with this hostname as an identifier, and will register it with IABDASH. If the account you logged in with is not priv'd for `iabooks-btserver`, this registration will fail and you will not be able to use corrections.

### 5.2 Metadata

Metadata that will be appended to every book uploaded can be configured in the "Metadata" options panel. In case of preloaded items, only `scanner` and `operator` values are updated upon upload.

### 5.3 Collection sets

There's no good way to say this: a collection set is a preset set of collections. It's often the case that you'll want your item to belong to more than one collection; Scribe3 operates on collections set. Collection set selection is mandatory for scanning non-preloaded items. 

### 5.4 Catalogs

Catalogs are being introduced in v1.30 as part of the MARC feature which allows querying IA MARC sources over our z39.50 through [Jude's api](http://www-judec.archive.org/book/marc/get_marc.php). This feature is available for preloaded items only.

### 5.5 Cameras
Scribe3 currently supports the following makers and models:

|  Maker |  Model |  gphoto2  | libgphoto2 |  Typical system |
|---|---|---|---|---|
| Nikon |  J3-5 | 2.5.4.1  | 0.10.0 | Old Internal TTS |
| Sony | Alpha 6000 | 2.5.10  | 0.12.0   | Internal TTS | 
| Sony | Alpha 6300  |2.5.10  | 0.12.0   | New TTS | 
| Sony | Alpha 7rII  |2.5.10  | 0.12.0   | FADGI TTS | 
| Canon | EOS 5D Mark II  |2.5.10  | 0.12.0   | Internal FFS | 


### 5.6 Other

- label printer
- barcode scanner
- MARC
- DWWI
- Update