import StringIO
import copy
import glob
import json
import logging
import shutil
import urllib
import uuid
import subprocess
import zipfile
from ia_scribe.upload.upload_status import UploadStatus, status_human_readable
from kivy.lang import Builder
from kivy.metrics import dp
from operator import itemgetter
from ia_scribe.uix.book_item_view import BookItemView
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from logging.handlers import SysLogHandler
import re
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from PIL import Image
import requests
import threading
import traceback
from Queue import Queue
from collections import Counter
from functools import partial
import xml.etree.ElementTree as ET
import time
from ia_scribe.uix.label import BlackLabel
import scribe_upload
from ia_scribe.scandata import ScanData
from ia_scribe.uix.message import ScribeMessage, ReScribe, IdentifierReservedMessage, ViewOrDeleteBookMessage, \
    ViewBookMessage, ReopenUploadMessage
from ia_scribe.xmltojson import xml2json
from os.path import join
import lazytable
import os
import scribe_config
from ia_scribe.uix.metadata import MetadataSwitch
from ia_scribe.uix.metadata import MetadataTextInput
from internetarchive.cli import ia
import scribe_globals
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.properties import ObjectProperty
from kivy.properties import StringProperty
from kivy.properties import NumericProperty
from kivy.uix.boxlayout import BoxLayout
from enum import Enum
from kivy.uix.screenmanager import NoTransition
from kivy.uix.switch import Switch
from optics.camera import Cameras
from scribe_globals import ScribeException

# UploadWidget
# this class initialize the main UI widget view: with the book list and the worker
# the worker is launched as a thead
# all the worker's functions are defined here
# all the main interactions with the cluster republisher happens here
# _________________________________________________________________________________________
from ttsservices.iabdash import push_event
from ttsservices.tts import TTSServices
from utils.metadata import get_collections_metadata, set_sc_metadata, get_sc_metadata, set_metadata
from utils.metadata import get_metadata
from utils.util import check_software_update, resource_path, download_annotated_scandata_from_republisher, UpdateStatus

Builder.load_file(os.path.join(os.path.dirname(__file__), 'upload_.kv'))

configuration_ok = True
update_status = UpdateStatus.unknown.value
worker_thread = None
worker_terminate = None


class UploadWidget(BoxLayout):
    num_books = NumericProperty()
    status_txt = StringProperty()
    # book_list = ListProperty([])
    scribe_widget = ObjectProperty(None)
    screen_manager = ObjectProperty(None)
    ia_session = ObjectProperty()
    book_metadata = ObjectProperty()

    # here are defined the values for the different Cluster RePulisher state
    class RepubState(Enum):
        # from RePublisherShared.inc
        uploaded = 10
        autocropped = 11
        failed_autocrop = 12
        post_autocropped = 13
        need_corrections = 14
        rerepublish = 15
        save_for_later = 16
        claimed = 17
        checked_in = 18
        derived = 19
        done = 20
        local = 21
        corrections_sent_to_ttscribe = 31
        book_download_to_ttscribe = 32
        corrections_ready_for_review = 33
        corrections_rejected = 34
        corrections_accepted = 35

    # LoadDeletedSwitch
    # _________________________________________________________________________________________
    class LoadDeletedSwitch(Switch):
        pass

    def load_deleted_switch_callback(self, value, *largs):
        try:
            instance = self.ids['_load_deleted_switch']
            instance.active = value
            self.config['load_deleted'] = value
        except Exception as e:
            print e

    # init()
    # book_list and worker run here when the widget starts
    # the worker is launched as a thread
    # _____________________________________________________________________________________
    def __init__(self, **kwargs):
        super(UploadWidget, self).__init__(**kwargs)

        global worker_thread

        self.logger = self.init_logger()
        self.book_list = []

        self.book_metadata = None

        t = threading.Thread(target=self.worker)
        t.daemon = True
        t.start()
        worker_thread = t

    def show_dwwi_wizard(self):
        """
        Display DWWI_Wizard popup
        :return:
        """
        capture_screen = self.scribe_widget.ids['_capture_screen']
        # capture_screen.dwwi_popup.inst_capture_cover = None
        capture_screen.dwwi_popup.capture_screen = capture_screen
        capture_screen.dwwi_popup.sm.transition = NoTransition()
        capture_screen.dwwi_popup.go_screen('search', 'left')
        capture_screen.dwwi_popup.launched_from = 'Upload'
        capture_screen.dwwi_popup.open()

    # reload_book_list()
    # this function updates the book list view
    # it it the main thread
    # _____________________________________________________________________________________
    def reload_book_list(self, q, book_list, processed_books, *largs):
        """
        This function needs to be called on the main thread, since it updates the UI
        """
        # self.status_label.text = 'Refreshing book list'
        self.status_txt = 'c book list'
        # self.num_books_label.text = 'Number of books: ' + str(len(self.book_list))
        self.num_books = len(book_list) + len(processed_books)
        Logger.debug('Reloaded book_list: Total books = {0}'.format(len(book_list)))

        # hack to fix broken books in the db which have a NULL date
        for book in book_list:
            if 'date' not in book:
                book['date'] = None

        q.get()
        self.upload_file_list.reset_book_list(self.book_list, processed_books)
        q.task_done()

    # worker init - worker thread
    # this worker reloads the book list with the books status from the user's drive and the db
    # and update the UI, on the main thread every 60 seconds.
    # __________________________________________________________________________________________
    def worker(self):
        import time
        global configuration_ok
        global update_status
        global worker_terminate
        # global camera_lib
        # there are two main sections to this thread. This function is the entry point
        # that is called from run(). It will first initialize itself, loading configs and db,
        # then enter a loop where it goes through all the books and syncronizes
        # them with Cluster Republisher, then sleeps for 60 seconds.
        Logger.info('Worker: Begin')
        try:
            Clock.schedule_once(partial(self.set_status_callback, "Initializing worker"))
            self.init_pid_file()
            self.config = self.init_config()
            HEARTBEAT_INTERVAL = 15

            # check that collection sets are present
            csets = get_collections_metadata(scribe_globals.config_dir)
            try:
                if not csets['collections']:
                    raise ScribeException('No collection set is defined.')
            except:
                raise ScribeException('No collection metadata file.')
            Logger.info('Worker: Collection sets OK')
            # upload_db should be accessed only from worker thread
            self.upload_db = self.init_upload_db()
            Logger.info('Worker: Upload db OK')

            # WARNING: If we call this function here, book status date will not be updated anymore.
            # Call this in the `while` loop bellow?
            self.book_metadata = self.get_books()

            # Upload the switch widget with the value read from config
        except ScribeException as e:
            # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, e.msg))
            configuration_ok = False
            Logger.error('Worker:: ScribeException in worker -> {0}'.format(e.msg))
            Clock.schedule_once(partial(self.set_status_callback, e.msg))
            self.logger.exception(e)
            return
        except Exception as e:
            # you could end up in here by having an empty config yml file.
            msg = 'Initialization error. Your metadata configuration may be missing or incorrect.'
            Logger.error('Worker:: Exception in worker -> {0}'.format(e))
            configuration_ok = False
            # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, msg))
            Clock.schedule_once(partial(self.set_status_callback, msg))
            self.logger.exception(e)
            return

        # anyway if there is no config, just quit
        if self.config is None:
            configuration_ok = False
            return

        Logger.info('Worker: Configuration OK = {0}'.format(configuration_ok))

        Clock.schedule_once(partial(self.set_status_callback, "Checking for software update"))
        # check for software updates
        check_software_update()

        Logger.info('Worker: Update status is {0}'.format(update_status))

        Logger.info('Worker: Initializing Archive.org session')
        access = self.config['s3']['access_key']
        secret = self.config['s3']['secret_key']
        self.ia_session = ia.get_session(
            config={'general': {'secure': False}, 's3': {'access': access, 'secret': secret},
                    'cookies': {'logged-in-user': self.config['email'], 'logged-in-sig': self.config['cookie']}})

        Logger.info('Worker:: Initialized Archive.org session with SSL = {0}'.format(self.ia_session.secure))
        Logger.info('Worker:: Pushing init to IABDASH')
        Clock.schedule_once(partial(self.set_status_callback, "Pushing init to metrics server"))
        local_metadata = dict(
            (k, v) for k, v in get_metadata(os.path.expanduser(scribe_globals.config_dir)).iteritems() if v)
        local_settings = dict(k for k in self.config.iteritems() if k[0] not in ['s3', 'cookie'])
        collection_sets = get_collections_metadata(scribe_globals.config_dir)['collections']
        camera_config = None
        # Being a little paranoid here
        try:
            a = Cameras()
            camera_config = a.get_raw_config()
        except Exception:
            Logger.error(traceback.format_exc())
            Logger.error("Could not read the camera config, pushing None to IABDASH")

        payload = {'local_metadata': local_metadata, 'local_settings': local_settings, 'camera_config': camera_config,
                   'collection_sets': collection_sets}
        push_event('tts-initialized', payload)

        Logger.info('Worker:: Init complete. Starting main loop.')
        Clock.schedule_once(partial(self.set_status_callback, "Starting books worker"))
        # otherwise, let's go
        i = 0
        pushcounter = 14
        while True:
            Logger.info('Worker: Loop:: i={0}, pushcounter ={1}/{2}'.format(i, pushcounter, HEARTBEAT_INTERVAL))

            # First of all sets up the load stale books toggle to the value in config, or assume false
            try:
                Clock.schedule_once(partial(self.load_deleted_switch_callback,
                                            self.config['load_deleted']))
            except:
                Clock.schedule_once(partial(self.load_deleted_switch_callback,
                                            False))

            Clock.schedule_once(partial(self.set_status_callback, "Loading books..."))
            # Reload the book list from the user's drive and the db
            self.book_list = self.load_books_from_drive()
            Logger.info('Worker: Loop:: Loaded {0} books from drive'.format(len(self.book_list)))
            processed_books = self.load_processed_books_from_db(self.book_list)
            Logger.info('Worker: Loop:: Loaded {0} books from catalog'.format(len(processed_books)))

            # Now check in the TTscribe item if there are other books that need to
            # to be downloaded.
            Clock.schedule_once(partial(self.set_status_callback, "Checking pending downloads..."))

            pending_downloads = self.pending_downloads()
            pending_downloads_count = len(pending_downloads) if pending_downloads else 0
            Logger.info('Worker: Loop: Found {0} pending books to dowload'.format(pending_downloads_count))
            if pending_downloads is not None:
                try:
                    Logger.info('Worker: Loop: Loaded {0} books from catalog'.format(len(processed_books)))
                    self.download_books(pending_downloads)
                    self.book_list = self.load_books_from_drive()
                    processed_books = self.load_processed_books_from_db(self.book_list)
                except:
                    msg = "Couldn't download books from btserver. Skipping..."
                    Clock.schedule_once(partial(self.set_status_callback, msg))
                    Logger.exception(msg)
                    pass

            # Update the UI, on the main thread.
            # Block until the book list has been reloaded
            q = Queue()
            q.put(True)
            Clock.schedule_once(partial(self.reload_book_list, q, self.book_list, processed_books))
            q.join()

            Logger.info('Worker:: Loop:: Now syncing {0} books'.format(len(self.book_list)))
            # go through every book in the hard drive and synchronize it
            for i, book in enumerate(self.book_list):
                Logger.warning('Worker:: Loop:: ------------------------------- Processing {0}/{1} '
                               '---------------------------------'.format(i + 1, len(self.book_list)))
                try:
                    self.process_book_dir(book)
                    Clock.schedule_once(partial(self.update_status_callback, book))
                except ScribeException as e:
                    Clock.schedule_once(partial(self.handle_error_callback, e.msg, book))
                    self.logger.exception(e)
                except Exception as e:
                    self.logger.exception(e)
                Logger.warning('Worker:: Loop:: ------------------------------- Processed {0}/{1} '
                               '----------------------------------\n'.format(i + 1, len(self.book_list)))

            # Now check in the TTscribe item if there are other books that need to
            # to be downloaded.
            Clock.schedule_once(partial(self.set_status_callback, "Checking pending downloads..."))
            pending_downloads = self.pending_downloads()

            if pending_downloads is not None:
                try:
                    self.download_books(pending_downloads)
                    # thread.start_new_thread(self.download_books, (pending_downloads,) )
                    # self.download_books(pending_downloads)
                    self.book_list = self.load_books_from_drive()
                    processed_books = self.load_processed_books_from_db(self.book_list)
                except Exception as e:
                    print e
                    msg = "Couldn't download books from btserver. Skipping..."
                    Clock.schedule_once(partial(self.set_status_callback, msg))
                    print msg
                    pass

            # Sleepy sleepy
            # Check btserver metadata is in sync
            try:
                mdc = self.check_metadata_registration()
                Logger.info('Worker: Loop: Metadata success {0}'.format(mdc))
            except Exception as e:
                msg = "Metadata update failed"
                Logger.exception('Worker:: Loop:: Scanner item registration failed with error {0}'.format(e))
                Clock.schedule_once(partial(self.set_status_callback, msg))
                pass

            from collections import Counter
            bookslist_stats = Counter(key for dic in self.book_list for key in dic.keys())
            books_by_state = Counter(value for dic in self.book_list for key, value in dic.items() if key == "status")

            # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, 'Waiting'))
            Clock.schedule_once(partial(self.set_status_callback, 'Waiting'))

            if pushcounter == HEARTBEAT_INTERVAL:
                Logger.info('Worker:: Loop:: Sending heartbeat to iabdash')
                local_metadata = dict(
                    (k, v) for k, v in get_metadata(os.path.expanduser(scribe_globals.config_dir)).iteritems() if v)
                # books_by_tts_state = {}

                payload = {'books_active': len(self.book_list),
                           'books_total': len(self.book_list) + len(processed_books),
                           'books_inactive': len(processed_books),
                           'books_by_tts_state': dict(books_by_state),
                           'bookslist_stats': dict(bookslist_stats),
                           }
                # 'scancenter' : mdc['scancenter'], }
                ret = {}
                ret.update(payload)
                ret.update(local_metadata)
                push_event('tts-heartbeat', ret)
                pushcounter = 0
            pushcounter += 1
            i += 1
            self.logger.debug("worker sleeping for 60s")
            time.sleep(60)

    def get_books(self):
        self.book_list = self.load_books_from_drive()
        processed_books = self.load_processed_books_from_db(self.book_list)
        bookslist_stats = Counter(key for dic in self.book_list for key in dic.keys())
        books_by_state = Counter(value for dic in self.book_list for key, value in dic.items() if key == "status")
        payload = {'books_active': len(self.book_list),
                   'books_total': len(self.book_list) + len(processed_books),
                   'books_inactive': len(processed_books),
                   'books_by_tts_state': dict(books_by_state),
                   'bookslist_stats': dict(bookslist_stats),
                   }
        return payload

    def check_metadata_registration(self):
        Logger.info('Check btserver registration: Begin.')

        config = scribe_config.read()
        local_metadata = dict(
            (k, v) for k, v in get_metadata(os.path.expanduser(scribe_globals.config_dir)).iteritems() if v)
        tts = TTSServices(config, local_metadata)
        upd = None
        try:
            upd = tts.is_metadata_updated(config['identifier'], local_metadata)
        except Exception as e:
            print "Error in check_metadata_registration", str(e)
            pass
        # if updated, tell metrics server
        Logger.info('Check btserver registration: Result = {0}'.format(upd))
        if upd is not True:
            Logger.info("Metdata is not up to date, pushing to btserver")
            try:
                tts.update_metadata_tts(config['identifier'], local_metadata)
                return True
            except:
                Logger.error("Failed to update metdata fields {0} for {1}".format(upd, local_metadata['identifier']))
                return False
        else:
            return True

    # init_pid_file() - worker thread
    # this function initialize the pid_file and check if other copy of the ttScribe are working
    # ___________________________________________________________________________________________
    def init_pid_file(self):
        Logger.info("PID Init: Begin")
        config_dir = os.path.expanduser(scribe_globals.config_dir)
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)

        if not os.access(config_dir, os.W_OK | os.X_OK):
            raise ScribeException('Config dir {d} not writable'.format(d=config_dir))

        # check to see if another copy of the app is running,
        # and if needed, remove stale pidfiles
        path = os.path.join(config_dir, 'scribe_pid')
        Logger.info("PID Init: Looking for pidfile at {0}".format(path))
        if os.path.exists(path):
            f = open(path)
            old_pid = f.read().strip()
            f.close()
            pid_dir = os.path.join('/proc', old_pid)
            if os.path.exists(pid_dir):
                self.logger.error("There seems to be a pid file at " + str(
                    pid_dir) + ". Try removing it and relaunching the applicaiton.")
                raise ScribeException('Another copy of the Scribe application is running!')
            else:
                os.unlink(path)

        pid = os.getpid()
        f = open(path, 'w')
        f.write(str(pid))
        f.close()
        Logger.info("PID Init: End")

    # init_config() init - worker thread
    # this function read the scribe_donfig file
    #
    # TODO: update the dialog strings
    # _____________________________________________________________________________________
    def init_config(self):
        Logger.info("Config initalization: Begin")
        global camera_lib

        md_version = {'tts_version': scribe_globals.build_number}
        set_sc_metadata(md_version)

        config = scribe_config.read()

        if 's3' not in config:
            raise ScribeException('You are logged out. Please login from the "Account" options screen.')

        if 'access_key' not in config['s3']:
            raise ScribeException('IAS3 Access Key not in scribe_config.yml')

        if 'secret_key' not in config['s3']:
            raise ScribeException('IAS3 Secret Key not in scribe_config.yml')

        if 'printer' not in config:
            Logger.info(
                "Config initalization: Printer not found in config. Setting default = DYMO_LabelWriter_450_Turbo ")
            try:
                scribe_config.update(config, 'printer', 'DYMO_LabelWriter_450_Turbo')
                scribe_config.save(config)
            except:
                raise ScribeException('Unable to set self default printer to DYMO_LabelWriter_450_Turbo')

        # Check whether the TTS has an item. If not, just go ahead and create it
        if 'identifier' not in config:
            Logger.info("Config initalization: No scanner identifier specified in config.")
            try:
                Logger.info("Config initalization: Reading scanner name from metadata")
                scanner_id = get_sc_metadata()['scanner']
                if scanner_id:
                    Logger.info("Found scanner id in metadata but not in settings. Adding that...", )
                    scribe_config.update(config, 'identifier', scanner_id)
                    scribe_config.save(config)
                    Logger.info("Done.")
                else:
                    Logger.error("Config initalization: No scanner name in metadata!")
            except:
                raise ScribeException('No "scanner" metadata specified. Please visit Settings.')
        else:
            Logger.info("Config initalization: Found identifier in config.")
        # check if camera logging is enabled
        if 'camera_logging' not in config:
            Logger.info("Config initalization: Camera logging is not activated.")
            pass
        Logger.info("Config initalization: Read the following settings -> {0}".format(config.keys()))
        return config

    def save_config(self):
        Logger.info("Config save: Begin")
        # read config file
        config = scribe_config.read()
        # make a copy
        new_config = copy.deepcopy(config)
        # select all widgets types that will be data sources
        widgets = [x for x in self.ids._metadata.children if isinstance(x, (MetadataSwitch))]
        # Update each
        for widget in widgets:
            val = self.get_val(widget)
            # Don't overwrite empty fields
            if isinstance(widget, MetadataTextInput):
                if not val:
                    continue
            scribe_config.update(new_config, widget.metadata_key, val)
        # if something has changed, save
        if cmp(config, new_config) != 0:
            scribe_config.save(new_config)
        Logger.info("Config initalization: End")

    # init_upload_db() init - worker thread
    # upload the books status from the db
    # _____________________________________________________________________________________
    def init_upload_db(self):
        db_path = os.path.expanduser(scribe_globals.config_dir + '/' + scribe_globals.db_file)
        t = lazytable.open(db_path, 'uploads', index_all_columns=True)
        return t

    # update_upload_db() init - worker thread
    # this function updates the book status into the db
    # _____________________________________________________________________________________
    def update_upload_db(self, book):
        Logger.debug('Update DB: Updating db for {0}'.format(book['uuid']))
        try:
            temp = book.copy()
            if 'widget' in temp:
                del temp['widget']
            if 'status_label' in temp:
                del temp['status_label']
            self.upload_db.upsert(temp, {'uuid': temp['uuid']})
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not update db')

    def pending_downloads(self, raw_list=False):
        identifiers_to_be_downloaded = []
        ttscribe_item = None
        Logger.info("Pending Download: Begin")

        try:
            ttscribe_item = self.ia_session.get_item(self.config['identifier'])
            Logger.info(
                "Pending Download: I am {0}: checking if I exist on archive.org ...".format(ttscribe_item.identifier, ))
            if not ttscribe_item.exists:
                Logger.info("Books Download: I do not exist on Archive.org! I am gonna go ahead and create an item.")
                dd = dict(
                    (k, v) for k, v in get_metadata(os.path.expanduser(scribe_globals.config_dir)).iteritems() if v)
                dd['books'] = "[]"
                config = scribe_config.read()
                tts = TTSServices(config, dd)
                success, tts_id = tts.register_tts(tts_id=dd['scanner'], metadata=dd)
                Logger.info("Pending Download: Success = {0}".format(success))
                # self.check_metadata_registration()
            else:
                Logger.info("Pending Download: I do! Let's check if I have any books on there...")

        except Exception as e:
            print "Pending Download: Error retrieving the ttscribe item. " \
                  "Abandoning attempt to look for downloads. Error was:", str(e)

        try:
            Logger.debug("Pending Download: Pending Downloads: {0}.".format(ttscribe_item.metadata['books']))
            raw_books_list = ttscribe_item.metadata['books'].replace('[', '').replace(']', '')
            item_books_list = raw_books_list.split(",")
            item_books_list = [x.strip() for x in item_books_list]
            Logger.debug("Pending Download: Parsed: {0}.".format(item_books_list))
            # item_books_list = ast.literal_eval(ttscribe_item.metadata['books'])
        except Exception as e:
            Logger.debug("Pending Download: Pending Downloads: No items found in item ({0}).".format(e.args[0]))
            return None
        # book is a string

        # get a list of the identifiers of local books on drive that have one
        books_redux = [element for element in self.book_list if 'identifier' in element]
        # check if we already have any item, if not add to download list
        for book in item_books_list:
            # book_item =  [element for element in self.book_list if element['identifier'] == book]
            # print "test", not any(d['identifier'] == book for d in books_redux), book
            if not any(d['identifier'] == book for d in books_redux):
                Logger.debug("Books Download: Pending Downloads: Adding {0} to download pool.".format(book))
                identifiers_to_be_downloaded.append(book)

        Logger.debug(
            "Pending Download: Pending Downloads: Downloading these items: {0}.".format(identifiers_to_be_downloaded))
        Clock.schedule_once(partial(self.set_status_callback,
                                    "Found {0} books to be downloaded".format(len(identifiers_to_be_downloaded))))
        if raw_list:
            return item_books_list
        else:
            return identifiers_to_be_downloaded

    def download_books(self, books_list):
        def make_dir():
            import uuid
            uuid_str = str(uuid.uuid4())
            book_dir = os.path.expanduser(join(scribe_globals.books_dir, uuid_str))
            thumb_dir = join(book_dir, 'thumbnails')
            if not os.path.exists(thumb_dir):
                os.makedirs(thumb_dir)
            print 'Created new book dir', book_dir
            return book_dir, uuid_str

        class Object(object):
            pass

        def get_cluster_proxy_url_by_leaf(scc, item, leaf_num, **kwargs):
            PROXY_TYPE = "proxyFileName"

            thumb_path_full = scc.get_page_data(leaf_num)[PROXY_TYPE]
            if thumb_path_full is None:
                return None

            url = "http://{server}" \
                  "/repub/{item_identifier}/" \
                  "{thumb_path}" \
                  "".format(server=item.d1,
                            item_dir=item.dir,
                            item_identifier=item.identifier,
                            thumb_path=thumb_path_full,
                            )
            return url

        Logger.info("Download books: Got {0} books to download".format(len(books_list)))

        for book in books_list:
            Logger.info("Download books: Downloading {0}".format(book))
            time_start = time.time()
            if book == '':
                continue

            try:
                item = self.ia_session.get_item(book)
                assert item.metadata['repub_state'] is not None
            except:
                Logger.error("No repub_state or item darkened. Skipping...")
                continue
            try:
                item = self.ia_session.get_item(book)
                Logger.info("Download book: target item: {0} (repub_state = {1}".format(item.identifier,
                                                                                        item.metadata['repub_state']))

                # if item.metadata['repub_state'] < 14 or item.metadata['repub_state'] > 35 :
                #    print "Item hasn't been republished or corrections have been accepted"
                #    return

                if int(item.metadata['repub_state']) not in [31, 34]:
                    Logger.error(
                        "Download book: Repub state is not 31 or 34 (is {1}), refusing to download item {0}".format(
                            item, item.metadata['repub_state']))
                    continue

                md_url = "https://" + item.d1 + "/RePublisher/RePublisher-viewScanData.php?id=" + book
                md = requests.get(md_url, verify=False, timeout=5)
                Logger.info("Download book: Got scandata from cluster")

                book_path, uuid = make_dir()
                Logger.info("Download book: Created directory {0}, uuid = {1}".format(book_path, uuid))

                f1 = open(join(book_path, "identifier.txt"), 'w+')
                f1.write(item.identifier)
                f1.close()
                f2 = open(join(book_path, "downloaded"), 'w+')
                f2.write('True')
                f2.close()
                f3 = open(join(book_path, "uuid"), 'w+')
                f3.write(uuid)
                f3.close()
                f = open(join(book_path, "scandata.xml"), 'w+')
                f.write(md.content)
                f.close()
                item.get_file(item.identifier + "_meta.xml").download(file_path=book_path + '/metadata.xml')
                Logger.info(
                    "Download book: Created files, now converting scandata from RePublisher XML to Scribe3 JSON")

                if not os.path.exists(join(book_path, 'reshooting')):
                    os.makedirs(join(book_path, 'reshooting'))
                Logger.info("Download book: Created reshooting directory")

                sc_path = join(book_path, "scandata.xml")
                tree = ET.parse(sc_path)
                pd = tree.getroot().find('pageData')
                for jsel in pd.iterfind('page'):
                    jsel.tag = "p_" + jsel.attrib['leafNum']
                    del jsel.attrib['leafNum']
                tree.write(join(book_path, "scandata_norm.xml"))
                Logger.info("Download book: Normalized p-nums in scandata")

                scandata_xml = open(join(book_path, "scandata_norm.xml")).read()
                obj = Object()
                obj.pretty = False
                json_data = json.loads(xml2json(scandata_xml, obj))

                # Posing new dics
                json_new = {}
                pageNumData_block = {}
                temp_bookData = copy.deepcopy(json_data['book']['bookData'])
                Logger.info("Download book: Rewriting page number assertions...")
                try:
                    if temp_bookData['pageNumData'] and 'assertion' in temp_bookData['pageNumData']:
                        if type(temp_bookData['pageNumData']['assertion']) == type({}):
                            Logger.debug("download_books: Fixing single assertion dict -> list")
                            a1 = temp_bookData['pageNumData']['assertion']
                            temp_bookData['pageNumData']['assertion'] = [a1]
                        for assertion in temp_bookData['pageNumData']['assertion']:
                            Logger.debug("Download book: adding assertion", assertion['leafNum'], ':',
                                         assertion['pageNum'])
                            pageNumData_block[assertion['leafNum']] = int(assertion['pageNum'])

                    temp_bookData['pageNumData'] = pageNumData_block
                    Logger.info("Download book: Done. Imported {0} assertions".format(len(pageNumData_block.keys())))
                except Exception as e:
                    Logger.exception("Download book: FAIL - No pageNumData block found. ({0})".format(str(e)))
                    pass
                # print '\n\n\n DEBUG\n\n temp_bookData[pageNumData] \n'
                # print temp_bookData['pageNumData']

                json_new['bookData'] = temp_bookData
                json_new['pageData'] = {}
                for page in json_data['book']['pageData']:
                    json_new['pageData'][page.split('p_')[1]] = json_data['book']['pageData'][page]

                Logger.info(
                    "Download book: Imported {0} p-nums into JSON scandata".format(len(json_data['book']['pageData'])))

                with open(join(book_path, "scandata.json"), 'w') as outfile:
                    json.dump(json_new, outfile)

                # preprocess for tts

                sdata = ScanData(book_path)
                # sdata.compute_page_nums(sdata.count_pages())
                sdata.save()

                Logger.info("Download book: Recomputed page numbers. Now onto downloading proxy images")

                for i, page in enumerate(sdata.dump_raw()['pageData']):
                    if int(page) != i:
                        Logger.error("Download book: Download Proxies: CRITICAL MISMATCH")
                        break

                    url = get_cluster_proxy_url_by_leaf(sdata, item, page)
                    leafnr = None

                    # Logger.debug("Download book: Got proxies for leaf #{0}({1}) at {2}".format(page,i, url))
                    Logger.debug("Download book: Got proxies for leaf #{0}".format(page))
                    # try:
                    #     leafnr = scc.get_page_num(page)['num']
                    # except:
                    #     pass

                    file_target = '{n:04d}.jpg'.format(n=int(page))
                    dest = os.path.join(book_path, "thumbnails", file_target)
                    print dest

                    if url is not None:
                        image = requests.get(url).content
                        with open(dest, "w+") as proxy:
                            Logger.debug("Written {0}".format(dest))
                            proxy.write(image)
                    else:
                        import shutil
                        Logger.debug("Page {0} has no proxy, adding missing image at {1}..".format(page, dest))
                        shutil.copyfile(resource_path('images/missing.png'), dest)

                Logger.info("Download book: Downloaded {0} proxy images.".format(len(sdata.dump_raw()['pageData'])))

                self.update_upload_db({'path': book_path, 'uuid': uuid, 'identifier': item.identifier, 'status': 800,
                                       'date': os.path.getmtime(book_path)})
                Logger.info("Download book: Book {0} now in status {1}.".format(item.identifier, 800))

                # remove book from tts item
                item.modify_metadata({'repub_state': 32})
                Logger.info("Download book: Set book status to 32 = Item Downloaded to Scribe3 station.")

                payload = {'repub_state': self.RepubState.book_download_to_ttscribe.value,}
                push_event('tts-book-downloaded', payload, 'book', book)

                # payload = {'local_id': book['uuid'], 'status': book['status'], }
                # push_event('tts-book-first-seen', payload, 'book', book['identifier'])
                total_time = time.time() - time_start
                Logger.info("Download book: ------ DONE. Downloaded {0} in {1}s ----------".format(book, total_time))

            except Exception:
                print traceback.format_exc()
                import shutil
                shutil.rmtree(book_path)
                continue

    def convert_scandata_from_xml(self):
        return False

    # process_book_dir() - worker thread
    # this function package and upload a single book
    # the input but must have a key named 'path'
    # _____________________________________________________________________________________
    def process_book_dir(self, book):
        """Package and upload a single book. This function may be called repeatedly
        on the same book in case processing was stopped before upload was finished

        The input book dict must have a key named 'path'
        """

        error = None

        status_msg = 'processing book ' + book['path']
        Logger.info("Process_book_dir: " + status_msg)
        # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, status_msg))
        Clock.schedule_once(partial(self.set_status_callback, status_msg))

        if book['status'] is None:
            # An item was downloaded and needs to be synced up
            Logger.debug(
                "process_book_dir: {0} has been downloaded and will now be added to the local database.".format(
                    book['identifier']))
            self.check_qa_status(book)
            self.update_upload_db(book)
            Logger.debug("process_book_dir: {0} has been added to the local DB and has now status {1}.".format(
                book['identifier'], book['status']))
            return

        self.get_uuid(book)
        self.get_book_status(book)
        self.update_upload_db(book)

        if book['status'] == UploadStatus.scribing.value:
            Logger.info("Book is being scanned")
            # An upload has not yet been queued, so no further processing is necessary.
            # TODO: Legacy books will skip the status==scribing state, and upload without
            # having to have the user queue an upload. Is this behavior what we want?
            return

        if book['status'] == UploadStatus.delete_queued.value:
            self.delete_unfinished_book(book)
            return

        if book['status'] == UploadStatus.item_exists_nonempty.value:
            book['status'] = UploadStatus.identifier_reserved.value
            return

        if book['status'] == UploadStatus.identifier_reserved.value:
            Logger.info("Book is in status identifier_reserved, removing identifier if present.")
            id_file = os.path.join(book['path'], 'identifier.txt')
            if os.path.exists(join(book['path'], 'upload_queued')):
                Logger.warning("Removing upload tag")
                os.remove(join(book['path'], 'upload_queued'))
            if os.path.exists(id_file) and not os.path.exists(id_file + "_rejected"):
                shutil.move(id_file, id_file + "_rejected")
                return
            elif os.path.exists(id_file + "_rejected"):
                book["status"] = UploadStatus.scribing.value
                self.update_upload_db(book)
                return
            return

        if book['status'] == UploadStatus.uploaded_awaiting_qa.value:
            # The book was uploaded. Delete.
            Logger.info("Book was uploaded, deleting local copy.")
            book['status'] = UploadStatus.done.value
            self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.has_notes.value:
            Logger.info("Book has notes, pulling a fresh scandata")
            download_annotated_scandata_from_republisher(book)
            self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.needs_corrections.value:
            Logger.debug("process_book_dir: Looks like {0} needs corrections. Checking current QA status.".format(
                book['identifier']))
            self.check_qa_status(book)
            if self.check_tts_is_authoritative(book):
                Logger.debug(
                    "process_book_dir: This scanner has been assigned corrections for {0}".format(book['identifier']))
            else:
                Logger.debug("process_book_dir: This is not assigned to correct {0}. Marking as done.".format(
                    book['identifier']))
                book['status'] = UploadStatus.done.value
                self.update_upload_db(book)
                return

            # This transition happens independently of cluster rep
            # thus we use a different function that returns a bool
            if self.check_rescribing_began(book):
                Logger.debug("process_book_dir: {0} is being corrected. Setting status.".format(book['identifier']))
                book['status'] = UploadStatus.scribing_corrections.value
            else:
                Logger.debug(
                    "process_book_dir: Correction of {0} hasn't begun. Pulling most recent scandata from cluster.".format(
                        book['identifier']))
                # if rescribing hasn't began, let's pull a fresh copy
                download_annotated_scandata_from_republisher(book)
            # weither way, update
            self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.scribing_corrections.value:
            if os.path.exists(os.path.join(book['path'], 'reshooting', 'done')):
                print "Donefile exists. Transitioning", book, "to finished correction and upload"
                book['status'] = UploadStatus.uploading_corrections.value
                self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.uploading_corrections.value:
            print "Uploading corrections for book",
            self.upload_book_corrections(book)
            self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.corrected.value:
            # At this point repub state must be > 30
            # self.check_qa_status(book)
            book['status'] = UploadStatus.done.value
            self.update_upload_db(book)
            return

        if book['status'] == UploadStatus.done.value:
            print "Book", book['identifier'], "is done and will be deleted."
            open(join(book['path'], 'delete_queued'), 'a').close()
            self.delete_finished_book(book)
            self.update_upload_db(book)
            return

        self.get_identifier(book)
        self.package_book(book)
        self.upload_book(book)

    def check_rescribing_began(self, book):
        a = os.path.join(book['path'], 'reshooting')
        # reshooting_files = next(os.walk(os.path.join(book['path'], 'reshooting')))[2]
        try:
            reshooting_files = next(os.walk(os.path.join(book['path'], 'reshooting')))[2]
            if reshooting_files:
                return True
            else:
                return False
        except Exception as e:
            print str(e)
            print "An exception has occurred whilst checking whether rescribing has begun. Assuming no."
            return False

    def check_tts_is_authoritative(self, book):
        Logger.debug(
            "check_tts_is_authoritative: Checking if this Scribe is authoritative for {0}".format(book['identifier']))
        ret = False
        try:
            tts_downloads = self.pending_downloads(raw_list=True)
            if book['identifier'] in tts_downloads:
                ret = True
            else:
                ret = False
        except Exception as e:
            ret = False
        Logger.debug("check_tts_is_authoritative: {0}".format(ret))
        return ret

    # load_books_from_drive() - worker thread
    # this function load the books status from the user's drive
    # _____________________________________________________________________________________
    def load_books_from_drive(self):
        book_list = []
        legacy_books = glob.glob(scribe_globals.legacy_books)
        modern_books = glob.glob(os.path.expanduser(scribe_globals.modern_books))
        book_dirs = legacy_books + modern_books
        for path in book_dirs:
            d = {'path': path}

            try:
                md = get_metadata(d['path'])
                if 'title' in md:
                    d['title'] = md['title']
                if 'author' in md:
                    print "Detected a legacy autor value of {0} while loading metadata in directory {1}. " \
                          "Will convert to creator.".format(md['author'], path)
                    d['creator'] = md['author']
                if 'creator' in md:
                    d['creator'] = md['creator']
                d['date'] = os.path.getmtime(path)

                # set 'uuid' field, and write uuid to disk if necessary
                self.get_uuid(d)
                self.get_book_status(d)
            except Exception:
                self.logger.error('Could not load metadata for ' + path)
                self.logger.error(traceback.format_exc())
                pass

            book_list.append(d)

        return book_list

    # load_processed_books_from_db() - worker thread
    # _____________________________________________________________________________________
    def load_processed_books_from_db(self, book_list):
        '''
        Load processed books from the database, excluding those found in books_list
        '''
        try:
            load_deleted = self.config['load_deleted']
        except:
            load_deleted = False
        uuids = set([book['uuid'] for book in book_list if 'uuid' in book])
        processed_books = []
        for row in self.upload_db.get():
            uuid = row.get('uuid')
            status = row.get('status')
            if (uuid is not None) and (uuid not in uuids):
                if not load_deleted:
                    if status < 700:
                        processed_books.append(row)
                elif load_deleted:
                    processed_books.append(row)
        return processed_books

    # get_uuid() - worker thread
    # _____________________________________________________________________________________
    def get_uuid(self, book):
        """
        Book paths are not unique (/var/www/book/book_1 gets deleted and then the path
        is reused), and IA identifiers are generally not yet set, so we generate a uuid
        for long-term tracking of the state of this book.

        We write the uuid in book_dir/uuid, and we use it as the primary key in our
        persistent sqlite db for tracking processing state and for long-term tracking of
        stats. We check to see if book_dir is writable in this function, which also
        ensures we can delete the directory after upload.

        Update: books scanned with the old software write images into /var/www/book/*
        Books scanned with this software will write books into ~/scribe_books/uuid/
        """
        error = None
        # Logger.debug('Get UUID: Acquiring uuid for ' + book['path'])

        if not os.access(book['path'], os.W_OK | os.X_OK):
            raise ScribeException('Book path not writable')

        if not book['path'].startswith('/var/www/book/'):
            # modern book, uuid is the dir name
            uuid_str = os.path.basename(book['path'])
            if len(uuid_str) != 36:
                raise ScribeException('Malformed book dir')
        else:
            uuid_path = os.path.join(book['path'], 'uuid')
            if os.path.exists(uuid_path):
                try:
                    uuid_str = open(uuid_path).read().strip()
                    self.logger.debug('Using already generated uuid ' + uuid_str)
                except Exception:
                    raise ScribeException('Could not read uuid file ' + uuid_path)
            else:
                uuid_str = str(uuid.uuid4())
                self.logger.debug('Generated uuid ' + uuid_str)
                try:
                    f = open(uuid_path, 'w')
                    f.write(uuid_str)
                    f.close()
                    self.logger.debug('Wrote uuid to ' + uuid_path)
                except Exception:
                    raise ScribeException('Could not write uuid file ' + uuid_path)

        book['uuid'] = uuid_str
        return error

    # get_book_status() - worker thread
    # _____________________________________________________________________________________
    def get_book_status(self, book):
        """We keep track of scanned books using a uuid for tracking, since book paths
        may be reused. If upload of a book was not completed previously, a row for this
        book might already exist.

        If the book already exists in the db, then we read the status value from the db
        and add it to the book dict. Otherwise, we initialize it to UploadStatus.uuid_assigned

        If we have already assigned an identifier, we will read that from the db as well.
        """

        error = None
        # Logger.debug('Get book status: Checking db for {u}'.format(u=book['uuid']))

        # sql = '''SELECT status, identifier FROM uploads WHERE uuid = ?'''
        try:
            row = self.upload_db.getone({'uuid': book['uuid']})
            # if we do not have an entry in the db
            if row is None:
                if book['path'].startswith('/var/www/book/'):
                    # legacy book
                    status = UploadStatus.uuid_assigned.value
                else:
                    # modern scribes use this execution branch
                    if os.path.exists(join(book['path'], 'delete_queued')):
                        status = UploadStatus.delete_queued.value
                    elif os.path.exists(join(book['path'], 'upload_queued')):
                        status = UploadStatus.upload_queued.value
                    else:
                        status = UploadStatus.scribing.value
                identifier = None

            else:
                status = row['status']
                # if we're scribing, check the book's not listed for delete or upload
                if status == UploadStatus.scribing.value:
                    if os.path.exists(join(book['path'], 'delete_queued')):
                        status = UploadStatus.delete_queued.value
                    elif os.path.exists(join(book['path'], 'upload_queued')):
                        status = UploadStatus.upload_queued.value

                if row.get('identifier') is not None:
                    identifier = row.get('identifier')
                else:
                    identifier = None
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException(
                'Could not query db for book status. Remove the uuid file to add this book to the db again.')

        book['status'] = status
        if identifier is not None:
            book['identifier'] = identifier

        return error

    # item_ready_for_upload() - worker thread
    # _____________________________________________________________________________________
    def item_ready_for_upload(self, identifier, book=None):
        """Book items might have already been preloaded with metadata in the IA scan process.
        However, prevent uploading to ia items which already have images uploaded.
        """
        try:
            item = self.ia_session.get_item(identifier)

            if not item.exists:
                if book:
                    preloaded_path = join(book['path'], 'preloaded')
                    if os.path.exists(preloaded_path):
                        # This item was created in offline mode, but the idenfier doesn't exist
                        print "Item {0} is tagged as preloaded, but the identifier does not exist. " \
                              "Aborting upload and reverting to scribing status."
                        return False
                    else:
                        print "User wants to upload to item {0}. Ok'ing that"
                        # no existing item, so safe to use this identifier
                        return True
            allowed_formats = set(
                ['Metadata', 'MARC', 'MARC Source', 'MARC Binary', 'Dublin Core', 'Archive BitTorrent',
                 'Web ARChive GZ', 'Web ARChive', 'Log', 'OCLC xISBN JSON', 'Internet Archive ARC',
                 'Internet Archive ARC GZ', 'CDX Index', 'Item CDX Index', 'Item CDX Meta-Index',
                 'WARC CDX Index', 'Metadata Log']);
            for file in item.files:
                if file['format'] not in allowed_formats:
                    # files have already been uploaded to this item
                    return False

        except:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not check status of IA item {i}'.format(i=identifier))

        return True

    # get_identifier() - worker thread
    # _____________________________________________________________________________________
    def get_identifier(self, book):
        self.logger.debug('Getting identifier')
        id_file = os.path.join(book['path'], 'identifier.txt')
        meta_file = os.path.join(book['path'], 'metadata.xml')

        # If we have already set the identifier in the db, use it. However, if the upload
        # has gotten stuck the user might have added/edited identifier.txt, in which case
        # overwrite the value in the db.
        if (book.get('identifier') is not None) and (not os.path.exists(id_file)):
            self.logger.debug('Using previously generated identifier ' + book['identifier'])
            return

        if os.path.exists(id_file):
            try:
                identifier = open(id_file).read().strip()
                self.logger.debug('Using preset identifier ' + identifier)
                if 'identifier' in book:
                    if identifier != book['identifier']:
                        self.logger.warn('RESETTING IDENTIFIER from {old} to {new}'.format(
                            old=book['identifier'], new=identifier))
            except Exception:
                raise ScribeException('identifier.txt exists but cannot be read')

            if not self.item_ready_for_upload(identifier, book):
                # We can be here for two reasons:
                # there is data in the items, or the item was preloaded with a wrong identifier
                Logger.error('item {i} is not cleared for upload!'.format(i=identifier))
                preloaded_path = join(book['path'], 'preloaded')
                if os.path.exists(preloaded_path):
                    Logger.error('item was tagged as preloaded, but no identifier exists')
                    status = UploadStatus.identifier_reserved.value
                    book['status'] = status
                else:
                    Logger.error('Item exists and is non empty')
                    status = UploadStatus.item_exists_nonempty.value
                    book['status'] = status

                if os.path.exists(join(book['path'], 'upload_queued')):
                    Logger.warning("Deleting upload_queued tag")
                    os.remove(join(book['path'], 'upload_queued'))

                self.update_upload_db(book)
                raise ScribeException('item {i} exists and already contains data!'.format(i=identifier))

        elif os.path.exists(meta_file):
            try:
                identifier = self.make_identifier(book)
                self.logger.debug('Generated identifier ' + identifier)
            except ScribeException as e:
                raise e
            except Exception as e:
                self.logger.error(traceback.format_exc())
                raise ScribeException('metadata.xml exists but could not make id')
        else:
            raise ScribeException('Neither identifier.txt nor metadata.xml could be found')

        status = UploadStatus.identifier_assigned.value

        book['status'] = status
        book['identifier'] = identifier

        # Notify BTSERVER that a book was created
        payload = {'local_id': book['uuid'], 'status': book['status'],}
        push_event('tts-book-first-seen', payload, 'book', book['identifier'])

        self.update_upload_db(book)

    # check_db_for_identfier() - worker thread
    # _____________________________________________________________________________________
    def check_db_for_identfier(self, identifier, uuid):
        rows = self.upload_db.get({'identifier': identifier})

        # if this is the first book scanned, the identifier column might not yet exist
        if rows is None:
            return False

        for row in rows:
            if uuid != row.get('uuid'):
                # print 'found', identifier, 'uuid', uuid, 'existing uuid', row.get('uuid')
                return True
        return False

    # make_identifier() - worker thread
    # _____________________________________________________________________________________
    def make_identifier(self, book):
        """
        Create identifier from metadata.xml
            $ideal=$title.$volume.$creator;
        """

        md = get_metadata(book['path'])

        title = scribe_upload.purify_string(md.get('title', 'unset'))
        volume = scribe_upload.purify_string(md.get('volume', '00'))
        creator = scribe_upload.purify_string(md.get('creator', 'unset'))

        # no longer use contributor
        # library = scribe_upload.purify_string(md.get('contributor', 'unset'))

        num_attempts = 10
        for i in range(num_attempts):
            if i == 0:
                random_str = ''
            else:
                random_str = '_' + scribe_upload.random_string()

            identifier = '{title}{vol:>04}{creator}{rand}'.format(
                title=title[:16], vol=volume[:4], creator=creator[:4],
                rand=random_str)

            if self.check_db_for_identfier(identifier, book.get('uuid')):
                # if we have already assigned this id to a different book (probably
                # with the same metadata), continue checking for a unique id
                self.logger.debug('Found existing book for identifier {i}, continuing'.format(i=identifier))
                continue

            if scribe_upload.id_available(identifier):
                return identifier

        raise ScribeException('Could not make unique identifier!')

    def create_jp2(self, jpegs):
        print "Creating JP2. Is exiftool available?", scribe_globals.EXIFTOOL, "(If False EXIF tags won't be copied.)"
        env = os.environ.copy()
        env['LD_LIBRARY_PATH'] = resource_path('kakadu')
        jp2s = []
        self.logger.debug("Now converting camera files to JPEG2000")
        for item in jpegs:
            path = item.split(".jpg")[0]
            im = Image.open(item)
            tmp_path = str(path + "_interchange.bmp")
            im.save(tmp_path, 'BMP')
            try:
                self.logger.debug(subprocess.check_output(
                    [resource_path(scribe_globals.kakadu), '-i', path + "_interchange.bmp", '-o', path + ".jp2",
                     '-slope', '42808'], env=env))
                jp2s.append(path + ".jp2")
                try:
                    if scribe_globals.EXIFTOOL:
                        subprocess.Popen([scribe_globals.EXIFTOOL, '-tagsFromFile', item, '-All:All', '-IFD1:All',
                                          path + ".jp2"]).communicate()
                        t_file = path + '.jp2' + "_original"
                        print os.remove(t_file)
                except Exception as e:
                    print "Exiftool not available or error in converting image", path + ".jp2"
            except Exception as e:
                print e
                raise ScribeException("There was an error whilst encoding the images to JPEG2000")
            finally:
                os.remove(tmp_path)
        return jp2s

    # package_book() - worker thread
    # _____________________________________________________________________________________
    def package_book(self, book):
        """Create an id_preimage.zip file in book_dir for uploading
        """

        if book['status'] >= UploadStatus.uploaded_awaiting_qa.value:
            return
        self.logger.debug('Creating preimage.zip')

        try:
            zip_path = os.path.join(book['path'], '{id}_preimage.zip'.format(id=book['identifier']))
            compression = zipfile.ZIP_STORED
            allow_zip64 = True
            jpegs = sorted(glob.glob(os.path.join(book['path'], '*.jpg')))
            target = jpegs

            try:
                jp2s = self.create_jp2(jpegs)
                if jp2s is not None:
                    target = jp2s
            except Exception as e:
                print "Defaulting to JPEG compression."

            with zipfile.ZipFile(zip_path, 'w', compression, allow_zip64) as preimage_zip:
                for jpeg in target:
                    self.logger.debug('adding ' + jpeg + ' to ' + zip_path)
                    arcname = '{id}_preimage/{j}'.format(id=book['identifier'], j=os.path.basename(jpeg))
                    preimage_zip.write(jpeg, arcname)

                scandata = join(book['path'], 'scandata.json')
                if os.path.exists(scandata):
                    arcname = '{id}_preimage/scandata.json'.format(id=book['identifier'])
                    preimage_zip.write(scandata, arcname)
        except ScribeException as e:
            raise e
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not create preimage.zip')

        book['status'] = UploadStatus.packaging_completed.value

    def upload_book_corrections(self, book):

        if not os.path.exists(os.path.join(book['path'], 'downloaded')):
            msg_box = ScribeMessage()
            msg_box.text = 'Corrections can only be uploaded\nfrom a book that was downloaded'
            popup = Popup(title='Not allowed yet', content=msg_box,
                          auto_dismiss=False, size_hint=(None, None), size=(400, 300))
            msg_box.popup = popup
            msg_box.trigger_func = self.popup_dismiss
            popup.open()
            return ScribeException('Cannot use this upload function on a book that was not downloaded')

        # TTS item
        ttscribe_item = self.ia_session.get_item(self.config['identifier'], )
        # At this point this is guaranteed to exist. It could be owned by a different scribe
        book_item = self.ia_session.get_item(book['identifier'], )
        # this is unnecessarily ugly
        access = self.config['s3']['access_key']
        secret = self.config['s3']['secret_key']

        config = scribe_config.read()
        book_folder = 'corrections'
        scandata = json.loads(ScanData(book['path']).dump())
        cdic = {}
        tdic = {}
        rdic = {}
        rtdic = {}
        '''
        This subprogram uploads a corrected book back to republisher.
        It:
        - Verifies that the book was downloaded.
        - Gets ahold of the tts identifier to later remove the book from the item's "books" list.
        - Constructs and maintains four dictionaries: new pages (cdic), new pages thumbs (tdic), reshot pages(rdic),
            reshot pages thumbs (rtdic) that will later become the scandata.
        -- Looks for new pages (spreads insertions and/or appends) and their thumbs
        -- Add republisher tags (that's what post-processing would do)
        -- Looks for replacements (in bookpath/reshooting) if present
        -- saves a scandata_rerepublished.json
        - Uploads the pictures and scandatas
        - Updates tts item, repub state and metrics
        '''

        # here we add the new pages

        # cdic is the corrections dictionary, and it contains entries in the form
        # { item path : local path } - for example:
        # { "corrections/0044.jpg" : "~/scribe_books/1234/0022.jpg"}
        try:
            cdic = {book_folder + '/' + k: os.path.join(book['path'], k) for k in
                    next(os.walk(book['path']))[2]
                    if re.match('\d{4}\.jpg$', os.path.basename(k))}
            # and the thumbs from the new pages
            # REMOVE THUMB FROM OS WALK PATH
            tdic = {book_folder + '/thumbnails/' + k: os.path.join(book['path'], 'thumbnails', k) for k in
                    next(os.walk(os.path.join(book['path'], )))[2]
                    if re.match('\d{4}\.jpg$', os.path.basename(k))}
        except:
            print "no corrections found."
            pass

        # ensure the scandata has the appropriate tags for re-republishing

        # NEW PAGES DICT
        print "processing new pages...",
        for k in cdic:
            page_num = str(int(k.split('.jpg')[0].split('/')[1]))
            print page_num,
            try:
                print "(Exists? {0}) |".format(scandata['pageData'][page_num] is not None),
            except Exception as e:
                raise ScribeException(e)

            # rotate images
            im = Image.open(cdic[k])
            # im = im.rotate(int(scandata['pageData'][page_num]['rotateDegree']))
            width, height = im.size
            im.save(cdic[k])
            print "\n\n\n---->>> CORRECTIONS DEBUG - please report this \n\n" \
                  "Physical rotation DISABLED | Logical rotation DISABLED \n" \
                  "iteration={6}\npage_num={0} | rotation={1} \n" \
                  "origWidth={2} | origHeight={3}\n" \
                  "PIL-width={4} | PIL-height={5}\n" \
                  "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -\n\n\n".format(page_num,
                                                                                     scandata['pageData'][page_num][
                                                                                         'rotateDegree'],
                                                                                     scandata['pageData'][page_num][
                                                                                         'origWidth'],
                                                                                     scandata['pageData'][page_num][
                                                                                         'origHeight'],
                                                                                     width,
                                                                                     height,
                                                                                     k,
                                                                                     )
            # scandata['pageData'][page_num]['rotateDegree'] = 0
            if scandata['pageData'][page_num]['rotateDegree'] == 0:
                scandata['pageData'][page_num]['origWidth'] = str(width)
                scandata['pageData'][page_num]['origHeight'] = str(height)
            elif abs(int(scandata['pageData'][page_num]['rotateDegree'])) == 90:
                scandata['pageData'][page_num]['origWidth'] = str(height)
                scandata['pageData'][page_num]['origHeight'] = str(width)

            print '\n\n\n ---->>> CORRECTIONS DEBUG - PAGE INSERT- please report this \n\n'
            print "rotatedegree is {2}, origWidth = {0}, height= {1}".format(
                scandata['pageData'][page_num]['origWidth'],
                scandata['pageData'][page_num]['origHeight'],
                scandata['pageData'][page_num]['rotateDegree']
            )
            print "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -\n\n\n"

            scandata['pageData'][page_num]['origFileName'] = k.split('/')[1]
            scandata['pageData'][page_num]['sourceFileName'] = k
            scandata['pageData'][page_num]['correctionType'] = "INSERT"
            scandata['pageData'][page_num]['proxyFullFileName'] = k
            scandata['pageData'][page_num]['TTSflag'] = 0
            print '\n\n\n ---->>> CORRECTIONS DEBUG - please report this \n\n'
            print scandata['pageData'][page_num]
            print "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -\n\n\n"
        # THUMBS FOR NEW PAGES
        for k in tdic:
            page_num = str(int(k.split('.jpg')[0].split('/')[2]))
            scandata['pageData'][page_num]['proxyFileName'] = k

        print "\nProcessed {0} new images.".format(len(cdic))

        try:
            # here we add the reshot images
            rdic = {book_folder + '/' + k: os.path.join(book['path'], 'reshooting', k) for k in
                    next(os.walk(os.path.join(book['path'], 'reshooting')))[2]
                    if re.match('\d{4}\.jpg$', os.path.basename(k))}

            # RESHOT IMAGES DICT
            for k in rdic:
                page_num = str(int(k.split('.jpg')[0].split('/')[1]))
                # rotate images
                im = Image.open(rdic[k])
                # im = im.rotate(int(scandata['pageData'][page_num]['rotateDegree']))
                width, height = im.size
                # im.save(rdic[k])
                print "\n\n\n---->>> CORRECTIONS DEBUG - please report this \n\n" \
                      "Physical rotation DISABLED | Logical rotation DISABLED \n" \
                      "iteration={6}\npage_num={0} | rotation={1} \n" \
                      "origWidth={2} | origHeight={3}\n" \
                      "PIL-width={4} | PIL-height={5}\n" \
                      "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -\n\n\n".format(page_num,
                                                                                         scandata['pageData'][page_num][
                                                                                             'rotateDegree'],
                                                                                         scandata['pageData'][page_num][
                                                                                             'origWidth'],
                                                                                         scandata['pageData'][page_num][
                                                                                             'origHeight'],
                                                                                         width,
                                                                                         height,
                                                                                         k,
                                                                                         )

                # sometimes rotation is unreliable. Conforming to SIDE

                degree_for_side = {'left': '-90',
                                   'right': '90',
                                   'foldout': '0',
                                   }
                try:
                    # use scandata.xml naming convention
                    scandata_side = scandata['pageData'][page_num]['handSide'].lower()
                    degree = degree_for_side.get(scandata_side, 0)
                    print "Verifying that rotation {2} for side {0} matches expected value of {1}".format(scandata_side,
                                                                                                          degree,
                                                                                                          scandata[
                                                                                                              'pageData'][
                                                                                                              page_num][
                                                                                                              'rotateDegree'])
                    if scandata['pageData'][page_num]['rotateDegree'] != degree:
                        scandata['pageData'][page_num]['rotateDegree'] = degree
                        print "ROTATION MISMATCH: set rotation for side {0} to {1}".format(scandata_side, degree)
                    else:
                        print "ROTATION MATCH. All good."
                except Exception as e:
                    print "Exception while inferring side from scandata"
                    pass

                # scandata['pageData'][page_num]['rotateDegree'] = 0
                if scandata['pageData'][page_num]['rotateDegree'] == 0:
                    scandata['pageData'][page_num]['origWidth'] = str(width)
                    scandata['pageData'][page_num]['origHeight'] = str(height)
                elif abs(int(scandata['pageData'][page_num]['rotateDegree'])) == 90:
                    scandata['pageData'][page_num]['origWidth'] = str(height)
                    scandata['pageData'][page_num]['origHeight'] = str(width)

                print '---->>> CORRECTIONS DEBUG - PAGE RESHOOT'
                print "rotatedegree is {2}, origWidth = {0}, height= {1}".format(
                    scandata['pageData'][page_num]['origWidth'],
                    scandata['pageData'][page_num]['origHeight'],
                    scandata['pageData'][page_num]['rotateDegree']
                )
                print "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -"

                scandata['pageData'][page_num]['origFileName'] = k.split('/')[1]
                scandata['pageData'][page_num]['sourceFileName'] = k
                scandata['pageData'][page_num]['correctionType'] = "REPLACE"
                scandata['pageData'][page_num]['proxyFullFileName'] = k
                scandata['pageData'][page_num]['TTSflag'] = 0

                print '---->>> CORRECTIONS DEBUG - please report this'
                print scandata['pageData'][page_num]
                print "<<<---- END CORRECTIONS DEBUG - - - - - - - - - - -"

            # here we add the thumbs from the reshooting
            rtdic = {book_folder + '/thumbnails/' + k: os.path.join(book['path'], 'reshooting', 'thumbnails', k) for k
                     in next(os.walk(os.path.join(book['path'], 'reshooting', 'thumbnails')))[2]
                     if re.match('\d{4}\.jpg$', os.path.basename(k))}

            # THUMBS FOR RESHOT IMAGES
            for k in rtdic:
                page_num = str(int(k.split('.jpg')[0].split('/')[2]))
                scandata['pageData'][page_num]['proxyFileName'] = k

            print "Processed {0} reshot images.".format(len(rdic))

        except Exception as e:
            print "No reshot pages were found - ", str(e)

        # Super Solenoid Scandata from disk (page info)
        sss = {int(k): v for k, v in scandata['pageData'].items()}

        # Now we want our own piece of memory for this one
        new_scandata = copy.deepcopy(scandata)
        new_scandata['pageData'] = {}
        new_scandata['pageData']['page'] = []

        # Rewrite pages section

        print "adding all computed pages to new scandata...",
        for page in sorted(sss):
            print page,
            sss[page]['leafNum'] = page
            try:
                pnum = sss[page]['pageNumber']['num']
                sss[page]['pageNumber'] = pnum
            except:
                pass
            new_scandata['pageData']['page'].append(sss[page])

        # Rewrite assertions to be compatible with republisher

        print "\nNow rewriting page assertions for repub compatibility if present"
        temp_pageNumData = copy.deepcopy(scandata['bookData']['pageNumData'])
        temp_pageNumData['assertion'] = []
        for entry in scandata['bookData']['pageNumData']:
            if entry.isdigit():
                del temp_pageNumData[entry]

        try:
            for assertion in scandata['bookData']['pageNumData'].iteritems():
                temp_assertion = {'leafNum': str(assertion[0]), 'pageNum': str(assertion[1])}
                temp_pageNumData['assertion'].append(temp_assertion)

            print 'OK done. New pageNumData block:', temp_pageNumData
        except Exception as e:
            print "No pageNumData block found or error processing it. More info: ", e

        new_scandata['bookData']['pageNumData'] = temp_pageNumData

        # Write it all to file
        with open(os.path.join(book['path'], 'scandata_rerepublished.json'), 'w+') as outfile:
            json.dump(new_scandata, outfile)

        print "DONE CONSTRUCTING, uploading...."

        try:
            # Upload the pictures
            print "Uploading pics"

            book_item.upload(cdic, retries=10, retries_sleep=60, queue_derive=False, )
            book_item.upload(tdic, retries=10, retries_sleep=60, queue_derive=False, )

            try:
                book_item.upload(rdic, retries=10, retries_sleep=60, queue_derive=False, )
                book_item.upload(rtdic, retries=10, retries_sleep=60, queue_derive=False, )
            except:
                pass

            print "Done. Uploading scandata..."
            # Upload the scandata
            scandata = os.path.join(book['path'], 'scandata_rerepublished.json')
            book_item.upload({'corrections/scandata.json': scandata}, retries=10, retries_sleep=60,
                             queue_derive=False, )
            # corrections_uploaded
            print "Done. Chaging repub state"
            book_item.modify_metadata({'repub_state': 33}, )
            print "Removing book from tts list"
            original_btserver_list = ttscribe_item.metadata['books'][1:-1].split(',')
            original_btserver_list.remove(book['identifier'])
            updated_btserver_list = str(original_btserver_list).replace("'", "")
            ttscribe_item.modify_metadata({'books': updated_btserver_list}, )
            book['status'] = UploadStatus.corrected.value

            payload = {'repub_state': self.RepubState.corrections_ready_for_review.value,}
            push_event('tts-book-corrections-sent', payload, 'book', book['identifier'])
            print "All done."

        except requests.ConnectionError as e:
            raise ScribeException('Upload Failed. Please check network and S3 Keys')

        except Exception as e:
            print str(e)
            raise ScribeException('Upload Failed!')

    # upload_book() - worker thread
    # _____________________________________________________________________________________
    def upload_book(self, book):
        """Upload then delete id_preimage.zip file in book_dir
        """

        if book['status'] >= UploadStatus.uploaded_awaiting_qa.value:
            return
        self.logger.debug('Creating preimage.zip for ' + book['identifier'])
        book['status'] = UploadStatus.upload_started.value
        Clock.schedule_once(partial(self.update_status_callback, book))

        try:
            scandata = ScanData(book['path'])
            zip_path = os.path.join(book['path'], '{id}_preimage.zip'.format(id=book['identifier']))
            Logger.warning("Upload book: Calling IA")
            print self.ia_session.secure
            item = self.ia_session.get_item(book['identifier'])
            access = self.config['s3']['access_key']
            secret = self.config['s3']['secret_key']
            Logger.info("Got item {0}".format(item.identifier))
            md = get_metadata(book['path'])
            md['repub_state'] = '10'
            md['mediatype'] = 'texts'
            try:
                collections_list = md['collection']
            except:
                print "Couldn't find collection or collection set specification in book metadata. "
            scancenter_md = get_sc_metadata()

            # if the item has a pre-existing identifier
            # we want update only the value for 'scanner' and 'operator'
            # from the /usr/local/scribett/metadata/metadata.xml
            scancenter_md_clean = {}
            print "upload_book: Looking up item on archive."
            if item.exists:
                print "Item exists already, pushing selective metadata"
                for k in scancenter_md.keys():
                    print "considering key", k
                    if k in ['scanner', 'operator']:
                        print "key", k, "added to metadata"
                        scancenter_md_clean[k] = scancenter_md[k]
                # in case the item exists, use the collection settings from Archive
                # md['collection'] = item.metadata['collection']
                # md.update(scancenter_md_clean)
                md = scancenter_md_clean
            else:
                print "Item doesn't exist. Pushing ALL metadata."
                md.update(scancenter_md)
                # otherwise, if it's a new item, get the collection from the item's metadata
                # if scancenter_md['collection'] is None:
                try:
                    md['collection'] = collections_list
                except:
                    # if this fails, it means that there is no collection information in the item's
                    # metadata.
                    print "No collection set information found"
                    pass

            # The old ttscribe software uses author instead of creator, so we do too,
            # but we need to change this on upload
            if 'author' in md:
                print "Converting author field to creator before upload to cluster"
                md['creator'] = md['author']
                del md['author']

            encoded_md = {}
            for k, v in md.iteritems():
                if isinstance(v, unicode):
                    encoded_md[k] = v.encode('utf-8')
                else:
                    encoded_md[k] = v

            self.logger.debug('upload metadata = ' + str(encoded_md))
            self.logger.debug('uploading origin.txt to ' + book['identifier'])

            # upload a legacy origin.txt
            origin_txt = '-*- text -*-\n{t}\n'.format(t=time.strftime('%Y/%m/%d %H:%M:%S %Z %z'))
            fh = StringIO.StringIO(origin_txt)
            fh.name = '{id}_origin.txt'.format(id=book['identifier'])

            # upload a  metasource.xml
            metasource_file_location = ''
            metasource = get_metadata(book['path'], 'metasource.xml')
            # if there is a metasource file present, add upload-time fields
            if metasource != {}:
                import datetime
                metasource['textid'] = book['identifier']
                metasource['userid'] = self.config['email']
                metasource['date'] = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                set_metadata(metasource, book['path'], 'metasource.xml', 'metasource')
                metasource_file_location = os.path.join(book['path'], 'metasource.xml')
                metasource_file_upload_name = book['identifier'] + '_metasource.xml'
                self.logger.debug('Written metasource file at {0}'.format(metasource_file_location))

            # if the item already exists (preexistent identifier case)
            # we need to modify the metadata items before upload the file
            responses = []
            if item.exists:
                print "Since the item already exists, only pushing this metadata:", encoded_md
                response = item.modify_metadata(encoded_md)
                responses.append([response])

            origin_response = item.upload(fh, metadata=encoded_md, retries=10, retries_sleep=60, queue_derive=False, )
            responses.append(origin_response)

            self.logger.debug('Response from origin.txt upload: {0} | {1}'.format(origin_response,
                                                                                  {r.request.url: r.status_code for r in
                                                                                   origin_response}))

            self.logger.debug('Uploading preimage.zip to ' + book['identifier'])
            upload_response = item.upload(zip_path, metadata=encoded_md, delete=True, retries=10, retries_sleep=60,
                                          queue_derive=False, )
            responses.append(upload_response)
            self.logger.debug('Response from upload: {0} | {1}'.format(upload_response,
                                                                       {r.request.url: r.status_code for r in
                                                                        upload_response}))

            if metasource_file_location != '':
                self.logger.debug('Uploading metasource file {0} as {1}'.format(metasource_file_location,
                                                                                metasource_file_upload_name))
                response = item.upload({metasource_file_upload_name: metasource_file_location}, retries=10,
                                       retries_sleep=60, queue_derive=False, )
                responses.append(response)
                self.logger.debug('Response from upload: {0} | {1}'.format(origin_response,
                                                                           {r.request.url: r.status_code for
                                                                            r in response}))

            if os.path.exists(os.path.join(book['path'], 'marc.xml')):
                self.logger.debug(
                    'Uploading MARCXML file {0}'.format(os.path.join(book['path'], book['identifier'] + '_marc.xml')))
                upload_name_mapping = {book['identifier'] + '_marc.xml': os.path.join(book['path'], 'marc.xml')}
                response = item.upload(upload_name_mapping, retries=10, retries_sleep=60, queue_derive=False, )
                responses.append(response)
                self.logger.debug('Response from upload: {0} | {1}'.format(origin_response,
                                                                           {r.request.url: r.status_code for
                                                                            r in response}))

            if os.path.exists(os.path.join(book['path'], 'marc.bin')):
                self.logger.debug('Uploading MARC Binary file {0}'.format(
                    os.path.join(book['path'], book['identifier'] + '_marc.bin')))
                upload_name_mapping = {book['identifier'] + '_meta.mrc': os.path.join(book['path'], 'marc.bin')}
                response = item.upload(upload_name_mapping, retries=10, retries_sleep=60, queue_derive=False, )
                responses.append(response)
                self.logger.debug('Response from upload: {0} | {1}'.format(origin_response,
                                                                           {r.request.url: r.status_code for
                                                                            r in response}))

            self.logger.debug('Finished uploads to ' + book['identifier'])
            book['status'] = UploadStatus.done.value
            self.update_upload_db(book)

            # Push to iabdash
            if os.path.exists(os.path.join(book['path'], 'time.log')):
                with open(os.path.join(book['path'], 'time.log'), 'r') as file:
                    global_time_open = float(file.readline())
            else:
                global_time_open = None

            responses_dict = {}
            for item in responses:
                try:
                    for r in item:
                        responses_dict[str(r.request.url)] = r.status_code
                except:
                    responses_dict[item] = "Error"

            payload = {'local_id': book['uuid'], 'status': book['status'],
                       'activeTime': global_time_open,
                       "leafNum": scandata.count_pages(),
                       'metadata': encoded_md,
                       "responses": responses_dict,
                       }

            push_event('tts-book-uploaded', payload, 'book', book['identifier'])
            self.logger.debug('Finished upload for ' + book['identifier'])

            Clock.schedule_once(partial(self.update_status_callback, book))
            time.sleep(60)  # wait for book to be added to metadata api
        except requests.ConnectionError as e:
            book['status'] = UploadStatus.upload_failed.value
            raise ScribeException('Upload Failed. Please check network and S3 Keys')
        except Exception:
            book['status'] = UploadStatus.upload_failed.value
            self.logger.error(traceback.format_exc())
            raise ScribeException('Upload Failed!')

    def check_qa_status(self, book):
        status_msg = 'Check QA status: Begin for ' + book['path']
        Logger.info(status_msg)
        # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, status_msg))
        Clock.schedule_once(partial(self.set_status_callback, str(status_msg)))
        try:
            md_url = 'https://archive.org/metadata/{id}/metadata'.format(id=book['identifier'])
            md = json.load(urllib.urlopen(md_url))
        except Exception as e:
            Logger.exception("Check QA status: Error retrieving metadata | {0}".format(e))
            raise ScribeException('Could not query archive.org for repub_state!')

        try:
            if (md is None) or ('result' not in md):
                raise ScribeException('Could not query metadata')

            repub_state = md['result'].get('repub_state')
            status_msg = "Repub state for {0} is {1}".format(str(book['identifier']), str(repub_state))
            Logger.info('Check QA status: ' + status_msg)
            # Clock.schedule_once(partial(self.set_prop_callback, self.status_label, status_msg))
            Clock.schedule_once(partial(self.set_status_callback, str(status_msg)))

            if repub_state is None:
                self.logger.warning('Check QA status: Repub state not found for ' + book['identifier'])
                return

            if int(repub_state) == self.RepubState.uploaded.value:
                # Book has been uploaded and no more action is required at this time
                return

            elif int(repub_state) == self.RepubState.post_autocropped.value:
                # book['status'] = UploadStatus.uploaded_awaiting_qa.value
                return

            elif int(repub_state) == self.RepubState.need_corrections.value:
                # THIS IS NO LONGER USED BY NUPUB
                # check whether this scribe was assigned this book tbc, or move on?
                return

            # elif int(repub_state) == self.RepubState.has_message.value:
            #    book['status'] = UploadStatus.has_notes.value

            elif int(repub_state) == self.RepubState.corrections_sent_to_ttscribe.value:
                if book['status'] != UploadStatus.needs_corrections.value:
                    book['status'] = UploadStatus.needs_corrections.value
                return

            elif int(repub_state) == self.RepubState.book_download_to_ttscribe.value:
                if book['status'] != UploadStatus.needs_corrections.value:
                    book['status'] = UploadStatus.downloaded.value
                return

            elif int(repub_state) == self.RepubState.corrections_ready_for_review.value:
                book['status'] = UploadStatus.corrected.value
                pass

            elif int(repub_state) == self.RepubState.corrections_rejected.value:
                if book['status'] != UploadStatus.needs_corrections.value:
                    book['status'] = UploadStatus.needs_corrections.value
                return

            elif int(repub_state) == self.RepubState.rerepublish.value:
                book['status'] = UploadStatus.corrected.value
                pass

            elif int(repub_state) == self.RepubState.corrections_accepted.value:
                book['status'] = UploadStatus.done.value
                pass

            elif int(repub_state) == self.RepubState.derived.value:
                book['status'] = UploadStatus.done.value
                pass

            elif int(repub_state) == self.RepubState.done.value:
                book['status'] = UploadStatus.done.value
                pass

        except Exception as e:
            Logger.exception(
                'Check QA status: There was an error with getting the QA status from RePulisher for ' + book['path'])
            Logger.exception('Check QA status: Error details: ' + str(e))

    # delete_unfinished_book() - worker thread
    # _____________________________________________________________________________________
    def delete_unfinished_book(self, book):
        '''delete a book that has not yet been uploaded'''
        if not os.path.exists(join(book['path'], 'delete_queued')):
            self.logger.error('delete_queued not found for ' + book['path'])
            return

        try:
            self.logger.debug('Deleting unfinished book' + book['path'])
            shutil.rmtree(book['path'])
            book['status'] = UploadStatus.deleted.value
            self.upload_db.delete({'uuid': book['uuid']})
        except ScribeException:
            raise
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not delete book!')

    # delete_finished_book() - worker thread
    # _____________________________________________________________________________________
    def delete_finished_book(self, book):
        # if book['status'] < UploadStatus.done.value:
        #    return

        self.logger.debug('Checking repub_state for ' + book['identifier'])

        try:
            md_url = 'https://archive.org/metadata/{id}/metadata'.format(id=book['identifier'])
            md = json.load(urllib.urlopen(md_url))
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not query archive.org for repub_state!')

        try:
            if (md is None) or ('result' not in md):
                raise ScribeException('Could not query metadata')

            repub_state = md['result'].get('repub_state')

            if repub_state is None:
                self.logger.warning('Repub state not found for ' + book['identifier'])
                return

            if int(
                    repub_state) == self.RepubState.done.value or self.RepubState.uploaded.value or \
                    self.RepubState.post_autocropped.value:
                if os.path.exists(book['path']):
                    # user may have already deleted local copy of this book
                    self.logger.debug('Deleting ' + book['path'])
                    payload = {'local_id': book['uuid'], 'status': book['status'],}
                    push_event('tts-book-deleted', payload, 'book', book['identifier'])
                    shutil.rmtree(book['path'])
                else:
                    print "Did you already delete the local copy of this book?"
                book['status'] = UploadStatus.uploaded_and_deleted.value
                self.update_upload_db(book)
            elif os.path.exists(join(book['path'], 'delete_queued')):
                self.logger.debug('Deleting local copy of deleted book:' + book['path'])
                shutil.rmtree(book['path'])
                book['status'] = UploadStatus.uploaded_and_deleted.value
                self.update_upload_db(book)
            else:
                self.logger.debug('Not deleting {p} repub_state={s}'.format(p=book['path'], s=repub_state))
        except ScribeException:
            raise
        except Exception:
            self.logger.error(traceback.format_exc())
            raise ScribeException('Could not delete book!')

    # update_status_callback() - main thread
    # this function update the labels on the book list view
    # it alters a kivy property and needs to be scheduled on the main thread
    # _____________________________________________________________________________________
    def update_status_callback(self, book, *largs):
        """
        This function alters a kivy property and needs to be scheduled on the main thread
        """

        try:
            status = UploadStatus(book.get('status')).name
        except ValueError:
            status = 'Unknown status: ' + str(book.get('status'))

        if book.get('status') == UploadStatus.scribing.value:
            book['status_label'].text = '[ref={path}]Scribing (click)[/ref]'.format(path=book['path'])
        elif book.get('status') == UploadStatus.uploaded_awaiting_qa.value and not os.path.exists(book.get('path')):
            book['status_label'].text = 'Deleted'  # deleted local copy
        elif book.get('status') >= UploadStatus.uploaded_awaiting_qa.value:
            book['status_label'].text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''),
                                                                    s=status_human_readable.get(status, status))
        elif book.get('status') == UploadStatus.identifier_reserved.value:
            book['status_label'].text = '[ref={path}]Please change your identifier[/ref]'.format(path=book['path'])
        else:
            status = status_human_readable.get(status, status)
            book['status_label'].text = status

            # Logger.debug('update_status_callback: Updating status for book {p}-> {s}'.format(
            # p=book['path'], s=book.get('status', '')))

    # handle_error_callback() - main thread
    # this function handle the error callback
    # it alters a kivy property and needs to be scheduled on the main thread
    # _____________________________________________________________________________________
    def handle_error_callback(self, msg, book, *largs):
        """
        This function alters a kivy property and needs to be scheduled on the main thread
        """
        # self.update_status_callback(book, *largs)
        err_msg = '{msg}\n\npath: {p}\nid: {id}\nuuid: {uuid}'.format(p=book['path'], msg=msg,
                                                                      id=book.get('identifier', ''),
                                                                      uuid=book.get('uuid', ''))
        t = u'[ref={msg}][color=ff0000]Error (click)[/color][/ref]'.format(msg=err_msg)
        book['status_label'].text = t
        book['status_label'].italic = True

        def err_popup(instance, value):
            popup = Popup(title='Error', content=Label(text=value),
                          size_hint=(None, None), size=(600, 200))
            popup.open()

        book['status_label'].bind(on_ref_press=err_popup)

        self.logger.error(
            'Error for book {u} at path {p}: {msg}'.format(u=book.get('uuid', ''), p=book['path'], msg=msg))

    # set_prop_callback() - main thread
    # it alters a kivy property and needs to be scheduled on the main thread
    # _____________________________________________________________________________________
    def set_prop_callback(self, key, value, *largs):
        """
        This function alters a kivy property and needs to be scheduled on the main thread
        """
        key.text = value

    # set_status_callback() - main thread
    # it alters a kivy property and needs to be scheduled on the main thread
    # _____________________________________________________________________________________
    def set_status_callback(self, value, *largs):
        """
        This function alters a kivy property and needs to be scheduled on the main thread
        """
        self.status_txt = value

    # init_logger() - main thread
    # _____________________________________________________________________________________
    def init_logger(self, level=logging.DEBUG):
        """
        Create a logger that logs to both stdout and syslog
        """

        logger = logging.getLogger('Scribe3')
        logger.setLevel(level)
        stream_handler = logging.StreamHandler()
        formatter = logging.Formatter('%(name)s: %(levelname)s %(message)s')
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
        try:
            syslog_handler = SysLogHandler(address='/dev/log', facility=SysLogHandler.LOG_DAEMON)
            syslog_handler.setFormatter(formatter)
            logger.addHandler(syslog_handler)
        except:
            print 'Could not init syslog logger!'

        # sentry
        '''
        import raven, traceback
        from raven import *
        from raven.conf import setup_logging
        from raven.handlers.logging import SentryHandler
        sconfig = scribe_config.read()
        smeta = get_metadata(scribe_globals.scancender_metadata)
        raven_client = Client(dsn="https://4af7fdad024f412eb45b933a400344a4:6434bde2c4da4f2ab6ea3a658043bb96@books-sentry.us.archive.org/3",
                        release = smeta['tts_version'],
                        data= {
                                'scanner' : smeta['scanner'],
                                'language' : smeta['language'],
                                'contributor' : smeta['contributor'],
                                'sponsor' : smeta['sponsor'],
                                'operator' : smeta['operator'],
                            }
                        )
        sentry_handler = SentryHandler(raven_client)
        logger.addHandler(sentry_handler)
        '''
        return logger


# UploadFileItem
# _________________________________________________________________________________________
class UploadFileItem(Widget):
    path_str = StringProperty()
    identifier = StringProperty()
    status_msg = StringProperty()
    uuid = StringProperty()

    def __init__(self, text='', **kwargs):
        super(UploadFileItem, self).__init__(**kwargs)
        self.path_str = text


# UploadFileList
# this class update the book list of the main view in the UI
# _________________________________________________________________________________________
class UploadFileList(GridLayout):
    def __init__(self, book_list=None, **kwargs):
        # super(UploadFileList, self).__init__(cols=7, spacing=10, size_hint_y=None)

        # To be able to scroll the grid layout, we need to both set size_hint_y to None
        # and also bind minimum_height to setter('height')
        super(UploadFileList, self).__init__(cols=1, size_hint_y=None, row_default_height=dp(30))

        # From examples/scrollview.py:
        # when we add children to the grid layout, its size doesn't change at
        # all. we need to ensure that the height will be the minimum required to
        # contain all the childs. (otherwise, we'll child outside the bounding
        # box of the childs)
        self.bind(minimum_height=self.setter('height'))

    def reset_book_list(self, book_list, processed_books):

        self.clear_widgets()
        i = 0
        # print book_list
        self.add_header_row()
        uuids = set()

        for book in sorted(book_list, key=itemgetter('date'), reverse=True):
            # widget = self.add_book(book['path'])
            # book['widget'] = widget
            status_label = self.add_grid_row(i, book)
            book['status_label'] = status_label

            # we just have a directory listing and not full book dicts.
            uuid_path = os.path.join(book['path'], 'uuid')
            try:
                uuid_str = open(uuid_path).read().strip()
                uuids.add(book['uuid'])
            except:
                # Logger.debug("Reset book list: No uuid at {0}".format(uuid_path))
                pass

            # print 'uuids', uuids
            i += 1

        for b in sorted(processed_books, key=itemgetter('date'), reverse=True):
            self.add_processed_book(i, b)
            i += 1

    def add_processed_book(self, i, b):
        self.add_grid_row(i, b, color=(0.6, 0.6, 0.6, 1), processed=True)

    def add_book(self, path):
        widget = UploadFileItem(text=path)
        self.add_widget(widget)
        return widget

    def add_grid_row(self, i, book, color=(0, 0, 0, 1), processed=False):
        item = BookItemView()
        insert_pos = len(self.children) - 1 if i == -1 else 0

        label = BlackLabel(text=str(i + 1))
        label.color = color
        label.set_width(50)
        label.padding_x = -15
        item.add_widget(label)

        # TODO: Must fix this issue
        # Crashes when there is no buplook.

        insert_pos = - len(self.children)

        label = BlackLabel()
        label.color = color
        label.font_name = resource_path(scribe_globals.font)
        title = book.get('title', None) or 'Untitled'
        label.text = title
        item.add_widget(label)

        author = book.get('creator', None) or book.get('author', None) or ''
        label = BlackLabel(text=author)
        label.color = color
        label.font_name = resource_path(scribe_globals.font)
        item.add_widget(label)

        t = time.strftime('%m/%d/%Y', time.localtime(book['date']))
        label = BlackLabel(text=t)
        label.color = color
        item.add_widget(label)

        identifier = book.get('identifier', None) or ''
        label = BlackLabel(text=identifier)
        label.color = color
        item.add_widget(label)

        status_label = BlackLabel()
        status_label.markup = True
        status_label.color = color
        try:
            status = UploadStatus(book.get('status')).name
            status = status_human_readable.get(status, status)
            Logger.info("Rendering library list entry -> {0} | {1}".format(book, status))
        except ValueError:
            status = 'Unknown status'

        if book.get('status') == UploadStatus.scribing.value and not processed:
            status_label.text = '[ref={path}]Scribing (click)[/ref]'.format(path=book['path'])
            status_label.bind(on_ref_press=self.open_book)

        elif book.get('status') == UploadStatus.uploaded_awaiting_qa.value and not processed and os.path.exists(
                join(book.get('path'), 'delete_queued')):
            status_label.text = 'Delete Queued'

        elif book.get('status') == UploadStatus.identifier_reserved.value:
            status_label.text = '[ref={path}]Please change your identifier[/ref]'.format(path=book['path'])
            press_callback = partial(self.open_book_identifier_reserved, book)
            status_label.bind(on_ref_press=press_callback)

        elif book.get('status') == UploadStatus.uploaded_awaiting_qa.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.open_web, book=book))

        elif book.get('status') == UploadStatus.has_notes.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.re_scribe, book=book))

        elif book.get('status') == UploadStatus.needs_corrections.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.re_scribe, book=book))

        elif book.get('status') == UploadStatus.scribing_corrections.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.re_scribe, book=book))

        elif book.get('status') == UploadStatus.corrected.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.done_rescribing, book=book))

        elif book.get('status') == UploadStatus.done.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.done, book=book))

        elif book.get('status') == UploadStatus.identifier_assigned.value:
            status_label.text = '[ref={i}]{s}[/ref]'.format(i=book.get('identifier', ''), s=status)
            status_label.bind(on_ref_press=partial(self.open_web_id, book=book))

        else:
            status_label.text = status

        item.add_widget(status_label)

        for x in range(1):
            label = BlackLabel()
            label.color = color
            # label.set_width(50)
            item.add_widget(label)

        note = self.get_book_notes(book)
        label = BlackLabel(text=note or "-")
        label.bind(on_ref_press=partial(self.re_scribe, book=book))
        item.add_widget(label)

        self.add_widget(item, insert_pos)
        return status_label

    def get_book_notes(self, book):
        tree_path = join(book['path'], "correction_scandata.xml")

        try:
            assert os.path.isfile(tree_path) is True
        except:
            # print "notes not present for book", book
            return '-'

        try:
            pages = [elem for event, elem in ET.iterparse(tree_path) if event == "end" and elem.tag == "page"]
        except:
            Logger.error("error parsing corrections file {0}".format(tree_path))
            return "-"

        count = 0

        for item in pages:
            if item.find("note") is not None:
                count += 1

        return str(count)

    def add_header_row(self):
        header = GridLayout(rows=1, size_hint_y=None, height=dp(30))
        label = BlackLabel(text='')
        label.set_width(50)
        header.add_widget(label)
        header.add_widget(BlackLabel(text='Title', bold=True))
        header.add_widget(BlackLabel(text='Creator', bold=True))
        header.add_widget(BlackLabel(text='Last Edit', bold=True))
        header.add_widget(BlackLabel(text='Identifier', bold=True))
        header.add_widget(BlackLabel(text='Status', bold=True))
        header.add_widget(BlackLabel(text='Flags', bold=True))
        header.add_widget(BlackLabel(text='Notes', bold=True))
        self.add_widget(header)

    def _on_book_popup_dismiss(self, book_view, popup):
        # Book view widget has auto_deselect set to False,
        # so now we must set is_selected to False
        # so that view can change it's background color
        book_view.is_selected = False
        book_view.auto_deselect = True

    def open_book(self, instance, book_path):
        print 'open book', instance, book_path
        # TODO: there should be a better way of getting screen_manager and scribe_widget
        screen_manager = self.parent.parent.screen_manager
        scribe_widget = self.parent.parent.scribe_widget
        # capture_screen = scribe_widget.ids._capture_screen
        # capture_screen.book_dir = value
        # screen_manager.current = 'capture_screen'
        msg_box = ReopenUploadMessage(screen_manager, scribe_widget, book_path, instance)
        popup = Popup(title='Reopen or Upload book', content=msg_box,
                      size_hint=(None, None), size=(400, 400))
        msg_box.popup = popup
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def open_book_identifier_reserved(self, book, status_label, book_path):
        screen_manager = self.parent.parent.screen_manager
        scribe_widget = self.parent.parent.scribe_widget
        popup = Popup(title='Open book - Identifier reserved',
                      size_hint=(None, None),
                      size=(400, 280))
        content = IdentifierReservedMessage(identifier=book.get('identifier', ''),
                                            book_path=book_path,
                                            status_label=status_label,
                                            screen_manager=screen_manager,
                                            scribe_widget=scribe_widget,
                                            popup=popup)
        popup.content = content
        book_view = status_label.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def open_web(self, instance, identifier, book=None):
        print 'open_web', instance, identifier, book
        if identifier == '':
            return
        msg_box = ViewOrDeleteBookMessage(book)
        popup = Popup(title='Open web page?', content=msg_box,
                      size_hint=(None, None), size=(400, 400))
        msg_box.popup = popup
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def open_web_id(self, instance, identifier, book=None):
        print 'open_web', instance, identifier, book
        if identifier == '':
            return
        msg_box = ViewBookMessage(book)
        popup = Popup(title='Open web page?', content=msg_box,
                      size_hint=(None, None), size=(500, 260))
        msg_box.popup = popup
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def re_scribe(self, instance, identifier, book):
        print 'open book', instance, identifier, book
        screen_manager = self.parent.parent.screen_manager
        scribe_widget = self.parent.parent.scribe_widget
        msg_box = ReScribe(screen_manager, scribe_widget, book)
        popup = Popup(title='Re-Scribe', content=msg_box,
                      size_hint=(None, None), size=(500, 260))
        msg_box.popup = popup
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def done_rescribing(self, instance, identifier, book):
        print 'open book', instance, identifier, book
        msg_box = ScribeMessage()
        msg_box.text = 'Book ' + identifier + '\nhas been ReScribed.'
        popup = Popup(title='Correction completed', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(500, 260))
        msg_box.popup = popup
        msg_box.trigger_func = popup.dismiss
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()

    def done(self, instance, identifier, book):
        msg_box = ScribeMessage()
        msg_box.text = 'Book ' + identifier + '\nis done and will be archived soon.'
        popup = Popup(title='Scribing completed', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(500, 260))
        msg_box.popup = popup
        msg_box.trigger_func = popup.dismiss
        book_view = instance.parent
        book_view.auto_deselect = False
        popup.bind(on_dismiss=partial(self._on_book_popup_dismiss, book_view))
        popup.open()
