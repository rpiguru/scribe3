from enum import Enum


# in this class are defined the possible statuses of a book during the process
# this value is used by the UI and by the worker
class UploadStatus(Enum):
    uuid_assigned = 100
    scribing = 125
    delete_queued = 135
    deleted = 140
    upload_queued = 150
    identifier_invalid = 197
    could_not_reserve_id = 198
    item_exists_nonempty = 199
    identifier_assigned = 200
    identifier_reserved = 300
    packaging_completed = 400
    upload_started = 500
    upload_failed = 600
    uploaded_awaiting_qa = 700
    has_notes = 750
    downloaded = 799
    needs_corrections = 800
    scribing_corrections = 825
    uploading_corrections = 830
    corrected = 850
    done = 888
    uploaded_and_deleted = 900


# an aliases dictionary for the status
status_human_readable = {
    'uploaded_awaiting_qa': 'Uploaded',
}
