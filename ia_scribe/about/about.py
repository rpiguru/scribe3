"""
    AboutScreen
        this class manage the about view on the UI
"""
import ConfigParser
import copy
import os
import subprocess
import sys
import threading
from functools import partial

from kivy.clock import Clock
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.parser import parse_color
from kivy.properties import ObjectProperty, StringProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, SlideTransition, ScreenManagerException
from kivy.uix.scrollview import ScrollView
from utils.util import UpdateStatus, check_software_update
import xml.etree.ElementTree as et
import scribe_config
import scribe_globals
from ia_scribe.uix.text_input import TextInput
from metadata_collection import MetadataCollectionsScreen
from optics.UI.camera_settings import CameraSettingsScreen
from utils.metadata import get_metadata, set_metadata, get_catalogs_from_metadata, get_collections_from_metadata
from utils.util import restart_app
from widgets import SettingsSwitch, SettingsInputBox

cur_dir = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(cur_dir + '/about.kv')


update_status = UpdateStatus.unknown.value


class AboutScreen(Screen):
    scribe_widget = ObjectProperty(None)
    current_panel = ObjectProperty(None)
    sm = ObjectProperty(None)
    screens = {}
    current_screen = StringProperty()
    b_added_advanced = BooleanProperty(False)
    global update_status

    def __init__(self, **kwargs):
        super(AboutScreen, self).__init__(**kwargs)
        self.load_screen()
        self.screen_list = ['about', 'settings', 'metadata', 'catalogs', 'collections', 'camera', 'update', 'relnotes']

    def load_screen(self):
        self.screens['about'] = AboutSettingsScreen()
        self.screens['settings'] = GeneralSettingsScreen()
        self.screens['metadata'] = LimitedMetadataScreen()
        self.screens['camera'] = CameraSettingsScreen(about_screen=self)
        self.screens['update'] = UpdateWidget()
        self.screens['relnotes'] = RelNotesWidget()
        self.screens['catalogs'] = CatalogsWidget()
        self.screens['collections'] = MetadataCollectionsScreen()

    def go_screen(self, dest_screen):
        if self.current_screen != dest_screen:
            if self.screen_list.index(self.current_screen) > self.screen_list.index(dest_screen):
                direction = 'up'
            else:
                direction = 'down'
            self.ids['sm'].transition = SlideTransition()
            self.current_screen = dest_screen
            try:
                self.ids['sm'].switch_to(self.screens[dest_screen], direction=direction)
            except ScreenManagerException:
                print 'Screen is already managed...'
                pass

    def check_advanced_config(self):
        """
        Check 'show_advanced' config value and add MetadataCollectionsScreen button
        :return:
        """
        config = scribe_config.read()
        try:
            if config['show_advanced'] == 'True':
                print "'show_advanced' is set, adding MetadataCollectionsScreen to settings menu..."

                button = HoverButton(id='btn_collections', info='collections', on_release=self.btn_collection)
                self.ids['tab_container'].add_widget(button, 6)
                self.b_added_advanced = True
        except KeyError:
            pass

    def on_enter(self):
        # self.scribe_widget is None when initializing this class,
        # so we add `scribe_widget` instance here instead of constructor.
        self.screens['camera'].scribe_widget = self.scribe_widget
        if update_status == UpdateStatus.update_available.value:
            self.ids['btn_update'].background_color = (0, 1, 0, .9)

        if not self.b_added_advanced:
            self.check_advanced_config()

    def on_leave(self):
        try:
            self.ids['_widget_about'].remove_widget(self.current_panel)
        except:
            pass

    def btn_about(self):
        self.go_screen('about')

    def btn_settings(self):
        self.go_screen('settings')

    def btn_metadata(self):
        self.go_screen('metadata')

    def btn_camera(self):
        self.go_screen('camera')

    def btn_update(self):
        self.go_screen('update')

    def show_release_notes(self):
        self.go_screen('relnotes')

    def setup_manager(self, manager):
        print "assigned manager ", manager
        self.sm = manager
        if self.current_screen == '':
            self.current_screen = 'metadata'
            self.go_screen('about')
            # Not sure why HoverButton objects do not have their child image in __init__ function
            for widget in self.ids['tab_container'].children:
                if widget.__class__.__name__ == 'HoverButton':
                    widget.ids['_img'].on_leave()
                    if widget.info == 'about':
                        widget.on_press()

    def btn_collection(self, *args):
        self.go_screen('collections')

    def btn_calibrate(self):
        #print self.sm
        #print dir(self.sm)
        calibration_screen = self.sm.get_screen('calibration_screen')
        calibration_screen.screen_manager = self.screen_manager
        calibration_screen.scribe_widget = self.scribe_widget
        calibration_screen.target_screen = 'about_screen'
        self.sm.transition.direction = 'left'
        self.sm.current = 'calibration_screen'
        # Move to general settings screen after displaying calibration screen.
        # After calibration, screen will return here
        self.go_screen('about')
        self.ids['btn_about'].on_press()

    def btn_catalogs(self):
        self.go_screen('catalogs')


class MetadataLabel(Label):
    metadata_key = StringProperty(None)
    color = (0, 0, 0, 1)
    pass


class AboutSettingsScreen(Screen):

    upload_widget = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(AboutSettingsScreen, self).__init__(**kwargs)
        try:
            f = open(cur_dir + "/notes/about_display_notes.txt", 'r')
            self.ids['lb_help'].text = f.read()
        except IOError as e:
            print e

    def on_enter(self, *args):
        print 'Entering about screen'
        self.ids['lb_version'].text = scribe_globals.build_number
        self.update_matedata()
        self.update_catalog()
        self.update_books()
        self.update_camera_data()

    def update_matedata(self):
        """
        Update scanner metadata
        :return:
        """
        config = get_metadata(scribe_globals.scancender_metadata)
        # Get all widgets
        # Since there are also "EmailTextInput" class instances, let us see each widget has 'metadata_key' property
        for widget in [x for x in self.ids['_metadata'].children]:
            try:
                val = config.get(widget.metadata_key)
                widget.text = val
            except AttributeError:
                pass

    def update_catalog(self):
        """
        Update number of catalogs and collection sets
        :return:
        """
        try:
            catalogs = get_catalogs_from_metadata()
            self.ids['lb_cat_num'].text = str(len(catalogs))
            collections = get_collections_from_metadata()
            self.ids['lb_col_num'].text = str(len(collections))
        except Exception as e:
            print 'Failed to get catalogs: {}'.format(e)

    def update_books(self):
        """
        Retrieve books status data and display
        :return:
        """
        print "----------->>>>> UPDATE_BOOKS"
        print type(self.upload_widget)
        if self.upload_widget is None:
            try:
                # TODO: Come up with a better way to reference Upload_widget in this context or pass by value
                self.upload_widget = self.parent.parent.parent.parent.manager.children[0].children[0]  # Debugging magic here!
            except:
                print "Couldnt load upload_widget"

        books = []
        try:
            books = self.upload_widget.book_metadata if self.upload_widget is not None else []
        except Exception as e:
            print "Warning: failed to load book metadata in absolute reference to upload_widget | {0}".format(e)

        for widget in [x for x in self.ids['_books'].children]:
            try:
                val = books.get(widget.metadata_key)
                widget.text = str(val)
            except AttributeError:
                pass

    def update_camera_data(self):
        """
        Get camera metadata
        :return:
        """
        config = ConfigParser.ConfigParser()
        config.read(os.path.expanduser(os.path.join(scribe_globals.config_dir, scribe_globals.camera_subsys)))
        try:
            self.ids['lb_camera_left'].text = config.get('left', 'model')
            self.ids['lb_camera_right'].text = config.get('right', 'model')
            self.ids['lb_camera_foldout'].text = config.get('foldout', 'model')
        except ConfigParser.NoSectionError as e:
            print e
        except ConfigParser.NoOptionError as e:
            print e


class CatalogsWidget(Screen):
    catalogs = []
    conf_file = []

    def __init__(self, **kwargs):
        super(CatalogsWidget, self).__init__(**kwargs)
        meta_dir_expanded = os.path.expanduser(scribe_globals.config_dir)
        self.conf_file = os.path.join(meta_dir_expanded, 'catalogs_metadata.xml')
        self.update_catalogs()

    def update_catalogs(self):
        # Remove existing widgets
        widgets = [x for x in self.ids['_grid_layout'].children]
        for widget in widgets:
            self.ids['_grid_layout'].remove_widget(widget)

        self.catalogs = get_catalogs_from_metadata()
        self.catalogs.sort()
        self.catalogs.reverse()
        # Redraw widgets from file
        if len(self.catalogs) > 0:
            for i in range(len(self.catalogs) - 1):
                self.create_cat_layout(self.catalogs[i])
            self.create_cat_layout(self.catalogs[-1], enable_add_button=True)

    def create_cat_layout(self, cat, enable_add_button=False, *largs):
        """
        When the user presses '+' button, 'cat' value should be blank string
        :param cat:
        :param enable_add_button:
        :param largs:
        :return:
        """
        if cat != '':
            index = self.catalogs.index(cat)
        else:
            # check whether current textinput box is blank or not.
            if len(self.ids['_grid_layout'].children) > 0:
                if len(self.ids['_grid_layout'].children[0].children[3].text) == 0:
                    Popup(title='Error',
                          title_size=22, width=500,
                          content=Label(text='Please input catalog before adding another one.', font_size=20),
                          size_hint=(None, .22)).open()
                    return False

            index = len(self.catalogs)

        layout = BoxLayout(id="_box_layout_cat_root_" + str(index), orientation='horizontal', size_hint_y=None,
                           height=30, spacing=5)

        label = Label(text="Catalog: ", bold=True, size_hint=(None, 1), valign='middle', color=parse_color('010101'), width=150,
                      markup=True)
        textbox = TextInput(id="_textbox_cat", text=cat)
        add_button = Button(text="+", on_press=partial(self.create_cat_layout, '', True), size_hint=(None, 1),
                            font_size='30sp', width=50)
        remove_button = Button(text="-", size_hint=(None, 1), font_size='30sp', width=50)
        remove_button.bind(on_press=partial(self.remove_cat, remove_button))
        label2 = Label(text="", size=(50, 30), size_hint=(None, 1), valign='middle')
        label3 = Label(text="", size=(100, 30), size_hint=(0.2, 1), valign='middle')
        layout.add_widget(label)
        layout.add_widget(textbox)
        layout.add_widget(remove_button)
        # layout.add_widget(label2)
        if enable_add_button:
            layout.add_widget(add_button)
            # check previous catalog and remove "+" button there.
            if len(self.ids['_grid_layout'].children) > 0:
                prev_cat = self.ids['_grid_layout'].children[0]
                if prev_cat.children[1].text == '+':
                    self.ids['_grid_layout'].children[0].children[1] = label2

        else:  # Add blank label instead of "+" button
            layout.add_widget(label2)

        layout.add_widget(label3)

        self.ids['_grid_layout'].add_widget(layout)

    def remove_cat(self, button, *largs):
        """
        Remove index when user presses '-' button
        :param button: '-' button itself
        :param largs:
        :return:
        """
        # Get index of the catalog containing this button
        index = self.ids['_grid_layout'].children.index(button.parent)
        self.ids['_grid_layout'].remove_widget(self.ids['_grid_layout'].children[index])

        # When the last catalog is removed, enable '+' button on the previous catalog
        if index == 0 and len(self.ids['_grid_layout'].children) > 0:
            add_button = Button(text="+", on_press=partial(self.create_cat_layout, '', True), width=70,
                                size_hint=(0.2, 1))
            self.ids['_grid_layout'].children[0].children[1] = add_button

    def save_catalogs(self):
        """
        Saves all catalogs to the disk.
        :return:
        """
        self.catalogs = [layout.children[3].text for layout in self.ids['_grid_layout'].children]
        while '' in self.catalogs:
            self.catalogs.remove('')

        top = et.Element('catalogs')
        for cat in self.catalogs:
            et.SubElement(top, 'catalog').text = cat
        tree = et.ElementTree(top)
        tree.write(self.conf_file)

        Popup(title='Scribe',
              title_size=22, width=500,
              content=Label(text='Saved catalogs successfully.', font_size=20),
              size_hint=(None, .22)).open()


class LimitedMetadataScreen(Screen):
    # Initialize the widget
    # Main thread
    def __init__(self, **kwargs):
        super(LimitedMetadataScreen, self).__init__(**kwargs)
        try:
            self.on_enter()
        except:
            pass

    # on_enter()
    # This method initializes the widget upon loading in the UI
    # Fields are hard-coded into the widget section in the kv and
    # values are contained in a metadata_key field.
    # Main thread
    def on_enter(self):
        # Get scancenter metadata.xml from disk
        config = get_metadata(scribe_globals.scancender_metadata)
        # Get all widgets
        # Since there are also "EmailTextInput" class instances, let us see each widget has 'metadata_key' property
        for widget in [x for x in self.ids['_metadata'].children]:
            try:
                val = config.get(widget.metadata_key)
                widget.text = val
            except AttributeError:
                pass

    # save_metadata()
    # This method writes metadata to disk
    # Main thread
    def save_metadata(self):
        # Get the metadata from the default location
        # TODO: Try catch, dependency on figuring out what the best course of action would be
        config = get_metadata(scribe_globals.scancender_metadata)
        config.pop("collection", None)
        # Make a copy of it
        new_config = copy.deepcopy(config)
        # Load all textboxs in the interface in a dict
        # For each of them, get the value in the textbox  and assign it to the new copy of the dict
        for widget in [x for x in self.ids['_metadata'].children]:
            try:
                meta_key = widget.metadata_key
                new_config[meta_key] = widget.text
            except AttributeError:
                pass

        # if the two dics are different, set the new one as default
        if cmp(config, new_config) != 0:
            set_metadata(new_config, scribe_globals.scancender_metadata)

        Popup(title='Scribe',
              title_size=22,
              content=Label(text='Saved matadata successfully.', font_size=20),
              size_hint=(.5, .22)).open()


class UpdateWidget(Screen):
    global update_status

    def __init__(self, **kwargs):
        super(UpdateWidget, self).__init__(**kwargs)
        self.ids['_status_label'].text = self.get_update_status()

    def display_popup(self, tit, message):
        popup = Popup(title=tit, content=Label(text=message),
                      size_hint=(None, None), size=(600, 200))
        popup.open()
        return popup

    def get_update_status(self):
        i, c, e = check_software_update()
        if update_status == UpdateStatus.up_to_date.value:
            return "Your Scribe is up to date.\n You are currently running version " + str(i)
        elif update_status == UpdateStatus.update_available.value:
            self.ids['_update_button'].disabled = False
            return "There is an update available!\nCandidate version: " + str(c)
        elif update_status == UpdateStatus.error.value or e is not None:
            return "An error occurred. Here's some more information:\n" + str(e)
        else:
            return "Couldn't get that information. Try again later.\n Error details:\n" + str(e)

    def check_update(self):
        self.ids['_check_update_button'].disabled = True
        p = self.display_popup("Checking", "Refreshing apt sources and looking for update.\nPlease wait...")
        p.auto_dismiss = False
        try:
            callback = partial(self.dismiss_popup, p)
            thread = self.check_update_inner(onExit=callback)
        except Exception as e:
            self.ids['_check_update_button'].disabled = False
            p.dismiss()
            self.display_popup("Error", "The update couldn't be completed. "
                                        "Run in terminal mode for a debug log.\n Error: {}".format(e))
            return

        self.ids['_status_label'].text = self.get_update_status()
        self.ids['_check_update_button'].disabled = False

    def check_update_inner(self, onExit):
        """
        Runs the given args in a subprocess.Popen, and then calls the function
        onExit when the subprocess completes.
        onExit is a callable object, and popenArgs is a list/tuple of args that
        would give to subprocess.Popen.
        """

        def runInThread(onExit):
            proc = subprocess.Popen('sudo apt-get update', shell=True, stdin=None, executable="/bin/bash")
            proc.wait()
            onExit()
            return

        thread = threading.Thread(target=runInThread, args=[onExit])
        thread.start()
        # returns immediately after the thread starts
        return thread

    def dismiss_popup(self, p):
        p.dismiss()

    def do_update(self):
        print "Upadting now..."
        p = self.display_popup("Updating",
                               "Update taking place now.\n"
                               "Scribe will restart automatically once the update is completed.\nPlease wait...")

        args = sys.argv[:]
        _startup_cwd = os.getcwd()

        args.insert(0, sys.executable)

        # We are ready to update.
        try:
            proc = subprocess.check_call('sudo apt-get update && sudo apt-get install -y ia-scribe', shell=True,
                                         stdin=None, executable="/bin/bash")
        except subprocess.CalledProcessError:
            print "Update failed"
            update_status = UpdateStatus.error.value
            self.display_popup("Error", "The update couldn't be completed. Run in terminal mode for a debug log.")
            return

        # before leaving, remove the current pid lock.
        config_dir = os.path.expanduser(scribe_globals.config_dir)
        path = os.path.join(config_dir, 'scribe_pid')
        if os.path.exists(path):
            f = open(path)
            old_pid = f.read().strip()
            f.close()
            pid_dir = os.path.join('/proc', old_pid)
            if os.path.exists(pid_dir):
                print("Got the pid file at " + str(pid_dir) + ". Now unlinking before relaunch...")
                os.unlink(path)

        # restart
        os.chdir(_startup_cwd)
        os.execv(sys.executable, args)


class ScrollableLabel(ScrollView):
    text = StringProperty('')


class RelNotesWidget(Screen):
    global update_status

    def __init__(self, **kwargs):
        super(RelNotesWidget, self).__init__(**kwargs)
        f = open("CHANGELOG.md", 'r')
        self.ids['_changelog_text'].text = f.read()

    def on_enter(self):
        # populate the changelog here
        pass


# SettingsScreen
# this class manages the "settings" view on the about UI
# Main thread
# _________________________________________________________________________________________
class GeneralSettingsScreen(Screen):
    scribe_widget = ObjectProperty(None)

    # __init__()
    # _____________________________________________________________________________________
    def __init__(self, **kwargs):
        super(GeneralSettingsScreen, self).__init__(**kwargs)

    # on_enter()
    #
    # _____________________________________________________________________________________
    def on_enter(self):
        # read scribe_config.yml
        config = scribe_config.read()
        # Check sound_delay value
        if config.get('sound_delay') is None:
            config['sound_delay'] = .1
            scribe_config.save(config)

        # Get all widgets that we want to initialize
        widgets = [x for x in self.ids['_metadata'].children if isinstance(x, (SettingsSwitch, SettingsInputBox))]
        # For each, initialize
        for widget in widgets:
            # get value from  dict
            val = scribe_config.get(config, widget.metadata_key)
            # set value to widget
            try:
                widget.set_value(val)
            except ValueError:
                print 'Failed to update new values'
        box = self.ids['bl_box']
        if not (scribe_globals.EXIFTOOL or type(box.children[2]) is ExifBanner):
            banner = ExifBanner()
            box.add_widget(banner, 2)

    def set_switch(self, switch):
        """
        Callback function when the MetadataSwitch is switched.
        :param switch:
        :return:
        """
        metadata_key = switch.metadata_key
        if isinstance(switch, SettingsSwitch):
            switch = switch.ids.switch
        new_val = 'True' if switch.active else 'False'
        config = scribe_config.read()
        if config.get(metadata_key):
            config[metadata_key] = new_val
            scribe_config.save(config)

        if metadata_key == 'en_sound_delay':
            if self.ids['txt_sound_delay'].disabled:
                config['sound_delay'] = -1
                scribe_config.save(config)

    # save_config()
    # Main thread
    # _____________________________________________________________________________________
    def save_config(self):
        # read config file
        config = scribe_config.read()
        print config
        # make a copy
        new_config = copy.deepcopy(config)
        # select all widgets types that will be data sources
        widgets = [x for x in self.ids['_metadata'].children if isinstance(x, (SettingsInputBox, SettingsSwitch))]
        # Update each
        for widget in widgets:
            val = str(widget.get_value())
            # Don't overwrite empty fields
            if isinstance(widget, SettingsInputBox):
                if not val or len(val.strip()) == 0:
                    continue
            scribe_config.update(new_config, widget.metadata_key, val)
        # if something has changed, save

        print new_config
        print cmp(config, new_config)
        if cmp(config, new_config) != 0:
            scribe_config.save(new_config)

        restart_app()

    def get_current_camera(self):
        return 'Sony'

    def get_camera_models(self):
        # models, ports = self.scribe_widget.cameras.get_cameras()
        return ['Nikon J3', 'Nikon J4', 'Nikon J5']

    def set_camera(self):
        return


class HoverBehavior(object):
    """
    :Events:
        `on_enter`
            Fired when mouse enter the bbox of the widget.
        `on_leave`
            Fired when the mouse exit the widget
    """
    hovered = BooleanProperty(False)
    border_point = ObjectProperty(None)
    '''Contains the last relevant point received by the Hoverable. This can
    be used in `on_enter` or `on_leave` in order to know where was dispatched the event.
    '''

    def __init__(self, **kwargs):
        self.register_event_type('on_enter')
        self.register_event_type('on_leave')
        Window.bind(mouse_pos=self.on_mouse_pos)
        super(HoverBehavior, self).__init__(**kwargs)

    def on_mouse_pos(self, *args):
        if not self.get_root_window():
            return  # do proceed if I'm not displayed <=> If have no parent
        pos = args[1]
        # Next line to_widget allow to compensate for relative layout
        inside = self.collide_point(*self.to_widget(*pos))
        if self.hovered == inside:
            # We have already done what was needed
            return
        self.border_point = pos
        self.hovered = inside
        if inside:
            self.dispatch('on_enter')
        else:
            self.dispatch('on_leave')

    def on_enter(self):
        pass

    def on_leave(self):
        pass


Factory.register('HoverBehavior', HoverBehavior)


class HoverImage(Image, HoverBehavior):
    info = StringProperty('')
    active = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(HoverImage, self).__init__(**kwargs)
        self.active = False
        Clock.schedule_once(self.on_leave)

    def on_enter(self, *args):
        if not self.active:
            img_file = cur_dir + '/images/' + self.info + '-tab-hover.png'
            if os.path.exists(img_file):
                self.source = img_file
            else:
                self.source = cur_dir + '/images/system-tab-hover.png'

    def on_leave(self, *args):
        if not self.active:
            img_file = cur_dir + '/images/' + self.info + '-tab-inactive.png'
            if os.path.exists(img_file):
                self.source = cur_dir + '/images/' + self.info + '-tab-inactive.png'
            else:
                self.source = cur_dir + '/images/system-tab-inactive.png'


class HoverButton(Button):
    info = StringProperty('')

    def __init__(self, **kwargs):
        super(HoverButton, self).__init__(**kwargs)

    def on_press(self):
        """
        Walk through other HoverButtons and de-activate them
        """
        for btn in self.parent.children:
            if btn.__class__.__name__ == self.__class__.__name__ and btn != self:
                btn.ids['_img'].active = False
                btn.ids['_img'].on_leave()
        self.ids['_img'].active = True
        self.ids['_img'].source = cur_dir + '/images/' + self.info + '-tab-active.png'


class ExifBanner(BoxLayout):
    pass
