import os

from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout

from ia_scribe.uix.behaviors.focus import FocusBehavior

cur_dir = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(cur_dir + '/widgets.kv')


class SettingsSwitch(BoxLayout):

    label = StringProperty('')
    metadata_key = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_switch')
        super(SettingsSwitch, self).__init__(**kwargs)
        self.bind(label=self._on_label)

    def _on_label(self, *args):
        self.ids.label.text = self.label

    def on_switch(self, *args):
        pass

    def _on_switch(self, *args):
        self.dispatch('on_switch', args)

    def set_value(self, new_val):
        if type(new_val) == str:
            new_val = True if new_val == 'True' else False
        elif type(new_val) != bool:
            new_val = False
            #raise ValueError('Invalid type of value')

        self.ids.switch.active = new_val

    def get_value(self):
        return self.ids.switch.active


class SettingsInputBox(BoxLayout, FocusBehavior):

    label = StringProperty('')
    metadata_key = StringProperty('')
    focus_previous = ObjectProperty(None)
    focus_next = ObjectProperty(None)
    hint_text = StringProperty('')
    description = StringProperty('')

    def __init__(self, **kwargs):
        super(SettingsInputBox, self).__init__(**kwargs)
        self.bind(label=self._on_label)
        self.bind(focus_previous=self._on_focus_previous)
        self.bind(focus_next=self._on_focus_next)
        self.bind(hint_text=self._on_hint_text)
        self.bind(description=self._on_description)

    def _on_label(self, *args):
        self.ids.label.text = self.label

    def _on_focus_next(self, *args):
        self.ids.txt_input.focus_next = self.focus_next

    def _on_focus_previous(self, *args):
        self.ids.txt_input.focus_previous = self.focus_previous

    def _on_hint_text(self, *args):
        self.ids.txt_input.hint_text = self.hint_text

    def _on_description(self, *args):
        self.ids.lb_dsc.text = self.description

    def get_value(self):
        return self.ids.txt_input.text

    def set_value(self, new_val):
        self.ids.txt_input.text = str(new_val)


Factory.register('SettingsSwitch', cls=SettingsSwitch)
Factory.register('SettingsInputBox', cls=SettingsInputBox)
