import os
import xml.etree.ElementTree as ET
from functools import partial

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen

import scribe_globals
from ia_scribe.uix.metadata import MetadataTextInput
from utils.metadata import set_collections_metadata, get_collections_metadata

cur_dir = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(cur_dir + '/metadata_collection.kv')


class MetadataCollectionsScreen(Screen):
    def __init__(self, **kwargs):
        super(MetadataCollectionsScreen, self).__init__(**kwargs)

    def on_enter(self):
        self.load_metadata()
        print cur_dir

    def load_metadata(self):
        # Get scancenter metadata.xml from disk
        config = get_collections_metadata(scribe_globals.config_dir)
        # Remove existing widgets
        widgets = [x for x in self.ids['_grid_layout'].children]
        for widget in widgets:
            self.ids['_grid_layout'].remove_widget(widget)
        # Redraw widgets from file

        try:  # to get the collection sets list
            for cset in config["collections"]['set']:
                self.ids['_grid_layout'].add_widget(self.create_set_layout(cset))
        except:
            # create file
            root = ET.Element('collections')
            tree = ET.ElementTree(root)
            set_collections_metadata(tree, scribe_globals.config_dir)

    def create_set_layout(self, cset):
        set_layout = BoxLayout(id="_box_layout_cset_" + str(cset["name"]), orientation='vertical', size_hint_y=None,
                               height=0)
        layout = BoxLayout(id="_box_layout_cset_root_" + str(cset["name"]), orientation='horizontal', size_hint_y=None,
                           height=40)
        # traversing collection sets
        label = BlackLabel(text="Set", bold=True, size=(250, 40), valign='middle', size_hint=(1, 1))
        textbox = MetadataTextInput(id="_textbox_cset", text=str(cset["name"]), size_hint=(None, 1), size=(300, 40),
                                    padding=(10, 10, 10, 10))
        add_button = Button(text="+ collection", on_press=partial(self.add_metadata_field, set_layout), width=70,
                            size_hint=(0.5, 1))
        remove_button = Button(text="-", on_press=partial(self.remove_set, set_layout), width=15, size_hint=(0.14, 1))
        label2 = BlackLabel(text="", size=(100, 40), size_hint=(0.45, 1), valign='middle')
        label3 = BlackLabel(text="", size=(100, 40), size_hint=(0.9, 1), valign='middle')
        layout.add_widget(label)
        layout.add_widget(textbox)
        layout.add_widget(remove_button)
        # layout.add_widget(label2)
        layout.add_widget(add_button)
        layout.add_widget(label3)
        set_layout.height += layout.height
        set_layout.add_widget(layout)
        for collection in cset["collection"]:
            layout = BoxLayout(id="_box_layout_collection_" + str(collection["_text"]), size_hint_y=None,
                               orientation='horizontal', height=40, padding=2)
            label = BlackLabel(text="Collection", size=(250, 40), valign='middle', size_hint=(0.65, 1))
            textbox = MetadataTextInput(text=str(collection["_text"]), id="_textbox_" + str(collection["_text"]),
                                        size_hint=(None, 1), size=(300, 40), padding=(10, 10, 10, 10))
            remove_button = Button(text="-", on_press=partial(self.remove_collection, set_layout, layout), width=15,
                                   size_hint=(0.08, 1))
            label2 = BlackLabel(text="", size_hint=(0.9, 1), size=(200, 40), valign='middle')
            layout.add_widget(label)
            layout.add_widget(textbox)
            layout.add_widget(remove_button)
            layout.add_widget(label2)
            set_layout.add_widget(layout)
            set_layout.height += layout.height
        return set_layout

        # add buttons

    def add_set(self):
        cset = {'name': "", "collection": ""}
        grid_layout = self.ids['_grid_layout']
        set_layout = self.create_set_layout(cset)
        # TODO: Move <tab> cycling to specific GridLayout or ScrollView class
        if grid_layout.children:
            first_input = grid_layout.children[-1].children[-1].children[-2]
            old_last_input = grid_layout.children[0].children[0].children[-2]
            old_last_input.focus_next = None
        else:
            first_input = set_layout.children[-1].children[-2]
        last_input = set_layout.children[0].children[-2]
        last_input.focus_next = first_input.proxy_ref
        first_input.focus_previous = last_input.proxy_ref
        grid_layout.add_widget(set_layout)

    def remove_set(self, pivot, *largs):
        grid_layout = self.ids['_grid_layout']
        last_input_proxy = None
        if pivot is grid_layout.children[-1]:
            # pivot layout is on first place
            last_input_proxy = pivot.children[-1].children[-2].focus_previous
        grid_layout.remove_widget(pivot)
        if grid_layout.children and last_input_proxy:
            first_input = grid_layout.children[-1].children[-1].children[-2]
            first_input.focus_previous = last_input_proxy

    def add_metadata_field(self, pivot, *largs):
        layout = BoxLayout(id="_box_layout_collection_", orientation='horizontal', size_hint_y=None, height=40,
                           padding=2)
        label = BlackLabel(text="Collection", size=(250, 40), valign='middle', size_hint=(0.65, 1))
        text_input = MetadataTextInput(text="", id="_textbox_", size_hint=(None, 1), size=(300, 40),
                                       padding=(10, 10, 10, 10))
        remove_button = Button(text="-", on_press=partial(self.remove_collection, pivot, layout), width=15,
                               size_hint=(0.08, 1))
        label2 = BlackLabel(text="", size_hint=(0.9, 1), size=(200, 40), valign='middle')
        text_input.hint_text = "New collection"
        layout.add_widget(label)
        layout.add_widget(text_input)
        layout.add_widget(remove_button)
        layout.add_widget(label2)
        pivot.add_widget(layout)
        pivot.height += layout.height
        parent_layout = pivot.parent
        if parent_layout.children[0] is pivot:
            # pivot layout is on the end
            first_input = parent_layout.children[-1].children[-1].children[-2]
            first_input.focus_previous = text_input.proxy_ref
            text_input.focus_next = first_input.proxy_ref
            old_last_input = pivot.children[0].children[-2]
            old_last_input.focus_next = None

    def remove_collection(self, layout, pivot, *largs):
        layout.height -= pivot.height
        layout.remove_widget(pivot)
        if pivot.children[-2].focus_next:
            layout.children[0].children[-2].focus_next = pivot.focus_next

    # save_metadata()
    # This method writes collections metadata to disk
    # Main thread
    def save_metadata(self):
        config = get_collections_metadata(scribe_globals.config_dir)

        # for cset in self.ids._metadata.children if isinstance(x, (MetadataTextInput)):
        root = ET.Element('collections')
        tree = ET.ElementTree(root)
        widgets = [x for x in self.ids['_grid_layout'].children if "_box_layout_cset_" in x.id]
        if not widgets:
            popup = Popup(title='Error', content=Label(text='At least one collection set is required'),
                          size_hint=(None, None), size=(600, 200))
            popup.open()
            return
        # verify that every item is full
        for cset in reversed(widgets):
            box = [x for x in cset.children if "_box_layout_cset_root_" in x.id][0]
            tbox = [x for x in box.children if isinstance(x, MetadataTextInput)][0]
            collection_set_name = tbox.text
            collection_set = ET.SubElement(root, "set", attrib={"name": collection_set_name})
            collections_widgets = [x for x in cset.children if "_box_layout_collection_" in x.id]
            if not collections_widgets:
                popup = Popup(title='Error', content=Label(text='At least one collection per named set is required'),
                              size_hint=(None, None), size=(600, 200))
                popup.open()
                return
            for collection in reversed(collections_widgets):
                tbox = [x for x in collection.children if isinstance(x, MetadataTextInput)][0]
                if tbox.text == "":
                    popup = Popup(title='Error', content=Label(
                        text='You have not specified a name for one or more collections in set ' + collection_set_name),
                                  size_hint=(None, None), size=(600, 200))
                    popup.open()
                    return
                collection_name = tbox.text
                collection_name_element = ET.SubElement(collection_set, "collection")
                collection_name_element.text = collection_name.replace(" ", "")
                print "> Created collection tree node name ", collection_name, " in collection set ", collection_set_name
        print ET.tostring(root, encoding='utf8', method='xml')

        set_collections_metadata(tree, scribe_globals.config_dir)
        return True


class BlackLabel(Label):
    def set_width(self, w):
        self.size_hint_x = None
        self.width = w
