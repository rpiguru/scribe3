from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from ia_scribe.calibration.widget.calibration_widget import CalibrationWidget
from ia_scribe.calibration.widget.foldout_widget import CalibrationWidgetFoldout
from ia_scribe.calibration.widget.single_calibration_widget import SingleCalibrationWidget

Builder.load_file(join(dirname(__file__), 'calibration.kv'))


class CalibrationScreen(Screen):
    scribe_widget = ObjectProperty(None)
    screen_manager = ObjectProperty(None)
    target_screen = ObjectProperty('capture_screen')

    def on_enter(self):
        # self.query_cameras()
        print "Entering CalibrationScreen"
        self.ids['_calibration_box'].clear_widgets()
        info_box = CalibrationInfoMessage()
        info_box.rescan_func = self.query_cameras
        self.ids['_calibration_box'].add_widget(info_box)

    def on_leave(self, *args):
        for widget in self.ids['_calibration_box'].children:
            if isinstance(widget, (CalibrationWidget, SingleCalibrationWidget)):
                widget.use_tooltips = False
            elif isinstance(widget, CalibrationWidgetFoldout):
                widget.ids.image_menu_bar.use_tooltips = False

    def query_cameras(self):
        print "CalibrationScreen::query_cameras"
        self.ids['_calibration_box'].clear_widgets()
        # models, ports = self.scribe_widget.cameras.get_cameras()
        num_cameras = self.scribe_widget.cameras.num_cameras()


        if num_cameras not in [1, 2, 3]:
            error_box = CalibrationErrorMessage()
            error_box.error_msg = 'Number of cameras: {n}\n' \
                                  'You must have one, two or three cameras connected!'.format(n=num_cameras)
            error_box.rescan_func = self.query_cameras
            self.ids['_calibration_box'].add_widget(error_box)
            return

        """
        print "CalibrationScreen:: setting camera ports. NCAM:", num_cameras
        self.scribe_widget.cameras.set_camera('left', ports[0])
        self.scribe_widget.cameras.set_camera('right', ports[1])
        if num_cameras == 3:
            self.scribe_widget.cameras.set_camera('foldout', ports[2])
        else:
            sel
            f.scribe_widget.cameras.set_camera('foldout', None)
        """
        # self.scribe_widget.left_camera, self.scribe_widget.right_camera = ports[:2]
        # self.scribe_widget.left_camera_model, self.scribe_widget.right_camera_model = models[:2]
        b_single = True if self.scribe_widget.cameras.num_cameras() == 1 else False
        if b_single:
            calibration_box = SingleCalibrationWidget(scribe_widget=self.scribe_widget)
            calibration_box.screen_manager = self.screen_manager
            calibration_box.callibration_screen = self
            calibration_box.target_screen = self.target_screen
            calibration_box.ids.image_menu_bar.attach_tooltip_to = self
            foldout_widget = calibration_box.foldout_widget
            if foldout_widget:
                foldout_widget.ids.image_menu_bar.attach_tooltip_to = self
        else:
            calibration_box = CalibrationWidget(scribe_widget=self.scribe_widget)
            calibration_box.screen_manager = self.screen_manager
            calibration_box.callibration_screen = self
            calibration_box.target_screen = self.target_screen
            calibration_box.ids.left_image_menu_bar.attach_tooltip_to = self
            calibration_box.ids.right_image_menu_bar.attach_tooltip_to = self
            foldout_widget = calibration_box.foldout_widget
            if foldout_widget:
                foldout_widget.ids.image_menu_bar.attach_tooltip_to = self
        calibration_box.use_tooltips = True
        self.ids['_calibration_box'].add_widget(calibration_box)


# CalibrationErrorMessage
# _________________________________________________________________________________________
class CalibrationErrorMessage(BoxLayout):
    error_msg = StringProperty()
    button_text = StringProperty('Scan for cameras again')
    rescan_func = ObjectProperty(None)


# CalibrationInfoMessage
# _________________________________________________________________________________________
class CalibrationInfoMessage(BoxLayout):
    rescan_func = ObjectProperty(None)
