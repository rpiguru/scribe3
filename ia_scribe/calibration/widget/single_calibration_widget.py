import os
import tempfile
import webbrowser
from functools import partial
import Image
import shutil
from os.path import join, dirname
from kivy import Logger
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from ia_scribe.uix.file_chooser import FileChooser
from ia_scribe.uix.behaviors.tooltip import TooltipControl
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from ia_scribe.uix.message import ScribeMessageLearnMore, ScribeMessage
from utils.util import resource_path

Builder.load_file(join(dirname(__file__), 'single_calibration_widget.kv'))


class SingleCalibrationWidget(BoxLayout):
    screen_manager = ObjectProperty(None)
    scribe_widget = ObjectProperty(None)
    callibration_screen = ObjectProperty(None)
    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    cam_port = StringProperty()
    target_screen = ObjectProperty('capture_screen')

    use_tooltips = BooleanProperty(False)

    def __init__(self, scribe_widget=scribe_widget, **kwargs):
        super(SingleCalibrationWidget, self).__init__(**kwargs)
        self.scribe_widget = scribe_widget
        self.image = None
        self.thumb = None
        self.foldout_image = None
        self.foldout_thumb = None
        self.foldout_widget = None
        if scribe_widget.cameras.camera_ports['foldout'] is None:
            self.cam_port = ''
        else:
            self.cam_port = scribe_widget.cameras.camera_ports['foldout']

        self.capture_images()
        Clock.schedule_once(self._bind_image_menus)

    def _bind_image_menus(self, *args):
        menu_bar = self.ids.image_menu_bar
        menu_bar.bind(on_option_select=self.on_image_menu_option)
        if self.foldout_widget:
            menu = self.foldout_widget.ids.image_menu_bar
            menu.bind(on_option_select=self.on_foldout_image_menu_option)

    def on_image_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_image(self.image)
        elif option == 'export':
            self.start_export_filechooser(self.image)

    def on_foldout_image_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_image(self.foldout_image)
        elif option == 'export':
            self.start_export_filechooser(self.foldout_image)

    def start_export_filechooser(self, source_path):
        if source_path is not None:
            filename = os.path.basename(source_path)
            default_path = join(os.path.expanduser('~'), filename)
            root, ext = os.path.splitext(source_path)
            filechooser = FileChooser()
            callback = partial(self.on_file_chooser_selection, source_path)
            filechooser.bind(on_selection=callback)
            filechooser.save_file(title='Export image',
                                  icon='./images/window_icon.png',
                                  filters=[ext.replace('.', '*')],
                                  path=default_path)

    def on_file_chooser_selection(self, source_path, chooser, selection):
        if selection:
            destination_path = selection[0]
            root, ext = os.path.splitext(source_path)
            if not destination_path.endswith(ext):
                destination_path += ext
            self.export_image(source_path, destination_path)

    @staticmethod
    def show_image(path):
        try:
            firefox = webbrowser.get('firefox')
            firefox.open(path)
        except Exception as e:
            Logger.exception('Calibration: Unable to open image "{}", error: {}'
                             .format(path, e))

    @staticmethod
    def export_image(source, destination):
        try:
            shutil.copyfile(source, destination)
            Logger.info('Calibration: Image exported from "{}" to "{}"'
                        .format(source, destination))
        except shutil.Error:
            Logger.exception('Calibration: Image source path are the same. '
                             'Source "{}", destionation "{}"'
                             .format(source, destination))
        except IOError:
            Logger.exception('Calibration: Destination "{}" is not writable'
                             .format(destination))
        except Exception as e:
            Logger.exception('Calibration: Unable to export image from "{}" '
                             'to "{}", error: {}'.format(source, destination, e))

    def capture_images(self):
        if self.cam_port != '':
            page = self.ids['_cam_image']
            page.source = self.loading_image

            self.clean_temp_images()

            f = tempfile.NamedTemporaryFile(prefix='calibration', suffix='.jpg', delete=False)
            self.image = f.name
            f.close()

            f = tempfile.NamedTemporaryFile(prefix='calibration_thumb', suffix='.jpg', delete=False)
            self.thumb = f.name
            f.close()

            callback = self.show_image_callback
            stats = None
            self.scribe_widget.foldout_queue.put(('foldout', self.image, self.thumb, callback, page, stats))

    def clean_temp_images(self):
        for image in (self.foldout_image, self.foldout_thumb):
            if image is not None and os.path.exists(image):
                os.unlink(image)

    def show_image_callback(self, thumb_path, img_obj, *largs):
        """
        This function modifies UI elements and needs to be scheduled on the main thread
        """
        side = largs[0]

        # Returns false if the parameter is outside its baseline
        def check_item_factory(key, value, baseline=None):
            ret = True
            # when adding widgets, we need to specify the height manually (disabling
            # the size_hint_y) so the dropdown can calculate the area it needs.
            btn = Button(text=str(key + ':' + value), size_hint_y=None, height=20)
            if baseline:
                if value > baseline:
                    btn.background_color = [1, 0, 0, 1]
                    ret = False
                else:
                    background_color = [0, 1, 0, 1]
            # btn.bind(on_release=lambda btn: dropdown.select(btn.text))
            dropdown.add_widget(btn)
            return ret

        dropdown = DropDown()

        # dropdown.bind(on_select=lambda instance, x: setattr(mainbutton, 'text', x))

        mainbutton = self.ids['_preflight_button']
        mainbutton.bind(on_release=dropdown.open)
        r1 = check_item_factory('Focal Length', self.exif_tag('FocalLength'), )
        r2 = check_item_factory('Aperture', self.exif_tag('ApertureValue'), )
        r3 = check_item_factory('Exposure Time:', self.exif_tag('ExposureTime'), )
        # r4 = check_item_factory('F Number:', self.exif_tag('FNumber'), )
        # r5 = check_item_factory('Depth of field:', field_depth())
        if r1 and r2 and r3 and r3:
            mainbutton.background_color = (0, 1, 0, .9)
        else:
            mainbutton.background_color = (1, 0, 0, .9)

        img_obj.source = thumb_path

    @staticmethod
    def parse_gphoto_output(output):
        value = ''
        for line in output.split('\n'):
            if line.startswith('Current:'):
                value = line[8:].strip()
                break
        return value

    def retry(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.callibration_screen.query_cameras()

    def show_config_error(self, setting, value, current_val, side):
        msg_box = ScribeMessage()
        msg_box.button_text = 'Retry'
        msg_box.trigger_func = self.retry
        msg_box.text = '\n\n'.join(['The {side}-page camera is not configured correctly.'.format(side=side),
                                    'The setting {s} should be set to {v},\n'
                                    'but it is set to {curr}.'.format(s=setting, v=value, curr=current_val),
                                    'Note that the {side}-page camera is mounted '
                                    'OPPOSITE the page it is capturing'.format(side=side), ])
        popup = Popup(title='Camera Configuration Error', content=msg_box,
                      auto_dismiss=False)
        msg_box.popup = popup
        popup.open()

    def reshoot_button(self):
        self.clean_temp_images()
        self.capture_images()

    @staticmethod
    def show_help_popup():
        msg_box = ScribeMessageLearnMore()
        popup = Popup(title='Calibration help', content=msg_box,
                      auto_dismiss=True, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()

    # done_button() - main thread
    # _____________________________________________________________________________________
    def done_button(self):
        # self.check_camera_config()

        self.clean_temp_images()
        self.screen_manager.transition.direction = 'left'
        print self.screen_manager.screens
        self.screen_manager.current = self.target_screen
        # self.screen_manager.current = 'capture_screen'

    def exif_tag(self, tag):
        return "Unavailable"

    def on_use_tooltips(self, widget, use_tooltips):
        stack = self.children[:]
        while stack:
            widget = stack.pop()
            if isinstance(widget, TooltipControl):
                widget.use_tooltips = use_tooltips
            stack.extend(widget.children)
