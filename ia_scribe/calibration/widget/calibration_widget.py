import os
import tempfile
import webbrowser
from functools import partial
import Image
import shutil
from os.path import join, dirname
from kivy import Logger
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from ia_scribe.uix.file_chooser import FileChooser
from ia_scribe.uix.behaviors.tooltip import TooltipControl
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from ia_scribe.uix.message import ScribeMessageLearnMore, ScribeMessage
from foldout_widget import CalibrationWidgetFoldout
from utils.util import resource_path

Builder.load_file(join(dirname(__file__), 'calibration_widget.kv'))


class CalibrationWidget(BoxLayout):
    screen_manager = ObjectProperty(None)
    scribe_widget = ObjectProperty(None)
    callibration_screen = ObjectProperty(None)
    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    left_port = StringProperty()
    right_port = StringProperty()
    target_screen = ObjectProperty('capture_screen')
    use_tooltips = BooleanProperty(False)
    """Pass this value to `use_tooltips` of every TooltipControl child widget.
    """

    def __init__(self, scribe_widget=scribe_widget, **kwargs):
        super(CalibrationWidget, self).__init__(**kwargs)
        self.scribe_widget = scribe_widget
        self.left_image = None
        self.left_thumb = None
        self.right_image = None
        self.right_thumb = None
        self.foldout_image = None
        self.foldout_thumb = None
        self.foldout_widget = None
        self.left_port = scribe_widget.cameras.camera_ports['left']
        self.right_port = scribe_widget.cameras.camera_ports['right']

        if scribe_widget.cameras.camera_ports['foldout'] is not None:
            self.add_foldout_widget()
        self.capture_images()
        Clock.schedule_once(self._bind_image_menus)

    def _bind_image_menus(self, *args):
        left = self.ids.left_image_menu_bar
        left.bind(on_option_select=self.on_left_image_menu_option)
        right = self.ids.right_image_menu_bar
        right.bind(on_option_select=self.on_right_image_menu_option)
        if self.foldout_widget:
            menu = self.foldout_widget.ids.image_menu_bar
            menu.bind(on_option_select=self.on_foldout_image_menu_option)

    def on_left_image_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_image(self.left_image)
        elif option == 'export':
            self.start_export_filechooser(self.left_image)

    def on_right_image_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_image(self.right_image)
        elif option == 'export':
            self.start_export_filechooser(self.right_image)

    def on_foldout_image_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_image(self.foldout_image)
        elif option == 'export':
            self.start_export_filechooser(self.foldout_image)

    def start_export_filechooser(self, source_path):
        filename = os.path.basename(source_path)
        default_path = join(os.path.expanduser('~'), filename)
        root, ext = os.path.splitext(source_path)
        filechooser = FileChooser()
        callback = partial(self.on_file_chooser_selection, source_path)
        filechooser.bind(on_selection=callback)
        filechooser.save_file(title='Export image',
                              icon='./images/window_icon.png',
                              filters=[ext.replace('.', '*')],
                              path=default_path)

    def on_file_chooser_selection(self, source_path, chooser, selection):
        if selection:
            destination_path = selection[0]
            root, ext = os.path.splitext(source_path)
            if not destination_path.endswith(ext):
                destination_path += ext
            self.export_image(source_path, destination_path)

    def show_image(self, path):
        try:
            firefox = webbrowser.get('firefox')
            firefox.open(path)
        except Exception:
            Logger.exception('Calibration: Unable to open image "{}"'
                             .format(path))

    def export_image(self, source, destination):
        try:
            shutil.copyfile(source, destination)
            Logger.info('Calibration: Image exported from "{}" to "{}"'
                        .format(source, destination))
        except shutil.Error:
            Logger.exception('Calibration: Image source path are the same. '
                             'Source "{}", destionation "{}"'
                             .format(source, destination))
        except IOError:
            Logger.exception('Calibration: Destination "{}" is not writable'
                             .format(destination))
        except Exception:
            Logger.exception('Calibration: Unable to export image from "{}" '
                             'to "{}"'.format(source, destination))

    def add_foldout_widget(self):
        self.foldout_widget = CalibrationWidgetFoldout()
        self.foldout_widget.scribe_widget = self.scribe_widget
        self.foldout_widget.calibration_widget = self
        self.foldout_widget.ids['_foldout_spinner'].values = [self.scribe_widget.cameras.camera_ports['left'],
                                                              self.scribe_widget.cameras.camera_ports['right'],
                                                              self.scribe_widget.cameras.camera_ports['foldout']]
        self.foldout_widget.ids['_foldout_spinner'].text = self.scribe_widget.cameras.camera_ports['foldout']
        self.add_widget(self.foldout_widget)

    def capture_images(self):
        page_left = self.ids['_left_image']
        page_left.source = self.loading_image

        self.clean_temp_images()

        f = tempfile.NamedTemporaryFile(prefix='calibration', suffix='.jpg', delete=False)
        self.left_image = f.name
        f.close()

        f = tempfile.NamedTemporaryFile(prefix='calibration_thumb', suffix='.jpg', delete=False)
        self.left_thumb = f.name
        f.close()

        callback = self.show_image_callback
        stats = None
        self.scribe_widget.left_queue.put(('left', self.left_image, self.left_thumb, callback, page_left, stats))

        f = tempfile.NamedTemporaryFile(prefix='calibration', suffix='.jpg', delete=False)
        self.right_image = f.name
        f.close()
        f = tempfile.NamedTemporaryFile(prefix='calibration_thumb', suffix='.jpg', delete=False)
        self.right_thumb = f.name
        f.close()
        page_right = self.ids['_right_image']
        page_right.source = self.loading_image
        self.scribe_widget.right_queue.put(('right', self.right_image, self.right_thumb, callback, page_right, stats))

        if self.scribe_widget.cameras.camera_ports['foldout'] is not None:
            if self.foldout_widget is None:
                self.add_foldout_widget()
            foldout = self.foldout_widget.ids['_foldout_image']
            foldout.source = self.loading_image

            f = tempfile.NamedTemporaryFile(prefix='calibration', suffix='.jpg', delete=False)
            self.foldout_image = f.name
            f.close()

            f = tempfile.NamedTemporaryFile(prefix='calibration_thumb', suffix='.jpg', delete=False)
            self.foldout_thumb = f.name
            f.close()

            self.scribe_widget.foldout_queue.put(
                ('foldout', self.foldout_image, self.foldout_thumb, callback, foldout, stats))

    def clean_temp_images(self):
        for image in (self.left_image, self.left_thumb, self.right_image, self.right_thumb, self.foldout_image,
                      self.foldout_thumb):
            if image is not None and os.path.exists(image):
                os.unlink(image)

    # show_image_callback() - main thread
    # _____________________________________________________________________________________
    def show_image_callback(self, thumb_path, img_obj, *largs):
        """
        This function modifies UI elements and needs to be scheduled on the main thread
        """
        side = largs[0]

        # Returns false if the parameter is outside its baseline
        def check_item_factory(key, value, baseline=None):
            ret = True
            # when adding widgets, we need to specify the height manually (disabling
            # the size_hint_y) so the dropdown can calculate the area it needs.
            btn = Button(text=str(key + ':' + value), size_hint_y=None, height=20)
            if baseline:
                if value > baseline:
                    btn.background_color = [1, 0, 0, 1]
                    ret = False
                else:
                    background_color = [0, 1, 0, 1]
            # btn.bind(on_release=lambda btn: dropdown.select(btn.text))
            dropdown.add_widget(btn)
            return ret

        dropdown = DropDown()

        # dropdown.bind(on_select=lambda instance, x: setattr(mainbutton, 'text', x))

        if side == 'left':
            mainbutton = self.ids['_preflight_button_left']
            mainbutton.bind(on_release=dropdown.open)
            r1 = check_item_factory('Focal Length', self.exif_tag('FocalLength', "LEFT"), )
            r2 = check_item_factory('Aperture', self.exif_tag('ApertureValue', "LEFT"))
            r3 = check_item_factory('Exposure Time:', self.exif_tag('ExposureTime', "LEFT"))
            r4 = check_item_factory('F Number:', self.exif_tag('FNumber', "LEFT"))
            # r5 = check_item_factory('Depth of field:', field_depth())
            if r1 and r2 and r3 and r3:
                mainbutton.background_color = (0, 1, 0, .9)
            else:
                mainbutton.background_color = (1, 0, 0, .9)
        elif side == 'right':
            mainbutton = self.ids['_preflight_button_right']
            mainbutton.bind(on_release=dropdown.open)
            r1 = check_item_factory('Focal Length', self.exif_tag('FocalLength', "RIGHT"))
            r2 = check_item_factory('Aperture', self.exif_tag('ApertureValue', "RIGHT"))
            r3 = check_item_factory('Exposure Time:', self.exif_tag('ExposureTime', "RIGHT"))
            r4 = check_item_factory('F Number:', self.exif_tag('FNumber', "RIGHT"))
            if r1 and r2 and r3 and r3:
                mainbutton.background_color = (0, 1, 0, .9)
            else:
                mainbutton.background_color = (1, 0, 0, .9)
        else:
            pass
            # check_item_factory('Zoom level', self.zoom_level("FOLDOUT"))
            # self.ids._right_drop_layout.add_widget(Label(text="Not available"))

        img_obj.source = thumb_path
        # self.check_agreement()

    def check_agreement(self):
        '''
        try:
            if self.ids._preflight_button_left.background_color ==  self.ids._preflight_button_right.background_color:
                self.ids._label_agreement.text = "OK!"
            else:
                self.ids._label_agreement.text = "NOT OK!"
        except Exception as e:
            pass
        left_tags = [x for x in self.ids._preflight_button_left.children if type(x) == Button ]
        '''
        return

    # parse_gphoto_output
    # _____________________________________________________________________________________
    def parse_gphoto_output(self, output):
        value = ''
        for line in output.split('\n'):
            if line.startswith('Current:'):
                value = line[8:].strip()
                break
        return value

    # retry
    # _____________________________________________________________________________________
    def retry(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.callibration_screen.query_cameras()

    # show_config_errorx
    # _____________________________________________________________________________________
    def show_config_error(self, setting, value, current_val, side):
        msg_box = ScribeMessage()
        msg_box.button_text = 'Retry'
        msg_box.trigger_func = self.retry
        msg_box.text = '\n\n'.join(['The {side}-page camera is not configured correctly.'.format(side=side),
                                    'The setting {s} should be set to {v},\n'
                                    'but it is set to {curr}.'.format(s=setting, v=value, curr=current_val),
                                    'Note that the {side}-page camera is mounted '
                                    'OPPOSITE the page it is capturing'.format(side=side), ])
        popup = Popup(title='Camera Configuration Error', content=msg_box,
                      auto_dismiss=False)
        msg_box.popup = popup
        popup.open()

    # swap_button() - main thread
    # _____________________________________________________________________________________
    def swap_button(self):
        print "swapping!"
        # this needs to tap into the config
        # camera_ports = self.scribe_widget.cameras.camera_ports
        # camera_ports['left'], camera_ports['right'] = camera_ports['right'], camera_ports['left']

        self.scribe_widget.cameras.swap()

        if self.scribe_widget.cameras.num_cameras == 3:
            self.scribe_widget.cameras.set_camera('foldout', ports[2])
        else:
            self.scribe_widget.cameras.set_camera('foldout', None)

        self.left_port = self.scribe_widget.cameras.camera_ports['left']
        self.right_port = self.scribe_widget.cameras.camera_ports['right']
        print "CAPTURING AFTER SWAP WITH L = {0}, R= {1}".format(self.left_port, self.right_port)
        self.clean_temp_images()
        self.capture_images()

    def reshoot_button(self):
        self.clean_temp_images()
        self.capture_images()

    def show_help_popup(self):
        msg_box = ScribeMessageLearnMore()
        # msg_box.ids._learn_button.on_press = self.open_link("https://internetarchivebooks.zendesk.com/hc/en-us/articles/204584071-Calibrating-your-cameras-after-a-hard-reset")
        popup = Popup(title='Calibration help', content=msg_box,
                      auto_dismiss=True, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()

    # done_button() - main thread
    # _____________________________________________________________________________________
    def done_button(self):
        # self.check_camera_config()

        self.clean_temp_images()
        self.screen_manager.transition.direction = 'left'
        print self.screen_manager.screens
        self.screen_manager.current = self.target_screen
        # self.screen_manager.current = 'capture_screen'


    # switch_foldout()
    # _____________________________________________________________________________________
    def switch_foldout(self, spinner):
        text = spinner.text
        camera_ports = self.scribe_widget.cameras.camera_ports
        if text == camera_ports['foldout']:
            return

        if text == camera_ports['right']:
            camera_ports['right'], camera_ports['foldout'] = camera_ports['foldout'], camera_ports['right']
        else:
            camera_ports['left'], camera_ports['foldout'] = camera_ports['foldout'], camera_ports['left']

        self.left_port = camera_ports['left']
        self.right_port = camera_ports['right']

        self.clean_temp_images()
        self.capture_images()



    def exif_tag(self, tag, side):
        return "Unavailable"

    def on_use_tooltips(self, widget, use_tooltips):
        stack = self.children[:]
        while stack:
            widget = stack.pop()
            if isinstance(widget, TooltipControl):
                widget.use_tooltips = use_tooltips
            stack.extend(widget.children)
