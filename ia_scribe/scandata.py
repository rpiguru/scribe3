#!/usr/bin/env python

from collections import OrderedDict
import fcntl
import json
import os
from os.path import join



class ScanData:

    page_num_types = {
        'assert'   : 1,
        'match'    : 0,
        'mismatch' : -1,
    }

    # init()
    #_____________________________________________________________________________________
    def __init__(self, book_dir):
        self.book_dir = book_dir
        self.scandata = self.load()

    # load()
    #_____________________________________________________________________________________
    def load(self):
        #naming follows scandata.xml
        scandata = {
            'bookData': {},
            'pageData': OrderedDict(),
        }

        if self.book_dir is None:
            return scandata

        path = join(self.book_dir, 'scandata.json')
        if not os.path.exists(path):
            return scandata

        try:
            f = open(path)
            #print 'f:', f
            tmp = json.load(f)
            #print 'tmp', tmp
            #load json data into an OrderedDict
            for i in range( len(tmp.get('pageData', {})) ):
                if str(i) in tmp['pageData']:
                    scandata['pageData'][str(i)] = tmp['pageData'][str(i)]

            if 'bookData' in tmp:
                scandata['bookData'] = tmp['bookData']
        except Exception:
            print 'Could not load scandata from ', path
        finally:
            f.close()
        return scandata


    # save()
    #_____________________________________________________________________________________
    def save(self, name='scandata.json'):
        #print 'saving scandata'
        if self.book_dir is None:
            return
        try:
            path = join(self.book_dir, name)
            f = open(path, 'w')
            fcntl.lockf(f, fcntl.LOCK_EX)
            json.dump(self.scandata, f, indent=4, separators=(',', ': '))
            #print json.dumps(self.scandata, f, indent=4, separators=(',', ': '))
            fcntl.lockf(f, fcntl.LOCK_UN)
            f.close()
        except:
            pass
            #print 'Could not save scandata!'
            #print self.scandata
        #print self.dump()


    # backfill()
    #_____________________________________________________________________________________
    def backfill(self, curr_page_num):
        '''If we are adding images to a book that was started before scandata.json
        files were being written, backfill the data using default values.
        '''

        if curr_page_num == 0:
            return

        #note that range is not inclusive, so we add one to the end
        for i in range(curr_page_num+1):
            if i%2 == 0:
                side = 'left'
            else:
                side = 'right'
            if str(i) not in self.scandata['pageData']:
                self.update(i, side)


    # get_page_data()
    #_____________________________________________________________________________________
    def get_page_data(self, leaf_num):
        return self.scandata['pageData'].get(str(leaf_num), {})


    # update()
    #_____________________________________________________________________________________
    def update(self, page_num, side, page_type='Normal'):
        '''actually performs an INSERT or update'''
        degree_for_side = {'left': -90,
                           'right': 90,
                           'foldout': 0,
                          }

        #use scandata.xml naming convention
        degree = degree_for_side.get(side, 0)
        page_data = self.get_page_data(page_num)
        page_data['rotateDegree'] = degree
        if (side == 'foldout'):
            page_data['pageType'] = 'Foldout'
        else:
            page_data['pageType'] = page_type

        #insert:
        self.scandata['pageData'][str(page_num)] = page_data
        #print json.dumps(self.scandata, indent=4, separators=(',', ': '))


    # update_page()
    #_____________________________________________________________________________________
    def update_page(self, leaf_num, page_type, page_assertion):
        valid_page_types = set(['Normal', 'Cover', 'Title', 'Copyright', 'Contents',
                                'Tissue', 'Color Card', 'Chapter', 'White Card', 'Foldout', 
                                 'Abstract', 'Appendix', 'Contributions', 'Delete', 'Foreword', 
                                 'Glossary', 'Index', 'Inscriptions', 'Introduction', 'List of Figures', 
                                 'List of Tables', 'Notes', 'Preface', 'Quotations', 'Reference',
                                 'Subchapter', 'SubSubchapter' ])
        if page_type not in valid_page_types:
            return

        page_data = self.get_page_data(leaf_num)
        page_data['pageType'] = page_type
        self.update_page_assertion(leaf_num, page_assertion)
        #print json.dumps(self.scandata, indent=4, separators=(',', ': '))



    # get_page_assertion()
    #_____________________________________________________________________________________
    def get_page_assertion(self, leaf_num):
        book_data = self.scandata['bookData']
        if 'pageNumData' not in book_data:
            return None

        return book_data['pageNumData'].get(str(leaf_num), None)


    # update_page_assertion()
    #_____________________________________________________________________________________
    def update_page_assertion(self, leaf_num, page_assertion):
        book_data = self.scandata['bookData']
        if 'pageNumData' not in book_data:
            book_data['pageNumData'] = {}

        page_num_data = book_data['pageNumData']
        if page_assertion is not None:
            page_num_data[str(leaf_num)] = page_assertion
        else:
            if str(leaf_num) in page_num_data:
                del page_num_data[str(leaf_num)]


    # update_rotate_degree()
    # _____________________________________________________________________________________
    def update_rotate_degree(self, leaf_number, degree):
        data = self.get_page_data(leaf_number)
        data['rotateDegree'] = degree
        self.scandata['pageData'][str(leaf_number)] = data

    def get_ppi(self, leaf_number):
        data = self.get_page_data(leaf_number)
        return data.get('ppi', None)

    def set_ppi(self, leaf_number, ppi_value):
        data = self.get_page_data(leaf_number)
        data['ppi'] = ppi_value
        self.scandata['pageData'][str(leaf_number)] = data

    # delete_spread()
    #_____________________________________________________________________________________
    def delete_spread(self, left_leaf_num, right_leaf_num, end_leaf_num):
        pagedata = self.scandata['pageData']
        book_data = self.scandata['bookData']
        page_num_data = book_data.get('pageNumData', None)

        if str(left_leaf_num) in pagedata:
            del pagedata[str(left_leaf_num)]
        if str(right_leaf_num) in pagedata:
            del pagedata[str(right_leaf_num)]

        if end_leaf_num == right_leaf_num:
            return

        #fill hole
        for leaf_num in range(right_leaf_num+1, end_leaf_num+1):
            if str(leaf_num) in pagedata:
                pagedata[str(leaf_num-2)] = pagedata[str(leaf_num)]
                del pagedata[str(leaf_num)]

        if page_num_data is None:
            return

        #fix page assertions
        #asserted_leafs is a list of strings, but sorted by int val
        asserted_leafs = sorted(page_num_data, key=lambda x: int(x))
        for asserted_leaf_str in asserted_leafs:
            asserted_leaf_int = int(asserted_leaf_str)
            if asserted_leaf_int >right_leaf_num+1:
                page_num_data[str(asserted_leaf_int-2)] = page_num_data[asserted_leaf_str]
                del page_num_data[asserted_leaf_str]

        #recompute page nums
        self.compute_page_nums(end_leaf_num)


    # get_page_num()
    #_____________________________________________________________________________________
    def get_page_num(self, leaf_num):
        page_data = self.get_page_data(leaf_num)
        return page_data.get('pageNumber', None)


    # add_page_num()
    #_____________________________________________________________________________________
    def add_page_num(self, leaf_num, page_num, page_num_type):
        page_data = self.get_page_data(leaf_num)
        page_data['pageNumber'] = {'num': page_num, 'type': page_num_type}


    # add_page_nums()
    #_____________________________________________________________________________________
    def add_page_nums(self, first_leaf_num, first_page_num, range_end, page_num_type):
        '''range_end is last leaf + 1'''
        page_num = first_page_num
        for leaf_num in range(first_leaf_num, range_end):
            self.add_page_num(leaf_num, page_num, page_num_type)
            page_num += 1


    # clear_page_nums()
    #_____________________________________________________________________________________
    def clear_page_nums(self, first_leaf_num, range_end):
        for leaf_num in range(first_leaf_num, range_end):
            page_data = self.get_page_data(leaf_num)
            if 'pageNumber' in page_data:
                del page_data['pageNumber']


    # compute_page_nums()
    #_____________________________________________________________________________________
    def compute_page_nums(self, end_leaf_num):
        book_data = self.scandata['bookData']
        print "---> Compute page nums --", book_data
        if 'pageNumData' not in book_data:
            return

        page_num_data = book_data['pageNumData']

        for x in page_num_data:
            print x

        asserted_leaf_nums = sorted(page_num_data, key=lambda x: int(x))

        #clear page number for every leaf before the first asserted leaf
        #if there are no asserted leafs, clear all page numbers
        if len(asserted_leaf_nums) == 0:
            clear_leaf_range = end_leaf_num+1
        else:
            clear_leaf_range = int(asserted_leaf_nums[0])
        self.clear_page_nums(0, clear_leaf_range)

        prev_leaf_num = None
        prev_page_num = None
        for leaf_num in asserted_leaf_nums:
            self.add_page_num(leaf_num, page_num_data[leaf_num], 'assert')

            if prev_leaf_num is None:
                prev_leaf_num = leaf_num
                prev_page_num = page_num_data[leaf_num]
                continue

            prev_leaf_int = int(prev_leaf_num)
            leaf_int      = int(leaf_num)
            page_num      = page_num_data[leaf_num]
            if (leaf_int - prev_leaf_int) == (page_num - prev_page_num):
                self.add_page_nums(prev_leaf_int+1, prev_page_num+1, leaf_int, 'match')
            else:
                self.add_page_nums(prev_leaf_int+1, prev_page_num+1, leaf_int, 'mismatch')

            prev_leaf_num = leaf_num
            prev_page_num = page_num_data[leaf_num]

        #from the last assertion, carry page nums forward to the end
        if prev_leaf_num is not None:
            self.add_page_nums(int(prev_leaf_num)+1, int(prev_page_num)+1, end_leaf_num+1, 'match')

    def dump_raw(self):
        return self.scandata
        
    # dump()
    #_____________________________________________________________________________________
    def dump(self):
        return json.dumps(self.scandata, indent=4, separators=(',', ': '))

    def count_pages(self):
        book_data = self.scandata['pageData']
        return int(next(reversed(book_data)))
