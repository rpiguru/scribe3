import sys
from kivy.uix.screenmanager import Screen
from kivy.properties import BooleanProperty, ObjectProperty, NumericProperty, ListProperty, StringProperty, DictProperty
import scribe_config
from kivy.cache import Cache
import os
import glob
from os.path import join, dirname
import re
import subprocess

from ia_scribe.barcode_widget.barcode import BarcodeWidget
from ia_scribe.uix.message import ScribeMessageOKCancel
from scandata import ScanData
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
import xml.etree.ElementTree as ET

Builder.load_file(join(dirname(__file__), 'reshootscreen.kv'))


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

class ReShootScreen(Screen):
    scribe_widget = ObjectProperty(None)
    screen_manager = ObjectProperty(None)
    capture_spread_box = ObjectProperty(None, allownone=True)
    reopen_at = NumericProperty(None)
    reverse_cams = BooleanProperty(False)

    # Workaround for a kivy issue with touch_up events being fired twice, and also
    # the fact that touch_up fires when changing the value when a new spread is scanned
    slider_val = NumericProperty()

    def __init__(self, **kwargs):
        super(ReShootScreen, self).__init__(**kwargs)
        self.page_num=0
        self.book_dir = None
        self.uuid = None
        self.camera_status = {'left': None, 'right': None, 'foldout': None}
        self.page_data = None
        self.editing_metadata = False
        self.page_nums = None
        self.notes = {}

    def get_book_notes(self, book_dir):
        tree_path = join(book_dir, "correction_scandata.xml" )
        pages = [elem for event, elem in ET.iterparse(tree_path) if event == "end" and elem.tag=="page"]
        
        book_notes = {}

        for item in pages:
            if item.find("note") is not None:
                book_notes[item.attrib["leafNum"]] = item.find("note").text

        return book_notes

    def goto_metadata_screen(self, popup):
        self.screen_manager.transition.direction = 'down'
        self.screen_manager.current = 'metadata_screen'
        popup.dismiss()

    def abort_shooting(self):
        msg_box = ScribeMessageOKCancel()
        msg_box.text = 'The configuration is incorrect or incomplete.\nPlease check your metadata.'
        msg_box.ok_text = 'Edit metadata'
        msg_box.button_text = 'Close'
        msg_box.trigger_func  = self.goto_metadata_screen
        popup = Popup(title='Configuration error', content=msg_box,
              auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()
        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = 'upload_screen'

    # on_enter()
    #_____________________________________________________________________________________
    def on_enter(self):
        print 'on_enter, book_dir is ', self.book_dir
        self.config = scribe_config.read()
        #global configuration_ok

        #if we re-entered a book after a camera calibration issue, we may have
        #stale images in the cache
        Cache.remove('kv.image')
        Cache.remove('kv.texture')

        #print "CONFIGURATION OK?   " + str(configuration_ok)
        # If metadata is not set correctly, abort
        #if not configuration_ok:
        #    self.abort_shooting()
        #    return

        
        should_capture_cover = True
        if self.book_dir is None:
            should_capture_cover = True
            #if self.capture_spread_box is not None:
            #    self.show_cover_widget()
            self.page_num = 0
            #self.show_cover_widget()
        else:
            self.uuid = os.path.basename(self.book_dir)
            #TODO: check whether this is affected by  JP2
            jpgs = sorted(glob.glob(join(self.book_dir, '*.jpg')))
            if not jpgs:
                should_capture_cover = True
                self.page_num = 0
                #self.show_cover_widget()
            else:
                should_capture_cover = False
                numbered_jpgs = [x for x in jpgs if re.match('\d+\.jpg$', os.path.basename(x))]
                last_img = int(os.path.basename(numbered_jpgs[-1]).strip('.jpg'))
                if (last_img%2) != 1:
                    #we always need an even number of images, starting with zero
                    last_img += 1
                #if self.reopen_at:
                #    if (self.reopen_at%2) != 1:
                #        self.reopen_at += 1
                #    self.page_num = self.reopen_at
                #else:
                #    self.page_num = last_img
                have_images = True

            #If book_dir is set, init scandata. If not, init scandata in capture_cover.
            self.scandata = ScanData(self.book_dir)
            self.scandata.backfill(self.page_num)
            self.notes = self.get_book_notes(self.book_dir)
            print self.notes


        self.slider_val = -1
        curr_spread = int(self.reopen_at)  #int(self.page_num * .5) 
        print self.reopen_at
        print self.page_num
        print curr_spread
        self.ids._slider.max = curr_spread
        self.ids._slider.value = curr_spread
        self.slider_touch_up(curr_spread)

        self.ids._scan_button.disabled = False
        #Window.bind(on_key_down=self.keyboard_callback)


    # on_leave()
    #_____________________________________________________________________________________
    def on_leave(self):
        self.screen_manager.remove_widget(self)
        pass


    def clean_temp_image(self, image):
        if image is not None and os.path.exists(image):
            print "Unlinking", image
            os.unlink(image)

    def switch_camera(self):
        print "BEFORE CHANGING REVERSE CAMERA IS", self.reverse_cams
        self.reverse_cams = not self.reverse_cams
        print "AFTER CHANGING REVERSE CAMERA IS", self.reverse_cams
        self.capture_spread()

    # capture_cover()
    #_____________________________________________________________________________________
    def capture_cover(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.page_num = 0
        self.book_dir = None
        self.uuid = None

        try:
            self.book_dir, self.uuid = self.create_book_dir()
        except Exception as e:
            self.show_error(e, 'Could not create book dir')
            return

        #add to library
        book = {'path':   self.book_dir,
                'status': UploadStatus.scribing.value,
                'date': os.path.getmtime(self.book_dir),
               }
        upload_screen = self.scribe_widget.ids._upload_screen
        upload_widget = None
        for widget in upload_screen.children:
             if isinstance(widget, UploadWidget):
                upload_widget = widget
                break
        upload_file_list = upload_widget.upload_file_list
        status_label = upload_file_list.add_grid_row(-1, book)
        book['status_label'] = status_label
        upload_widget.num_books += 1

        #init scandata, now that book_dir is set
        self.scandata = ScanData(self.book_dir)

        self.ids._scan_button.disabled = True
        self.camera_status['left']  = None
        self.camera_status['right'] = None

        #capture_cover = self.ids._capture_cover
        capture_cover = self.ids._spread_view.children[0]
        cover_image = capture_cover.ids._cover_image
        stats = None
        callback = self.show_image_callback
        path, thumb_path = self.get_paths(self.page_num)
        self.scribe_widget.left_queue.put(('left', path, thumb_path, callback, None, stats))
        self.scandata.update(self.page_num, 'left', page_type='Color Card')

        self.page_num += 1
        path, thumb_path = self.get_paths(self.page_num)
        self.scribe_widget.right_queue.put(('right', path, thumb_path, callback, cover_image, stats))
        self.scandata.update(self.page_num, 'right', page_type='Cover')

        curr_spread = int(self.page_num * .5)
        self.slider_val = curr_spread
        self.ids._slider.max = curr_spread
        self.ids._slider.value = curr_spread


    # reshoot_cover()
    #_____________________________________________________________________________________
    def shoot_leaf(self, *largs):

        self.ids._scan_button.disabled = True
        self.camera_status['left']  = None
        self.camera_status['right'] = None

        #capture_cover = self.ids._capture_cover

        callback = self.show_image_callback

        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        cover_image.allow_stretch = False
        cover_image.source = resource_path('images/image-loading.gif')

        #self.delete_current_spread()

        path, thumb_path = self.get_paths(0)
        self.scribe_widget.left_queue.put(('left', path, thumb_path, callback, None, stats))

        path, thumb_path = self.get_paths(1)
        self.scribe_widget.right_queue.put(('right', path, thumb_path, callback, cover_image, stats))


    # reshoot_cover_foldout()
    #_____________________________________________________________________________________
    def reshoot_cover_foldout(self, popup, *largs):
        popup.dismiss()
        if self.ids._slider.value != 0:
            print 'reshoot_cover called while not on the cover screen!'
            return

        self.ids._scan_button.disabled = True
        self.camera_status['right'] = None

        #capture_cover = self.ids._capture_cover
        capture_cover = self.ids._spread_view.children[0]
        cover_image = capture_cover.ids._cover_image
        stats = None
        callback = self.show_image_callback

        #TODO: can we use img.reload() instead?
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        cover_image.allow_stretch = False
        cover_image.source = resource_path('images/image-loading.gif')

        path, thumb_path = self.get_paths(1)
        self.scribe_widget.right_queue.put(('foldout', path, thumb_path, callback, cover_image, stats))


    # capture_spread()
    #_____________________________________________________________________________________
    def capture_spread(self, popup=None):
        if popup is not None:
            popup.dismiss()

        if self.capture_spread_box is None:
            self.show_spread_widget()

        loading_image = resource_path('images/image-loading.gif')

        self.ids._scan_button.disabled = True
        self.camera_status['left']  = None
        self.camera_status['right'] = None

        curr_spread = int(self.ids._slider.value)
        left_leaf_num = curr_spread

        page_left = self.capture_spread_box.ids._page_new
        left_stats = self.capture_spread_box.left_stats
        
        page_left.allow_stretch = False
        page_left.source = loading_image
        path, thumb_path = self.get_paths(left_leaf_num) 
        
        
        self.clean_temp_image(thumb_path)
        
        callback = self.show_image_callback

        print "REVERSE CAMERA IS", self.reverse_cams

        if int(left_leaf_num) % 2 == 0:
            if self.reverse_cams is True:
                camera_side = 'right'
            else:
                camera_side = 'left'
        else:
            if self.reverse_cams is True:
                camera_side = 'left'
            else:
                camera_side = 'right'

        self.scribe_widget.left_queue.put((camera_side, path, thumb_path, callback, page_left, left_stats))

        
        ids = self.capture_spread_box.ids
        #self.show_page_num(ids._page_num_left, left_leaf_num)
        

        left_stats['page_type'] = 'Normal'
        #self.save_scandata()

    # _free_disk_space
    #_____________________________________________________________________________________
    def _free_disk_space(self):
        proc = subprocess.Popen(["df ~/scribe_books|tail -1|awk '{print $5}'| sed s/%//"],stdout=subprocess.PIPE,shell=True)
        (out,err) = proc.communicate()
        if (int(out) > 95):
            return 0
        else:
            return 1



    # show_cover_widget()
    #_____________________________________________________________________________________
    def show_cover_widget(self):
        self.capture_spread_box = None
        self.ids._spread_view.clear_widgets()

        capture_cover = CaptureCover()
        capture_cover.capture_screen = self
        self.ids._spread_view.add_widget(capture_cover)

        cover_image = capture_cover.ids._cover_image
        if self.book_dir is not None:
            path, thumb_path = self.get_paths(1)
            if (not os.path.exists(thumb_path)) or (not os.path.exists(path)):
                cover_image.source = resource_path('images/missing.png')
            else:
                cover_image.allow_stretch = True
                cover_image.source = thumb_path
            self.load_metadata()

        if self.config.get('enable_preloaded_metadata', False):
            barcode_widget = BarcodeWidget(self)
            capture_cover.ids._preload_container.add_widget(barcode_widget)


    # show_spread_widget()
    #_____________________________________________________________________________________
    def show_spread_widget(self):
        self.capture_spread_box = ReShootSpread(self)
        self.ids._spread_view.clear_widgets()
        self.ids._spread_view.add_widget(self.capture_spread_box)


    # slider_touch_down()
    #_____________________________________________________________________________________
    def slider_touch_down(self, value):
        self.slider_val = value


    # slider_touch_up()
    #_____________________________________________________________________________________
    def slider_touch_up(self, value):
        if value == self.slider_val:
            return
        value = int(value)
        print 'slider up at ', value

        self.show_spread_widget()

        ids = self.capture_spread_box.ids
        page_left = ids._page_old
        page_right = ids._page_new

        loading_image = resource_path('images/image-loading.gif')
        page_left.allow_stretch = False
        page_right.allow_stretch = False
        page_left.source = loading_image
        page_right.source = loading_image

        # We load the original image in the left box
        left_val  = value
        path, thumb_path = self.get_original_paths(left_val)
        if (not os.path.exists(thumb_path)) and (not os.path.exists(path)):
            page_left.source = resource_path('images/missing.png')
        else:
            page_left.allow_stretch = True
            page_left.source = thumb_path

        # and the new one in the right box
        path, thumb_path = self.get_paths(left_val)
        if (not os.path.exists(thumb_path)) and (not os.path.exists(path)):
            page_right.source = resource_path('images/fake.jpg')
        else:
            page_right.allow_stretch = True
            page_right.source = thumb_path

        left_stats = self.capture_spread_box.left_stats
        left_stats['leaf_num'] = left_val

        print left_stats
        print self.notes

        left_stats['note'] = self.notes[str(left_stats['leaf_num'])]
        #right_stats = self.capture_spread_box.right_stats
        #right_stats['leaf_num'] = right_val

        page_data = self.scandata.get_page_data(left_val)
        left_stats['page_type'] = page_data.get('pageType', 'Normal')
        #page_data = self.scandata.get_page_data(right_val)
        #right_stats['page_type'] = page_data.get('pageType', 'Normal')

        print page_data
        print left_stats

        self.slider_val = value


    # prev_page_button()
    #_____________________________________________________________________________________
    def prev_page_button(self,value):
	if (value > 0):
		self.slider_touch_up(value-1)
        	self.ids._slider.value=value-1
	else:
    	   pass

    # next_page_button()
    #_____________________________________________________________________________________
    def next_page_button(self,value):
	if (value < self.ids._slider.max):
		self.slider_touch_up(value+1)
        	self.ids._slider.value=value+1
	else:
    	   pass

    # show_image_callback() - main thread
    #_____________________________________________________________________________________
    def show_image_callback(self, thumb_path, img_obj, side, err, *largs):
        """
        This function modifies UI elements and needs to be scheduled on the main thread
        """
        print "------ SHOW IMAGE CALLBACK --------"
        print  thumb_path, dir(img_obj), side, err

        if err is None:
            self.camera_status[side] = 1
            if img_obj is not None:
                img_obj.allow_stretch = True
                img_obj.source = thumb_path
                img_obj.reload()
        else:
            self.camera_status[side] = err


        if side != 'foldout':
            if (self.camera_status[side] is None) :
                #both cameras have not yet returned
                print side , "camera have not yet returned"
                self.ids._scan_button.disabled = False
                return


        retry_capture = False
        if (side == 'foldout'):
            if (self.camera_status['foldout'] != 1):
                retry_capture = True
        else:
            if (self.camera_status[side] != 1):
                retry_capture = True
                print "Gonna try recapture"

        if retry_capture:
            self.delete_current_spread()

            msg_box = ScribeErrorMessage2()
            msg_box.button_text = 'Retry image capture'
            if self.capture_spread_box is None:
                msg_box.trigger_func = self.capture_cover
            else:
                msg_box.trigger_func = self.capture_spread
            msg_box.error_msg = 'There was an error during image capture.'
            if side == 'foldout':
                msg_box.error_msg += '\n\nError capturing the FOLDOUT page: {e}'.format(e=self.camera_status['foldout'])
            else:
                if self.camera_status['left'] != 1:
                    msg_box.error_msg += '\n\nError capturing the LEFT page: {e}'.format(e=self.camera_status['left'])
                if self.camera_status['right'] != 1:
                    msg_box.error_msg += '\n\nError capturing the RIGHT page: {e}'.format(e=self.camera_status['right'])

            msg_box.button_text2 = 'Go to Camera Calibration Screen'
            msg_box.trigger_func2 = self.goto_calibration

            popup = Popup(title='Image Capture Error', content=msg_box,
                  auto_dismiss=False)
            msg_box.popup = popup
            popup.open()
        else:
            #self.scandata.save()
            self.ids._scan_button.disabled = False
            if (side == 'foldout') and (self.capture_spread_box is not None):
                self.capture_spread_box.disable_foldout_mode()


    # get_paths()
    #_____________________________________________________________________________________
    def get_paths(self, page_num):
        img = '{n:04d}.jpg'.format(n=page_num)
        print "Target is", img
        path = join(self.book_dir, 'reshooting', img)
        thumb_path = join(self.book_dir, 'reshooting', 'thumbnails' ,img)
        self.make_sure_path_exists(join(self.book_dir, 'reshooting'))
        self.make_sure_path_exists(join(self.book_dir, 'reshooting', 'thumbnails'))
        return path, thumb_path


    def get_original_paths(self, page_num):
        img = '{n:04d}.jpg'.format(n=page_num)
        path = join(self.book_dir, img)
        thumb_path = join(self.book_dir, 'thumbnails', img)
        return path, thumb_path


    def make_sure_path_exists(self, path):
        try:
            os.makedirs(path)
        except OSError as exception:
            print exception
            pass

    # show_error() - main thread
    #_____________________________________________________________________________________
    def show_error(self, e, msg):
        popup = Popup(title='Error', content=Label(text=msg), auto_dismiss=False,
                      size_hint=(None, None), size=(400, 300))
        popup.open()


    # on_focus()
    #_____________________________________________________________________________________
    def on_focus(self, text_widget, focus_value):
        #text_widget = args[0]
        #focus_value = args[1]
        if text_widget.id == '_barcode':
            return
        if focus_value == False:
            print('User defocused', self, text_widget, focus_value, text_widget.cursor)
            if self.slider_val != 0:
                #This text widget was defocused but we are no longer on the cover page
                return
            self.save_metadata()
            text_widget.do_cursor_movement('cursor_home')



    # show_page_attrs()
    #_____________________________________________________________________________________
    def show_page_attrs(self, button, side):
        padding = 10
        triangle_width = 10
        #we are now using Popup, so pos is now relative to the window
        #top = 1 - ((button.y+padding-button.size[1]*.5) / self.size[1])
        top = ((self.y-padding)+self.size[1]) / Window.size[1]

        curr_spread = int(self.ids._slider.value)

        popup_width = 400
        if side == 'left':
            leaf_num  = curr_spread*2
            right = float(button.x+button.width+padding+popup_width) / self.size[0]
        else:
            leaf_num  = curr_spread*2 + 1
            right = float(self.size[0]-button.width-padding) / self.size[0]

        page_data = self.scandata.get_page_data(leaf_num)
        page_type = page_data.get('pageType', 'Normal')
        page_assertion = self.scandata.get_page_assertion(leaf_num)
        #print self.scandata.dump()

        bubble = PageAttributes(leaf_num, page_type, side, page_assertion)
        #bubble.arrow_pos = 'right_top'
        #bubble.pos = [bubble.x, bubble.y]

        #self.capture_screen.add_widget(bubble)
        popup = PopupBubble(side, title='Page Type', content=bubble,
                      size_hint=(None, None), size=(popup_width, 300),
                      pos_hint={'right': right, 'top': top})

        bubble.popup = popup
        bubble.callback = self.set_page_attrs
        popup.bind(on_dismiss=self.dismiss_page_attrs)
        popup.open()
        self.editing_metadata = True


    # dismiss_page_attrs()
    #_____________________________________________________________________________________
    def dismiss_page_attrs(self, value):
        self.editing_metadata = False



    # show_page_num()
    #_____________________________________________________________________________________
    def show_page_num(self, widget, leaf_num):
        page_num = self.scandata.get_page_num(leaf_num)

        if page_num is None:
            widget.text = ''
            widget.set_color('clear')
            return

        colors = {
            'assert': 'green',
            'match': 'gray',
            'mismatch': 'red',
        }
        color = colors.get(page_num['type'], 'gray')
        widget.text = str(page_num['num'])
        widget.set_color(color)


    # load_metadata()
    #_____________________________________________________________________________________
    def load_metadata(self):
        print self.book_dir
        if self.book_dir is None:
            md = {}
        else:
            md = get_metadata(self.book_dir)
        #capture_cover = self.ids._capture_cover
        capture_cover = self.ids._spread_view.children[0]

        for k, v in md.iteritems():
            text_input = capture_cover.get_empty_field(k)
            if text_input is not None:
                text_input.text = v
		text_input.do_cursor_movement('cursor_home')
            else:
                if k != 'collection' and k != 'collection_set':
                    capture_cover.add_metadata_field(k, value=v)

        try:
            collections_list = get_collections_metadata(scribe_globals.config_dir)
            a = self.ids._spread_view.children[0].children[0].children[1].children[0]
            cset = a.children[0]
            # TODO: Is this guaranteed to exist?
            cset.text = md['collection_set']
        except Exception as e:
            print "Failed to retrieve collection set from book's metadata. Perhaps this item was created with an older version of Scribe?"
            print e

        # verify that the collections in the collection set saved in the
        # metadata matches the content of the collections field.
        # if not, ask the user what to trust

        print "Metadata loaded:" + str(md)

        #reset scrollview to top, so title and author are visible
        capture_cover.ids._scrollview.scroll_y = 1


    # save_metadata()
    #_____________________________________________________________________________________
    def save_metadata(self):
        print 'Saving metadata'
        md = {}
        #capture_cover = self.ids._capture_cover
        capture_cover = self.ids._spread_view.children[0]
        # iterate over the metadata fields
        for widget in capture_cover.ids._metadata.children:
            # if they are instances of MTI or widget
            if isinstance(widget, MetadataTextInput):
                #print widget, widget.metadata_key, repr(widget.text)
                if widget.disabled:
                    #preloaded metadata, do not save
                    return
                if widget.text != '':
                    key = widget.metadata_key
                    if key.lower() == 'other':
                        key_input = widget.key_input
                        if key_input.is_valid():
                            key = key_input.text
                        else:
                            print 'Skipping invalid key', key_input.text
                            continue
                    if isinstance(widget.text, str):
                        try:
                            md[key] = widget.text.decode('utf-8')
                        except Exception:
                            print 'could not decode', key, repr(widget.text)
                    else:
                        md[key] = widget.text
        print "Metadata to be saved prior to collection set processing: " + str(md)

        # get the collection set
        a = self.ids._spread_view.children[0].children[0].children[1].children[0]
        cset = a.children[0].text
        # If a collection set has ben set
        if cset is not "" or None:
            # get the metadata configuration
            config = get_collections_metadata(scribe_globals.config_dir)
            # This will be our collections list
            collections = []
            try:
                # extract the selected collection set from the configuration.
                # TODO: handle bad input
                collectionset = filter(lambda collectionset: collectionset['name'] == cset, config['collections']["set"])
                for collection in collectionset[0]["collection"]:
                    collections.append(collection["_text"])
            except Exception as e:
                print e
            md['collection'] =  collections
            md['collection_set'] = cset

        print "calling set-metadata with md = " +str(md)
        set_metadata(md, self.book_dir)


    # disable_metadata()
    #_____________________________________________________________________________________
    def disable_metadata(self):
        capture_cover = self.ids._spread_view.children[0]
        for widget in capture_cover.ids._metadata.children:
            if isinstance(widget, MetadataTextInput):
                widget.disabled = True


    # metadata_is_set()
    #_____________________________________________________________________________________
    def metadata_is_set(self):
        #if identifier is preloaded, then metadata is set
        id_path = join(self.book_dir, 'identifier.txt')
        if os.path.exists(id_path):
            return True

        required_keys = set(('title', 'creator'))
        capture_cover = self.ids._spread_view.children[0]
        widgets = capture_cover.ids._metadata.children
        metadata_widgets = [x for x in widgets if isinstance(x, MetadataTextInput)]
        for widget in metadata_widgets:
            if (widget.metadata_key in required_keys) and (widget.text == ''):
                return False
        # If at this point all metadata is present, verify that a collection set has been picked
        a = self.ids._spread_view.children[0].children[0].children[1].children[0]
        cset = a.children[0].text
        if cset == "":
            return False
        return True


    # show_metadata_key_error()
    #_____________________________________________________________________________________
    def show_metadata_key_error(self):
        '''Returns true if an error message was shown'''
        capture_cover = self.ids._spread_view.children[0]
        widgets = capture_cover.ids._metadata.children
        key_widgets = [x for x in widgets if isinstance(x, KeyInput)]
        for widget in key_widgets:
            if not widget.validate(widget):
                #validate() displays error
                return True
        return False


    # show_metadata_error()
    #_____________________________________________________________________________________
    def show_metadata_error(self):
        popup = Popup(title='Metadata Error', content=Label(text='Title, Creator and Collection set are required'),
                      size_hint=(None, None), size=(600, 200))
        popup.open()


    # keyboard_callback()
    #_____________________________________________________________________________________
    def keyboard_callback(self, window, keycode, key_str, text, modifiers):
        print 'kc:', repr(keycode), 't:', repr(text), 'm:', repr(modifiers)
        if (self.ids._slider.value == 0) or self.editing_metadata:
            #user might be entering metadata
            return False
        elif self.ids._slider.value != self.ids._slider.max:
            #not on last spread, do not allow enter to insert new spread
            #but eat the esc key so the app doesn't quit
            return True
        elif keycode in [13, 271, 32]:
            if not self.ids._scan_button.disabled:
                self.capture_spread()
        return True


    # goto_calibration()
    #_____________________________________________________________________________________
    def goto_calibration(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.screen_manager.transition.direction = 'right'
        self.screen_manager.current = 'calibration_screen'


    def capture_button(self):
        if (self._free_disk_space()):
            #self.shoot_leaf()
            self.capture_spread()
        else:
            content = Button(text='OK')
            popup = Popup(title='Error: the disk is full', content=content,auto_dismiss=False, size_hint=(None, None), size=(230, 100))
            content.bind(on_press=popup.dismiss)
            popup.open()


class ReShootSpread(BoxLayout):
    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    left_stats = DictProperty({'capture_time': '', 'thumb_time': '', 'leaf_num':0, 'page_type':'Normal', 'note': '',})
    right_stats = DictProperty({'capture_time': '', 'thumb_time': '', 'leaf_num':0, 'page_type':'Normal'})
    delete_button_normal  = StringProperty(resource_path('images/button_spread_delete.png'))
    delete_button_foldout = StringProperty(resource_path('images/button_spread_delete_red.png'))

    # __init__()
    #_____________________________________________________________________________________
    def __init__(self, capture_screen, **kwargs):
        super(ReShootSpread, self).__init__(**kwargs)
        self.capture_screen = capture_screen
        self.foldout_mode = False
        self.foldout_button_left = None
        self.foldout_button_right = None


    # delete_or_foldout()
    #_____________________________________________________________________________________
    def delete_or_foldout(self):
        if not self.foldout_mode:
            if self.capture_screen.scribe_widget.camera_ports['foldout'] is None:
                self.delete_spread()
            else:
                print 'Switching to foldout mode'
                self.enable_foldout_mode()
                button = self.ids._delete_button
                button.background_normal = self.delete_button_foldout
                button.background_down = self.delete_button_foldout
        else:
            self.delete_spread()


    # delete_spread()
    #_____________________________________________________________________________________
    def delete_spread(self):
        msg_box = DeleteSpreadMessage(self.capture_screen, self)
        popup = Popup(title='Delete Spread', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()


    # enable_foldout_mode()
    #_____________________________________________________________________________________
    def enable_foldout_mode(self):
        self.foldout_mode = True

        self.foldout_button_left = FoldoutButtonLeft()
        self.foldout_button_left.bind(on_press=partial(self.capture_screen.capture_foldout, 'left'))
        self.ids._imagebox_left.add_widget(self.foldout_button_left)

        self.foldout_button_right = FoldoutButtonRight()
        self.foldout_button_right.bind(on_press=partial(self.capture_screen.capture_foldout, 'right'))
        self.ids._imagebox_right.add_widget(self.foldout_button_right)



    # disable_foldout_mode()
    #_____________________________________________________________________________________
    def disable_foldout_mode(self):
        self.foldout_mode = False
        if self.foldout_button_left is not None:
            self.ids._imagebox_left.remove_widget(self.foldout_button_left)
        if self.foldout_button_right is not None:
            self.ids._imagebox_right.remove_widget(self.foldout_button_right)

        button = self.ids._delete_button
        button.background_normal = self.delete_button_normal
        button.background_down = self.delete_button_normal

   