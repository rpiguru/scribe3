"""

URL: https://archive.org/services/book/v1/do_we_want_it/?isbn=:isbn&debug=:debug

0: TESTISBN00014
1: TESTISBN00012
2: TESTISBN00018
3: TESTISBN00016

"""
import glob
import json
import os

import requests
from internetarchive import get_session
from kivy.adapters.args_converters import list_item_args_converter
from kivy.adapters.listadapter import ListAdapter
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, ListProperty, NumericProperty
from kivy.uix.listview import ListItemButton
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition

import scribe_config
from ia_scribe.uix.metadata import MetadataTextInput
from scribe_globals import ScribeException

cur_dir = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(cur_dir + '/DWWI.kv')


class DWWIDialog(Popup):
    current_title = StringProperty()  # Store title of current screen
    screen_names = ListProperty([])
    screens = {}  # Dict of all screens
    hierarchy = ListProperty([])

    r_popup = ObjectProperty(None)
    si_popup = ObjectProperty(None)
    n_popup = ObjectProperty(None)

    re_code = NumericProperty(-2)
    sm = ObjectProperty(None)
    response = ObjectProperty(None)

    dd_widget = None
    dd_button = None

    capture_screen = ObjectProperty(None)

    inst_capture_cover = ObjectProperty(None)

    ia_session = ObjectProperty(None)

    launched_from = StringProperty('')

    def __init__(self, capture_screen, inst_capture_cover, **kwargs):

        self.capture_screen = capture_screen
        self.inst_capture_cover = inst_capture_cover

        super(DWWIDialog, self).__init__(**kwargs)
        self.r_popup = ResultPopup()
        self.r_popup.set_main_widget(self)
        self.si_popup = SIPopup(self)
        self.n_popup = NotificationPopup()

        self.load_screen()

        try:
            self.get_ia_session()
        except ScribeException as e:
            print e

        self.go_screen('search', 'left')

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob("ia_scribe/dowewantit/screens/*.kv")
        if len(full_path_screens) == 0:
            full_path_screens = glob.glob("screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen

        self.sm = self.ids['sm']
        return True

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        if dest_screen == 'search':
            self.auto_dismiss = True
            self.screens['search'].ids['_identifier'].text = ''
        elif dest_screen == 'book_library':
            self.auto_dismiss = False
            self.update_books()
            # self.caution_popup.open()

        self.sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        if screen.name != self.current_title:
            self.sm.switch_to(screen, direction=direction)
        self.current_title = screen.name
        if dest_screen == 'search':
            self.screens['search'].ids['_identifier'].focus = True

    def search(self, wid):
        """
        Send request to the server and parse response.
        :param wid: Button widget itself
        :return:
        """
        def check_content(msg):
            """
            * Preprocessing function : Check msg is valid ASIN or ISBN.
            :param msg:
            :return:
            """
            # TODO: Add logic to check whether the value has valid type of ASIN/ISBN or not.
            if len(msg) == 0:
                self.n_popup.ids['lb_content'].text = 'Error, please input ASIN/ISBN.'
                self.n_popup.open()
                return False
            elif len(msg) not in [10, 13]:  # length should be 10 or 13
                self.n_popup.ids['lb_content'].text = 'Error, invalid ASIN/ISBN.'
                self.n_popup.open()
                self.screens['search'].ids['_identifier'].text = ''
                return False
            else:
                return True

        _id = self.screens['search'].ids['_identifier'].text

        if not check_content(_id):
            print "Invalid type of ASIN/ISBN number"
            return False

        wid.disabled = True

        api_url = 'https://archive.org/services/book/v1/do_we_want_it/?isbn='

        try:
            self.response = json.loads(requests.get(api_url + _id + '&debug=true').text)
        except requests.exceptions.ConnectionError as e:
            print e
            # TODO: Display Error message
            return False

        if self.response['status'] == 'ok':

            self.re_code = self.response['response']

            if self.re_code != 1:  # remove Dropdown widget which is created in case of response is 1
                if self.dd_button is not None:
                    self.r_popup.ids['gr_dd'].remove_widget(self.dd_button)
                    self.r_popup.ids['lb_dd'].text = ''
                    self.dd_button = None
                    self.dd_widget = None

            # Parse response
            if self.re_code == 0:   # we don't need this book
                self.r_popup.ids['btn_confirm'].text = 'Scan anyway'
                self.display_popup("We do not need to scan this book.")

            elif self.re_code == -1:  # invalid asin or isbn
                self.r_popup.ids['btn_confirm'].text = 'Confirm'
                self.display_popup('Invalid ASIN or ISBN')

            elif self.re_code == 1:   # we need to scan this book
                self.r_popup.ids['btn_confirm'].text = 'Scan this book'

                # # Retrieve metadata from book in ia_identifiers response
                # _id_list = [tmp['ia_identifier'] for tmp in self.response['ia_identifiers']]
                # if len(_id_list) == 0:
                #     self.re_code = -1
                #     self.r_popup.ids['btn_confirm'].text = 'Confirm'
                #     self.display_popup('No Identifier...')
                # else:
                #     # Create dropdown widget and add to result popup
                #     if self.dd_widget is not None:
                #         self.dd_widget.clear_widgets()
                #         self.r_popup.ids['gr_dd'].remove_widget(self.dd_button)
                #     else:
                #         self.dd_widget = DropDown()
                #
                #     for t in _id_list:
                #         btn = Button(text=t, size_hint_y=None, height=30)
                #         btn.bind(on_release=lambda btn: self.dd_widget.select(btn.text))
                #         self.dd_widget.add_widget(btn)
                #
                #     self.dd_button = Button()
                #     self.dd_button.bind(on_release=self.dd_widget.open)
                #     self.dd_widget.bind(on_select=lambda instance, x: setattr(self.dd_button, 'text', x))
                #     self.dd_button.text = _id_list[0]
                #     self.r_popup.ids['gr_dd'].add_widget(self.dd_button)
                #     self.r_popup.ids['lb_dd'].text = 'Identifier'
                #
                #     self.display_popup("We need to scan this book")
                self.display_popup("We need to scan this book")

            # we have already scanned this book, but need another physical copy for a partner
            elif self.re_code == 2:
                self.screens['book_library'].ids['lb_content'].text = "We need another copy for a partner"
                self.go_screen('book_library', 'left')

            # we have already scanned this book, but need another physical copy for a partner
            elif self.re_code == 3:
                self.screens['book_library'].ids['lb_content'].text = "We need another copy for ourselves"
                self.go_screen('book_library', 'left')

        else:
            self.re_code = -2
            self.display_popup("Error with the input string or the service.")

        # Enable new search
        wid.disabled = False

    def get_ia_session(self):

        config = scribe_config.read()

        if 's3' not in config:
            raise ScribeException('You are logged out. Please login from the "Account" options screen.')

        if 'access_key' not in config['s3']:
            raise ScribeException('IAS3 Access Key not in scribe_config.yml')

        if 'secret_key' not in config['s3']:
            raise ScribeException('IAS3 Secret Key not in scribe_config.yml')

        access = config['s3']['access_key']
        secret = config['s3']['secret_key']
        self.ia_session = get_session(config={
            'general': {'secure': False},
            's3': {'access': access, 'secret': secret},
            'cookies': {'logged-in-user': config['email'], 'logged-in-sig': config['cookie']}
        })

    def btn_retry(self):
        """
        Clear DWWI widget and allow new search
        :return:
        """
        self.go_screen('search', 'left')
        self.r_popup.dismiss()

    def btn_confirm(self):
        self.r_popup.dismiss()
        if self.re_code == -1:
            self.dismiss()

        elif self.re_code == 0:
            # Scan anyway
            print "The user decided to scan the book anyway"
            self.dismiss()
            if self.launched_from == 'Upload':
                print 'Opening capture screen.'
                self.capture_screen.screen_manager.transition.direction = 'left'
                self.capture_screen.screen_manager.get_screen('calibration_screen').target_screen = 'capture_screen'
                self.capture_screen.screen_manager.current = 'calibration_screen'

        elif self.re_code == 1:
            """
            Send request to IA and parse response to CaptureCover screen
            """
            print 'The user decided to scan the book after checking "WE NEED TO SCAN THIS BOOK."'
            self.dismiss()
            if self.launched_from == 'Upload':
                print 'Opening capture screen.'
                self.capture_screen.screen_manager.transition.direction = 'left'
                self.capture_screen.screen_manager.get_screen('calibration_screen').target_screen = 'capture_screen'
                self.capture_screen.screen_manager.current = 'calibration_screen'
            # _id = self.dd_button.text
            #
            # metadata = self.ia_session.get_item(_id).item_metadata['metadata']
            # self.inst_capture_cover.pass_metadata(metadata)
            # # for item in metadata['metadata'].items():
            # #     print item
            # self.dismiss()
            # f_name = self.capture_screen.book_dir + '/identifier.txt'
            # try:
            #     open(f_name, 'wb').write(_id)
            #     print "Wrote new identifier value to ", f_name
            # except:
            #     traceback.print_exc()
            #     print 'Failed to write identifier value'

    def display_popup(self, message):
        """
        Display caution popup
        :param message: Content to display
        :return:
        """
        self.r_popup.ids['lb_content'].text = message
        self.r_popup.open()

    def update_books(self):
        """
        Update list view on book library screen
        :return:
        """
        book_list_adapter = ListAdapter(data=self.response['books'], args_converter=list_item_args_converter,
                                        selection_mode='single', propagate_selection_to_data=False,
                                        allow_empty_selection=False, cls=ListItemButton,
                                        on_selection_change=self.on_select_list_item)
        self.screens['book_library'].ids['l_book'].adapter = book_list_adapter
        metadata = self.response['books'][self.response['books'].keys()[0]]['metadata']
        self.update_metadata(metadata)

    def update_metadata(self, metadata):
        """
        Update metadata fields with given new metadata...
        :param metadata:
        :return:
        """
        for widget in self.screens['book_library'].ids['_metadata'].children:
            if widget.__class__.__name__ == 'MetadataTextInput':
                if widget.metadata_key in metadata.keys():
                    # Check value is list or not
                    if type(metadata[widget.metadata_key]) == list:
                        tmp = ','.join(metadata[widget.metadata_key])
                    else:
                        tmp = metadata[widget.metadata_key]
                    if tmp is not None:
                        try:
                            widget.text = tmp.encode('ascii', 'ignore')
                        except AttributeError:      # Some fields are in type of 'bool'
                            widget.text = str(tmp).encode('ascii', 'ignore')

    def on_select_list_item(self, *args):
        """
        Update new metadata when new book is selected in the list view...
        :param args:
        :return:
        """
        # TODO: Confirm this function when there are multiple books in the response...
        cur_txt = self.screens['book_library'].ids['l_book'].adapter.selection[0]
        print cur_txt.text
        metadata = self.response['books'][cur_txt.text]['metadata']
        self.update_metadata(metadata)

    def print_label(self):
        """
        Call a modified version of 'print_barcode' (in BarcodeWidget) that takes the identifier as an argument.
        :return:
        """
        self.si_popup.dismiss()
        self.dismiss()
        _id = self.response['ia_identifiers'][0]['ia_identifier']
        # TODO: get BarcodeWidget instance from self.capture_screen and call that function.
        print 'Printing barcode, id: ', _id

    def show_si_popup(self):
        """
        This function is called when user presses "Send us this book" button after getting response 2 or 3
        :return:
        """
        # TODO: get correct config value from the setting file.
        config = scribe_config.read()
        try:
            b_ignore_donation_items = config['ignore_donation_items']
        except AttributeError:
            b_ignore_donation_items = False
        except KeyError:
            b_ignore_donation_items = False

        if b_ignore_donation_items:
            self.n_popup.ids['lb_content'].text = 'We have already scanned this book.\n' \
                                                  'Please send it to the boxing station'
            self.n_popup.open()
            self.go_screen('search', 'left')
        else:
            adapter = self.screens['book_library'].ids['l_book'].adapter
            self.si_popup.ids['lb_stat'].text = "Input Box ID: "
            self.si_popup.ids['lb_book_id'].text = "Book ID: " + adapter.selection[0].text
            self.si_popup.ids['txt_boxid_2'].text = ''
            self.si_popup.open()

    def update_box_id_2(self, box_id):
        """
        Update boxid_2 value of current book with the value from 'Shipping Instruction' popup.
        :param box_id:
        :return:
        """
        # Get current book item
        cur_txt = self.screens['book_library'].ids['l_book'].adapter.selection[0]
        print "Current identifier: ", cur_txt.text
        # update boxid_2 value
        self.response['books'][cur_txt.text]['metadata']['boxid_2'] = box_id
        self.on_select_list_item()
        # self.dismiss()
        # TODO: Do transition to the main screen.

    # get_val()
    # wrapper module that returns a MetadataTextInput widget's text content
    # _____________________________________________________________________________________
    @staticmethod
    def get_val(widget):
        if isinstance(widget, MetadataTextInput):
            return widget.text
        return None

    # set_val()
    # wrapper module that sets a MetadataTextInput widget's text content
    # _____________________________________________________________________________________
    @staticmethod
    def set_val(widget, val):
        if val is None:
            return
        if isinstance(widget, MetadataTextInput):
            widget.text = val

    # def scan_anyway(self, *args):
    #     """
    #     Return {'DWWI': False} dict to the main widget
    #     :return:
    #     """
    #     self.r_popup.remove_tmp_btn()
    #     self.r_popup.dismiss()
    #     ret_val = {'DWWI': False}
    #     print "The user decided to scan the book anyway"
    #     self.dismiss()


class ResultPopup(Popup):
    main_widget = ObjectProperty(None)

    def set_main_widget(self, wid):
        self.main_widget = wid

    def on_btn_retry(self):
        self.main_widget.btn_retry()

    def on_btn_confirm(self):
        self.main_widget.btn_confirm()

    def on_btn_close(self):
        self.dismiss()
        self.main_widget.dismiss()


class NotificationPopup(Popup):
    pass


class SIPopup(Popup):
    """
    Shipping Instruction Popup
    """
    boxid = StringProperty('')
    inst_dwwi = ObjectProperty(None)

    def __init__(self, dwwi_widget):
        super(Popup, self).__init__()
        # self.ids['lb_stat'].bind(focus=self.on_focus_text)
        self.inst_dwwi = dwwi_widget

    def on_btn_confirm(self):
        """
        :return:
        """
        boxid = self.ids['txt_boxid_2'].text
        if self.check_box_id(boxid):
            print "Box ID is valid: ", boxid
            self.dismiss()
            self.inst_dwwi.update_box_id_2(boxid)
        else:
            print "Invalid Box ID: ", boxid
            self.ids.lb_stat.text = "Invalid Box ID, please try again."
            Clock.schedule_once(self.change_label, 3)

    def check_box_id(self, boxid):
        """
        Check input box_id is valid or not
        :param boxid:
        :return:
        """
        # TODO: implement logic of validating box_id user input.
        if len(boxid.strip()) > 0:
            return True
        else:
            return False

    def change_label(self, *args):
        self.ids['lb_stat'].text = 'Input Box ID: '

    def on_btn_print_label(self):
        self.inst_dwwi.print_label()

