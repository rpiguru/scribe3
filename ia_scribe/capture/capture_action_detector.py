import json

from kivy.event import EventDispatcher

_KEY_ACTIONS = 'actions'
_KEY_BACKEND = 'backend'
_KEY_LAYOUT = 'layout'
_KEY_VERSION = 'version'

ALL_ACTIONS = ['shoot', 'toggle_autoshoot', 'reshoot', 'delete_spread',
               'delete_spread_confirm', 'delete_spread_or_foldout',
               'previous_spread', 'next_spread', 'assert_chapter',
               'assert_normal', 'assert_title', 'assert_copyright',
               'assert_cover', 'assert_contents', 'assert_white_card',
               'assert_foldout', 'assert_color_card', 'show_original_file',
               'go_main_screen', 'go_capture_cover', 'go_last_spread']

A_SHOOT = ALL_ACTIONS[0]
A_TOGGLE_AUTOSHOOT = ALL_ACTIONS[1]
A_RESHOOT = ALL_ACTIONS[2]
A_DELETE_SPREAD = ALL_ACTIONS[3]
A_DELETE_SPREAD_CONFIRM = ALL_ACTIONS[4]
A_DELETE_SPREAD_OR_FOLDOUT = ALL_ACTIONS[5]
A_PREVIOUS_SPREAD = ALL_ACTIONS[6]
A_NEXT_SPREAD = ALL_ACTIONS[7]
A_ASSERT_CHAPTER = ALL_ACTIONS[8]
A_ASSERT_NORMAL = ALL_ACTIONS[9]
A_ASSERT_TITLE = ALL_ACTIONS[10]
A_ASSERT_COPYRIGHT = ALL_ACTIONS[11]
A_ASSERT_COVER = ALL_ACTIONS[12]
A_ASSERT_CONTENTS = ALL_ACTIONS[13]
A_ASSERT_WHITE_CARD = ALL_ACTIONS[14]
A_ASSERT_FOLDOUT = ALL_ACTIONS[15]
A_ASSERT_COLOR_CARD = ALL_ACTIONS[16]
A_SHOW_ORIGINAL_FILE = ALL_ACTIONS[17]
A_GO_MAIN_SCREEN = ALL_ACTIONS[18]
A_GO_CAPTURE_COVER = ALL_ACTIONS[19]
A_GO_LAST_SPREAD = ALL_ACTIONS[20]


class CaptureAction(object):

    def __init__(self, name, scancode, modifiers=None, side=None):
        self.name = name
        self.scancode = scancode
        self.modifiers = modifiers or []
        self.side = side

    def __str__(self):
        return ('<CaptureAction name={!s}, scancode={:d}, '
                'modifiers={!s}, side={!s}>'
                .format(self.name, self.scancode, self.modifiers, self.side))

    def __repr__(self):
        return ('CaptureAction(\'{!s}\', {:d}, {!s}, \'{!s}\')'
                .format(self.name, self.scancode, self.modifiers, self.side))


class CaptureActionDetector(EventDispatcher):

    __events__ = ('on_action',)

    def __init__(self, config_path, auto_init=True):
        self.config_path = config_path
        self._actions = {}
        self._last_action = None
        self._version = None
        self._layout = None
        self._backend = None
        if auto_init:
            self.init()

    @property
    def version(self):
        return self._version

    @property
    def layout(self):
        return self._layout

    @property
    def backend(self):
        return self._backend

    def init(self):
        with open(self.config_path, mode='rb') as fd:
            data = json.load(fd, encoding='utf-8')
        self._layout = data.get(_KEY_LAYOUT, None)
        self._backend = data.get(_KEY_BACKEND, None)
        self._version = data.get(_KEY_VERSION, None)
        self._load_actions(data)
        self.reset()

    def reset(self):
        self._last_action = None

    def load_version(self):
        with open(self.config_path, mode='rb') as fd:
            data = json.load(fd, encoding='utf-8')
        return data.get(_KEY_VERSION, None)

    def _load_actions(self, data):
        actions = self._actions
        data_actions = data[_KEY_ACTIONS]
        valid_action_names = set(ALL_ACTIONS) & set(data_actions.iterkeys())
        for action_name in valid_action_names:
            options = data_actions[action_name]
            for option in options:
                scancode = option.get('scancode', None)
                if scancode:
                    modifiers = option.get('modifiers', [])
                    side = option.get('side', None)
                    action = CaptureAction(action_name, scancode,
                                           modifiers, side)
                    key = (scancode, tuple(modifiers))
                    actions[key] = action

    def on_key_down(self, keycode, scancode, codepoint=None, modifiers=None,
                    **kwargs):
        key = (scancode, tuple(modifiers))
        action = self._actions.get(key, None)
        if action and not self._last_action:
            self._last_action = action
            self.dispatch('on_action', action)
            return True
        return False

    def on_key_up(self, keycode, scancode, codepoint=None, modifiers=None,
                  **kwargs):
        if self._last_action and self._last_action.scancode == scancode:
            self._last_action = None
            return True
        return False

    def on_action(self, action_name):
        pass
