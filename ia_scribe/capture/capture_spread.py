

# CaptureSpread
# _________________________________________________________________________________________
import os
from functools import partial

from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import StringProperty, DictProperty
from kivy.uix.boxlayout import BoxLayout

from ia_scribe.uix.buttons import FoldoutButtonLeft, FoldoutButtonRight
from ia_scribe.uix.messages import DeleteSpreadMessage
from utils.util import resource_path

Builder.load_file(os.path.join(os.path.dirname(__file__), 'capture_spread.kv'))


class CaptureSpread(BoxLayout):

    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    left_stats = DictProperty({'capture_time': '', 'thumb_time': '', 'leaf_num': 0, 'page_type': 'Normal'})
    right_stats = DictProperty({'capture_time': '', 'thumb_time': '', 'leaf_num': 0, 'page_type': 'Normal'})
    delete_button_normal = StringProperty(resource_path('images/button_spread_delete.png'))
    delete_button_foldout = StringProperty(resource_path('images/button_spread_delete_red.png'))

    def __init__(self, capture_screen, **kwargs):
        super(CaptureSpread, self).__init__(**kwargs)
        self.capture_screen = capture_screen
        self.foldout_mode = False
        self.foldout_button_left = None
        self.foldout_button_right = None
        self._popup = None

    def on_left_stats(self, capture_spread, stats):
        panel = self.ids.left_leaf_info_panel
        self._update_leaf_info_panel(panel, stats)

    def on_right_stats(self, capture_spread, stats):
        panel = self.ids.right_leaf_info_panel
        self._update_leaf_info_panel(panel, stats)

    def _update_leaf_info_panel(self, panel, stats):
        panel.leaf_number = self._to_valid_value(stats, 'leaf_num')
        panel.ppi = self._to_valid_value(stats, 'ppi')
        panel.capture_time = self._to_valid_value(stats, 'capture_time')
        panel.thumb_time = self._to_valid_value(stats, 'thumb_time')
        panel.notes = self._to_valid_value(stats, 'notes')

    def _to_valid_value(self, stats, key, not_allowed=(None, '')):
        value = stats.get(key, None)
        return 'NULL' if value in not_allowed else str(value)

    def delete_or_foldout(self):
        if not self.foldout_mode:
            if self.capture_screen.scribe_widget.cameras.camera_ports['foldout'] is None:
                self.delete_spread()
            else:
                print 'Switching to foldout mode'
                self.enable_foldout_mode()
                button = self.ids.spread_menu_bar.delete_button
                button.background_normal = self.delete_button_foldout
                button.background_down = self.delete_button_foldout
        else:
            self.delete_spread()

    # delete_spread()
    # _____________________________________________________________________________________
    def delete_spread(self):
        msg_box = DeleteSpreadMessage(self.capture_screen, self)
        self.capture_screen.disable_capture_actions()
        self._popup = popup = self.capture_screen.create_popup(
            title='Delete Spread', content=msg_box,
            size_hint=(None, None), size=(400, 300)
        )
        popup.bind(on_open=self._on_popup_open,
                   on_dismiss=self._on_popup_dismiss)
        msg_box.popup = popup
        popup.open()

    def _on_popup_open(self, *args):
        Window.bind(on_key_down=self._on_key_down)

    def _on_popup_dismiss(self, *args):
        if self.foldout_mode:
            self.disable_foldout_mode()
        Window.unbind(on_key_down=self._on_key_down)
        self._popup = None

    def _on_key_down(self, window, keycode, scancode, codepoint=None,
                     modifiers=None, **kwargs):
        if keycode == 13 or keycode == 32:
            # Keys "enter" and "spacebar" are used to confirm
            # Key "esc" will dismiss popup by default
            self._popup.content.delete_spread_confirmed()
            return True

    # enable_foldout_mode()
    # _____________________________________________________________________________________
    def enable_foldout_mode(self):
        self.foldout_mode = True
        self.foldout_button_left = FoldoutButtonLeft()
        self.foldout_button_left.bind(on_press=partial(self.capture_screen.capture_foldout, 'left'))
        self.ids['_imagebox_left'].add_widget(self.foldout_button_left)
        self.foldout_button_right = FoldoutButtonRight()
        self.foldout_button_right.bind(on_press=partial(self.capture_screen.capture_foldout, 'right'))
        self.ids['_imagebox_right'].add_widget(self.foldout_button_right)

    def disable_foldout_mode(self):
        self.foldout_mode = False
        if self.foldout_button_left is not None:
            self.ids['_imagebox_left'].remove_widget(self.foldout_button_left)
        if self.foldout_button_right is not None:
            self.ids['_imagebox_right'].remove_widget(self.foldout_button_right)
        button = self.ids.spread_menu_bar.delete_button
        button.background_normal = self.delete_button_normal
        button.background_down = self.delete_button_normal
