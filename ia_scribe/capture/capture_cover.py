# CaptureCover
# this class manages the cover capture dialog
# Main thread
# _________________________________________________________________________________________
import os
from functools import partial

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.screenmanager import NoTransition

import scribe_globals
from ia_scribe.uix.key_input import KeyInput
from ia_scribe.uix.label import BlackLabel
from ia_scribe.uix.metadata import MetadataTextInput
from utils.metadata import get_catalogs_from_metadata, get_collections_metadata
from utils.util import resource_path

Builder.load_file(os.path.join(os.path.dirname(__file__), 'capture_cover.kv'))


class CaptureCover(BoxLayout):
    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    capture_screen = ObjectProperty(None)

    def select_default_collection(self):
        print self

        # self.ids._cset_list.text = self.get_collection_sets()[0]

    def add_marc(self):
        """
        Show MARC Popup window.
        If it is already showed before, move its screen to search screen.
        :return:
        """
        if len(get_catalogs_from_metadata()) == 0:
            self.capture_screen.create_popup(
                title='Error',
                title_size=22,
                content=Label(text='You must have configured catalogs to use MARC Search.', font_size=20),
                size_hint=(.6, .3)
            ).open()
            return False
        self.capture_screen.marc_popup.inst_capture_cover = self
        self.capture_screen.marc_popup.sm.transition = NoTransition()
        self.capture_screen.marc_popup.go_screen('search', 'left')
        self.capture_screen.marc_popup.open()

    def add_dwwi(self):
        self.capture_screen.dwwi_popup.inst_capture_cover = self
        self.capture_screen.dwwi_popup.sm.transition = NoTransition()
        self.capture_screen.dwwi_popup.go_screen('search', 'left')
        self.capture_screen.dwwi_popup.open()

    @staticmethod
    def get_collection_sets():
        try:
            config = get_collections_metadata(scribe_globals.config_dir)
            print "Got collection sets config as ", config
            csets = []
            for cset in config["collections"]['set']:
                csets.append(cset["name"])
            print "Returning these sets", csets
            return csets
        except:
            print "Couldn't load collection sets"
            return []

    # add_metadata()
    # This fuction is invoked by the "Add metadata" Spinner in the cover view
    # _____________________________________________________________________________________
    def add_metadata(self, spinner):
        text = spinner.text
        if text == 'Add Metadata':
            return
        self.add_metadata_field(text)
        spinner.text = 'Add Metadata'

    # add_metadata_field()
    # This fuction is responsible for adding a field to the interface
    # _____________________________________________________________________________________
    def add_metadata_field(self, key, value=None):
        # Declare a new text input
        text_input = MetadataTextInput(size_hint_y=None, height=40)
        text_input.multiline = False
        text_input.hint_text = key.upper()
        text_input.metadata_key = key.lower()
        text_input.font_name = resource_path(scribe_globals.font)
        # Initialize to value if any and bind
        if value is not None:
            if type(value) == list:
                text_input.text = ','.join(value)
            else:
                text_input.text = str(value.encode('utf-8', 'replace'))
        text_input.bind(focus=self.capture_screen.on_focus)
        # if the field is a custom one, add a KeyInput field (defined above)
        if key.lower() == 'other':
            key_input = KeyInput(hint_text='KEY', size_hint=(None, None), size=(75, 40), multiline=False)
            key_input.bind(on_text_validate=key_input.validate)
            key_input.bind(focus=self.capture_screen.on_focus)
            text_input.key_input = key_input
            self.ids['_metadata'].add_widget(key_input)
        # Otherwise, we just need to add a label to the right of the text field
        else:
            label = BlackLabel(text=key.title()+':', size_hint=(None, None),
                               size=(75, 40), valign='middle')
            self.ids['_metadata'].add_widget(label)
        # add and scroll
        self.ids['_metadata'].add_widget(text_input)
        self.ids['_scrollview'].scroll_y = 0

    # get_empty_field()
    # _____________________________________________________________________________________
    def get_empty_field(self, key):
        for widget in self.ids['_metadata'].children:
            if widget.__class__.__name__ == 'MetadataTextInput':
                if widget.metadata_key == key:
                    return widget
        return None

    # reshoot()
    # _____________________________________________________________________________________
    def reshoot(self):
        msg_box = BoxLayout(orientation='vertical', spacing=5)

        label = Label(markup=True, text='In order to reshoot the cover image,\n'
                                        'place the cover of the book in the Scribe\nand then press the RESHOOT button.')
        msg_box.add_widget(label)

        reshoot_button = Button(text='RESHOOT')
        msg_box.add_widget(reshoot_button)

        if self.capture_screen.scribe_widget.cameras.camera_ports['foldout'] is not None:
            reshoot_foldout_button = Button(text='RESHOOT as Foldout')
            msg_box.add_widget(reshoot_foldout_button)

        cancel_button = Button(text='Cancel')
        msg_box.add_widget(cancel_button)

        popup = self.capture_screen.create_popup(
            title='Reshoot cover image?', content=msg_box,
            size_hint=(None, None), size=(400, 300)
        )

        reshoot_button.bind(on_press=partial(self.capture_screen.reshoot_cover, popup))
        cancel_button.bind(on_press=popup.dismiss)
        if self.capture_screen.scribe_widget.cameras.camera_ports['foldout'] is not None:
            reshoot_foldout_button.bind(on_press=partial(self.capture_screen.reshoot_cover_foldout, popup))

        popup.open()

    def pass_metadata(self, metadata):
        """
        Pass metadata
        :param metadata:
        :return:
        """
        # Populate
        for widget in self.ids['_metadata'].children:
            if widget.__class__.__name__ == 'MetadataTextInput':
                if widget.metadata_key in metadata.keys():
                    # Check value is list or not
                    if type(metadata[widget.metadata_key]) == list:
                        tmp = ','.join(metadata[widget.metadata_key])
                    else:
                        tmp = metadata[widget.metadata_key]
                    if tmp is not None:
                        widget.text = tmp.encode('ascii', 'ignore')

    def focus_first_text_input(self, *args):
        preload_children = self.ids['_preload_container'].children
        if preload_children:
            barcode_input = preload_children[-1].barcode_input
            if not barcode_input.disabled:
                barcode_input.focus = True
                return
        first_input = self.ids['_metadata'].children[-2]
        if not first_input.disabled:
            first_input.focus = True

    def defocus_text_inputs(self, *args):
        preload_children = self.ids['_preload_container'].children
        if preload_children:
            barcode_widget = preload_children[-1]
            barcode_widget.barcode_input.focus = False
            return
        first_input = self.ids['_metadata'].children[-2]
        first_input.focus = False

    def on_spinner_is_open(self, spinner, is_open):
        if is_open:
            self.capture_screen.disable_capture_actions()
        else:
            self.capture_screen.enable_capture_actions()
