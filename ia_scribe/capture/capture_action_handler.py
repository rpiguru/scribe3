from kivy.logger import Logger

from ia_scribe.capture.capture_action_detector import *

_ASSERT_ACTIONS = {
    A_ASSERT_CHAPTER, A_ASSERT_NORMAL, A_ASSERT_TITLE, A_ASSERT_COPYRIGHT,
    A_ASSERT_COVER, A_ASSERT_CONTENTS, A_ASSERT_WHITE_CARD, A_ASSERT_FOLDOUT,
    A_ASSERT_COLOR_CARD
}

_ASSERT_ACTIONS_TO_PAGE_TYPE = {
    A_ASSERT_CHAPTER: 'Chapter',
    A_ASSERT_NORMAL: 'Normal',
    A_ASSERT_TITLE: 'Title',
    A_ASSERT_COPYRIGHT: 'Copyright',
    A_ASSERT_COVER: 'Cover',
    A_ASSERT_CONTENTS: 'Contents',
    A_ASSERT_WHITE_CARD: 'White Card',
    A_ASSERT_FOLDOUT: 'Foldout',
    A_ASSERT_COLOR_CARD: 'Color Card'
}


class CaptureActionHandler(object):

    def __init__(self, capture_screen):
        self.capture_screen = capture_screen
        self._handlers = {
            A_SHOOT: self.on_shoot,
            A_TOGGLE_AUTOSHOOT: self.on_toggle_autoshoot,
            A_RESHOOT: self.on_reshoot,
            A_DELETE_SPREAD: self.on_delete_spread,
            A_DELETE_SPREAD_CONFIRM: self.on_delete_spread_confirm,
            A_DELETE_SPREAD_OR_FOLDOUT: self.on_delete_spread_or_foldout,
            A_PREVIOUS_SPREAD: self.on_previous_spread,
            A_NEXT_SPREAD: self.on_next_spread,
            A_SHOW_ORIGINAL_FILE: self.on_show_original_file,
            A_GO_MAIN_SCREEN: self.on_go_main_screen,
            A_GO_CAPTURE_COVER: self.on_go_capture_cover,
            A_GO_LAST_SPREAD: self.on_go_last_spread,
        }

    def on_action(self, detector, action):
        handler = self._handlers.get(action.name, None)
        if not handler and action.name in _ASSERT_ACTIONS:
            handler = self.on_assert_action
        if handler:
            handler(action)
            Logger.debug('CaptureActionHandler: Handled action: {}'
                         .format(action.name))

    def on_shoot(self, action):
        self.capture_screen.capture_button()

    def on_toggle_autoshoot(self, action):
        screen = self.capture_screen
        if screen.is_autoshoot_capture_running():
            screen.stop_autoshoot_capture()
        else:
            screen.start_autoshoot_capture()

    def on_reshoot(self, action):
        screen = self.capture_screen
        if not screen.capture_spread_box:
            capture_cover = screen.ids['_spread_view'].children[0]
            capture_cover.reshoot()

    def on_delete_spread(self, action):
        screen = self.capture_screen
        if screen.capture_spread_box:
            screen.delete_current_spread_and_rename()

    def on_delete_spread_confirm(self, action):
        screen = self.capture_screen
        if screen.capture_spread_box:
            screen.capture_spread_box.delete_spread()

    def on_delete_spread_or_foldout(self, action):
        screen = self.capture_screen
        if screen.capture_spread_box:
            screen.capture_spread_box.delete_or_foldout()

    def on_previous_spread(self, action):
        screen = self.capture_screen
        screen.prev_page_button(screen.spread_slider_bar.slider.value)

    def on_next_spread(self, action):
        screen = self.capture_screen
        screen.next_page_button(screen.spread_slider_bar.slider.value)

    def on_assert_action(self, action):
        screen = self.capture_screen
        sides = screen.get_displayed_sides()
        page_side = screen.adjust_side(action.side)
        page_number = sides[page_side]
        page_type = _ASSERT_ACTIONS_TO_PAGE_TYPE[action.name]
        page_assertion = screen.scandata.get_page_assertion(page_number)
        screen.set_page_attrs(page_number, page_type, page_side, page_assertion)

    def on_show_original_file(self, action):
        self.capture_screen.show_original_file(None, action.side)

    def on_go_main_screen(self, action):
        screen_manager = self.capture_screen.screen_manager
        screen_manager.transition.direction = 'right'
        screen_manager.current = 'upload_screen'

    def on_go_capture_cover(self, action):
        self.capture_screen.show_cover_widget()

    def on_go_last_spread(self, action):
        slider = self.capture_screen.spread_slider_bar.slider
        self.capture_screen.slider_touch_down(slider.value)
        self.capture_screen.slider_touch_up(slider.max)
        slider.value = slider.max
