# CaptureScreen
# _________________________________________________________________________________________
import datetime
import glob
import os
import re
import shutil
import subprocess
import uuid
import webbrowser
from collections import OrderedDict
from functools import partial
from os.path import join

import ia_scribe.capture.capture_action_detector as capture_action
from PIL import Image
from ia_scribe.capture.capture_action_handler import CaptureActionHandler
from ia_scribe.capture.capture_cover import CaptureCover
from kivy.cache import Cache
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.metrics import dp
from kivy.properties import NumericProperty
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen

import scribe_config
import scribe_globals
from ia_scribe.barcode_widget.barcode import BarcodeWidget
from ia_scribe.capture.capture_spread import CaptureSpread
from ia_scribe.dowewantit.DWWI import DWWIDialog
from ia_scribe.marc.MARC import MARCDialog
from ia_scribe.scandata import ScanData
from ia_scribe.uix.capture_leaf import CaptureLeaf
from ia_scribe.uix.file_chooser import FileChooser
from ia_scribe.uix.key_input import KeyInput
from ia_scribe.uix.message import ScribeMessageOKCancel, ScribeMessage, BookExport, ConfirmUploadMessage, \
    ScribeErrorMessage2
from ia_scribe.uix.metadata import MetadataTextInput
from ia_scribe.uix.page_attributes import PageAttributes
from ia_scribe.uix.popup_bubble import PopupBubble
from ia_scribe.uix.popups import EditPPIPopup
from ia_scribe.uix.text_input import TextInput
from ia_scribe.upload.upload import UploadWidget
from ia_scribe.upload.upload_status import UploadStatus
from ttsservices.iabdash import push_event
from utils.metadata import get_metadata, get_sc_metadata, get_collections_metadata, set_metadata
from utils.util import resource_path

Builder.load_file(os.path.join(os.path.dirname(__file__), 'capture_screen.kv'))


configuration_ok = True


class CaptureScreen(Screen):
    scribe_widget = ObjectProperty(None)
    screen_manager = ObjectProperty(None)
    book_menu_bar = ObjectProperty()
    spread_slider_bar = ObjectProperty()
    capture_spread_box = ObjectProperty(None, allownone=True)
    reopen_at = NumericProperty(None)
    global_time_open = NumericProperty(None)
    # downloaded_book = BooleanProperty(None)

    # Workaround for a kivy issue with touch_up events being fired twice, and also
    # the fact that touch_up fires when changing the value when a new spread is scanned
    slider_val = NumericProperty()

    def __init__(self, **kwargs):
        super(CaptureScreen, self).__init__(**kwargs)
        self.page_num=0
        self.book_dir = None
        self.status_label = None
        self.uuid = None
        self.camera_status = {'left': None, 'right': None, 'foldout': None}
        self.cameras_count = 0
        self.page_data = None
        self.editing_metadata = False
        self.page_nums = None
        self.downloaded_book = None
        self.global_time_open = 0
        self.scandata = None
        self.action_detector = capture_action.CaptureActionDetector(scribe_globals.capture_action_bindings)
        self.action_handler = CaptureActionHandler(self)
        self._autoshoot_running = False
        self._safe_for_autoshoot = True
        self._capture_actions_enabled = False
        self._book_export = None
        self.marc_popup = MARCDialog(capture_screen=self, inst_capture_cover=None)
        self.marc_popup.bind(on_open=self.disable_capture_actions,
                             on_dismiss=self.enable_capture_actions)
        self.dwwi_popup = DWWIDialog(capture_screen=self, inst_capture_cover=None)
        self.dwwi_popup.bind(on_open=self.disable_capture_actions,
                             on_dismiss=self.enable_capture_actions)
        self.popup = None

    def on_slider_val(self, screen, value):
        if self.capture_spread_box:
            self._update_spread_menu_bar_page_type()

    def on_spread_slider_bar(self, screen, bar):
        bar.bind(on_option_select=self.on_spread_slider_bar_option_select,
                 on_value_down=self.on_spread_slider_bar_value_down,
                 on_value_up=self.on_spread_slider_bar_value_up,
                 on_autoshoot_toggle=self.on_spread_slider_bar_autoshoot_toogle,
                 on_knob_pos=self.on_spread_slider_bar_knob_pos)
        # TODO: Find better way to bind/unbind keyboard
        autoshoot_input = bar.ids.autoshoot_input
        autoshoot_input.bind(focus=self._on_autoshoot_input_focus)

    def on_spread_slider_bar_value_down(self, bar, value):
        self.slider_touch_down(value)

    def on_spread_slider_bar_value_up(self, bar, value):
        self.slider_touch_up(value)

    def on_spread_slider_bar_autoshoot_toogle(self, bar, value):
        if value == 'down':
            self.start_autoshoot_capture()
        elif value == 'normal':
            self.stop_autoshoot_capture()

    def on_spread_slider_bar_knob_pos(self, bar, pos):
        scandata = self.scandata
        if scandata:
            sides = self.get_displayed_sides()
            if len(sides) > 1:
                left_page = sides['left']
                right_page = sides['right']
                data = scandata.get_page_num(left_page)
                bar.tooltip_mode = 'dual'
                bar.left_page_number = self._get_page_number(data)
                data = scandata.get_page_num(right_page)
                bar.right_page_number = self._get_page_number(data)
            else:
                page_number = sides['foldout']
                data = scandata.get_page_num(page_number)
                asserted_page_number = self._get_page_number(data)
                bar.tooltip_mode = 'single'
                bar.asserted_page_number = asserted_page_number
        else:
            bar.left_page_number = bar.right_page_number = None
            bar.asserted_page_number = None

    def _get_page_number(self, page_number_data):
        # TODO: Remove this method when scandata structure becomes the same
        # for reshooting mode and otherwise
        if page_number_data:
            if isinstance(page_number_data, dict):
                return page_number_data.get('num', None)
            elif isinstance(page_number_data, basestring):
                return int(page_number_data)
        return None

    def is_autoshoot_capture_running(self):
        return self._autoshoot_running

    def start_autoshoot_capture(self, *args):
        if not self._autoshoot_running:
            self._autoshoot_running = True
            self._safe_for_autoshoot = True
            Clock.unschedule(self.auto_capture_spread)
            Clock.schedule_interval(self.auto_capture_spread,
                                    self.spread_slider_bar.ids.autoshoot_input.value)
            self.spread_slider_bar.ids.autoshoot_toggle_button.state = 'down'
            Logger.info('CaptureScreen: Autoshoot started')

    def stop_autoshoot_capture(self, *args):
        if self._autoshoot_running:
            self._autoshoot_running = False
            self._safe_for_autoshoot = False
            Clock.unschedule(self.auto_capture_spread)
            self.spread_slider_bar.ids.autoshoot_toggle_button.state = 'normal'
            Logger.info('CaptureScreen: Autoshoot stopped')

    def auto_capture_spread(self, *args):
        if self._safe_for_autoshoot:
            self.capture_button()
            self._safe_for_autoshoot = False

    def enable_capture_actions(self, *args):
        if not self._capture_actions_enabled:
            Window.bind(on_key_down=self.on_key_down)
            Window.bind(on_key_up=self.on_key_up)
            self.action_detector.reset()
            self.action_detector.bind(on_action=self.action_handler.on_action)
            self._capture_actions_enabled = True
            Logger.info('CaptureScreen: Enabled keyboard capture actions')

    def disable_capture_actions(self, *args):
        if self._capture_actions_enabled:
            Window.unbind(on_key_down=self.on_key_down)
            Window.unbind(on_key_up=self.on_key_up)
            self.action_detector.unbind(on_action=self.action_handler.on_action)
            self._capture_actions_enabled = False
            Logger.info('CaptureScreen: Disabled keyboard capture actions')

    def on_key_down(self, window, keycode, scancode=None, codepoint=None,
                    modifiers=None, **kwargs):
        self.action_detector.on_key_down(keycode, scancode, codepoint,
                                         modifiers, **kwargs)
        return True

    def on_key_up(self, window, keycode, scancode=None, codepoint=None,
                  modifiers=None, **kwargs):
        self.action_detector.on_key_up(keycode, scancode, codepoint,
                                       modifiers, **kwargs)
        return True

    def on_spread_slider_bar_option_select(self, bar, option):
        if option == 'previous_spread':
            self.prev_page_button(bar.slider.value)
        elif option == 'next_spread':
            self.next_page_button(bar.slider.value)
        elif option == 'capture_spread':
            self.capture_button()

    def _on_autoshoot_input_focus(self, autoshoot_input, focus):
        if focus:
            self.disable_capture_actions()
        else:
            self.enable_capture_actions()

    def _update_spread_menu_bar_page_type(self, *args):
        sides = self.get_displayed_sides()
        for side in sides:
            page_number = self.compute_page_number(side, self.slider_val)
            page_data = self.scandata.get_page_data(page_number)
            button = self.get_page_type_button(side)
            button.text = page_data.get('pageType', 'Normal')

    def goto_metadata_screen(self, popup):
        self.screen_manager.transition.direction = 'down'
        self.screen_manager.current = 'metadata_screen'
        popup.dismiss()

    def abort_shooting(self):
        msg_box = ScribeMessageOKCancel()
        msg_box.text = 'The configuration is incorrect or incomplete.\nPlease check your metadata.'
        msg_box.ok_text = 'Edit metadata'
        msg_box.button_text = 'Close'
        msg_box.trigger_func = self.goto_metadata_screen
        popup = Popup(title='Configuration error', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()
        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = 'upload_screen'

    def on_pre_leave(self, *args):
        self.stop_autoshoot_capture()
        self.disable_capture_actions()

    # on_enter()
    # _____________________________________________________________________________________
    def on_enter(self):
        Logger.debug('CaptureScreen: on_enter, book_dir is "{}"'
                     .format(self.book_dir))
        id_file = ''
        try:
            id_file = os.path.join(self.book_dir, 'identifier.txt')
        except:
            pass
        self.config = scribe_config.read()
        global configuration_ok
        self.downloaded_book = False
        # If we re-entered a book after a camera calibration issue, we may have
        # stale images in the cache
        Cache.remove('kv.image')
        Cache.remove('kv.texture')

        Logger.debug('CaptureScreen: Configuration ok: {}'.format(str(configuration_ok)))
        # If metadata is not set correctly, abort
        if not configuration_ok:
            self.abort_shooting()
            return
        if os.path.exists(id_file):
            try:
                identifier = open(id_file).read().strip()
                Logger.debug('CaptureScreen: Read id: {}'.format(identifier))
                payload = {'id': identifier, 'leafNum': self.page_num}
                push_event('tts-book-open', payload, 'book', identifier)
            except Exception:
                Logger.exception('CaptureScreen: Failed to push tts-book-open '
                                 'event with payload: {}'.format(payload))

        self.time_open = datetime.datetime.now()

        # check if there is a time-tracking file, and if so load its value
        self.global_time_open = self.get_time_tracking()

        try:
            '''
            md = get_metadata(self.book_dir)
            should_capture_cover = False
            print "Is the book being rescribed?", md['repub_state']
            #print md['repub_state'] > 13
            #print md['repub_state'] < 20
            #print md['repub_state'] > 13 and md['repub_state'] < 20
            if md['repub_state'] > 13: # and md['repub_state'] < 20:
                print "it is, don't reshoot cover and assume everything is fine"
                should_capture_cover = False
                downloaded_book = True
                self.scandata = ScanData(self.book_dir)
                self.page_num = self.scandata.count_pages()
                self.reopen_at = self.page_num
                self.scandata.backfill(self.page_num)
            else:
                print "repub state is < 13! skipping..."
                pass
            '''
            if os.path.exists(os.path.join(self.book_dir, 'downloaded')):
                self.downloaded_book = True
                Logger.debug('CaptureScreen: Downloaded book: "{}"'
                             .format(self.book_dir))
        except Exception:
            Logger.exception('CaptureScreen: Couldn\'t get metadata, original '
                             'book: "{}"'.format(self.book_dir))
        self.cameras_count = self.scribe_widget.cameras.num_cameras()
        single_camera = self.cameras_count == 1
        if self.book_dir is None:
            should_capture_cover = True
            # if self.capture_spread_box is not None:
            #    self.show_cover_widget()
            self.page_num = 0
            # self.show_cover_widget()
        else:
            self.uuid = os.path.basename(self.book_dir)
            # TODO: check whether this is affected by  JP2
            # CHANGED THIS TO READ FROM THUMBS. CAREFUL.
            jpgs = sorted(glob.glob(join(self.book_dir, 'thumbnails', '*.jpg')))
            if not jpgs and  self.downloaded_book is False:
                should_capture_cover = True
                self.page_num = 0
                # self.show_cover_widget()
            else:
                should_capture_cover = False
                numbered_jpgs = [x for x in jpgs if re.match('\d+\.jpg$', os.path.basename(x))]
                if numbered_jpgs:
                    last_img = int(os.path.basename(numbered_jpgs[-1]).strip('.jpg'))
                else:
                    last_img = 0
                if not single_camera:
                    if (last_img%2) != 1:
                        # we always need an even number of images, starting with zero
                        last_img += 1
                if self.reopen_at:
                    if not single_camera:
                        if (self.reopen_at % 2) != 1:
                            self.reopen_at += 1
                    self.page_num = self.reopen_at
                else:
                    self.page_num = last_img

        # If book_dir is set, init scandata. If not, init scandata in capture_cover.
        self.scandata = ScanData(self.book_dir)
        self.scandata.backfill(self.page_num)
        self.enable_capture_actions()
        self.slider_val = -1
        slider_value = int(self.page_num)
        slider_min_value = 1
        if not single_camera:
            slider_value = int(self.page_num * 0.5)
            slider_min_value = 0
        self.spread_slider_bar.slider.min = slider_min_value
        self.spread_slider_bar.slider.max = slider_value
        self.spread_slider_bar.slider.value = slider_value
        self.slider_touch_up(slider_value)
        self.spread_slider_bar.scan_button.disabled = False
        self._update_book_menu_bar()
        self.book_menu_bar.attach_tooltip_to = self
        self.book_menu_bar.use_tooltips = True
        self.spread_slider_bar.attach_tooltip_to = self
        self.spread_slider_bar.use_tooltips = True
        if should_capture_cover:
            capture_cover = self.ids['_spread_view'].children[0]
            capture_cover.defocus_text_inputs()
            self.disable_capture_actions()
            msg_box = ScribeMessage()
            msg_box.text = 'Place the cover of the book\non the right side of the Scribe.'
            msg_box.button_text = 'Shoot Cover'
            msg_box.trigger_func = self.capture_cover
            self.capture_cover_popup = self.create_popup(
                title='Load Cover', content=msg_box, auto_dismiss=False,
                size_hint=(None, None), size=(400, 300)
            )
            self.capture_cover_popup.bind(on_open=self._on_popup_open, on_dismiss=self._on_popup_dismiss)
            msg_box.popup = self.capture_cover_popup
            self.capture_cover_popup.open()

    def _on_popup_open(self, *args):
        Window.bind(on_key_down=self._on_key_down)

    def _on_popup_dismiss(self, *args):
        Window.unbind(on_key_down=self._on_key_down)
        self.capture_cover_popup = None
        capture_cover = self.ids['_spread_view'].children[0]
        Clock.schedule_once(capture_cover.focus_first_text_input, 0.1)

    def _on_key_down(self, window, keycode, scancode, codepoint=None,
                     modifiers=None, **kwargs):
        if keycode == 13 or keycode == 32:
            # Keys "enter" and "spacebar" are used to confirm
            self.capture_cover_popup.dismiss()
            self.capture_cover()
            return True

    def _update_book_menu_bar(self, *args):
        menu = self.book_menu_bar
        book_dir = self.book_dir
        if book_dir:
            if os.path.exists(join(book_dir, 'identifier.txt')):
                with open(os.path.join(book_dir, 'identifier.txt')) as fd:
                    menu_identifier = fd.read().strip()
            else:
                menu_identifier = u''
            metadata = get_metadata(book_dir)
            title = metadata.get('title', None) or 'NULL'
            author = metadata.get('creator', None) or metadata.get('author', None) or 'NULL'
            language = metadata.get('language', None) or 'NULL'
            menu_title = u'{}, {} ({})'.format(title, author, language)
        else:
            menu_title = u''
            menu_identifier = u''
        menu.identifier = menu_identifier
        menu.title = menu_title

    def on_book_menu_bar(self, screen, bar):
        bar.bind(on_option_select=self.on_book_menu_bar_option_select)

    def on_book_menu_bar_option_select(self, bar, option):
        if option == 'edit':
            self.show_cover_widget()
        elif option == 'delete':
            self.show_delete_book_popup()
        elif option == 'export':
            self.start_book_export()
        elif option == 'notes':
            self.show_notes_popup()
        elif option == 'upload':
            self.show_upload_book_popup()

    def show_delete_book_popup(self, *args):
        msg_box = ScribeMessageOKCancel()
        msg_box.text = (
            'Do you want to DELETE this book? You cannot undo this action. \n'
            'The images that you have captured will be removed permenantly.'
        )
        msg_box.ok_text = 'DELETE BOOK'
        msg_box.ids['_ok_button'].background_color = [1, 0, 0, 1]
        msg_box.trigger_func = self.confirm_delete
        popup = self.create_popup(
            title='Delete book?', content=msg_box, auto_dismiss=False,
            size_hint=(None, None), size=(600, 300)
        )
        msg_box.popup = popup
        popup.open()

    def confirm_delete(self, popup):
        print 'writing ', join(self.book_dir, 'delete_queued')
        open(join(self.book_dir, 'delete_queued'), 'a').close()
        popup.dismiss()
        self.status_label.text = 'Delete Queued'
        self.screen_manager.current = 'upload_screen'

    def start_book_export(self, *args):
        self.disable_capture_actions()
        self._book_export = export = BookExport(self.book_dir)
        export.bind(on_finish=self.on_book_export_finish)
        export.start()

    def on_book_export_finish(self, *args):
        self._book_export = None
        self.enable_capture_actions()

    def show_notes_popup(self, *args):
        content = BoxLayout(orientation='vertical')
        buttons = BoxLayout(size_hint_y=None, height=30)
        buttons.add_widget(Button(text='Save'))
        buttons.add_widget(Button(text='Cancel'))
        content.add_widget(TextInput())
        content.add_widget(buttons)
        popup = self.create_popup(
            title='Notes', content=content,
            size_hint=(None, None), size=(400, 200)
        )
        for button in buttons.children:
            button.bind(on_release=popup.dismiss)
        popup.open()

    def show_upload_book_popup(self, *args):
        self.disable_capture_actions()
        # check for missing images
        err = None
        jpgs = sorted(glob.glob(join(self.book_dir, '*.jpg')))
        numbered_jpgs = [os.path.basename(x) for x in jpgs if re.match('\d+\.jpg$', os.path.basename(x))]
        first_img = int(os.path.basename(numbered_jpgs[0]).strip('.jpg'))
        last_img = int(os.path.basename(numbered_jpgs[-1]).strip('.jpg'))

        if (first_img != 0) and (first_img != 1):
            # TODO: do not worry about image 0 for now since we never show it to the user,
            # but we do want the user to capture a color card there
            err = 'Cover image is missing!'
        elif (last_img%2) != 1:
            # image missing from the end
            err = 'Image #{n} is missing!'.format(n=last_img+1)
        else:
            if (last_img-first_img+1) != len(numbered_jpgs):
                # TODO: do not worry about image for now 0 since we never show it to the user,
                # but we do want the user to capture a color card there
                err = 'An image is missing'
                s = set(numbered_jpgs)
                for i in range (1, last_img+1):
                    jpg = '{n:04d}.jpg'.format(n=i)
                    if jpg not in s:
                        err = 'Image {n} is missing!'.format(n=jpg)
                        break

        if err is not None:
            text = err + '\n\nYou must reopen this book and reshoot\nthe missing image before uploading!'
            msg = Label(text=text)
            popup = self.create_popup(
                title='Missing Image!', content=msg,
                size_hint=(None, None), size=(400, 300)
            )
            popup.open()
            return

        # no missing images
        msg_box = ConfirmUploadMessage(self.book_dir, self.status_label)
        popup = Popup(title='Confirm Upload', content=msg_box,
                      size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.bind(on_dismiss=self._on_upload_popup_dismiss)
        popup.open()

    def _on_upload_popup_dismiss(self, popup):
        if os.path.exists(join(self.book_dir, 'upload_queued')):
            self.screen_manager.current = 'upload_screen'
        else:
            self.enable_capture_actions()

    def set_time_tracking(self):
        time_session = (datetime.datetime.now() - self.time_open).total_seconds()

        if self.global_time_open:
            self.global_time_open += time_session
        else:
            print "no time-tracking info for this book. Adding this session..."
            self.global_time_open = time_session

        with open(os.path.join(self.book_dir, 'time.log'), 'w+') as file:
            print "Now writing time tracking info to", file
            file.write(str("{0:.2f}".format(self.global_time_open)))

        return self.global_time_open

    def get_time_tracking(self):
        # check if there is a time-tracking file, and if so load its value
        print "check if there is a time-tracking file...",
        try:
            if os.path.exists(os.path.join(self.book_dir, 'time.log')):
                with open(os.path.join(self.book_dir, 'time.log'), 'r') as file:
                    global_time_open = float(file.readline())
                    if global_time_open:
                        return global_time_open
        except:
            pass

        print "no,  0"
        return 0

    # on_leave()
    # _____________________________________________________________________________________
    def on_leave(self):
        print "saving to iabdash"
        # USe timedelta
        id_file = os.path.join(self.book_dir, 'identifier.txt')
        identifier = None
        time_session = (datetime.datetime.now() - self.time_open).total_seconds()
        print "GTO", self.global_time_open
        if self.global_time_open:
            self.global_time_open += time_session
            print "GTO2", self.global_time_open
        else:
            print "no time-tacking info for this book. Adding this session..."
            self.global_time_open = time_session
            print "Added", time_session

        if os.path.exists(os.path.join(self.book_dir, 'time.log')):
            pass
        else:
            pass

        with open(os.path.join(self.book_dir, 'time.log'), 'w+') as file:
            print "writing to file"

            file.write(str("{0:.2f}".format(self.global_time_open)))

        if os.path.exists(id_file):
            try:
                identifier = open(id_file).read().strip()
                print "Read id", identifier
            except:
                pass

        if identifier and self.page_num:
            print "pushing to iabdash"
            payload = {'activeTime': self.global_time_open, 'sessionTime': time_session, 'numLeaf': self.page_num or ''}
            try:
                push_event('tts-book-update', payload, 'book', identifier)
            except Exception:
                # print "Coudln't log the {0} seconds spent scanning this book.".format(time_book)
                pass
        self.book_dir = None
        self.status_label = None
        self.disable_capture_actions()
        self.book_menu_bar.use_tooltips = False
        self.spread_slider_bar.use_tooltips = False

    # capture_cover()
    # _____________________________________________________________________________________
    def capture_cover(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.page_num = 0
        self.book_dir = None
        self.uuid = None

        try:
            self.book_dir, self.uuid = self.create_book_dir()
        except Exception as e:
            Logger.exception('CaptureScreen: Failed to create book directory')
            self.show_error(e, 'Could not create book dir')
            return
        Logger.debug('CaptureScreen: Created book directory: "{}"'
                     .format(self.book_dir))

        # Add to library
        book = {'path': self.book_dir,
                'status': UploadStatus.scribing.value,
                'date': os.path.getmtime(self.book_dir)
                }
        upload_screen = self.scribe_widget.ids['_upload_screen']
        upload_widget = None
        for widget in upload_screen.children:
            if isinstance(widget, UploadWidget):
                upload_widget = widget
                break
        upload_file_list = upload_widget.upload_file_list
        status_label = upload_file_list.add_grid_row(-1, book)
        book['status_label'] = status_label
        self.status_label = status_label
        upload_widget.num_books += 1

        # Init scandata, now that book_dir is set
        self.scandata = ScanData(self.book_dir)

        self.spread_slider_bar.scan_button.disabled = True
        sides = self.get_displayed_sides()
        for side in sides:
            self.camera_status[side] = None

        # capture_cover = self.ids._capture_cover
        capture_cover = self.ids['_spread_view'].children[0]

        try:
            if self.config.get('prefill_language', True):
                Logger.debug('CaptureScreen: User indicated language should '
                             'be filled from global metadata')
                sc_md = get_sc_metadata()
                if 'language' in sc_md:
                    Logger.debug('CaptureScreen: Found language in global '
                                 'metadata: {}'.format(sc_md['language']))
                    lang = [x for x in capture_cover.ids['_metadata'].children if
                            type(x) == MetadataTextInput and x.metadata_key == 'language']
                    if lang[0].text == "": lang[0].text = sc_md['language']
                    Logger.debug('CaptureScreen: Set language field to: {}'
                                 .format(lang[0].text))
                else:
                    Logger.debug('CaptureScreen: No language found in global '
                                 'metadata')
        except Exception:
            Logger.exception('CaptureScreen: Failed to set language field in '
                             'metadata form')

        left_side = self.adjust_side('left')
        right_side = self.adjust_side('right')
        cover_image = capture_cover.ids['_cover_image']
        stats = None
        callback = self.show_image_callback
        path, thumb_path = self.get_paths(self.page_num)
        queue = self.get_capture_queue(left_side)
        queue.put((left_side, path, thumb_path, callback, None, stats))
        self.scandata.update(self.page_num, left_side, page_type='Color Card')

        self.page_num += 1
        path, thumb_path = self.get_paths(self.page_num)
        queue = self.get_capture_queue(right_side)
        queue.put((right_side, path, thumb_path, callback, cover_image, stats))
        self.scandata.update(self.page_num, right_side, page_type='Cover')

        slider_value = int(self.page_num * 1.0 / len(sides))
        self.slider_val = slider_value
        self.spread_slider_bar.slider.max = slider_value
        self.spread_slider_bar.slider.value = slider_value

    # reshoot_cover()
    # _____________________________________________________________________________________
    def reshoot_cover(self, popup, *largs):
        popup.dismiss()
        slider = self.spread_slider_bar.slider
        if slider.value != slider.min:
            Logger.error('CaptureScreen: Called reshoot_cover while not on '
                         'the cover screen')
            return
        self.spread_slider_bar.scan_button.disabled = True
        sides = self.get_displayed_sides()
        for side in sides:
            self.camera_status[side] = None
        # capture_cover = self.ids._capture_cover
        capture_cover = self.ids['_spread_view'].children[0]
        cover_image = capture_cover.ids['_cover_image']
        stats = None
        callback = self.show_image_callback
        # TODO: can we use img.reload() instead?
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        cover_image.allow_stretch = False
        cover_image.source = resource_path('images/image-loading.gif')
        self.delete_current_spread()
        left_side = self.adjust_side('left')
        right_side = self.adjust_side('right')
        path, thumb_path = self.get_paths(0)
        queue = self.get_capture_queue(left_side)
        queue.put((left_side, path, thumb_path, callback, None, stats))
        path, thumb_path = self.get_paths(1)
        queue = self.get_capture_queue(right_side)
        queue.put((right_side, path, thumb_path, callback, cover_image, stats))

    # reshoot_cover_foldout()
    # _____________________________________________________________________________________
    def reshoot_cover_foldout(self, popup, *largs):
        popup.dismiss()
        slider = self.spread_slider_bar.slider
        if slider.value != slider.min:
            Logger.error('CaptureScreen: Called reshoot_cover_foldout while '
                         'not on the cover screen')
            return
        self.spread_slider_bar.scan_button.disabled = True
        side = self.adjust_side('right')
        self.camera_status[side] = None

        # capture_cover = self.ids._capture_cover
        capture_cover = self.ids['_spread_view'].children[0]
        cover_image = capture_cover.ids['_cover_image']
        stats = None
        callback = self.show_image_callback

        # TODO: can we use img.reload() instead?
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        cover_image.allow_stretch = False
        cover_image.source = resource_path('images/image-loading.gif')

        path, thumb_path = self.get_paths(1)
        queue = self.get_capture_queue(side)
        queue.put(('foldout', path, thumb_path, callback, cover_image, stats))

    # capture_spread()
    # _____________________________________________________________________________________
    def capture_spread(self, popup=None):
        if popup is not None:
            popup.dismiss()
        if self.page_num == 1:
            if not self.metadata_is_set():
                self.show_metadata_error()
                return
            elif self.show_metadata_key_error():
                return
            else:
                self.save_metadata()
        if self.capture_spread_box is None:
            self.show_spread_widget()
        self.spread_slider_bar.scan_button.disabled = True
        sides = self.get_displayed_sides()
        offset = len(sides)
        for side in sides:
            self.camera_status[side] = None
            # Increase page_number by offset as these are target page numbers
            sides[side] += offset
        slider = self.spread_slider_bar.slider
        if slider.value != slider.max:
            Logger.debug('CaptureScreen: Preparing book for insert')
            # we are inserting a spread, so we need to rename images after
            # the current spread to create a space to place the inserted images
            self.prepare_for_insert()
        images_dir = join(scribe_globals.app_working_dir, 'images')
        loading_image_path = join(images_dir, 'image-loading.gif')
        for side, page_number in sides.iteritems():
            self.set_page_widget(side, loading_image_path)
            stats = self.get_stats(side)
            stats['leaf_num'] = page_number
            stats['page_type'] = 'Normal'
            stats['ppi'] = self.get_leaf_ppi(page_number)
            path, thumb_path = self.get_paths(page_number)
            callback = self.show_image_callback
            page_widget = self.get_page_widget(side)
            queue = self.get_capture_queue(side)
            queue.put((side, path, thumb_path, callback, page_widget, stats))
            self.scandata.update(page_number, side)
            if side == 'foldout':
                # TODO: Fix to work with single camera.
                # Probably should be fixed in scandata.update method
                page_data = self.scandata.get_page_data(page_number)
                page_data['pageType'] = 'Normal'
        self.page_num += offset
        slider.max += 1
        slider.value += 1
        self.slider_val += 1
        # TODO: We can optimize compute_page_nums() for the capture_spread() case
        Logger.debug('CaptureScreen: Computing page numbers with '
                     'end_leaf_number: {}'.format(self.page_num))
        self.scandata.compute_page_nums(self.page_num)
        for side, page_number in sides.iteritems():
            self.set_page_number_button(side, page_number)
        self.scandata.save()

    # capture_foldout()
    # _____________________________________________________________________________________
    def capture_foldout(self, side, *largs):
        # TODO: can we use img.reload() instead?
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        images_dir = join(scribe_globals.app_working_dir, 'images')
        loading_image_path = join(images_dir, 'image-loading.gif')
        self.set_page_widget(side, loading_image_path)
        self.spread_slider_bar.scan_button.disabled = True
        self.camera_status[side] = None
        sides = self.get_displayed_sides()
        page_number = sides[side]
        path, thumb_path = self.get_paths(page_number)
        callback = self.show_image_callback
        page = self.get_page_widget(side)
        stats = self.get_stats(side)
        stats['page_type'] = 'Foldout'
        stats['ppi'] = self.get_leaf_ppi(page_number)
        queue = self.get_capture_queue(side)
        queue.put(('foldout', path, thumb_path, callback, page, stats))
        self.scandata.update(page_number, 'foldout', page_type='Foldout')

    # capture_button()
    # _____________________________________________________________________________________
    def capture_button(self):

        if self._free_disk_space():
            if self.spread_slider_bar.slider.value == self.spread_slider_bar.slider.max:
                self.capture_spread()
            else:
                if self.downloaded_book:
                # self.downloaded_book == True:
                    self.stop_autoshoot_capture()
                    content = Button(text='OK')
                    popup = self.create_popup(
                        title='Insert in corrections is not yet available',
                        content=content, auto_dismiss=False,
                        size_hint=(None, None), size=(230, 100)
                    )
                    content.bind(on_press=popup.dismiss)
                    popup.open()
                else:
                    self.stop_autoshoot_capture()
                    msg_box = ScribeMessageOKCancel()
                    msg_box.text = 'Do you want to INSERT a new spread\nAFTER the current spread?'
                    msg_box.ok_text = 'INSERT SPREAD'
                    msg_box.trigger_func = self.capture_spread
                    popup = self.create_popup(
                        title='Insert spread?', content=msg_box,
                        size_hint=(None, None), size=(400, 300)
                    )
                    msg_box.popup = popup
                    popup.open()
        else:
            self.stop_autoshoot_capture()
            content = Button(text='OK')
            popup = self.create_popup(
                title='Error: the disk is full',
                content=content, auto_dismiss=False,
                size_hint=(None, None), size=(230, 100)
            )
            content.bind(on_press=popup.dismiss)
            popup.open()


    #  _free_disk_space
    # _____________________________________________________________________________________
    def _free_disk_space(self):
        proc = subprocess.Popen(["df ~/scribe_books|tail -1|awk '{print $5}'| sed s/%//"],
                                stdout=subprocess.PIPE,shell=True)
        (out,err) = proc.communicate()
        if int(out) > 95:
            return 0
        else:
            return 1

    # prepare_for_insert()
    # _____________________________________________________________________________________
    def prepare_for_insert(self):
        """we are inserting a spread, so we need to rename images after
        the current spread to create a space to place the inserted images
        """
        sides = self.get_displayed_sides()
        current = sides.get('left', None)
        if current is None:
            current = sides['foldout']
        offset = len(sides)
        start_page_number = current + offset
        end_page_number = self.get_page_max_value()
        Logger.debug('CaptureScreen: Preparing for insert at {}'
                     .format(current))
        for page_number in range(end_page_number, start_page_number - 1, -1):
            old_path, old_thumb = self.get_paths(page_number)
            new_path, new_thumb = self.get_paths(page_number + offset)
            Logger.debug('CaptureScreen: Renaming "{}" to "{}"'
                         .format(old_path, new_path))
            try:
                os.rename(old_path, new_path)
            except Exception:
                Logger.exception('CaptureScreen: Failed to rename "{}" to "{}"'
                                 .format(old_path, new_path))
            Logger.debug('CaptureScreen: Renaming "{}" to "{}"'
                         .format(old_thumb, new_thumb))
            try:
                os.rename(old_thumb, new_thumb)
            except Exception:
                Logger.exception('CaptureScreen: Failed to rename "{}" to "{}"'
                                 .format(old_thumb, new_thumb))

    # delete_current_spread()
    # _____________________________________________________________________________________
    def delete_current_spread(self):
        sides = self.get_displayed_sides()
        for side, page_number in sides.iteritems():
            path, thumb_path = self.get_paths(page_number)
            try:
                Logger.debug('CaptureScreen: Deleting page {}'.format(page_number))
                if os.path.exists(path):
                    os.unlink(path)
                    Logger.debug('CaptureScreen: Deleted "{}"'.format(path))
                if os.path.exists(thumb_path):
                    os.unlink(thumb_path)
                    Logger.debug('CaptureScreen: Deleted "{}"'.format(thumb_path))
            # Do not alter scandata here, since it is called by functions that
            # expect to replace images in-place, like reshoot_cover()
            # self.scandata.delete_spread(left_page_num, right_page_num)
            except Exception:
                Logger.exception('CaptureScreen: Failed to delete page {}'
                                 .format(page_number))
                break
                # self.ids._label.text = 'ERROR! Could not delete spread. Your book is possibly corrupt!'
                # self.ids._cancel_button.disabled = False
                # return

    # delete_current_spread_and_rename()
    # _____________________________________________________________________________________
    def delete_current_spread_and_rename(self):
        sides = self.get_displayed_sides()
        offset = len(sides)
        current_page_number = sides.get('left', None)
        if current_page_number is None:
            current_page_number = sides['foldout']
        next_page_number = current_page_number + offset - 1
        self.delete_current_spread()
        # Note that range is not inclusive, so we add one to the end
        # TODO: Check if for loop works with CaptureLeaf
        for page_num in range(next_page_number + 1, self.page_num + 1):
            old_path, old_thumb = self.get_paths(page_num)
            new_path, new_thumb = self.get_paths(page_num - offset)
            Logger.debug('CaptureScreen: Renaming "{}" to "{}"'
                         .format(old_path, new_path))
            try:
                os.rename(old_path, new_path)
            except Exception:
                Logger.exception('CaptureScreen: Failed to rename "{}" to "{}"'
                                 .format(old_path, new_path))
            Logger.debug('CaptureScreen: Renaming "{}" to "{}"'
                         .format(old_thumb, new_thumb))
            try:
                os.rename(old_thumb, new_thumb)
            except Exception:
                Logger.exception('CaptureScreen: Failed to rename "{}" to "{}"'
                                 .format(old_path, new_path))
        self.scandata.delete_spread(current_page_number,
                                    next_page_number, self.page_num)
        self.page_num -= offset
        new_left_page_num = current_page_number - offset
        max_slider_value = int(self.page_num * 1.0 / offset)
        slider_value = int(new_left_page_num * 1.0 / offset)
        self.spread_slider_bar.slider.max = max_slider_value
        self.spread_slider_bar.slider.value = slider_value
        # Since we have renamed files, we need to get kivy to not used the cached
        # images. However, img_obj.reload() is not working for us. For now, we will
        # remove all cached images.
        Cache.remove('kv.image')
        Cache.remove('kv.texture')
        self.slider_val = -1
        self.slider_touch_up(slider_value)

    # show_cover_widget()
    # _____________________________________________________________________________________
    def show_cover_widget(self):
        slider = self.spread_slider_bar.slider
        slider.value = slider.min
        self.capture_spread_box = None
        self.clear_spread_view_widgets()
        capture_cover = CaptureCover()
        capture_cover.capture_screen = self
        menu = capture_cover.ids.leaf_menu_bar
        menu.attach_tooltip_to = self
        menu.use_tooltips = True
        page_data = self.scandata.get_page_data(1)
        leaf_menu_bar = menu.ids.leaf_menu_bar
        leaf_menu_bar.page_type = page_data.get('pageType', 'Normal')
        menu.bind(on_option_select=self.on_capture_cover_leaf_menu_option)
        camera_ppi_input = capture_cover.ids.camera_ppi_input
        camera_ppi_input.bind(focus=self.on_non_metadata_input_focus)
        self.ids['_spread_view'].add_widget(capture_cover)

        cover_image = capture_cover.ids['_cover_image']
        if self.book_dir is not None:
            path, thumb_path = self.get_paths(1)
            if (not os.path.exists(thumb_path)) or (not os.path.exists(path)):
                cover_image.source = resource_path('images/missing.png')
            else:
                cover_image.allow_stretch = True
                cover_image.source = thumb_path
            preloaded_path = join(self.book_dir, 'preloaded')
            if os.path.exists(preloaded_path):
                capture_cover.ids.dwwi_button.disabled = True
                capture_cover.ids.marc_button.disabled = True
            self.load_metadata()

        if self.config.get('enable_preloaded_metadata', 'True') == 'True':
            barcode_widget = BarcodeWidget(self)
            barcode_input = barcode_widget.barcode_input
            barcode_input.bind(focus=self.on_non_metadata_input_focus)
            title_input = capture_cover.ids.title_input
            title_input.focus_previous = None
            camera_ppi_input.focus_next = barcode_input
            barcode_input.focus_next = title_input
            barcode_input.focus_previous = camera_ppi_input
            capture_cover.ids['_preload_container'].add_widget(barcode_widget)
        leaf_menu_bar.ppi = int(self.get_leaf_ppi(1))
        capture_cover.focus_first_text_input()

    def on_non_metadata_input_focus(self, text_input, focus):
        if focus:
            self.disable_capture_actions()
        else:
            self.enable_capture_actions()

    def on_capture_cover_leaf_menu_option(self, menu, option):
        if option == 'view_source':
            self.show_original_file(None, 'right')
        elif option == 'page_attrs':
            self.show_page_attrs(None, 'right')
        elif option == 'reshoot':
            capture_cover = self.ids._spread_view.children[0]
            capture_cover.reshoot()
        elif option == 'export':
            path, thumb_path = self.get_paths(1)
            self.start_image_export_filechooser(path)
        elif option == 'insert':
            side = 'right'
            path, thumb_path = self.get_paths(1)
            self.start_insert_filechooser(side, path)
        elif option == 'rotate':
            self.rotate_image('right', 1)
        elif option == 'ppi':
            leaf_ppi = self.get_leaf_ppi(1)
            side = self.adjust_side('right')
            data = {'leaf_number': 1, 'side': side}
            self.show_ppi_popup(1, leaf_ppi, extra_data=data)

    def start_image_export_filechooser(self, source_path):
        self.disable_capture_actions()
        filename = os.path.basename(source_path)
        default_path = join(os.path.expanduser('~'), filename)
        _, ext = os.path.splitext(filename)
        filechooser = FileChooser()
        callback = partial(self.on_image_export_selection, source_path)
        filechooser.bind(on_selection=callback)
        filechooser.bind(on_dismiss=self.enable_capture_actions)
        filechooser.save_file(title='Export image',
                              icon='./images/window_icon.png',
                              filters=[('*' + ext).encode('utf-8')],
                              path=default_path)

    def on_image_export_selection(self, source_path, chooser, selection):
        if selection:
            destination_path = selection[0]
            root, ext = os.path.splitext(source_path)
            if not destination_path.endswith(ext):
                destination_path += ext
            self.export_image(source_path, destination_path)

    def export_image(self, source, destination):
        error = None
        try:
            shutil.copyfile(source, destination)
            Logger.info('CaptureScreen: Image exported from "{}" to "{}"'
                        .format(source, destination))
        except shutil.Error as error:
            Logger.exception('CaptureScreen: Image source path are the same. '
                             'Source "{}", destionation "{}".'
                             .format(source, destination))
        except IOError as error:
            Logger.exception('CaptureScreen: Destination "{}" is not writable'
                             .format(destination))
        except Exception as error:
            Logger.exception('CaptureScreen: Unable to export image from "{}" '
                             'to "{}".'.format(source, destination))
        if error:
            self.show_error(
                error,
                'Unable to export image to "{}"'.format(destination)
            )

    # show_spread_widget()
    #_____________________________________________________________________________________
    def show_spread_widget(self):
        if self.cameras_count == 1:
            spread = CaptureLeaf(capture_screen=self)
        else:
            spread = CaptureSpread(self)
        self.capture_spread_box = spread
        self.clear_spread_view_widgets()
        self.ids._spread_view.add_widget(spread)

    def clear_spread_view_widgets(self):
        '''Disables tooltips for menus and then clears widgets of spread_view.
        '''
        spread_view = self.ids._spread_view
        if spread_view.children:
            menu = None
            widget = spread_view.children[0]
            if isinstance(widget, (CaptureSpread, CaptureLeaf)):
                menu = widget.ids.spread_menu_bar
            elif isinstance(widget, CaptureCover):
                menu = widget.ids.leaf_menu_bar
            if menu:
                menu.use_tooltips = False
                menu.attach_tooltip_to = None
            spread_view.clear_widgets()

    def on_capture_spread_box(self, screen, capture_spread):
        self._update_book_menu_bar()
        if not capture_spread:
            self.stop_autoshoot_capture()
            return
        self._update_spread_menu_bar_page_type()
        menu = self.capture_spread_box.ids.spread_menu_bar
        menu.attach_tooltip_to = self
        menu.use_tooltips = True
        menu.left_ppi_button.text = '300ppi'
        menu.right_ppi_button.text = '300ppi'
        menu.bind(on_option_select=self._on_spread_menu_bar_option_select)
        menu.bind(on_type_button_release=self._on_spread_type_button_release)
        menu.bind(on_number_button_release=self._on_spread_number_button_release)

    def _on_spread_menu_bar_option_select(self, spread_menu_bar, side, option):
        side = self.adjust_side(side)
        if option == 'view_source':
            self.show_original_file(None, side)
        elif option == 'delete_spread':
            self.capture_spread_box.delete_or_foldout()
        elif option == 'export':
            leaf_number = self.get_slider_bar_value(side)
            path, thumb_path = self.get_paths(leaf_number)
            self.start_image_export_filechooser(path)
        elif option == 'insert':
            leaf_number = self.get_slider_bar_value(side)
            path, thumb_path = self.get_paths(leaf_number)
            self.start_insert_filechooser(side, path)
        elif option == 'rotate':
            leaf_number = self.get_slider_bar_value(side)
            self.rotate_image(side, leaf_number)
        elif option == 'ppi':
            leaf_number = self.get_slider_bar_value(side)
            leaf_ppi = self.get_leaf_ppi(leaf_number)
            data = {'leaf_number': leaf_number, 'side': side}
            self.show_ppi_popup(leaf_number, leaf_ppi, extra_data=data)

    def show_ppi_popup(self, leaf_number, default_ppi, extra_data=None):
        popup = self.create_popup(popup_cls=EditPPIPopup,
                                  default_ppi=default_ppi,
                                  extra_data=extra_data)
        popup.bind(on_submit=self.on_ppi_value_submit)
        popup.open()

    def on_ppi_value_submit(self, popup, ppi_value):
        leaf_number = popup.extra_data['leaf_number']
        if self.get_leaf_ppi(leaf_number) != ppi_value:
            if self.downloaded_book:
                # In corrections mode scandata.json values must be strings
                self.scandata.set_ppi(leaf_number, str(ppi_value))
            else:
                self.scandata.set_ppi(leaf_number, ppi_value)
            self.scandata.save()
            side = popup.extra_data['side']
            self.set_page_ppi_button(side, ppi_value)
            stats = self.get_stats(side)
            stats['ppi'] = ppi_value
            Logger.info('CaptureScreen: Set {}ppi for leaf {}'
                        .format(ppi_value, leaf_number))

    def get_leaf_ppi(self, leaf_number):
        fallback_ppi = 300
        leaf_ppi = self.scandata.get_ppi(leaf_number)
        if leaf_ppi is None and self.book_dir is not None:
            md = get_metadata(self.book_dir)
            leaf_ppi = md.get('ppi', None)
        if leaf_ppi is None:
            config = scribe_config.read()
            leaf_ppi = config.get('camera_ppi', None)
        if leaf_ppi is None:
            Logger.error('CaptureScreen: Unable to get camera ppi value from '
                         'scandata, metadata or scribe config. Using fallback '
                         'value of {}ppi'.format(fallback_ppi))
            leaf_ppi = fallback_ppi
        return int(leaf_ppi)

    def start_insert_filechooser(self, side, source_path):
        self.disable_capture_actions()
        _, ext = os.path.splitext(source_path)
        default_path = join(os.path.expanduser('~'), '')
        callback = partial(self.on_insert_image_selection, side, source_path)
        filechooser = FileChooser()
        filechooser.bind(on_selection=callback)
        filechooser.open_file(title='Select image',
                              icon='./images/window_icon.png',
                              filters=[('*' + ext).encode('utf-8')],
                              path=default_path)

    def on_insert_image_selection(self, side, source_path, chooser, selection):
        if selection:
            target_path = selection[0]
            if not os.path.isfile(target_path):
                self.show_error(None, '"{}" is not a file'.format(target_path))
                return
            elif not target_path.endswith('.jpg'):
                self.show_error(None, '"{}" not a JPEG file'.format(target_path))
                return
            side = self.adjust_side(side)
            leaf_number = self.get_displayed_sides()[side]
            data = self.scandata.get_page_data(leaf_number)
            angle = data.get('rotateDegree', 0)
            self.insert_image(source_path, target_path, angle)
            page_widget = self.get_page_widget(side)
            page_widget.reload()
        self.enable_capture_actions()

    def compute_image_angle(self, side):
        if side == 'foldout':
            return 0
        elif side == 'left':
            return 90
        elif side == 'right':
            return -90
        raise ValueError('Unknown side: {}'.format(side))

    def insert_image(self, source_path, target_path, rotate_angle=0):
        source_filename = os.path.basename(source_path)
        source_thumbnail_path = join(os.path.dirname(source_path),
                                     'thumbnails',
                                     source_filename)
        thumbnail_size = (2304, 1536)
        image = Image.open(target_path)
        image.thumbnail(thumbnail_size)
        image = image.rotate(rotate_angle)
        try:
            image.save(source_thumbnail_path, 'JPEG', quality=90)
            shutil.copyfile(target_path, source_path)
            Logger.info('CaptureScreen: Inserted image "{}" instead of "{}"'
                        .format(target_path, source_path))
        except Exception as error:
            message = ('Failed to insert "{}" instead of "{}"'
                       .format(target_path, source_path))
            Logger.exception('CaptureScreen: ' + message)
            self.show_error(error, message)

    def _on_spread_type_button_release(self, spread_menu_bar, side, button):
        side = self.adjust_side(side)
        self.show_page_attrs(button, side)

    def rotate_image(self, side, leaf_number):
        path, thumb_path = self.get_paths(leaf_number)
        angle = -90
        image = Image.open(thumb_path)
        image = image.rotate(angle)
        image.save(thumb_path, 'JPEG', quality=90)
        data = self.scandata.get_page_data(leaf_number)
        new_degree = (int(data.get('rotateDegree', 0)) + angle) % 360
        if os.path.exists(join(self.book_dir, 'reshooting')):
            new_degree = str(new_degree)
        self.scandata.update_rotate_degree(leaf_number, new_degree)
        self.scandata.save()
        page_widget = self.get_page_widget(side)
        page_widget.reload()
        Logger.info('CaptureScreen: Leaf {} rotated by {} degrees'
                    .format(leaf_number, new_degree))

    # show_page_attrs()
    # _____________________________________________________________________________________
    def show_page_attrs(self, button, side):
        leaf_num = self.get_slider_bar_value(side)
        page_data = self.scandata.get_page_data(leaf_num)
        page_type = page_data.get('pageType', 'Normal')
        page_assertion = self.scandata.get_page_assertion(leaf_num)
        bubble = PageAttributes(leaf_num, page_type, side, page_assertion)
        self.popup = popup = PopupBubble(
            side, title='Page Type', content=bubble, size_hint=(None, None),
            size=(dp(400), dp(640))
        )
        self._update_page_attributes_popup()
        self._bind_page_attributes_popup()
        bubble.popup = popup
        bubble.callback = self.set_page_attrs
        popup.bind(on_open=self.disable_capture_actions,
                   on_dismiss=self.dismiss_page_attrs)
        popup.open()
        self.editing_metadata = True

    def _bind_page_attributes_popup(self, *args):
        window = self.get_root_window()
        window.bind(size=self._update_page_attributes_popup)
        if self.capture_spread_box:
            widget = self.capture_spread_box.ids.spread_menu_bar
        else:
            capture_cover = self.ids['_spread_view'].children[0]
            widget = capture_cover.ids.leaf_menu_bar
        widget.bind(center=self._update_page_attributes_popup)

    def _unbind_page_attributes_popup(self, *args):
        window = self.get_root_window()
        window.unbind(size=self._update_page_attributes_popup)
        if self.capture_spread_box:
            widget = self.capture_spread_box.ids.spread_menu_bar
        else:
            capture_cover = self.ids._spread_view.children[0]
            widget = capture_cover.ids.leaf_menu_bar
        widget.unbind(center=self._update_page_attributes_popup)

    def _on_spread_number_button_release(self, spread_menu_bar, side, button):
        side = self.adjust_side(side)
        self.set_page_num(side)

    # slider_touch_down()
    # _____________________________________________________________________________________
    def slider_touch_down(self, value):
        self.slider_val = value

    # slider_touch_up()
    # ____________________________________________________________________________________
    def slider_touch_up(self, value):
        '''Argument `value` is page number if CaptureLeaf is used and spread
        number otherwise.
        '''
        has_single_camera = self.cameras_count == 1
        if value == self.slider_val:
            if value != 1:
                return
            elif value != 2 and has_single_camera:
                # This is a fix to work with single camera
                return
        value = int(value)
        Logger.info('Slider_touch_up: slider up at {0}'.format(value))
        if value == 0 or (value == 1 and has_single_camera):
            self.show_cover_widget()
            return
        elif self.capture_spread_box is None:
            self.show_spread_widget()
        images_dir = join(scribe_globals.app_working_dir, 'images')
        loading_image_path = join(images_dir, 'image-loading.gif')
        missing_image_path = join(images_dir, 'missing.png')
        sides = self.get_displayed_sides()
        for side in sides:
            self.set_page_widget(side, loading_image_path)
            page_number = self.compute_page_number(side, value)
            path, thumb_path = self.get_paths(page_number)
            if self.downloaded_book or (os.path.exists(thumb_path) and os.path.exists(path)):
                self.set_page_widget(side, thumb_path, allow_stretch=True)
            else:
                self.set_page_widget(side, missing_image_path)
            stats = self.get_stats(side)
            stats['leaf_num'] = page_number
            page_data = self.scandata.get_page_data(page_number)
            stats['page_type'] = page_data.get('pageType', 'Normal')
            ppi_value = self.get_leaf_ppi(page_number)
            stats['ppi'] = ppi_value
            self.set_page_number_button(side, page_number)
            self.set_page_ppi_button(side, ppi_value)
        self.slider_val = value

    def set_page_widget(self, side, image_path, allow_stretch=False):
        page = self.get_page_widget(side)
        page.source = image_path
        page.allow_stretch = allow_stretch

    def get_page_widget(self, side):
        if not self.capture_spread_box:
            capture_cover = self.ids._spread_view.children[0]
            return capture_cover.ids._cover_image
        ids = self.capture_spread_box.ids
        if side == 'left':
            return ids._page_left
        elif side == 'right':
            return ids._page_right
        elif side == 'foldout':
            return ids.page
        raise ValueError('Unknown side: {}'.format(side))

    def get_displayed_sides(self):
        '''Returns dict {side: page_number} where side can be 'left', 'rigt'
        or 'foldout'. Page number is calculated from spread_slider_bar.
        '''
        value = int(self.spread_slider_bar.slider.value)
        if self.cameras_count == 1:
            return OrderedDict({'foldout': value})
        out = OrderedDict()
        out['left'] = 2 * value
        out['right'] = 2 * value + 1
        return out

    def get_capture_queue(self, side):
        if side == 'left':
            return self.scribe_widget.left_queue
        elif side == 'right':
            return self.scribe_widget.right_queue
        elif side == 'foldout':
            return self.scribe_widget.foldout_queue
        raise ValueError('Unknown side: {}'.format(side))

    def get_stats(self, side):
        if side == 'left':
            return self.capture_spread_box.left_stats
        elif side == 'right':
            return self.capture_spread_box.right_stats
        elif side == 'foldout':
            return self.capture_spread_box.stats
        raise ValueError('Unknown side: {}'.format(side))

    def get_page_number_button(self, side):
        menu = self.capture_spread_box.ids.spread_menu_bar
        if side == 'left':
            return menu.left_number_button
        elif side == 'right':
            return menu.right_number_button
        elif side == 'foldout':
            return menu.right_number_button
        raise ValueError('Unknown side: {}'.format(side))

    def set_page_number_button(self, side, page_number):
        button = self.get_page_number_button(side)
        page_data = self.scandata.get_page_num(page_number)
        if page_data is None:
            button.text = ''
            button.set_color('clear')
            return
        colors = {
            'assert': 'green',
            'match': 'gray',
            'mismatch': 'red',
        }
        # TODO: Update following lines when scandata structure is same when
        # reshooting book and otherwise
        if isinstance(page_data, dict):
            # default scandata structure
            color = colors.get(page_data['type'], 'gray')
            button.text = str(page_data['num'])
            button.set_color(color)
        elif isinstance(page_data, basestring):
            # reshooting book
            button.text = page_data
            button.set_color('gray')

    def set_page_ppi_button(self, side, ppi_value):
        if self.capture_spread_box is None:
            capture_cover = self.ids._spread_view.children[0]
            menu = capture_cover.ids.leaf_menu_bar.ids.leaf_menu_bar
            menu.ppi = ppi_value
        else:
            menu = self.capture_spread_box.ids.spread_menu_bar
            if side == 'left':
                menu.left_ppi_button.text = '{}ppi'.format(ppi_value)
            elif side == 'right' or side == 'foldout':
                menu.right_ppi_button.text = '{}ppi'.format(ppi_value)

    def get_page_type_button(self, side):
        menu = self.capture_spread_box.ids.spread_menu_bar
        if side == 'left':
            return menu.left_type_button
        elif side == 'right':
            return menu.right_type_button
        elif side == 'foldout':
            return menu.right_type_button
        raise ValueError('Unknown side: {}'.format(side))

    def get_slider_bar_value(self, side):
        value = int(self.spread_slider_bar.slider.value)
        if side == 'foldout':
            return value
        elif side == 'left':
            return 2 * value
        elif side == 'right':
            return 2 * value + 1
        raise ValueError('Unknown side: {}'.format(side))

    def get_page_max_value(self):
        if isinstance(self.capture_spread_box, CaptureLeaf):
            return int(self.spread_slider_bar.slider.max)
        return 2 * int(self.spread_slider_bar.slider.max) + 1

    def compute_page_number(self, side, value):
        if side == 'foldout':
            return value
        elif side == 'left':
            return 2 * value
        elif side == 'right':
            return 2 * value + 1
        raise ValueError('Unknown side: {}'.format(side))

    def adjust_side(self, side):
        '''Returns 'foldout' is CaptureLeaf is displayed, but returns `side`
        unchanged otherwise.
        '''
        if self.cameras_count == 1:
            return 'foldout'
        return side

    # prev_page_button()
    # _____________________________________________________________________________________
    def prev_page_button(self, value):
        slider = self.spread_slider_bar.slider
        if value > slider.min:
            self.slider_touch_up(value - 1)
            slider.value = value - 1

    # next_page_button()
    #_____________________________________________________________________________________
    def next_page_button(self, value):
        if value < self.spread_slider_bar.slider.max:
            self.slider_touch_up(value+1)
            self.spread_slider_bar.slider.value= value + 1

    # show_image_callback() - main thread
    # _____________________________________________________________________________________
    def show_image_callback(self, thumb_path, img_obj, side, err, *largs):
        """
        This function modifies UI elements and needs to be scheduled on the main thread
        """
        print thumb_path, img_obj, side, err
        if err is None:
            self.camera_status[side] = 1
            if img_obj is not None:
                img_obj.allow_stretch = True
                img_obj.source = thumb_path
            self._safe_for_autoshoot = True
        else:
            self.camera_status[side] = err
            self.stop_autoshoot_capture()

        if side != 'foldout':
            if (self.camera_status['left'] is None) or (self.camera_status['right'] is None):
                # both cameras have not yet returned
                return

        retry_capture = False
        if side == 'foldout':
            if self.camera_status['foldout'] != 1:
                retry_capture = True
        else:
            if (self.camera_status['left'] != 1) or (self.camera_status['right'] != 1):
                retry_capture = True

        if retry_capture:
            self.delete_current_spread()

            msg_box = ScribeErrorMessage2()
            msg_box.button_text = 'Retry image capture'
            if self.capture_spread_box is None:
                msg_box.trigger_func = self.capture_cover
            else:
                msg_box.trigger_func = self.capture_spread
            msg_box.error_msg = 'There was an error during image capture.'
            if side == 'foldout':
                msg_box.error_msg += '\n\nError capturing the FOLDOUT page: {e}'.format(e=self.camera_status['foldout'])
            else:
                if self.camera_status['left'] != 1:
                    msg_box.error_msg += '\n\nError capturing the LEFT page: {e}'.format(e=self.camera_status['left'])
                if self.camera_status['right'] != 1:
                    msg_box.error_msg += '\n\nError capturing the RIGHT page: {e}'.format(e=self.camera_status['right'])

            msg_box.button_text2 = 'Go to Camera Calibration Screen'
            msg_box.trigger_func2 = self.goto_calibration

            popup = self.create_popup(title='Image Capture Error',
                                      content=msg_box, auto_dismiss=False)
            msg_box.popup = popup
            popup.open()
        else:
            self.scandata.save()
            self.spread_slider_bar.scan_button.disabled = False
            if (side == 'foldout') and (self.capture_spread_box is not None):
                self.capture_spread_box.disable_foldout_mode()

    # create_book_dir()
    # _____________________________________________________________________________________
    def create_book_dir(self):
        """
        This function generates a tracking uuid, creates a book dir using the uuid, and
        also creates a subdir for thumbnails
        """
        uuid_str = str(uuid.uuid4())
        book_dir = os.path.expanduser(join(scribe_globals.books_dir, uuid_str))
        thumb_dir = join(book_dir, 'thumbnails')
        if not os.path.exists(thumb_dir):
            os.makedirs(thumb_dir)
        return book_dir, uuid_str

    # get_paths()
    # _____________________________________________________________________________________
    def get_paths(self, page_num):
        img = '{n:04d}.jpg'.format(n=page_num)
        path = join(self.book_dir, img)
        thumb_path = join(self.book_dir, 'thumbnails', img)
        return path, thumb_path

    # show_error() - main thread
    # _____________________________________________________________________________________
    def show_error(self, e, msg):
        popup = self.create_popup(
            title='Error', content=Label(text=msg),
            size_hint=(None, None), size=(400, 300)
        )
        popup.open()

    # on_focus()
    # _____________________________________________________________________________________
    def on_focus(self, text_widget, focus_value):
        # text_widget = args[0]
        # focus_value = args[1]
        if focus_value:
            self.disable_capture_actions()
        else:
            self.enable_capture_actions()
        if text_widget.id == '_barcode':
            return
        if not focus_value:
            print('User defocused', self, text_widget, focus_value, text_widget.cursor)
            if self.slider_val != self.spread_slider_bar.slider.min:
                # This text widget was defocused but we are no longer on the cover page
                return
            self.save_metadata()
            text_widget.do_cursor_movement('cursor_home')

    def show_original_file(self, button, side):
        side = self.adjust_side(side)
        leaf_number = self.get_slider_bar_value(side)
        jpg = '{n:04d}.jpg'.format(n=leaf_number)
        file_url = os.path.join(self.book_dir, jpg)
        firefox = webbrowser.get('firefox')
        firefox.open(file_url)

    # dismiss_page_attrs()
    # _____________________________________________________________________________________
    def dismiss_page_attrs(self, value):
        self.editing_metadata = False
        self._unbind_page_attributes_popup()
        self.enable_capture_actions()

    def _update_page_attributes_popup(self, *args):
        if self.capture_spread_box:
            self._update_capture_spread_page_attributes_popup()
        else:
            self._update_capture_cover_page_attributes_popup()

    def _update_capture_cover_page_attributes_popup(self, *args):
        capture_cover = self.ids['_spread_view'].children[0]
        menu = capture_cover.ids.leaf_menu_bar
        target_x, target_top = menu.to_window(menu.x, menu.y)
        window = self.get_root_window()
        pos_hint = {'top': 1.0 * target_top / window.height}
        if self.popup.side == 'left':
            pos_hint['x'] = 1.0 * target_x / window.width
        else:
            pos_hint['right'] = 1.0 * (target_x + menu.width) / window.width
        self.popup.pos_hint = pos_hint

    def _update_capture_spread_page_attributes_popup(self, *args):
        menu = self.capture_spread_box.ids.spread_menu_bar
        menu_x, menu_y = menu.to_window(menu.x, menu.y)
        window = self.get_root_window()
        pos_hint = {'top': 1.0 * menu_y / window.height}
        if self.popup.side == 'left':
            pos_hint['x'] = 1.0 * menu_x / window.width
        else:
            pos_hint['right'] = (1.0 * menu_x + menu.width) / window.width
        self.popup.pos_hint = pos_hint

    # set_page_attrs()
    # _____________________________________________________________________________________
    def set_page_attrs(self, leaf_num, page_type, side, page_assertion):
        self.scandata.update_page(leaf_num, page_type, page_assertion)
        self.scandata.compute_page_nums(self.page_num)
        self.scandata.save()
        if self.capture_spread_box is None:
            capture_cover = self.ids['_spread_view'].children[0]
            # Access leaf_menu_bar of cover_leaf_menu_bar widget
            menu = capture_cover.ids.leaf_menu_bar.ids.leaf_menu_bar
            menu.page_type = page_type
            return
        menu = self.capture_spread_box.ids.spread_menu_bar
        if side == 'left':
            stats = self.capture_spread_box.left_stats
            stats['page_type'] = page_type
            self.show_page_num(menu.left_number_button, leaf_num)
            self.show_page_num(menu.right_number_button, leaf_num + 1)
            menu.left_type_button.text = page_type
        elif side == 'right':
            stats = self.capture_spread_box.right_stats
            stats['page_type'] = page_type
            self.show_page_num(menu.left_number_button, leaf_num - 1)
            self.show_page_num(menu.right_number_button, leaf_num)
            menu.right_type_button.text = page_type
        elif side == 'foldout':
            stats = self.capture_spread_box.stats
            stats['page_type'] = page_type
            self.set_page_number_button(side, leaf_num)
            menu.right_type_button.text = page_type
        self.editing_metadata = False

    # set_page_num()
    # _____________________________________________________________________________________
    def set_page_num(self, side):
        """The page number bubble was clicked. Toggle the assertion on/off
        If the page was previously asserted, remove the page num assertion.
        If it was not asserted, then create the assertion.
        """
        sides = self.get_displayed_sides()
        page_number = sides[side]
        page_data = self.scandata.get_page_num(page_number)
        if page_data is None:
            return
        if page_data['type'] == 'assert':
            num = None # Toggle assertion off
        else:
            # Toggle on to whatever the autofill value is
            num = page_data['num']
        self.scandata.update_page_assertion(page_number, num)
        self.scandata.compute_page_nums(self.page_num)
        self.scandata.save()
        for side, page_number in sides.iteritems():
            self.set_page_number_button(side, page_number)

    # show_page_num()
    # _____________________________________________________________________________________
    def show_page_num(self, widget, leaf_num):

        page_num = self.scandata.get_page_num(leaf_num)

        if page_num is None:
            widget.text = ''
            widget.set_color('clear')
            return

        colors = {
            'assert': 'green',
            'match': 'gray',
            'mismatch': 'red',
        }
        print page_num
        color = colors.get(page_num['type'], 'gray')
        widget.text = str(page_num['num'])
        widget.set_color(color)

    # load_metadata()
    # _____________________________________________________________________________________
    def load_metadata(self):
        print(self.book_dir)
        md = {} if self.book_dir is None else get_metadata(self.book_dir)
        capture_cover = self.ids['_spread_view'].children[0]
        for k, v in md.iteritems():
            if k == 'author':
                print "Detected legacy author field, converting to creator...",
                k = 'creator'
            elif k == 'ppi':
                # Skip creating ppi entry in form as ppi has it's own input
                # widget next to collection spinner
                continue
            text_input = capture_cover.get_empty_field(k)
            if text_input is not None and v is not None:
                text_input.text = v
                text_input.do_cursor_movement('cursor_home')
            else:
                if k not in ['collection', 'collection_set', 'catalog']:
                    capture_cover.add_metadata_field(k, value=v)
        try:
            collections_list = get_collections_metadata(scribe_globals.config_dir)
            cset_spinner = capture_cover.ids.collection_sets_spinner
            # TODO: Is this guaranteed to exist?
            cset_spinner.text = md['collection_set']
        except Exception as e:
            Logger.exception('CaptureScreen: Failed to retrieve collection '
                             'set book\'s metadata. Perhaps this item was '
                             'created with an older version of Scribe?')
        default_book_ppi = md.get('ppi', None)
        if default_book_ppi is None:
            config = scribe_config.read()
            default_book_ppi = scribe_config.get(config, 'camera_ppi')
            if default_book_ppi is None:
                fallback_ppi = 300
                Logger.error('CaptureScreen: Unable to get book default ppi '
                             'from book metadata or Scribe config. Using '
                             'fallback value of {}ppi'.format(fallback_ppi))
                default_book_ppi = fallback_ppi
        capture_cover.ids.camera_ppi_input.value = int(default_book_ppi)
        # Verify that the collections in the collection set saved in the
        # metadata matches the content of the collections field.
        # If not, ask the user what to trust
        print('Metadata loaded:' + str(md))
        # Reset scrollview to top, so title and author are visible
        capture_cover.ids['_scrollview'].scroll_y = 1

    # save_metadata()
    # _____________________________________________________________________________________
    def save_metadata(self):
        print 'Saving metadata'
        md = get_metadata(self.book_dir)
        # capture_cover = self.ids._capture_cover
        capture_cover = self.ids['_spread_view'].children[0]
        # iterate over the metadata fields
        for widget in capture_cover.ids._metadata.children:
            # if they are instances of MTI or widget
            if not widget.disabled and type(widget) is MetadataTextInput:
                if widget.text != '':
                    key = widget.metadata_key
                    if key.lower() == 'other':
                        key_input = widget.key_input
                        if key_input.is_valid():
                            key = key_input.text
                        else:
                            print 'Skipping invalid key', key_input.text
                            continue
                    if isinstance(widget.text, str):
                        try:
                            md[key] = widget.text.decode('utf-8')
                        except Exception:
                            print 'could not decode', key, repr(widget.text)
                    else:
                        md[key] = widget.text
        print "Metadata to be saved prior to collection set processing: " + str(md)

        # get the collection set
        cset = capture_cover.ids.collection_sets_spinner.text
        # If a collection set has ben set
        if cset is not "" or cset is not None:
            # get the metadata configuration
            config = get_collections_metadata(scribe_globals.config_dir)
            # This will be our collections list
            collections = []
            try:
                # extract the selected collection set from the configuration.
                # TODO: handle bad input
                collectionset = filter(lambda collectionset: collectionset['name'] == cset,
                                       config['collections']["set"])
                for collection in collectionset[0]["collection"]:
                    collections.append(collection["_text"])
            except Exception as e:
                print e
            md['collection'] = collections
            md['collection_set'] = cset
        md['ppi'] = u'{}'.format(capture_cover.ids.camera_ppi_input.value)
        print "calling set-metadata with md = " +str(md)
        set_metadata(md, self.book_dir)

    # disable_metadata()
    # _____________________________________________________________________________________
    def disable_metadata(self):
        capture_cover = self.ids['_spread_view'].children[0]
        for widget in capture_cover.ids['_metadata'].children:
            if isinstance(widget, MetadataTextInput):
                widget.disabled = True
        capture_cover.ids.collection_sets_spinner.disabled = True
        capture_cover.ids.camera_ppi_input.disabled = True
        capture_cover.ids.metadata_spinner.disabled = True
        capture_cover.ids.dwwi_button.disabled = True
        capture_cover.ids.marc_button.disabled = True

    # metadata_is_set()
    # _____________________________________________________________________________________
    def metadata_is_set(self):
        # if identifier is preloaded, then metadata is set
        id_path = join(self.book_dir, 'identifier.txt')
        if os.path.exists(id_path):
            return True

        required_keys = set(('title', 'creator', 'language'))
        capture_cover = self.ids['_spread_view'].children[0]
        widgets = capture_cover.ids['_metadata'].children
        metadata_widgets = [x for x in widgets if isinstance(x, MetadataTextInput)]
        for widget in metadata_widgets:
            if (widget.metadata_key in required_keys) and (widget.text == ''):
                return False
        # If at this point all metadata is present, verify that a collection set has been picked
        cset_spinner = capture_cover.ids.collection_sets_spinner
        return bool(cset_spinner.text)

    # show_metadata_key_error()
    # _____________________________________________________________________________________
    def show_metadata_key_error(self):
        """Returns true if an error message was shown"""

        capture_cover = self.ids._spread_view.children[0]
        widgets = capture_cover.ids._metadata.children
        key_widgets = [x for x in widgets if isinstance(x, KeyInput)]
        for widget in key_widgets:
            if not widget.validate(widget):
                #validate() displays error
                return True
        return False

    # show_metadata_error()
    # _____________________________________________________________________________________
    def show_metadata_error(self):
        popup = self.create_popup(
            title='Metadata Error',
            content=Label(text='Title, Creator, Language and Collection set are required'),
            size_hint=(None, None), size=(600, 200)
        )
        popup.open()

    # goto_calibration()
    # _____________________________________________________________________________________
    def goto_calibration(self, popup=None):
        if popup is not None:
            popup.dismiss()
        self.scribe_widget.show_calibration_screen(target_screen='capture_screen')

    def create_popup(self, **kwargs):
        popup_cls = kwargs.pop('popup_cls', Popup)
        popup = popup_cls(**kwargs)
        popup.bind(on_open=self.disable_capture_actions,
                   on_dismiss=self.enable_capture_actions)
        return popup
