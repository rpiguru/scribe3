import copy

import os
import scribe_globals
from ia_scribe.metadata.screen import MetadataScreen
from ia_scribe.uix.metadata import MetadataTextInput
from ia_scribe.wizard.sections.base import BaseSection
from kivy.lang import Builder
from utils.metadata import get_metadata, set_metadata


class MetadataSection(BaseSection, MetadataScreen):

    en_previous_button = False

    def __init__(self, **kwargs):
        super(MetadataSection, self).__init__(**kwargs)

    def before_next(self):
        if self.validate():
            self.save_metadata()
            return True
        else:
            return False

    def on_enter(self):
        super(MetadataSection, self).on_enter()
        self.remove_buttons()
