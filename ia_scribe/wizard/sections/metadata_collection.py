import os
import scribe_config
import scribe_globals
from ia_scribe.about.metadata_collection import MetadataCollectionsScreen
from ia_scribe.wizard.sections.base import BaseSection
from kivy.logger import Logger
from ttsservices.iabdash import push_event
from ttsservices.tts import TTSServices
from utils.metadata import get_metadata
from utils.util import restart_app


class MetadataCollectionSection(BaseSection, MetadataCollectionsScreen):

    def __init__(self, **kwargs):
        super(MetadataCollectionSection, self).__init__(**kwargs)

    def on_enter(self):
        super(MetadataCollectionSection, self).on_enter()

    def before_next(self):
        if not self.save_metadata():
            return False
        self.btserver_register()
        restart_app()

    @staticmethod
    def btserver_register():
        Logger.info("btserver_register: Registering scribe")
        try:
            dd = dict((k, v) for k, v in get_metadata(os.path.expanduser(scribe_globals.config_dir)).iteritems() if v)
            dd['books'] = "[]"

            config = scribe_config.read()
            tts = TTSServices(config, dd)
            success, tts_id = tts.register_tts(tts_id=scan_id, metadata=dd)
            print "Have I succeeded in creating the item and pushing metadata?", success
            if success:
                scribe_config.update(config, 'identifier', tts_id)
                push_event('tts-register', dd, 'tts', tts_id)
                print "Registered scribe: " + str(tts_id)

            else:
                print "Couldn't register this scribe with Archive.org or scribe already registered"
        except Exception as e:
            print str(e)
            pass
