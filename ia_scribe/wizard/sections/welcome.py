import os
from ia_scribe.wizard.sections.base import BaseSection
from kivy.lang import Builder
from kivy.uix.button import Button
from utils.metadata import get_sc_metadata

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'welcome.kv'))


class WelcomeSection(BaseSection):

    en_previous_button = False

    def __init__(self, **kwargs):
        super(WelcomeSection, self).__init__(**kwargs)

    def on_enter(self):
        super(WelcomeSection, self).on_enter()

    @staticmethod
    def send_email():
        import smtplib
        from email.mime.text import MIMEText
        import datetime

        recipient = "ttsupport@archive.org"
        # recipient = "davide@archive.org"
        sender = "ttscribe_noreply@archive.org"
        password = "welcome15"
        server = "mail.archive.org"
        # config = scribe_config.read()
        # scribe_registration_email = config["email"]
        metadata = get_sc_metadata()
        time = str(datetime.datetime.now())
        message = "A new scribe was registered on %s\n at %s\n by %s\n on %s" % (
                    time, metadata["scanningcenter"], metadata["operator"], metadata["scanner"])

        msg = MIMEText(message)
        msg['Subject'] = 'A new scribe was registered'
        msg['From'] = sender
        msg['To'] = recipient

        # Send the message via our own SMTP server, but don't include the
        # envelope header.
        s = smtplib.SMTP(server)
        s.ehlo()
        s.starttls()
        s.login(sender, password)
        s.sendmail(sender, [recipient], msg.as_string())
        s.quit()
