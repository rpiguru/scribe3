import copy
import threading
import urllib
import webbrowser
from functools import partial

from kivy.properties import BooleanProperty

import internetarchive
import os
import scribe_config
from ia_scribe.uix.message import ScribeMessage
from ia_scribe.uix.metadata import MetadataTextInput
from ia_scribe.wizard.sections.base import BaseSection
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from scribe_globals import ScribeException
from utils.util import resource_path, restart_app

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'login.kv'))


class LoginSection(BaseSection):

    load_img = Image(source=resource_path('images/image-loading.gif'))
    popup = Popup(title='Logging in...', content=load_img, auto_dismiss=False, size_hint=(None, None), size=(400, 200))
    success = BooleanProperty(True)

    en_next_button = False

    def __init__(self, **kwargs):
        super(LoginSection, self).__init__(**kwargs)

    def popup_success_closed_callback(self, instance):
        if self.success:
            self.root_widget.go_next()
        else:
            restart_app()

    # The screen is logged out by default, and when it starts up, it
    # verifies the presence of the s3 keys and hides widgets appropriately
    # Main thread, called by kivy
    def on_enter(self):
        super(LoginSection, self).on_enter()
        self.success = False

        config = scribe_config.read()
        try:
            # if the s3 keys are present, need to change a bunch of widgets
            if 's3' in config:
                # If there is no email on file, ignore it (necessary for compatibility)
                try:
                    self.ids['_login_message'].text = "Logged in as " + config.get("email")
                    self.ids['_login_email_input'].hint_text = config.get("email")
                    self.ids['_login_password_input'].hint_text = "*******"
                except Exception as e:
                    Logger.exception(e)
                    self.ids['_login_message'].text = "Logged in with s3 keys. Re-log in to see your email."
                    self.ids['_login_email_input'].hint_text = ""
                    self.ids['_login_password_input'].hint_text = ""
                # enable logout and disable login/sign up
                self.ids['_logout_button'].disabled = False
                self.ids['_login_button'].disabled = True
                self.ids['_signup_label'].disabled = True
                self.ids['_signup_button'].disabled = True
                # disable all text fields
                widgets = [x for x in self.ids['_metadata'].children if isinstance(x, MetadataTextInput)]
                for widget in widgets:
                    widget.disabled = True
        except Exception as e:
            Logger.exception(e)
            pass

    # Login with archive.org, get s3 keys, store them, done
    # main thread, called by _login_button
    def archive_login(self):
        creds = {'email': self.ids['_login_email_input'].text, 'password': self.ids['_login_password_input'].text}
        t = threading.Thread(target=self.archive_login_thread, args=(creds,))
        t.daemon = True
        t.start()
        self.popup.open()

    def archive_login_thread(self, creds):
        try:
            keys = internetarchive.config.get_auth_config(creds["email"], creds["password"])
            # keys = internetarchive.config.get_auth_config(creds["email"],creds["password"] )
        except Exception as e:
            # if it doesn't work, tell the user
            # Is there a way to get the HTTP error code for this?
            popup = self.create_popup("Couldn't log in",
                                      "Couldn't log in.\n\nPlease check your credentials and\n"
                                      "internet connectivity and try again.\n Error:\n" + str(e), success=False)
            Clock.schedule_once(partial(self.post_login, popup))
            return
        # It has worked try and write the update to disk and tell the user to restart the app
        if keys:
            # here it calls update_config, and if we write successfully
            if self.update_config(keys) is True:
                # TODO: Send email
                # pop it
                self.success = True
                popup_success = self.create_popup("Success", "You are now logged in as: \n\n" + str(
                    urllib.unquote(keys.get("cookies")["logged-in-user"])) + ".")
                Clock.schedule_once(partial(self.post_login, popup_success))

    def create_popup(self, title, msg, success=True):
        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = Popup(title=title, content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        if success:
            popup.bind(on_dismiss=self.popup_success_closed_callback)
        msg_box.popup = popup
        msg_box.trigger_func = lambda popup: popup.dismiss()
        return msg_box

    def post_login(self, p, *args):
        self.popup.dismiss()
        p.popup.open()

    # delete s3 keys and account information from scribe_config.yml
    # main thread called by _logout_button
    def archive_logout(self):
        # copy
        config = scribe_config.read()
        new_config = copy.deepcopy(config)
        # Try to delete s3 and email from said condifuration and tell the user to restart if it goes ok
        try:
            # remove s3 block from yml
            del new_config["s3"]
            # It's possible that there is no email field in the config file, so ignore errors deleting it
            if 'email' in new_config.keys():
                del new_config["email"]

            # save and notify the user
            scribe_config.save(new_config)
            popup_success = Popup(title='Logged out',
                                  content=Label(text="You have logged out" + ". \n\nRestarting the application..."),
                                  auto_dismiss=True, size_hint=(None, None), size=(400, 200))
            popup_success.open()
            restart_app()
        except:
            # if the keys didn't exist in the first place, a field was missing or whatever, cry.
            raise ScribeException('There was an error logging out.')

    @staticmethod
    def archive_signup():
        webbrowser.open("http://archive.org/account/login.createaccount.php")

    # A little wrapper that updates the s3 configuration in scribe_config.yml
    def update_config(self, keys):
        config = scribe_config.read()
        # if there is no config, make a new one
        if config is not None:
            new_config = copy.deepcopy(config)
        else:
            new_config = {}

        # Add items in the s3 dictionary to the s3 section of the yml
        # for key, val in keys.get("s3").iteritems():
        #    scribe_config.update(new_config, "s3/"+key, str(val))
        scribe_config.update(new_config, "s3/access_key", str(keys['s3']['access']))
        scribe_config.update(new_config, "s3/secret_key", str(keys['s3']['secret']))
        # get email and escape @
        email = urllib.unquote(keys.get("cookies")["logged-in-user"])
        scribe_config.update(new_config, "email", email)
        scribe_config.update(new_config, "cookie", str(keys.get("cookies")["logged-in-sig"]))

        # if something changed, write
        if cmp(config, new_config) != 0:
            scribe_config.save(new_config)
            print scribe_config.read()
            return True
        return False

    @staticmethod
    def archive_forgot_pwd():
        webbrowser.open("https://archive.org/account/login.forgotpw.php")

    def on_touch_down(self, touch):
        local_pos = self.to_local(*touch.pos)
        if self.ids['lb_forgot_pwd'].collide_point(*local_pos):
            self.archive_forgot_pwd()
        elif self.ids['lb_signup'].collide_point(*local_pos):
            self.archive_signup()
        super(LoginSection, self).on_touch_down(touch)
