import os
from kivy.lang import Builder
from kivy.uix.label import Label

Builder.load_file(os.path.join(os.path.dirname(__file__), 'label.kv'))

# UploadLabel
# _________________________________________________________________________________________


class UploadLabel(Label):
    pass


class BlackLabel(Label):
    def set_width(self, w):
        self.size_hint_x = None
        self.width = w
