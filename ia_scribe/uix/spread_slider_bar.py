from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import ObjectProperty, OptionProperty
from kivy.uix.boxlayout import BoxLayout

from ia_scribe.uix.behaviors.tooltip import TooltipControl

Builder.load_file(join(dirname(__file__), 'spread_slider_bar.kv'))


class SpreadSliderBar(TooltipControl, BoxLayout):
    """Widget contains slider for selecting spread, buttons for selecting
    next/previous spread, autoshoot toggle button and autoshoot time input.

    :Events:
        `on_option_select`: option
            Available options: 'previous_spread', 'next_spread',
            'capture_spread'.
        `on_value_down`: value
            Dispatched when slider receives on_touch_down event.
        `on_value_up`: value
            Dispatched when slider receives on_touch_up event.
        `on_autoshoot_toggle`: state
            Dispatched when autoshoot toggle button changes it's state which
            can be 'normal' or 'down'.
        `on_autoshoot_value`: value
            Dispatched when new value for autoshoot is inputted and `enter`
            key pressed.
        `on_knob_pos`: pos
            Dispatched when slider's knob changes it's position.
    """

    left_page_number = ObjectProperty(allownone=True)
    # Asserted left page number

    right_page_number = ObjectProperty(allownone=True)
    # Asserted right page number

    asserted_page_number = ObjectProperty(allownone=True)
    # Used we slider bar is used to select pages

    tooltip_mode = OptionProperty('dual', options=['dual', 'single'])

    slider = ObjectProperty()
    scan_button = ObjectProperty()

    __events__ = ('on_option_select', 'on_value_down', 'on_value_up',
                  'on_autoshoot_toggle', 'on_autoshoot_value',
                  'on_knob_pos')

    def __init__(self, **kwargs):
        update = self.update_knob_tooltip
        self.bind(left_page_number=update,
                  right_page_number=update,
                  asserted_page_number=update)
        super(SpreadSliderBar, self).__init__(**kwargs)

    def on_slider(self, bar, slider):
        update = self.update_knob_tooltip
        slider.bind(value=update, max=update)

    def update_knob_tooltip(self, *args):
        slider = self.slider
        if not slider:
            return
        if self.tooltip_mode == 'dual':
            current, total = int(slider.value), int(slider.max)
            left = 2 * current
            right = left + 1
            asserted_left = str(self.left_page_number)
            asserted_right = str(self.right_page_number)
            if self.left_page_number is None:
                asserted_left = '-'
            if self.right_page_number is None:
                asserted_right = '-'
            slider.tooltip = ('{}/{} | {},{} | {},{}'
                              .format(current, total, left, right,
                                      asserted_left, asserted_right))
        elif self.tooltip_mode == 'single':
            current, total = int(slider.value), int(slider.max)
            asserted = str(self.asserted_page_number)
            if self.asserted_page_number is None:
                asserted = '-'
            slider.tooltip = '{} | {} | {}'.format(current, total, asserted)

    def on_option_select(self, option):
        pass

    def on_value_down(self, value):
        pass

    def on_value_up(self, value):
        pass

    def on_autoshoot_value(self, value):
        pass

    def on_autoshoot_toggle(self, state):
        pass

    def on_knob_pos(self, pos):
        pass
