from kivy.uix.gridlayout import GridLayout


class SimpleGridLayout(GridLayout):
    """A GridLayout that works in a ScrollView, since we bind minimum_height to setter(height)"""
    def __init__(self, **kwargs):
        super(SimpleGridLayout, self).__init__(**kwargs)
        self.bind(minimum_height=self.setter('height'))
