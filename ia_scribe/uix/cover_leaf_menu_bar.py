from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.floatlayout import FloatLayout

from ia_scribe.uix.behaviors.tooltip import TooltipControl

Builder.load_file(join(dirname(__file__), 'cover_leaf_menu_bar.kv'))

OPTION_RESHOOT = 'reshoot'


class CoverLeafMenuBar(TooltipControl, FloatLayout):
    '''Menu bar showing option for book's leaf in CaptureCover widget.

    Menu dispatches same options as `LeafMenuBar` with addition of
    `OPTION_RESHOOT`.

    :Events:

        `on_option_select`: option
            Dispatched when option is selected from menu.
    '''

    background_color = ListProperty([1.0, 1.0, 1.0, 1.0])

    __events__ = ('on_option_select',)

    def on_option_select(self, option):
        pass
