from functools import partial
from os.path import join, dirname

from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import StringProperty, DictProperty
from kivy.uix.gridlayout import GridLayout

from ia_scribe.uix.buttons import FoldoutButton
from ia_scribe.uix.messages import DeleteSpreadMessage

Builder.load_file(join(dirname(__file__), 'capture_leaf.kv'))
_default_stats = {'capture_time': '',
                  'thumb_time': '',
                  'leaf_num': 0,
                  'page_type': 'Normal'}


class CaptureLeaf(GridLayout):

    loading_image = StringProperty('images/image-loading.gif')
    stats = DictProperty(_default_stats)
    delete_button_normal = StringProperty('images/button_spread_delete.png')
    delete_button_foldout = StringProperty('images/button_spread_delete_red.png')

    def __init__(self, **kwargs):
        super(CaptureLeaf, self).__init__(**kwargs)
        self.capture_screen = kwargs['capture_screen']
        self.foldout_mode = False
        self.foldout_button = None
        self._popup = None

    def on_stats(self, capture_leaf, stats):
        panel = self.ids.leaf_info_panel
        self._update_leaf_info_panel(panel, stats)

    def _update_leaf_info_panel(self, panel, stats):
        panel.leaf_number = self._to_valid_value(stats, 'leaf_num')
        panel.ppi = self._to_valid_value(stats, 'ppi')
        panel.capture_time = self._to_valid_value(stats, 'capture_time')
        panel.thumb_time = self._to_valid_value(stats, 'thumb_time')
        panel.notes = self._to_valid_value(stats, 'notes')

    def _to_valid_value(self, stats, key, not_allowed=(None, '')):
        value = stats.get(key, None)
        return 'NULL' if value in not_allowed else str(value)

    def delete_or_foldout(self):
        if not self.foldout_mode:
            cameras = self.capture_screen.scribe_widget.cameras
            if cameras.camera_ports['foldout'] is None:
                self.delete_spread()
            else:
                print 'Switching to foldout mode'
                self.enable_foldout_mode()
                button = self.ids.spread_menu_bar.delete_button
                button.background_normal = self.delete_button_foldout
                button.background_down = self.delete_button_foldout
        else:
            self.delete_spread()

    def delete_spread(self):
        msg_box = DeleteSpreadMessage(self.capture_screen, self)
        self.capture_screen.disable_capture_actions()
        self._popup = popup = self.capture_screen.create_popup(
            title='Delete Spread', content=msg_box,
            size_hint=(None, None), size=(400, 300)
        )
        popup.bind(on_open=self._on_popup_open,
                   on_dismiss=self._on_popup_dismiss)
        msg_box.popup = popup
        popup.open()

    def _on_popup_open(self, *args):
        Window.bind(on_key_down=self._on_key_down)

    def _on_popup_dismiss(self, *args):
        if self.foldout_mode:
            self.disable_foldout_mode()
        Window.unbind(on_key_down=self._on_key_down)
        self._popup = None

    def _on_key_down(self, window, keycode, scancode, codepoint=None,
                     modifiers=None, **kwargs):
        if keycode == 13 or keycode == 32:
            # Keys "enter" and "spacebar" are used to confirm
            # Key "esc" will dismiss popup by default
            self._popup.content.delete_spread_confirmed()
            return True

    def enable_foldout_mode(self):
        self.foldout_mode = True
        button = FoldoutButton(pos_hint={'center': (0.5, 0.5)})
        callback = partial(self.capture_screen.capture_foldout, 'foldout')
        button.bind(on_press=callback)
        self.foldout_button = button
        self.ids.image_box.add_widget(button)

    def disable_foldout_mode(self):
        self.foldout_mode = False
        if self.foldout_button is not None:
            self.ids.image_box.remove_widget(self.foldout_button)
        button = self.ids.spread_menu_bar.delete_button
        button.background_normal = self.delete_button_normal
        button.background_down = self.delete_button_normal
