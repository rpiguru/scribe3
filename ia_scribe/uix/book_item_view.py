import time
from os.path import dirname
from os.path import join

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ListProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.selectableview import SelectableView

Builder.load_file(join(dirname(__file__), 'book_item_view.kv'))


class BookItemView(SelectableView, BoxLayout):

    selected_color = ListProperty([0.2, 0.65, 0.8, 1])
    deselected_color = ListProperty([1, 1, 1, 1])
    auto_deselect = BooleanProperty(True)

    def __init__(self, **kwargs):
        self._touch_time = None
        super(BookItemView, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y) and not touch.is_mouse_scrolling:
            touch.grab(self)
            self.is_selected = True
            self._touch_time = time.time()
            super(BookItemView, self).on_touch_down(touch)
            return True
        return False

    def on_touch_move(self, touch):
        if touch.grab_current is self:
            return True
        if super(BookItemView, self).on_touch_move(touch):
            return True

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
            delta_time = time.time() - self._touch_time
            if delta_time < 0.035:
                Clock.schedule_once(self._release_selection,
                                    0.035 - delta_time)
            else:
                self._release_selection()
            super(BookItemView, self).on_touch_up(touch)
            return True
        return False

    def _release_selection(self, *args):
        if self.auto_deselect:
            self.is_selected = False
