from os.path import join, dirname

from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import NumericProperty
from kivy.uix.floatlayout import FloatLayout

Builder.load_file(join(dirname(__file__), 'image_box.kv'))


class ImageBox(FloatLayout):

    padding = NumericProperty(dp(5))
