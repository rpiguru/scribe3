from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import ListProperty, StringProperty
from kivy.uix.behaviors import ButtonBehavior, ToggleButtonBehavior
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.label import Label

from ia_scribe.uix.behaviors.tooltip import TooltipBehavior

Builder.load_file(join(dirname(__file__), 'buttons.kv'))


class ColorButton(ButtonBehavior, Label):

    color_normal = ListProperty([0.35, 0.35, 0.35, 1.0])
    color_down = ListProperty([0.2, 0.64, 0.8, 1.0])


class ImageButton(ButtonBehavior, Image):

    source_normal = StringProperty(
        'atlas://data/images/defaulttheme/button')

    source_down = StringProperty(
        'atlas://data/images/defaulttheme/button_pressed')

    source = StringProperty('atlas://data/images/defaulttheme/button')

    def __init__(self, **kwargs):
        super(ImageButton, self).__init__(**kwargs)
        self.bind(source_normal=self._update_source)
        self.bind(source_down=self._update_source)
        self.bind(state=self._update_source)
        self._update_source()

    def _update_source(self, *args):
        state = self.state
        self.source = self.source_down if state == 'down' else self.source_normal

    def collide_point(self, x, y):
        norm_w = self.norm_image_size[0]
        norm_h = self.norm_image_size[1]
        norm_x = self.center_x - self.norm_image_size[0] / 2.0
        norm_y = self.center_y - self.norm_image_size[1] / 2.0
        return norm_x <= x < norm_x + norm_w and norm_y <= y <= norm_y + norm_h


class TooltipImageButton(TooltipBehavior, ImageButton):
    pass


class ImageToggleButton(ToggleButtonBehavior, Image):

    source_normal = StringProperty(
        'atlas://data/images/defaulttheme/button')

    source_down = StringProperty(
        'atlas://data/images/defaulttheme/button_pressed')

    source = StringProperty('atlas://data/images/defaulttheme/button')

    def __init__(self, **kwargs):
        super(ImageToggleButton, self).__init__(**kwargs)
        self.bind(source_normal=self._update_source)
        self.bind(source_down=self._update_source)
        self.bind(state=self._update_source)
        self._update_source()

    def _update_source(self, *args):
        state = self.state
        self.source = self.source_down if state == 'down' else self.source_normal

    def collide_point(self, x, y):
        norm_w = self.norm_image_size[0]
        norm_h = self.norm_image_size[1]
        norm_x = self.center_x - self.norm_image_size[0] / 2.0
        norm_y = self.center_y - self.norm_image_size[1] / 2.0
        return norm_x <= x < norm_x + norm_w and norm_y <= y <= norm_y + norm_h


class TooltipImageToggleButton(TooltipBehavior, ImageToggleButton):
    pass


class FoldoutButton(Button):
    pass


class FoldoutButtonLeft(FoldoutButton):
    pass


class FoldoutButtonRight(FoldoutButton):
    pass
