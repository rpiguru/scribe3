from kivy.properties import BooleanProperty, StringProperty, ObjectProperty

import re
from kivy.uix.switch import Switch

from ia_scribe.uix.text_input import TextInput


class MetadataTextInput(TextInput):

    write_tab = BooleanProperty(False)
    metadata_key = StringProperty(None)
    key_input = ObjectProperty(None)

    def mark_as_error(self):
        self.background_color = (.5, 0, 0, .5)

    def mark_as_normal(self):
        self.background_color = (1, 1, 1, 1)

    def on_touch_down(self, touch):
        super(MetadataTextInput, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self.mark_as_normal()


class MetadataSwitch(Switch):

    metadata_key = StringProperty(None)


class EmailTextInput(MetadataTextInput):
    pat = re.compile('\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+')

    is_valid = BooleanProperty(False)

    def on_text(self, instance, substring):
        pat = self.pat
        if re.match(pat, substring):
            self.background_color = (0, 1, 0, .5)
            self.is_valid = True
        else:
            self.is_valid = False
            if substring != "":
                self.mark_as_error()
