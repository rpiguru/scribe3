from functools import partial

from kivy.properties import ListProperty, NumericProperty, StringProperty
import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.graphics import *
from kivy.uix.label import Label

Builder.load_file(os.path.join(os.path.dirname(__file__), 'linear_stepper.kv'))


class CircleCheck(BoxLayout):

    index = NumericProperty(1)
    state = StringProperty('disabled')   # `current`, `passed`, `disabled`

    def __init__(self, **kwargs):
        super(CircleCheck, self).__init__(**kwargs)
        Clock.schedule_once(partial(self.update_state, self.state))

    def update_state(self, new_state=None, *args):
        if new_state is not None:
            self.state = new_state
        self.clear_widgets()
        self.canvas.clear()
        if new_state == 'current':
            with self.canvas.before:
                Color(.1, .1, .1, 1)
                Ellipse(size=self.size, pos=self.pos)
            self.add_widget(Label(text=str(self.index)))
        elif new_state == 'disabled':
            with self.canvas.before:
                Color(.3, .3, .3, .8)
                Ellipse(size=self.size, pos=self.pos)
            self.add_widget(Label(text=str(self.index)))
        else:
            # self.add_widget(Image(source='images/check.png'))
            self.add_widget(Image(source='../../images/check.png'))


class LinearStepper(BoxLayout):

    values = ListProperty()
    index = NumericProperty(0)

    def __init__(self, **kwargs):
        super(LinearStepper, self).__init__(**kwargs)
        if len(self.values) > 0:
            self.build_stepper()

    def build_stepper(self):
        self.add_widget(CircleCheck(index=1, state='disabled'))
        self.add_widget(CircleCheck(index=2, state='current'))
        self.add_widget(CircleCheck(index=3, state='passed'))
        print self.values


if __name__ == '__main__':
    from kivy.app import App

    class LinearStepperApp(App):

        def build(self):
            a = LinearStepper(values=['step1', 'step2', 'step3'])
            return a


    LinearStepperApp().run()
