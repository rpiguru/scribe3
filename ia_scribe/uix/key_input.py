# KeyInput
# _________________________________________________________________________________________
import re

from kivy.properties import BooleanProperty
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput


class KeyInput(TextInput):

    write_tab = BooleanProperty(False)

    def validate(self, instance):
        print('User pressed enter in', instance, self.text)
        if not self.is_valid():
            print ' Invalid!'

            label = Label(text='Metadata key should start with a lowercase ascii letter,\n'
                               'and only lowercase ascii, hypens, and underscores are allowed.')
            popup = Popup(title='Metadata Key Error', content=label,
                          size_hint=(None, None), size=(500, 200))
            popup.open()
            return False
        else:
            return True

    # is_valid()
    # _____________________________________________________________________________________
    def is_valid(self):
        if re.match(r'^[a-z][a-z\d_-]*$', self.text) is not None:
            return True
        else:
            return False
