import os
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from ia_scribe.reshootscreen import ReShootScreen


Builder.load_file(os.path.join(os.path.dirname(__file__), 'republisher_note.kv'))


class RepublisherNote(BoxLayout):
    target_page = ObjectProperty()
    notes = ObjectProperty()
    book = ObjectProperty(None)

    def __init__(self, page, note, book, scribe_widget, screen_manager, **kwargs):
        super(RepublisherNote, self).__init__(**kwargs)
        self.notes = note
        self.target_page = page
        self.book = book
        self.scribe_widget = scribe_widget
        self.screen_manager = screen_manager
        self.ids['_page_label'].text = self.target_page
        self.ids['_note_label'].text = self.notes
        if self.is_done():
            self.ids['_open_button'].background_color = [0, 1, 0, .8]

    def open_book(self):
        print "Gonna open book number " + self.book["identifier"]
        screen_name = "reshoot_screen"

        try:
            capture_screen = self.screen_manager["reshoot_screen"]
        except Exception as e:
            print '[RepublisherNote ] Failed to get capture screen: {}'.format(e)
            capture_screen = ReShootScreen(name=screen_name)

        capture_screen.book_dir = self.book["path"]
        capture_screen.reopen_at = int(self.target_page)
        capture_screen.screen_manager = self.screen_manager
        capture_screen.scribe_widget = self.scribe_widget
        self.screen_manager.add_widget(capture_screen)

        target_screen = screen_name
        models, ports = self.scribe_widget.cameras.get_cameras()
        camera_ports = self.scribe_widget.cameras.camera_ports
        if camera_ports['left'] not in ports:
            target_screen = 'calibration_screen'
        if camera_ports['right'] not in ports:
            target_screen = 'calibration_screen'
        if (camera_ports['foldout'] is not None) and (camera_ports['foldout'] not in ports):
            target_screen = 'calibration_screen'

        if target_screen == 'calibration_screen':
            self.screen_manager.get_screen('calibration_screen').target_screen = 'reshoot_screen'
            self.screen_manager.transition.direction = 'left'
            self.screen_manager.current = target_screen
        else:
            self.screen_manager.transition.direction = 'left'
            self.screen_manager.current = screen_name

    def is_done(self):
        img = '{n:04d}.jpg'.format(n=int(self.target_page))
        print "Looking for", img, "...",
        if os.path.exists(os.path.join(self.book['path'], 'reshooting', img)):
            print "found!"
            return True
        else:
            print "not found!"
            return False
