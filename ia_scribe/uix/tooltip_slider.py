from kivy.core.window import Window
from kivy.metrics import sp
from kivy.properties import BooleanProperty
from kivy.uix.slider import Slider

from ia_scribe.uix.behaviors.tooltip import TooltipBehavior


class TooltipSlider(TooltipBehavior, Slider):
    '''Slider which shows tooltip when mouse indicator is over slider's knob
    or when touch starts moving the knob.
    '''

    touch_moving = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.bind(touch_moving=self._update_tooltip_label)
        super(TooltipSlider, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        if super(TooltipSlider, self).on_touch_down(touch):
            if not touch.is_mouse_scrolling:
                self.touch_moving = self.hovered = True
            elif self.collide_mouse_pos(*Window.mouse_pos):
                self.hovered = True
            else:
                self.hovered = False
            return True

    def on_touch_move(self, touch):
        if super(TooltipSlider, self).on_touch_move(touch):
            self._reposition_tooltip_label()
            return True

    def on_touch_up(self, touch):
        if super(TooltipSlider, self).on_touch_up(touch):
            self.touch_moving = False
            return True

    def on_value_pos(self, slider, pos):
        if not self.disabled:
            self.hovered = self.collide_mouse_pos(*Window.mouse_pos)

    def collide_mouse_pos(self, x, y):
        # Collide with slider's knob
        pos = self.value_pos
        if not(pos[1] <= y <= pos[1] + sp(32)):
            return False
        if not(pos[0] - sp(16) <= x <= pos[0] + sp(16)):
            return False
        return True

    def _update_tooltip_label(self, *args):
        label_parent = self._tooltip_label.parent
        hovered, moving = self.hovered, self.touch_moving
        if (hovered or moving) and not (self.disabled or label_parent):
            window = self.get_tooltip_window()
            if window:
                self._tooltip_label.bind(size=self._reposition_tooltip_label)
                window.add_widget(self._tooltip_label)
                self._reposition_tooltip_label()
        elif (not(hovered or moving) or self.disabled) and label_parent:
            self._tooltip_label.unbind(size=self._reposition_tooltip_label)
            label_parent.remove_widget(self._tooltip_label)

    def _reposition_tooltip_label(self, *args):
        super(TooltipSlider, self)._reposition_tooltip_label()
        label = self._tooltip_label
        window = label.parent
        center_x = self.value_pos[0]
        half_label_width = label.width / 2.0
        if center_x + half_label_width > window.width:
            center_x = window.width - half_label_width
        if center_x - half_label_width < 0:
            center_x = half_label_width
        label.center_x = center_x
