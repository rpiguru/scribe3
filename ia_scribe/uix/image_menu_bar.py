from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import OptionProperty, ListProperty
from kivy.uix.gridlayout import GridLayout

from ia_scribe.uix.behaviors.tooltip import TooltipControl

Builder.load_file(join(dirname(__file__), 'image_menu_bar.kv'))


class ImageMenuBar(TooltipControl, GridLayout):
    '''Menu bar showing options for image.

    :Events:

        `on_option_select`: option
            Dispatched when option is selected from menu.
    '''

    orientation = OptionProperty('left', options=['left', 'right'])
    '''Orientation of menu items which can be:

        'left' - export button, view source file button
        'right' - view source file button, export button

    Defaults to 'left'.
    '''

    background_color = ListProperty([0.92, 0.92, 0.92, 1.0])
    '''Menu background color which defaults to [0.92, 0.92, 0.92, 1.0].
    '''

    __events__ = ('on_option_select',)

    def on_orientation(self, menu, orientation):
        children = self.children[:]
        self.clear_widgets()
        for child in children:
            self.add_widget(child)

    def on_option_select(self, option):
        pass
