from os.path import join, dirname

from kivy.lang import Builder
from kivy.properties import ObjectProperty, ListProperty, NumericProperty
from kivy.uix.image import Image

Builder.load_file(join(dirname(__file__), 'image_zoom.kv'))


class ImageZoom(Image):
    '''A subclass of kivy.uix.image.Image that adds a zoom box / loupe.

    The zoom box shows a magnified version of the image texture. The zoom
    ratio is currently hardcoded below, and is set to
    box_radius/tex_radius = 2x.

    Note that this is the zoom ratio is currently set to a multiple of the
    original pixel texture, but the image shown to the user is resizable.
    Resizing the app window does not change the zoom ratio, but it probably
    should.

    zoom_pos is the center of the pixels shown in the zoom box, but it is
    an offset into the scaled image rendered to the user, which has
    size=self.size

    When we need to set upper and lower limits for zoom_pos, we need to talk
    about the offset that zoom_pos represents in the raw texture, which has
    size=self.texture_size. We can call this offset zoom_pos_tex:

        (zoom_pos/size)*texture_size = zoom_pos_tex

    In the X direction, we need to limit zoom_pos_tex to be between
    +tex_radius and texture_width-tex_radius. There for, we need to set the
    lower bound to be:

        (zoom_pos_min/ui_size)*texture_size = tex_radius
    so:
        zoom_pos_min = tex_radius * ui_size / texture_size

    And we need to set the upper limit to be:

        (zoom_pos_max/ui_size)*texture_size = texture_size-tex_radius
    so:
        zoom_pos_max = (texture_size-tex_radius) * ui_size / texture_size

    These zoom_pos limits are set in on_touch_move()

    In order to see the edges of the image of the zoom_box, the zoom_box would
    have to be placed the bounds of the image (if the zoom_ratio is > 1).
    However, we want to limit the zoom_box to be within the bounds of the
    image. In order to accomplish this, we don't allow the zoom_box to travel
    past the image bound, but even if the zoom_box is stuck to the edge, we
    allow the user to continue to drag and set zoom_pos, until zoom_pos is
    hits the limits described above.

    The zoom_box limits are set in zoom_box_bounds()
    '''

    start_pos = ObjectProperty(None, allownone=True)
    zoom_init = ListProperty([0, 0])
    zoom_pos = ListProperty([100, 100])
    box_radius = NumericProperty(50)
    tex_radius = NumericProperty(25)
    new_texture_size = ListProperty([0, 0])

    def get_texture(self, tex, pos, size):
        if tex is None:
            return None
        if self.texture_size[0] <= 32:
            #Don't zoom the loading image
            # This is a hack that shows just a single pixel from the loading image.
            # Since the edge pixels of the loading image are all transparent, this
            # just has the effect of making the zoom box transparent during loading.
            return tex.get_region(0, 0, 1, 1)

        percent_x = (pos[0]) / size[0]
        percent_y = (pos[1]) / size[1]
        tex_x = int(percent_x * self.texture_size[0])
        tex_y = int(percent_y * self.texture_size[1])
        #print 'get_tex', pos, size, self.size, self.texture_size, percent_x, percent_y, tex_x, tex_y
        return tex.get_region(tex_x - self.tex_radius, tex_y - self.tex_radius,
                              self.tex_radius * 2, self.tex_radius * 2)

    def on_touch_down(self, touch, *args):
        #print 'touch down!', touch, args
        if self.collide_point(touch.pos[0], touch.pos[1]):
            #print ' COLLIDE!'
            self.start_pos = touch.pos
            self.zoom_init = self.zoom_pos
            return True

    def on_touch_move(self, touch, *args):
        #print 'touch move!', touch, args

        box_radius_d2 = self.box_radius*.5 + 4; #we zoom the texture 2x
        if self.start_pos is not None:
            delta_x = touch.pos[0] - self.start_pos[0]
            delta_y = touch.pos[1] - self.start_pos[1]
            size = self.norm_image_size
            # please see the docstring for this class for the derivation of these limits
            texture_size = self.texture_size if self.texture else size
            min_x = float(self.tex_radius)*size[0]/texture_size[0]
            min_y = float(self.tex_radius)*size[1]/texture_size[1]
            max_x = size[0]*(1-float(self.tex_radius)/texture_size[0])
            max_y = size[1]*(1-float(self.tex_radius)/texture_size[1])

            x = min(max(min_x, self.zoom_init[0]+delta_x), max_x)
            y = min(max(min_y, self.zoom_init[1]+delta_y), max_y)

            self.zoom_pos = [x, y]
            #print delta_x, delta_y, self.zoom_pos
            return True

    def on_texture_size(self, widget, size):
        if size[0] > 32 and size[1] > 32 and size != self.new_texture_size:
            self.new_texture_size = size

    def on_touch_up(self, touch, *args):
        #print 'touch up!'
        self.start_pos = None

    def zoom_box_bounds(self):
        size = self.norm_image_size
        x = self.center_x - size[0] / 2.0
        y = self.center_y - size[1] / 2.0
        min_x = x
        max_x = x+size[0] - self.box_radius*2
        min_y = y
        max_y = y+size[1] - self.box_radius*2
        new_x = min(max(min_x, x + self.zoom_pos[0]-self.box_radius), max_x)
        new_y = min(max(min_y, y + self.zoom_pos[1]-self.box_radius), max_y)
        return new_x, new_y

    def zoom_box_border(self):
        box = self.zoom_box_bounds()
        return box[0]-2, box[1]-2, self.box_radius*2+2, self.box_radius*2+2
