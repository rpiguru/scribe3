from os.path import join, dirname

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.floatlayout import FloatLayout

from ia_scribe.uix.behaviors.tooltip import TooltipControl

Builder.load_file(join(dirname(__file__), 'spread_menu_bar.kv'))


class SpreadMenuBar(TooltipControl, FloatLayout):

    left_type_button = ObjectProperty()
    right_type_button = ObjectProperty()
    left_number_button = ObjectProperty()
    right_number_button = ObjectProperty()
    left_ppi_button = ObjectProperty()
    right_ppi_button = ObjectProperty()
    delete_button = ObjectProperty()

    use_left_menu = BooleanProperty(True)
    use_right_menu = BooleanProperty(True)

    __events__ = ('on_option_select', 'on_type_button_release',
                  'on_number_button_release')

    def __init__(self, **kwargs):
        self.bind(width=self._update_children_width,
                  children=self._update_children_width)
        super(SpreadMenuBar, self).__init__(**kwargs)
        # Update children on next frame, because kv rule will be loaded
        Clock.schedule_once(self._update_children_width)

    def _update_children_width(self, *args):
        if len(self.children) < 3:
            return
        rc, mc, lc = self.children
        if self.width >= dp(946):
            self._set_absolute_width(lc, mc, rc)
        else:
            self._set_relative_width(lc, mc, rc)

    def _set_relative_width(self, lc, mc, rc):
        lc.col_default_width = rc.col_default_width = 0
        lc.size_hint_x = rc.size_hint_x = 0.403
        mc.size_hint_x = 0.193
        lc.spacing = mc.spacing = rc.spacing = dp(6)
        # Uncomment following lines to enable page_type_button scaling
        # lc.children[-1].size_hint_x = rc.children[0].size_hint_x = 0.303
        # for child in lc.children[:-1] + rc.children[1:]:
        #     child.size_hint_x = 0.1394

    def _set_absolute_width(self, lc, mc, rc):
        lc.size_hint_x = rc.size_hint_x = mc.size_hint_x = None
        lc.width = rc.width = dp(394)
        mc.width = dp(158)
        lc.spacing = mc.spacing = rc.spacing = dp(10)
        lc.col_default_width = rc.col_default_width = dp(36)
        # Uncomment following lines to enable page_type_button scaling
        # lc.children[-1].size_hint_x = rc.children[0].size_hint_x = None
        # lc.children[-1].width = rc.children[0].width = dp(100)
        # for child in lc.children[:-1] + rc.children[1:]:
        #     child.size_hint_x = 1

    def on_option_select(self, side, option):
        pass

    def on_type_button_release(self, side, button):
        pass

    def on_number_button_release(self, side, button):
        pass
