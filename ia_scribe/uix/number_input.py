from os.path import join, dirname

import re
from kivy.lang import Builder
from kivy.properties import NumericProperty, ObjectProperty

from ia_scribe.uix.behaviors.tooltip import TooltipBehavior
from ia_scribe.uix.text_input import TextInput

Builder.load_file(join(dirname(__file__), 'number_input.kv'))


class NumberInput(TooltipBehavior, TextInput):
    '''Input which accepts number input, but prevent entering if number is not 
    within `min_value` - `max_value` range. 


    .. warning::
    
        When used in KV language instance or in Python code always set range 
        first and then value.
    '''

    value = NumericProperty(0)
    value_type = ObjectProperty(int)
    min_value = ObjectProperty(None, allownone=True)
    max_value = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        self._valid_re = \
            re.compile(r'^(-?[1-9]+\.\d*|-?0\.\d*|-?[1-9]\d*|0|-)$')
        update_text = self.update_text
        self.bind(focus=update_text,
                  value=update_text,
                  value_type=update_text,
                  min_value=update_text,
                  max_value=update_text)
        super(NumberInput, self).__init__(**kwargs)
        self.text = str(self.value_type(self.value))

    def insert_text(self, substring, from_undo=False):
        text, cursor_col = self.text, self.cursor_col
        new_text = text[:cursor_col] + substring + text[cursor_col:]
        if self.is_valid(new_text):
            return super(NumberInput, self).insert_text(substring, from_undo)

    def is_valid(self, text):
        if not (text and self._valid_re.match(text)):
            return False
        has_min_value = self.min_value is not None
        has_max_value = self.max_value is not None
        if text == '-':
            # Check if negative numbers are allowed
            if has_min_value and self.min_value < 0:
                return True
            elif has_max_value and self.max_value < 0:
                return True
            else:
                # Negative numbers are not allowed
                return False
        value = self.value_type(text)
        if has_min_value and value < self.min_value:
            return False
        if has_max_value and value > self.max_value:
            return False
        return True

    def update_text(self, *args):
        text, focus = self.text, self.focus
        if not (focus or text) or not self.is_valid(text):
            self.text = str(self.value_type(self.value))
        elif not focus and self.is_valid(text):
            if text == '-':
                self.text = str(self.value)
            else:
                self.value = value = self.value_type(text)
                self.text = str(value)
        if not focus:
            self.cursor = [0, 0]
