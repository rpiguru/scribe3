from os.path import join, dirname

from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.properties import ListProperty
from kivy.uix.behaviors import ButtonBehavior

Builder.load_file(join(dirname(__file__), 'pagenumlabel.kv'))


class PageNumLabel(ButtonBehavior, Label):

    rgba = ListProperty([0, 0, 0, 0])

    def set_color(self, color):
        colors = {'clear': [0, 0, 0, 0],
                  'gray':  [.765, .765, .765, 1],
                  'green': [.2, .6, 0, 1],
                  'red':   [1, 0, 0, 1],
                 }
        self.rgba = colors.get(color, colors['clear'])
