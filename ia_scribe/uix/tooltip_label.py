from os.path import join, dirname

from kivy.lang import Builder
from kivy.uix.label import Label

Builder.load_file(join(dirname(__file__), 'tooltip_label.kv'))


class TooltipLabel(Label):
    pass
