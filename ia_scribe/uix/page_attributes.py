from os.path import join, dirname

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty, StringProperty, NumericProperty

Builder.load_file(join(dirname(__file__), 'page_attributes.kv'))


class PageAttributes(BoxLayout):
    page_num = NumericProperty()
    page_assertion = StringProperty()
    popup = ObjectProperty(None)
    callback = ObjectProperty(None)

    def __init__(self, page_num, page_type, side, page_assertion, **kwargs):
        super(PageAttributes, self).__init__(**kwargs)
        self.page_num = page_num
        self.side = side
        if page_assertion is not None:
            self.page_assertion = str(page_assertion)

        for button in self.ids._buttons.children:
            if button.text == page_type:
                button.state = 'down'
            else:
                button.state = 'normal'
            # Don't allow user to change page_type of foldouts
            # if page_type == 'Foldout':
            #    button.disabled = True

    def save(self):
        page_type = 'Normal'
        for button in self.ids._buttons.children:
            if button.state == 'down':
                page_type = button.text
                break

        page_assertion = None
        if self.ids._page_num.text:
            try:
                page_assertion = int(self.ids._page_num.text)
            except ValueError:
                label = Label(text='Page Number must be an integer')
                error = Popup(title='Page Num Error', content=label,
                              size_hint=(None, None), size=(400, 300))
                error.open()
                return

        if (page_assertion is not None) and (page_assertion < 1):
            label = Label(text='Page Numbers must be larger than 1')
            error = Popup(title='Page Num Error', content=label,
                          size_hint=(None, None), size=(400, 300))
            error.open()
            return

        self.callback(self.page_num, page_type, self.side, page_assertion)
        self.popup.dismiss()

    def cancel(self):
        self.popup.dismiss()


class PageAttrButton(ToggleButton):
    def on_press(self):
        # Ensure at least one button is selected by
        # re-selecting previously enabled buttons
        if self.state == 'normal':
            self.state = 'down'
