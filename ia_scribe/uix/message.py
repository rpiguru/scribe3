import glob
import shutil
import webbrowser
from functools import partial

from ia_scribe.upload.upload_status import UploadStatus
from kivy.adapters.listadapter import ListAdapter
from kivy.clock import Clock
from kivy.logger import Logger
from concurrent import futures
from ia_scribe.uix.file_chooser import FileChooser
from kivy.event import EventDispatcher
from kivy.uix.listview import ListItemButton
from kivy.uix.listview import ListView
from kivy.uix.popup import Popup
import os
import re
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from os.path import join, dirname
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, DictProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file(join(dirname(__file__), 'message.kv'))


# ScribeMessage
# _________________________________________________________________________________________

class ScribeMessage(BoxLayout):
    text = StringProperty()
    button_text = StringProperty('OK')
    trigger_func = ObjectProperty(None)
    popup = ObjectProperty(None)
    pass


# ScribeMessageOKCancel
# _________________________________________________________________________________________
class ScribeMessageOKCancel(BoxLayout):
    text = StringProperty()
    ok_text = StringProperty('OK')
    cancel_text = StringProperty('Cancel')
    data = ObjectProperty(None)
    trigger_func = ObjectProperty(None)
    popup = ObjectProperty(None)
    pass


# ScribeMessageLearnMode
# _________________________________________________________________________________________
class ScribeMessageLearnMore(BoxLayout):
    popup = ObjectProperty(None)
    # trigger_func = ObjectProperty(None)

    def open_link(self, link):
        print self
        webbrowser.open(link)
        return
    pass


# ScribeErrorMessage
# _________________________________________________________________________________________
class ScribeError(BoxLayout):
    text = StringProperty()
    error_msg = StringProperty()
    button_text = StringProperty()
    popup = ObjectProperty(None)
    pass


# ScribeErrorMessage2
# _________________________________________________________________________________________
class ScribeErrorMessage2(BoxLayout):
    error_msg = StringProperty()
    button_text = StringProperty()
    trigger_func = ObjectProperty(None)
    button_text2 = StringProperty()
    trigger_func2 = ObjectProperty(None)
    popup = ObjectProperty(None)
    pass


class IdentifierReservedMessage(BoxLayout):

    identifier = StringProperty()
    book_path = StringProperty()
    status_label = ObjectProperty()
    screen_manager = ObjectProperty()
    scribe_widget = ObjectProperty()
    popup = ObjectProperty()
    message = StringProperty()

    def __init__(self, **kwargs):
        super(IdentifierReservedMessage, self).__init__(**kwargs)
        if not self.identifier:
            self.on_identifier(self, self.identifier)

    def on_identifier(self, widget, identifier):
        print('filip: Identifier set', identifier)
        self.message = \
            'The identifier you selected ({0}) was not available ' \
            'for upload.The book is back in scribing mode, ' \
            'and you can try a new one. ' \
            'No files have been uploaded yet.'.format(identifier)

    def on_open_button(self, *args):
        capture_screen = self.scribe_widget.ids['_capture_screen']
        capture_screen.book_dir = self.book_path
        capture_screen.status_label = self.status_label
        try:
            models, ports = self.scribe_widget.cameras.get_cameras()
        except:
            target_screen = 'calibration_screen'
            self.screen_manager.transition.direction = 'left'
            self.screen_manager.current = target_screen
            self.popup.dismiss()
            return
        target_screen = 'capture_screen'
        camera_ports = self.scribe_widget.cameras.camera_ports
        print 'Camera ports in reopen_book:', camera_ports
        print 'Ports param', ports
        if camera_ports['left'] not in ports:
            target_screen = 'calibration_screen'
        if camera_ports['right'] not in ports:
            target_screen = 'calibration_screen'
        if (camera_ports['foldout'] is not None) and (
                camera_ports['foldout'] not in ports):
            target_screen = 'calibration_screen'
        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = target_screen
        self.popup.dismiss()

    def on_cancel_button(self, *args):
        self.popup.dismiss()


# ReopenUploadMessage
# this class manage reopening or the upload of a book
# it's the window appearing when you click on a book on the book list
# continue scribing, upload, export, delete
# _________________________________________________________________________________________
class ReopenUploadMessage(BoxLayout):
    trigger_func = ObjectProperty(None)
    popup = ObjectProperty(None)

    def __init__(self, screen_manager, scribe_widget, book_path, label, **kwargs):
        super(ReopenUploadMessage, self).__init__(**kwargs)
        self.screen_manager = screen_manager
        self.scribe_widget = scribe_widget
        self.book_path = book_path
        self.status_label = label
        self._book_export = None
        self.ids.id_label.text = "UUID: " + str(book_path.split('/')[-1:][0])

    # repoen_book()
    # _____________________________________________________________________________________
    def reopen_book(self):
        capture_screen = self.scribe_widget.ids['_capture_screen']
        capture_screen.book_dir = self.book_path
        capture_screen.status_label = self.status_label
        try:
            models, ports = self.scribe_widget.cameras.get_cameras()
        except:
            target_screen = 'calibration_screen'
            self.screen_manager.transition.direction = 'left'
            self.screen_manager.current = target_screen
            self.popup.dismiss()
            return

        target_screen = 'capture_screen'
        camera_ports = self.scribe_widget.cameras.camera_ports
        print "Camera ports in reopen_book:", camera_ports
        print "Ports param", ports
        if camera_ports['left'] not in ports:
            target_screen = 'calibration_screen'
        if camera_ports['right'] not in ports:
            target_screen = 'calibration_screen'
        if (camera_ports['foldout'] is not None) and (camera_ports['foldout'] not in ports):
            target_screen = 'calibration_screen'

        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = target_screen
        self.popup.dismiss()

    # queue_upload()
    # _____________________________________________________________________________________
    def queue_upload(self):
        self.popup.dismiss()

        # check for missing images
        err = None
        jpgs = sorted(glob.glob(join(self.book_path, '*.jpg')))
        numbered_jpgs = [os.path.basename(x) for x in jpgs if re.match('\d+\.jpg$', os.path.basename(x))]
        first_img = int(os.path.basename(numbered_jpgs[0]).strip('.jpg'))
        last_img = int(os.path.basename(numbered_jpgs[-1]).strip('.jpg'))

        if (first_img != 0) and (first_img != 1):
            # TODO: do not worry about image 0 for now since we never show it to the user,
            # but we do want the user to capture a color card there
            err = 'Cover image is missing!'
        elif (last_img%2) != 1:
            # image missing from the end
            err = 'Image #{n} is missing!'.format(n=last_img+1)
        else:
            if (last_img-first_img+1) != len(numbered_jpgs):
                # TODO: do not worry about image for now 0 since we never show it to the user,
                # but we do want the user to capture a color card there
                err = 'An image is missing'
                s = set(numbered_jpgs)
                for i in range (1, last_img+1):
                    jpg = '{n:04d}.jpg'.format(n=i)
                    if jpg not in s:
                        err = 'Image {n} is missing!'.format(n=jpg)
                        break

        if err is not None:
            text = err + '\n\nYou must reopen this book and reshoot\nthe missing image before uploading!'
            msg = Label(text=text)
            popup = Popup(title='Missing Image!', content=msg,
                          size_hint=(None, None), size=(400, 300))
            popup.open()
            return

        # no missing images
        msg_box = ConfirmUploadMessage(self.book_path, self.status_label)
        popup = Popup(title='Confirm Upload', content=msg_box,
                      size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        popup.open()

    # queue_delete()
    # _____________________________________________________________________________________
    def queue_delete(self):
        self.popup.dismiss()

        msg_box = ScribeMessageOKCancel()
        msg_box.text = 'Do you want to DELETE this book?  You cannot undo this action. \n' \
                       'The images that you have captured will be removed permenantly.'
        msg_box.ok_text = 'DELETE BOOK'
        msg_box.ids['_ok_button'].background_color = [1, 0, 0, 1]
        msg_box.trigger_func = self.confirm_delete
        popup = Popup(title='Delete book?', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(600, 300))
        msg_box.popup = popup
        popup.open()

    # confirm_delete()
    # _____________________________________________________________________________________
    def confirm_delete(self, popup):
        self.status_label.text = 'Delete Queued'
        print 'writing ', join(self.book_path, 'delete_queued')
        open(join(self.book_path, 'delete_queued'), 'a').close()
        popup.dismiss()

    # export()
    # _____________________________________________________________________________________
    def export(self):
        self.popup.dismiss()
        self._book_export = export = BookExport(self.book_path)
        export.bind(on_finish=self.on_export_finish)
        export.start()

    def on_export_finish(self, *args):
        self._book_export = None


# ConfirmUploadMessage
# this class manages the UI dialog confirm upload message and put a book in the upload queue
# ___________________________________________________________________________________________
class ConfirmUploadMessage(BoxLayout):
    popup = ObjectProperty(None)
    def __init__(self, book_path, label, **kwargs):
        super(ConfirmUploadMessage, self).__init__(**kwargs)
        self.book_path = book_path
        self.status_label = label


    def cancel(self):
        self.popup.dismiss()


    def confirm_upload(self):
        self.status_label.text = 'Upload Queued'
        open(join(self.book_path, 'upload_queued'), 'a').close()
        self.popup.dismiss()


# ViewBookMessage
# this class manage the dialog when you click on a book alread uploaded
# TODO: should be integrated in ViewOrDeleteBookMessage
# _________________________________________________________________________________________
class ViewBookMessage(BoxLayout):
    popup = ObjectProperty(None)
    awaiting_qa_msg = StringProperty()
    book = DictProperty()  # kv file references this dict and is loaded before __init__() is run

    # init()
    # _____________________________________________________________________________________
    def __init__(self, book, **kwargs):
        super(ViewBookMessage, self).__init__(**kwargs)
        self.book = book

        self.url = 'https://archive.org/details/{i}'.format(i=book.get('identifier', ''))

    # open_web()
    # _____________________________________________________________________________________
    def open_web(self, popup):
        webbrowser.open(self.url)
        popup.dismiss()


# ViewOrDeleteBookMessage
# UI dialog: view, delete, open, export
# _________________________________________________________________________________________
class ViewOrDeleteBookMessage(BoxLayout):
    popup = ObjectProperty(None)
    awaiting_qa_msg = StringProperty()
    book = DictProperty()  # kv file references this dict and is loaded before __init__() is run

    # init()
    # _____________________________________________________________________________________
    def __init__(self, book, **kwargs):
        super(ViewOrDeleteBookMessage, self).__init__(**kwargs)
        self.book = book
        self.url = 'https://archive.org/details/{i}'.format(i=book.get('identifier', ''))
        self._book_export = None
        if book.get('status') == UploadStatus.uploaded_awaiting_qa.value:
            self.awaiting_qa_msg += '\n\nHowever, this book is still awaiting QA,\nand is in an intermediate state.'
            delete_button = Button(text='Delete Local Copy', size_hint_y=None, height=40, background_color=[1,0,0,1])
            delete_button.bind(on_press=self.queue_delete)
            self.add_widget(delete_button)

    # open_web()
    # _____________________________________________________________________________________
    def open_web(self, popup):
        webbrowser.open(self.url)
        popup.dismiss()

    # queue_delete()
    # _____________________________________________________________________________________
    def queue_delete(self, *largs):
        self.popup.dismiss()

        msg_box = ScribeMessageOKCancel()
        msg_box.text = 'This book has been uploaded but has not yet passed the QA process.\n' \
                       'If you delete it, you will not be able to make changes. Are you sure?'
        msg_box.ok_text = 'DELETE BOOK'
        msg_box.ids['_ok_button'].background_color = [1,0,0,1]
        msg_box.trigger_func = self.confirm_delete
        popup = Popup(title='Delete book?', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(600, 300))
        msg_box.popup = popup
        popup.open()

    # confirm_delete()
    # _____________________________________________________________________________________
    def confirm_delete(self, popup):
        self.book.status_label.text = 'Delete Queued'
        print 'writing ', join(self.book['path'], 'delete_queued')
        open(join(self.book['path'], 'delete_queued'), 'a').close()
        popup.dismiss()

    # export()
    # _____________________________________________________________________________________
    def export(self):
        self.popup.dismiss()
        self._book_export = export = BookExport(self.book['path'])
        export.bind(on_finish=self.on_export_finish)
        export.start()

    def on_export_finish(self, *args):
        self._book_export = None


# ExportDialog
# _________________________________________________________________________________________
class ExportDialog(FloatLayout):
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)
    home_dir = StringProperty(os.path.expanduser('~'))
    book_path = StringProperty()
    popup = ObjectProperty(None)

    # do_export()
    # _____________________________________________________________________________________
    def do_export(self, file_path, folder_name):
        print self.book_path, file_path, folder_name
        export_path = join(file_path, folder_name)
        if os.path.exists(export_path):
            self.show_error('The path {p} already exists!\n\nPlease choose a unique name.'.format(p=export_path))
            return

        self.popup.dismiss()
        popup = Popup(title='Exporting book', content=Label(text='Please wait'),
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        popup.open()

        with futures.ThreadPoolExecutor(max_workers=1) as executor:
            f = executor.submit(shutil.copytree, self.book_path, export_path)

        try:
            result = f.result()
            popup.dismiss()
            if result is not None:
                self.show_error('An error occured during export!')
        except Exception:
            popup.dismiss()
            self.show_error('An error occured during export!')

    # show_error()
    # _____________________________________________________________________________________
    def show_error(self, msg):
        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = Popup(title='Export Error', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        msg_box.trigger_func = self.popup_dismiss
        popup.open()

    # popup_dismiss()
    # _____________________________________________________________________________________
    @staticmethod
    def popup_dismiss(popup):
        popup.dismiss()


class ReScribe(BoxLayout):
    popup = ObjectProperty(None)
    book = DictProperty()

    # init()
    # _____________________________________________________________________________________
    def __init__(self, screen_manager, scribe_widget, book, **kwargs):
        super(ReScribe, self).__init__(**kwargs)
        self.screen_manager = screen_manager
        self.scribe_widget = scribe_widget
        # self.status_label = label
        self.book = book

    def show_notes(self):
        self.screen_manager.transition.direction = 'left'
        self.screen_manager.get_screen("rescribe_screen").book = self.book
        self.screen_manager.get_screen("rescribe_screen").scribe_widget = self.scribe_widget
        self.screen_manager.get_screen("rescribe_screen").screen_manager = self.screen_manager
        self.screen_manager.current = 'rescribe_screen'
        self.popup.dismiss()

    def reopen_book(self):
        capture_screen = self.scribe_widget.ids['_capture_screen']
        capture_screen.book_dir = self.book['path']
        models, ports = self.scribe_widget.cameras.get_cameras()

        target_screen = 'capture_screen'
        camera_ports = self.scribe_widget.cameras.camera_ports
        if camera_ports['left'] not in ports:
            target_screen = 'calibration_screen'
        if camera_ports['right'] not in ports:
            target_screen = 'calibration_screen'
        if (camera_ports['foldout'] is not None) and (camera_ports['foldout'] not in ports):
            target_screen = 'calibration_screen'

        self.screen_manager.transition.direction = 'left'
        self.screen_manager.current = target_screen
        self.popup.dismiss()

    def upload_corrections_down(self):
        pass

    def reshoot(self):
        self.show_notes()

    @staticmethod
    def popup_dismiss(popup):
        popup.dismiss()


class BookExport(EventDispatcher):
    '''Class handling book export process.

    :Events:
        `on_finish`:
            Dispatched from :meth:`finish` to notify that book export is
            completed or canceled.
    '''

    __events__ = ('on_finish',)

    def __init__(self, book_dir):
        self.book_dir = book_dir

    def start(self, *args):
        filechooser = FileChooser()
        filechooser.bind(on_selection=self.on_directory_selection)
        filechooser.choose_dir(title='Select or create an empty directory',
                               icon='./images/window_icon.png',
                               path=join(os.path.expanduser('~'), ''))

    def on_directory_selection(self, chooser, selection):
        if selection:
            export_path = selection[0]
            if os.listdir(export_path):
                self.show_error('Directory "{}" is not empty.\n\nSelect '
                                'an empty directory.'.format(export_path))
                return
            self.export(export_path)
        else:
            self.finish()

    def export(self, export_path):
        popup = Popup(title='Exporting book', content=Label(text='Please wait'),
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        popup.open()
        with futures.ThreadPoolExecutor(max_workers=1) as executor:
            f = executor.submit(self.copy_files, self.book_dir, export_path)
        try:
            result = f.result()
            popup.dismiss()
            if result is not None:
                self.show_error('An error occured during export!')
            else:
                Logger.info('BookExport: Exported book from "{}" to "{}"'
                            .format(self.book_dir, export_path))
                self.finish()
        except Exception:
            Logger.exception('BookExport: Failed to export book from '
                             '"{}" to "{}"'.format(self.book_dir, export_path))
            popup.dismiss()
            self.show_error('An error occured during export!')

    def copy_files(self, src, dst, symlinks=False, ignore=None):
        '''Copied from shutil.copytree, but `os.makedirs(dst)` is not called,
        so it assumes that `dst` exists.
        '''
        names = os.listdir(src)
        if ignore is not None:
            ignored_names = ignore(src, names)
        else:
            ignored_names = set()
        errors = []
        for name in names:
            if name in ignored_names:
                continue
            srcname = os.path.join(src, name)
            dstname = os.path.join(dst, name)
            try:
                if symlinks and os.path.islink(srcname):
                    linkto = os.readlink(srcname)
                    os.symlink(linkto, dstname)
                elif os.path.isdir(srcname):
                    shutil.copytree(srcname, dstname, symlinks, ignore)
                else:
                    # Will raise a SpecialFileError for unsupported file types
                    shutil.copy2(srcname, dstname)
            # catch the Error from the recursive copytree so that we can
            # continue with other files
            except shutil.Error, err:
                errors.extend(err.args[0])
            except EnvironmentError, why:
                errors.append((srcname, dstname, str(why)))
        try:
            shutil.copystat(src, dst)
        except OSError, why:
            if shutil.WindowsError is not None and isinstance(why, shutil.WindowsError):
                # Copying file access times may fail on Windows
                pass
            else:
                errors.append((src, dst, str(why)))
        if errors:
            raise shutil.Error, errors

    def show_error(self, msg):
        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = Popup(title='Export Error', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None),
                      size=(400, 300))
        popup.bind(on_dismiss=self.finish)
        msg_box.popup = popup
        msg_box.trigger_func = self.on_ok_button_release
        popup.open()

    def on_ok_button_release(self, popup):
        popup.unbind(on_dismiss=self.finish)
        popup.dismiss()
        self.start()

    def finish(self, *args):
        self.dispatch('on_finish')

    def on_finish(self):
        pass


# UniversalIDDialog
# this is the widget to that shows a list of possible identifiers
# obtained by querying the ISBN-to-id API on archive.org
# _________________________________________________________________________________________
class UniversalIDDialog(FloatLayout):
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)
    book_path = StringProperty()
    popup = ObjectProperty(None)
    id_list = ListProperty(None)
    callback = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(UniversalIDDialog, self).__init__(**kwargs)
        adapter = ListAdapter(
                data=self.id_list,
                selection_mode='single',
                allow_empty_selection=False,
                cls=ListItemButton)
        # adapter.bind(on_selection_change = self.selected_callback)
        self.listview = ListView(adapter=adapter)
        rootbox = self.ids['_list_box']
        rootbox.add_widget(self.listview)

    def button_callback(self):
        print "SELECTED ITEM", self.listview.adapter.selection[0].text
        print self.callback
        Clock.schedule_once(partial(self.callback, self.listview.adapter.selection[0].text))

    # show_error()
    # _____________________________________________________________________________________
    def show_error(self, msg):
        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = Popup(title=' Error', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(400, 300))
        msg_box.popup = popup
        msg_box.trigger_func = self.popup_dismiss
        popup.open()

    # popup_dismiss()
    # _____________________________________________________________________________________
    def popup_dismiss(self, popup):
        popup.dismiss()
