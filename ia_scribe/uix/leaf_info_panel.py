from os.path import join, dirname

from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import OptionProperty, StringProperty, NumericProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label

Builder.load_file(join(dirname(__file__), 'leaf_info_panel.kv'))


class LeafInfoPanel(GridLayout):

    halign = OptionProperty('left', options=['left', 'center', 'right',
                            'justify'])
    leaf_number = StringProperty('NULL')
    ppi = StringProperty('NULL')
    capture_time = StringProperty('NULL')
    thumb_time = StringProperty('NULL')
    notes = StringProperty('NULL')

    maximum_width = NumericProperty(dp(250))

    def __init__(self, **kwargs):
        self._current_max_width = dp(0)
        self._work_label = Label()
        self._work_label.bind(texture_size=self._update_width)
        self.bind(padding=self._update_width,
                  maximum_width=self._update_width)
        super(LeafInfoPanel, self).__init__(**kwargs)

    def add_widget(self, widget, index=0):
        widget.bind(text=self._on_child_text)
        super(LeafInfoPanel, self).add_widget(widget, index)

    def remove_widget(self, widget):
        widget.unbind(text=self._on_child_text)
        super(LeafInfoPanel, self).remove_widget(widget)

    def _on_child_text(self, child, text):
        self._work_label.text = text
        self._work_label.texture_update()

    def _update_width(self, *args):
        texture_size = self._work_label.texture_size
        padding = self.padding[0] + self.padding[2]
        temp_max = max(self._current_max_width,
                       texture_size[0])
        if temp_max > self._current_max_width:
            self._current_max_width = temp_max
        self.width = min(self.maximum_width,
                         self._current_max_width + padding)


class LeafInfoPanelItem(Label):
    pass
