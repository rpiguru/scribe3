# This subclass of Popup adds an arrow to the left or right side of the popup.
# This is similar to how Kivy Bubble works, but provides the advantages of
# using a Popup, namely the controls behind the popup are deactivated.
from os.path import join, dirname

from kivy.lang import Builder
from kivy.uix.popup import Popup
from kivy.properties import StringProperty

Builder.load_file(join(dirname(__file__), 'popup_bubble.kv'))


class PopupBubble(Popup):

    side = StringProperty()

    def __init__(self, side, **kwargs):
        super(PopupBubble, self).__init__(**kwargs)
        self.side = side

    def tri_points(self, pos, size):
        shadow = 8
        top_offset = 10
        tri_size = 10
        top = self.y + self.height - top_offset

        if self.side == 'right':
            right = self.x + self.width - shadow
            return right, top, right+10, top-tri_size, right, top-tri_size*2
        else:
            right = self.x + shadow
            return right, top, right-10, top-tri_size, right, top-tri_size*2
