from os.path import join, dirname, exists

from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file(join(dirname(__file__), 'messages.kv'))


class DeleteSpreadMessage(BoxLayout):

    popup = ObjectProperty(None)

    def __init__(self, capture_screen, capture_spread, **kwargs):
        super(DeleteSpreadMessage, self).__init__(**kwargs)
        self.capture_screen = capture_screen
        self.capture_spread = capture_spread

    def cancel(self):
        self.capture_spread.disable_foldout_mode()
        self.popup.dismiss()

    def delete_spread_confirmed(self):
        self.capture_spread.disable_foldout_mode()
        self.ids._delete_button.disabled = True
        self.ids._cancel_button.disabled = True
        self.ids._label.text = 'Please wait while we delete this spread...'
        capture_screen = self.capture_screen
        book_dir = capture_screen.book_dir
        if not exists(book_dir):
            self.ids._label.text = 'ERROR! Book Dir does not exist'
            self.ids._cancel_button.disabled = False
            return
        capture_screen.delete_current_spread_and_rename()
        self.popup.dismiss()
