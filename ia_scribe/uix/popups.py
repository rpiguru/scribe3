from kivy.lang import Builder
from kivy.properties import NumericProperty, ObjectProperty
from kivy.uix.popup import Popup
from os.path import join, dirname

Builder.load_file(join(dirname(__file__), 'popups.kv'))


class EditPPIPopup(Popup):

    default_ppi = NumericProperty(300)
    extra_data = ObjectProperty(allownone=True)

    __events__ = ('on_submit',)

    def on_open(self):
        super(EditPPIPopup, self).on_open()
        ppi_input = self.ids.ppi_input
        ppi_input.value = self.default_ppi
        ppi_input.do_cursor_movement('cursor_end')
        ppi_input.focus = True
        ppi_input.select_all()

    def on_submit(self, value):
        self.dismiss()
