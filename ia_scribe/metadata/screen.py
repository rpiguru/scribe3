import copy

import os
import scribe_globals
from ia_scribe.uix.metadata import MetadataTextInput
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from utils.metadata import get_metadata, set_metadata


Builder.load_file(os.path.join(os.path.dirname(__file__), 'metadata.kv'))


class MetadataScreen(Screen):

    def __init__(self, **kwargs):
        super(MetadataScreen, self).__init__(**kwargs)

    def on_enter(self):
        # Get scan center metadata.xml from disk
        config = get_metadata(scribe_globals.scancender_metadata)
        # Get all widgets
        widgets = [x for x in self.ids['_metadata'].children if isinstance(x, MetadataTextInput)]
        # Populate
        for widget in widgets:
            val = config.get(widget.metadata_key)
            self.set_val(widget, val)

    def save_metadata(self):
        # Get the metadata from the default location
        # TODO: Try catch, dependency on figuring out what the best course of action would be
        config = get_metadata(scribe_globals.scancender_metadata)
        config.pop("collection", None)
        # Make a copy of it
        new_config = copy.deepcopy(config)

        # For each of them, get the value in the textbox  and assign it to the new copy of the dict
        for widget in self.ids['_metadata'].children:
            if widget.__class__.__name__ in ['MetadataTextInput', 'EmailTextInput']:
                val = self.get_val(widget)
                print val
                new_config[widget.metadata_key] = val

        print("Read from widget: {0}".format(new_config))

        # if the two dics are different, set the new one as default
        if cmp(config, new_config) != 0:
            set_metadata(new_config, scribe_globals.scancender_metadata)

    @staticmethod
    def get_scribe_version():
        return 'ttScribe v{v}'.format(v=scribe_globals.release_version)

    def remove_buttons(self):
        for _id in ['btn_edit', 'lb_edit', 'btn_save', 'lb_save']:
            self.ids['_metadata'].remove_widget(self.ids[_id])

    def validate(self):
        """
        Walk through all metadata input widgets and validate values
        :return:
        """
        is_valid = True
        for widget in self.ids['_metadata'].children:
            if widget.__class__.__name__ in ['MetadataTextInput', 'EmailTextInput']:
                if len(widget.text.strip()) == 0:
                    is_valid = False
                    widget.mark_as_error()
        return is_valid
