"""

Working code:

    b47286854
    b47286866
    b47286878
    b47286878
    b47286891
    b47286908
    b4728691x
    b47286921
    b47286933
    b47286945
    b47286957
    b47286969
    b47286994
    b47287007
    b47287019
    b47287020
    b47287020
    b47286957

    Sample rul:
    http://books-yaz.archive.org/book/marc/get_marc.php?term=local&value=b47286866&catalog=The%20University%20of%20Western%20Ontario%20Catalog

"""
import copy
import glob
import json
import os

import requests
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition

import scribe_globals
from ia_scribe.uix.metadata import MetadataTextInput
from utils.metadata import get_metadata, get_catalogs_from_metadata
from utils.metadata import set_metadata

cur_dir = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(cur_dir + '/MARC.kv')


class NotifyPopup(Popup):
    pass


class MARCDialog(Popup):
    """
    Sample rul:
    http://books-yaz.archive.org/book/marc/get_marc.php?term=local&value=b47286866&catalog=The%20University%20of%20Western%20Ontario%20Catalog
    """
    current_title = StringProperty()  # Store title of current screen
    screen_names = ListProperty([])
    screens = ObjectProperty(None)  # Dict of all screens
    hierarchy = ListProperty([])

    sm = ObjectProperty(None)

    n_popup = ObjectProperty(None)

    dd_type = ObjectProperty(None)
    dd_cat = ObjectProperty(None)

    response = ObjectProperty(None)

    inst_capture_cover = ObjectProperty(None)       # Instance of CaptureCover widget...

    def __init__(self, capture_screen, inst_capture_cover, **kwargs):

        # self.root = Builder.load_file('ia_scribe/marc/MARC.kv')

        super(MARCDialog, self).__init__(**kwargs)

        self.capture_screen = capture_screen
        self.inst_capture_cover = inst_capture_cover

        self.n_popup = NotifyPopup()
        self.dd_type = DropDown()
        self.dd_cat = DropDown()

        self.load_screen()

        self.go_screen('search', 'left')

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        self.screens = dict()
        available_screens = []

        full_path_screens = glob.glob("ia_scribe/marc/screens/*.kv")
        if len(full_path_screens) == 0:
            full_path_screens = glob.glob("screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen

        self.sm = self.ids['sm']
        return True

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        print self.height

        if dest_screen == 'search':
            self.auto_dismiss = True
            self.add_dropdowns()
            self.initialize_search_screen()
        elif dest_screen == 'result':
            self.auto_dismiss = False
            self.parse_result()

        self.sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        if screen.name != self.current_title:
            self.sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def initialize_search_screen(self):
        """
        Initiazlie search screen...
        :return:
        """
        # config = get_metadata(self.capture_screen.book_dir, 'metasource.xml')
        # print ">>>>> Metasource.xml >>>" + str(config)
        # # Get all widgets
        # widgets = [x for x in self.ids._metadata.children if isinstance(x, (MetadataTextInput))]
        # # Populate
        # for widget in widgets:
        #     val = config.get(widget.metadata_key)
        #     self.set_val(widget, val)
        self.screens['search'].ids['txt_query'].text = ''

    def on_dismiss(self):
        self.content.clear_widgets()

    def add_dropdowns(self):
        """
        Added child widgets to dropdown widgets
        :return:
        """
        self.dd_type.clear_widgets()
        self.dd_cat.clear_widgets()

        # Added type of MARC
        l_type = ['local', 'isbn', 'standard', 'call', 'keyword', 'title', 'creator']
        for t in l_type:
            btn = Button(text=t, size_hint_y=None, height=30)
            btn.bind(on_release=lambda btn: self.dd_type.select(btn.text))
            self.dd_type.add_widget(btn)

        t_button = self.screens['search'].ids['btn_dd_type']
        t_button.bind(on_release=self.dd_type.open)
        self.dd_type.bind(on_select=lambda instance, x: setattr(t_button, 'text', x))
        t_button.text = l_type[0]

        catalogs = get_catalogs_from_metadata()
        # Added catalog dropdown
        if len(catalogs) == 0:
            print "There is no configured catalog..."
            return False

        for cat in catalogs:
            btn = Button(text=cat, size_hint_y=None, height=30)
            btn.bind(on_release=lambda btn: self.dd_cat.select(btn.text))
            self.dd_cat.add_widget(btn)

        c_button = self.screens['search'].ids['btn_dd_catalog']
        c_button.bind(on_release=self.dd_cat.open)
        self.dd_cat.bind(on_select=lambda instance, x: setattr(c_button, 'text', x))
        c_button.text = catalogs[0]

    def search(self):
        """
        Send get request
        :return:
        """
        self.cur_cat = self.screens['search'].ids['btn_dd_catalog'].text
        # cur_cat = cur_cat.replace(' ', '+')

        self.cur_type = self.screens['search'].ids['btn_dd_type'].text
        self.query = self.screens['search'].ids['txt_query'].text
        if len(self.query.strip()) == 0:
            self.show_notification('Error\nPlease input query value.')
            return False

        # http://www-judec.archive.org/book/marc/get_marc.php?term=isbn&value=9780750700160&catalog=Independence+Seaport+Museum+-+WorldCat+access
        self.api_url = 'http://books-yaz.archive.org/book/marc/get_marc.php?term='
        try:
            request_url = self.api_url + self.cur_type + '&value=' + self.query + '&catalog=' + self.cur_cat
            # request_url = 'http://books-yaz.archive.org/book/marc/get_marc.php?term=local&value=b47286866&' \
            #               'catalog=The%20University%20of%20Western%20Ontario%20Catalog'
            self.response = json.loads(requests.get(request_url).text, 'ISO-8859-15')
        except requests.exceptions.ConnectionError as e:
            print e
            # TODO: Display Error message
            return False

        if self.response['sts'] == 'OK':
            print self.response
            self.go_screen('result', 'left')

        elif self.response['sts'] == 'ERROR':
            print self.response['error']
            self.show_notification('Invalid value of request\nPlease try again.')

    def parse_result(self):
        metadata = self.response['extracted_metadata']['metadata']
        widgets = [x for x in self.screens['result'].ids['_metadata'].children if isinstance(x, MetadataTextInput)]
        # Populate
        for widget in widgets:
            if widget.metadata_key in metadata.keys():
                # Check value is list or not
                if type(metadata[widget.metadata_key]) == list:
                    tmp = ','.join(metadata[widget.metadata_key])
                else:
                    tmp = metadata[widget.metadata_key]

                if tmp is not None:
                    self.set_val(widget, tmp.encode('ascii', 'ignore'))

        return True

    def show_notification(self, msg, font_size=20):
        """
        Open Notification Popup window with given parameters
        :param msg:
        :param font_size:
        :return:
        """
        self.n_popup.ids['lb_content'].font_size = font_size
        self.n_popup.ids['lb_content'].text = msg
        self.n_popup.open()

    def btn_accept(self):
        """
        Callback function of ACCEPT button on the result screen
        :return:
        """
        self.save_metadata()
        self.inst_capture_cover.pass_metadata(self.response['extracted_metadata']['metadata'])
        self.capture_screen.save_metadata()

    # save_metadata()
    # This method writes metadata to disk
    # Main thread
    def save_metadata(self):
        # Get the metadata from the default location
        # TODO: Try catch, dependency on figuring out what the best course of action would be
        config = get_metadata(self.capture_screen.book_dir, 'catalogs_metadata.xml')
        # Make a copy of it
        new_config = copy.deepcopy(config)
        # Load all textboxs in the interface in a dict
        widgets = [x for x in self.screens['result'].ids['_metadata'].children if isinstance(x, MetadataTextInput)]
        # For each of them, get the value in the textbox  and assign it to the new copy of the dict
        for widget in widgets:
            val = self.get_val(widget)
            new_config[widget.metadata_key] = val

        # if the two dics are different, set the new one as default
        if cmp(config, new_config) != 0:
            set_metadata(new_config, self.capture_screen.book_dir, 'catalogs_metadata.xml')

        # save marc xml
        xml_f_name = self.capture_screen.book_dir + '/marc.xml'
        f = open(xml_f_name, 'w+b')
        try:
            tmp = self.response['marc_xml']
            if tmp is not None:
                f.write(tmp.encode('ascii', 'ignore'))
                print "Saved MARC xml to >>> ", xml_f_name
        except OSError as e:
            print e
        f.close()

        # save marc binary
        xml_f_name = self.capture_screen.book_dir + '/marc.bin'
        f = open(xml_f_name, 'w+b')
        try:
            if self.response['marc_binary'] is not None:
                f.write(self.response['marc_binary'].encode('ascii', 'ignore'))
                print "Saved MARC binary to >>> ", xml_f_name
        except OSError as e:
            print e
        f.close()

        # save metasource
        try:
            metasource = {'zquery': "{0}={1}".format(self.cur_type, self.query), 'target': "{0}".format(self.cur_cat),
                          'service': "archive.org/book/marc/get_marc.php"}
            set_metadata(metasource, self.capture_screen.book_dir, 'metasource.xml', 'metasource')
            print "Saved metasource.xml to >>> {0}{1}".format(self.capture_screen.book_dir + "/", 'metasource.xml')
        except OSError as e:
            print e

        self.dismiss()

    # get_val()
    # wrapper module that returns a MetadataTextInput widget's text content
    # _____________________________________________________________________________________
    @staticmethod
    def get_val(widget):
        if isinstance(widget, MetadataTextInput):
            return widget.text
        return None

    # set_val()
    # wrapper module that sets a MetadataTextInput widget's text content
    # _____________________________________________________________________________________
    @staticmethod
    def set_val(widget, val):
        if val is None:
            return
        if isinstance(widget, MetadataTextInput):
            widget.text = val


class MARC(App):
    """
    Launch app
    """

    catalogs = ['calcas', 'olcas', 'notre_dame', ]
    # catalogs = []

    def __init__(self, **kwargs):
        super(MARC, self).__init__(**kwargs)

    def build(self):
        self.title = 'Do We Want it?'
        return MARCDialog()


if __name__ == '__main__':
    MARC().run()
