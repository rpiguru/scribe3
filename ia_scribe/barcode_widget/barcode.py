# BarcodeWidget
# this is the widget to initialize book metadata
# scaning a barcode or inserting a pre-existing identifier
# _________________________________________________________________________________________
import json
import os
import traceback
import urllib
from os.path import join
import requests
from kivy.lang import Builder
from kivy.network.urlrequest import UrlRequest
from kivy.properties import ObjectProperty
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
import scribe_config
from ia_scribe.uix.message import ScribeMessage, ScribeMessageOKCancel, UniversalIDDialog
from utils.metadata import get_metadata
from utils.util import resource_path

Builder.load_file(os.path.join(os.path.dirname(__file__), 'barcode_widget.kv'))


class BarcodeWidget(BoxLayout):
    capture_screen = ObjectProperty(None)
    loading_image = StringProperty(resource_path('images/image-loading.gif'))
    transparent_image = StringProperty(resource_path('images/image-transparent.png'))

    barcode_input = ObjectProperty()

    # __init__()
    # _____________________________________________________________________________________
    def __init__(self, capture_screen, **kwargs):
        super(BarcodeWidget, self).__init__(**kwargs)
        self.capture_screen = capture_screen
        if self.capture_screen.book_dir is None:
            return
        id_path = join(self.capture_screen.book_dir, 'identifier.txt')
        if os.path.exists(id_path):
            f = open(id_path)
            identifier = f.read().strip()
            f.close()
            self.ids._barcode.text = identifier
            self.ids._button.disabled = True
            self.ids._button_isbn.disabled = True
            preloaded_path = join(self.capture_screen.book_dir, 'preloaded')
            self.ids._barcode.disabled = os.path.exists(preloaded_path)
            self.capture_screen.disable_metadata()

    def load_universal(self):
        self.ids._image.source = self.loading_image
        identifier = self.ids._barcode.text
        archive_ids = []
        if not identifier:
            self.show_error('You must scan an identifier barcode!')
            return

        api_url = 'https://archive.org/book/want/isbn_to_identifier.php?isbn='
        try:
            response = json.loads(requests.get(api_url + identifier, verify=False).text)
            if response['status'] == 'ok':
                archive_ids = [x for x in response['scannable_identifiers']]
                content = UniversalIDDialog(id_list=archive_ids, callback=self.setup_scannable_id)
                self.popup = self.capture_screen.create_popup(
                    title="Select an identifier to scan",
                    content=content, size_hint=(0.7, 0.5)
                )
                content.popup = self.popup
                self.popup.open()

            else:
                content = Label(text="No items were found")
                self.popup = self.capture_screen.create_popup(
                    title="Select an identifier to scan", content=content,
                    size_hint=(0.3, 0.3), auto_dismiss=True
                )
                content.popup = self.popup
                self.popup.open()
        except Exception as e:
            print traceback.print_exc()
            content = Label(
                text="Could not retrieve ISBN.\nCheck the console log for more information.\n\nError type:{0}".format(
                    type(e)), )
            self.popup = self.capture_screen.create_popup(
                title="Error retrieving ISBN", content=content,
                size_hint=(0.3, 0.3), auto_dismiss=True,
            )
            content.popup = self.popup
            self.popup.open()
        self.ids._image.source = self.transparent_image

    def setup_scannable_id(self, identifier, *args):
        self.ids._barcode.text = identifier
        self.ids._button.disabled = True
        self.ids._button_isbn.disabled = True
        self.popup.dismiss()
        try:
            self.load_metadata()
        except:
            print "\n This is really not supposed to happen."

    def print_barcode(self):
        try:
            print "Creating QR Code"
            from utils import label_book
            md = get_metadata(self.capture_screen.book_dir)
            author = [md['creator']] if 'creator' in md else [""]
            title = md['title'] if 'title' in md else [""]
            label_obj = label_book.BookLabel(self.ids._barcode.text, title, author, )
            label_raw = label_obj.create_label(self.ids._barcode.text)
            print(
            "created label object {0}, type: {1},  content:{2}".format(label_raw, type(label_raw), dir(label_raw)))
            # f = tempfile.NamedTemporaryFile(prefix='qrcode', suffix='.png', delete=False)
            l = label_raw.save("/tmp/iascribe-label.png")
            print "saved barcode as", l
            # webbrowser.open(l)
            try:
                import cups
                conn = cups.Connection()
                config = scribe_config.read()
                printer_name = "DYMO_LabelWriter_450_Turbo"
                try:
                    printer_name = config['printer']
                    print "Found printer in config", printer_name
                except:
                    print "Defaulting to DYMO_LabelWriter_450_Turbo"
                    pass
                conn.printFile(printer_name, "/tmp/iascribe-label.png", "", {})
            except Exception as e:
                print "Couldn't print label. Error was", e
                raise e
        except Exception as e:
            msg = "There was an error creating the barcode."
            print msg
            self.show_error('{0}\n{1}'.format(msg, e))

            pass

    # load_metadata()
    # _____________________________________________________________________________________
    def load_metadata(self):
        self.ids._image.source = self.loading_image
        self.ids._button.disabled = True
        identifier = self.ids._barcode.text
        if not identifier:
            self.show_error('You must scan an identifier barcode!')
            return
        self.metadata = None
        self.identifier = identifier
        files_url = 'https://archive.org/metadata/{id}/files'.format(id=self.identifier)
        req = UrlRequest(files_url, self.load_metadata_callback, on_error=self.show_create_confirm,
                         on_failure=self.load_metadata_callback)

        '''
        The logic here is:

        IF -> call to IA fails =  Offline mode -- error_callback is called
              -> Tell user that machine is offline, ask if they want to scan to that identifier anyway.
                    IF -> YES = Create item
                    ELSE -> Do nothing
             -> At upload time:
                   IF -> call to IA success,
                         IF -> item exists
                               IF item is empty -> Adopt item identifier
                               ELSE IF item is not empty -> Display error message and do nothing
                         ELSE -> Tell user that item does not exist, do they want to create new item
                               IF -> YES = Create new item
                               ELSE -> Ask for new identifier, repeat upload attempt

        IF -> call to IA success -- either on_failure or load_metadata_callback are called, depending whether the item exists or not
              IF -> item exists
                    IF item is empty -> Adopt item identifier
                    ELSE IF item is not empty -> Display error message and do nothing
              ELSE -> Create identifier
        '''

    def show_create_confirm(self, *largs):
        if self.popup is not None:
            self.popup.dismiss()

        self.ids._image.source = self.transparent_image
        self.ids._button.disabled = False
        msg_box = ScribeMessageOKCancel()
        msg_box.text = 'show_create_confirm: Cannot verify item with archive.org\nWould you like to create the item\n[b]{id}[/b] locally?'.format(
            id=self.identifier)
        msg_box.ok_text = 'OK'
        msg_box.ids._ok_button.background_color = [1, 0, 0, 1]
        msg_box.trigger_func = self.create_offline_item

        popup = Popup(title='Offline mode', content=msg_box,
                      auto_dismiss=False, size_hint=(None, None), size=(600, 300))
        msg_box.popup = popup
        popup.open()

    # load_metadata_callback()
    # _____________________________________________________________________________________
    def load_metadata_callback(self, req, files):

        if files is None:
            self.show_error('Could not fetch metadata from archive.org')
            return

        if 'error' in files:
            self.show_error(files['error'])
            return

        if 'result' not in files:
            self.show_error('archive.org Metadata API returned an empty response')
            return

        allowed_formats = set(['Metadata', 'MARC', 'MARC Source', 'MARC Binary', 'Dublin Core', 'Archive BitTorrent',
                               'Web ARChive GZ', 'Web ARChive', 'Log', 'OCLC xISBN JSON', 'Internet Archive ARC',
                               'Internet Archive ARC GZ', 'CDX Index', 'Item CDX Index', 'Item CDX Meta-Index',
                               'WARC CDX Index', 'Metadata Log']);
        for file in files['result']:
            if file['format'] not in allowed_formats:
                self.show_error(
                    'This item already contains data files!\n(file={file}, format={format})'.format(file=file['name'],
                                                                                                    format=file[
                                                                                                        'format']))
                return

        # download meta.xml and put it in the item
        # TODO: make this non-blocking. Since /download/ returns a redirect, using
        # kivy's UrlRequest is not straightforward
        meta_url = 'https://archive.org/download/{id}/{id}_meta.xml'.format(id=self.identifier)
        meta_path = join(self.capture_screen.book_dir, 'metadata.xml')
        try:
            urllib.urlretrieve(meta_url, meta_path)
        except Exception:
            self.show_error('Could not download meta.xml from archive.org!')
        self.capture_screen.load_metadata()
        self.capture_screen.disable_metadata()
        self.ids._image.source = self.transparent_image
        self.ids._barcode.disabled = True
        self.ids._button_isbn.disabled = True

        # write out identifier.txt
        id_path = join(self.capture_screen.book_dir, 'identifier.txt')
        with open(id_path, 'w') as f:
            f.write(self.identifier)
        preloaded_file = join(self.capture_screen.book_dir, 'preloaded')
        with open(preloaded_file, 'w'):
            pass

    # error_callback()
    # This is what gets called in case network is unreachable
    # _____________________________________________________________________________________
    def create_offline_item(self, *largs):
        '''
        Archive.org is not available when this function is called (e.g. network down)
        -> Tell user that machine is offline, ask if they want to scan to that identifier anyway.
                IF -> YES = Create item
                ELSE -> Do nothing
        '''
        try:
            largs[0].dismiss()
        except:
            pass

        try:

            id_path = join(self.capture_screen.book_dir, 'identifier.txt')
            print "PATH ->", id_path
            f = open(id_path, 'w')
            f.write(self.identifier)
            f.close()
            preloaded_file = join(self.capture_screen.book_dir, 'preloaded')
            with open(preloaded_file, 'w'):
                pass
            self.ids._button.disabled = True
        except:
            self.show_error('There was an error creating id {id}'.format(id=self.identifier))

        msg = "Ok, created {0} successfully!".format(self.identifier)

        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = self.capture_screen.create_popup(
            title='Offline mode', content=msg_box, auto_dismiss=False,
            size_hint=(None, None), size=(400, 300)
        )
        msg_box.popup = popup
        msg_box.trigger_func = popup.dismiss
        popup.open()

        self.ids._image.source = self.transparent_image

    # failure_callback()
    # This gets called when the server returns 4xx or 5xx
    # _____________________________________________________________________________________
    def failure_callback(self, request, error):
        self.show_error('Failure {error}\n ID:{id}'.format(id=self.identifier, error=error))

    # metadata_thread()
    # _____________________________________________________________________________________
    def metadata_thread(self, url):
        try:
            self.metadata = json.load(urllib.urlopen(url))
        except Exception:
            self.metadata = None

    # show_error()
    # _____________________________________________________________________________________
    def show_error(self, msg):
        self.ids._image.source = self.transparent_image
        self.ids._button.disabled = False
        msg_box = ScribeMessage()
        msg_box.text = msg
        popup = self.capture_screen.create_popup(
            title='Barcode Error', content=msg_box, auto_dismiss=False,
            size_hint=(None, None), size=(400, 300)
        )
        msg_box.popup = popup
        msg_box.trigger_func = popup.dismiss
        popup.open()
