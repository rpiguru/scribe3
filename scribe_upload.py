#!/usr/bin/env python

import random
import re
import requests
import string
import traceback

from scribe_globals import ScribeException


# random_string()
#_________________________________________________________________________________________
def random_string():
    '''
    from TextsRemote::random_string()

    >>> s = random_string()
    >>> m = re.findall('^[a-z]\d[a-z]\d$', s)
    >>> m == [s]
    True
    '''

    random_str = ''.join([random.choice(string.ascii_lowercase),
                          random.choice(string.digits),
                          random.choice(string.ascii_lowercase),
                          random.choice(string.digits)])
    return random_str


# purify_string()
#_________________________________________________________________________________________
def purify_string(s):
    '''
    From BiblioString::purify_string()

    >>> purify_string('THE UPPER And the lower')
    'upperlower'
    '''
    patterns = ('^a\s+','^an\s+','^the\s+',
                '\sa\s','\san\s',
                '\sand\sthe\s','\sand\s','\sthe\s',
                '\s+',
                '\!','\@','\#','\$','\%','\^',
                '\&','\*','\(','\)','\+','\=',
                '\{','\}','\[','\]','\|','\\\\',
                '\:','\;','\'','\"',
                '\<','\>','\,','\?','\/',
                '\.','\-',
                '~/',
                'electronic.*resource','microform',
                'microfilm',
                '[^a-zA-Z0-9\.\-_]+', #strip non-ascii
               )
    s = s.lower()
    for p in patterns:
        s = re.sub(p, '', s)

    if s == '':
        s = 'unset'
    return s


# id_available()
#_________________________________________________________________________________________
def id_available(identifier):
    '''
    Query the upload_api to see if an identifier is available.
    '''
    try:
        url = 'http://archive.org/upload/app/upload_api.php'
        params = {'name':'identifierAvailable', 'identifier':identifier}
        r = requests.get(url, params=params)
        ret = r.json()
        success = ret.get('success', False)
    except Exception:
        print(traceback.format_exc())
        raise ScribeException('Could not query upload_api for identifierAvailable')

    if success == False:
        return False
    return True


# main()
#_________________________________________________________________________________________
if __name__ == "__main__":
    import doctest
    doctest.testmod()
