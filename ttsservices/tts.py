"""
tts tools

a library wrapping the Internet Archive API

:copyright: (c) 2016 Internet Archive.
:author: Giovanni Damiola <gio@archive.org>
:license: AGPL 3, see LICENSE for more details.
"""

from ia_api import InternetArchive
import re
import rstr
import logging
import time

log = logging.getLogger('TTSServices')
log.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(process)s] [%(name)s] [%(levelname)s] %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

import raven, traceback
from raven import *
from raven.conf import setup_logging
from raven.handlers.logging import SentryHandler
raven_client = Client(dsn="https://4af7fdad024f412eb45b933a400344a4:6434bde2c4da4f2ab6ea3a658043bb96@books-sentry.us.archive.org/3")
sentry_handler = SentryHandler(raven_client)
log.addHandler(sentry_handler)


def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, same


class TTSServices(InternetArchive):
    def __init__(self, config, metadata, **kwargs):
        InternetArchive.__init__(self, config, metadata, **kwargs)
        self.REGEX_IDENTIFIER = 'tts[A-Z]{2}[0-9]{4}'

    def show_all_tts(self):
        tts_data = self.show_all()
        data = []
        for i in tts_data:
            data.append(i['identifier'])
        return data


    def is_metadata_updated(self, tts_id, local_metadata):
        remote_metadata = self.get_item_metadata(tts_id)
        #print "remote metadata:", remote_metadata        
        if 'books' not in remote_metadata.keys():
            print "books not in remote metadata, adding..."
            local_metadata['books'] = '[]'
        added, removed, modified, same = dict_compare(local_metadata, remote_metadata)
        log.info("Added: {0}, Removed: {1}, Modified: {2}".format(added, removed, modified))
        if added or modified:
            return False
        else:
            return True

    def register_tts(self, tts_id= None, metadata='{}'):
        if not tts_id:
            tts_id = self._generate_id_for_tts()
            self.register_tts(tts_id, metadata)
        else:
            if self._validate_id(tts_id):
                if self._create_tts_item(tts_id):
                    #time.sleep(4)
                    if self.update_metadata_tts(tts_id,metadata):
                        msg = "New TTScribe {0} registered: OK".format(tts_id)
                        log.info(msg)
                        self.created = True
                        self.identifier = tts_id
                        return True, tts_id
                    else:
                        msg = "Cannot update metadata for the new ttscribe {0}".format(tts_id)
                        log.error(msg)
                        self.created = False
                        self.error_msg = msg
                        return False, msg
                else:
                    msg = "Cannot create the new ttscribe {0}".format(tts_id)
                    log.error(msg)
                    self.created = False
                    self.error_msg = msg
                    return False, msg
            else:
                msg = "the tts identifier {0} is not syntax compliant".format(tts_id)
                log.error(msg)
                self.created = False
                self.error_msg = msg
                return False, msg

    def _create_tts_item(self,identifier,content=InternetArchive.DEFAULT_CONTENT_VALUE):
        res = self.add_file_item(identifier, filename=InternetArchive.DEFAULT_FILENAME, content=content)
        if res[0].status_code == 200:
            return True
        else:
            return False

    def update_metadata_tts(self,identifier,metadata):
        res = self.set_item_metadata(identifier,metadata)
        if res == 200:
            msg = "Metadata updated succesfully for item {0}".format(identifier)
            log.info(msg)
            return True, msg
        else:
            msg = "Metadata was not updated for item {0} with code {1}".format(identifier, res)
            log.info(msg)
            return False

    def _validate_id(self,identifier):
        return True
        '''
        if not self.item_exists(identifier) and identifier != '':
            if re.match(r'tts[A-Z]{2}[0-9]{4}', identifier):
                return True
            else:
                return False
        else:
            return False
        '''

    def _generate_id_for_tts(self):
        id_offered = ''
        count = 0
        while not self._validate_id(id_offered) and count < 3:
            id_offered = rstr.xeger(self.REGEX_IDENTIFIER)
            count += 1
        return id_offered

    def tts_update_metadata(self,idenfifier,metadata):
        if self.set_item_metadata(identifier,metadata):
            log.info("Metadata updated for item {0} with values: {1}".format(identifier,metadata))
            return "Metadata updated for item {0} with values: {1}".format(identifier,metadata)
        else:
            log.error("Cannot update metadata for {0}: Item does not exist".format(identifier))
            return "Cannot update metadata for {0}: Item does not exist".format(identifier)
