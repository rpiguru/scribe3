"""
iabdash tools

A wrapper to push data to the iabooks telemetry server

:copyright: (c) 2016 Internet Archive.
:author: <davide@archive.org>
:license: AGPL 3, see LICENSE for more details.
"""

import os
import requests, json
import scribe_globals
import xml.etree.ElementTree as ET
from kivy.logger import Logger

# get_metadata()
def get_scribe_metadata(meta_dir, file_name = 'metadata.xml'):
    md = {}
    meta_dir_expanded =  os.path.expanduser(meta_dir)
    meta_file = os.path.join(meta_dir_expanded, file_name)
    if os.path.exists(meta_file):
        tree = ET.parse(meta_file)
        root = tree.getroot()
        collections = []
        for key in root:
            if key.tag == "collection" and key.text != None:
                collections.append(key.text)
            else:
                md[key.tag] = key.text
        if collections != []: md['collection'] = collections
    return md


scribe_md = get_scribe_metadata(os.path.expanduser(scribe_globals.config_dir))

BTSERVER_URL = "https://iabooks.archive.org/"
API_PATH = "api/v0.1/events"
URL = BTSERVER_URL + API_PATH
headers = {'Authorization': 'fc6add639e80f76e047f642fe6952168', 'Content-Type': 'application/json'}

default_target = ''
try:
    default_target = scribe_md['scanner']
except:
    print "No scanner in scribe metadata. Skipping push to iabdash..."
    pass

def push_event(event_type, payload, target_type= 'tts', target_id = default_target,):
    try:
        from_section = {'user': scribe_md['operator'], 'device': 'tts' , 'device_id': scribe_md['scanner']}
        target_section= {'type': target_type, 'id': target_id}
        pload = {}
        for key in payload:
            pload['tts_' + key] = payload[key]
        pload['scanningcenter'] = scribe_md['scanningcenter']
        res = {'from': from_section, 'target' : target_section, 'event_type': event_type, 'data': pload}
        x = requests.post(URL, data = json.dumps(res), headers = headers, verify=False)
        Logger.info("IABDASH: Pushed event {0} with return code {1}".format(event_type,str(x)))
        return x
    except Exception as e:
        Logger.debug("IABDASH: Error {0} in pushing to metrics server. Assuming offline mode and skipping...".format(e))
        pass











