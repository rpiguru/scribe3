#!/usr/bin/env python

import os
import yaml
import scribe_globals
from scribe_globals import ScribeException

path = scribe_globals.config_dir+'/'+scribe_globals.scribe_config

# read()
#_________________________________________________________________________________________
def read():
    try:
        f = open(os.path.expanduser(path))
        config = yaml.safe_load(f)
    except Exception:
        #raise ScribeException('Could not read {p}'.format(p=path))
        config = {}
    return config


# save()
#_________________________________________________________________________________________
def save(config):
    try:
        f = open(os.path.expanduser(path), 'w')
        yaml.dump(config, f, default_flow_style=False)
        print yaml.dump(config, default_flow_style=False)
    except Exception:
        raise ScribeException('Could not save config to {p}'.format(p=path))


# update()
#_________________________________________________________________________________________
def update(node, key, val):
    path = None
    parts = key.split('/')
    for part in parts[:-1]:
        if part not in node:
            node[part] = {}
        node = node[part]
    node[parts[-1]] = val


# get()
#_________________________________________________________________________________________
def get(node, key):
    parts = key.split('/')
    for part in parts:
        if part not in node:
            return None
        node = node[part]
    return node
