#!/usr/bin/env python


## this file contains the main configurations for the ttScribe software


## Release Information

release_date = '03-03-2017'
release_version = '1.40'
release_status = 'Testing'

import os
from os.path import join
from distutils.spawn import find_executable

EXIFTOOL = find_executable('exiftool')

#build_number = subprocess.check_output(['git', 'describe', '--tags'])
try: 
	bnum_file = open('build_number', 'r').read().strip('\n')
except:
	bnum_file = None

build_number =  bnum_file or release_version

# we use gphoto2 to control the cameras capturing pictures
# the default gphoto2 location:
# gphoto2 = '/usr/bin/gphoto2'
# we are using our patched version to support Nikon J3 and J4

# Unnecessary with camera subsystem
#gphoto2 = 'gphoto/bin/gphoto2'
#gphoto2_2000 = 'gphoto_2000/bin/gphoto2'
#gphoto_canon = 'gphoto_canon/bin/gphoto2'

kakadu = 'kakadu/kdu_compress'

# for debugging WITHOUT CAMERAS connected set fake_cameras = 2
# for debugging foldouts with three fake cameras, set fake_cameras to 3
#
fake_cameras = 2
#fake_cameras = None


# font that can display UTF-8 glyphs for internationalization
font = 'FreeSansTTScribe.ttf'


# where the books are stored
books_dir = '~/scribe_books'

# to be compatible with old Scribe setups
legacy_books = '/var/www/book/book_*'
modern_books = '~/scribe_books/*'


# the main configuration folder
config_dir = '~/.scribe'
# add-ons configurations like the S3 keys locatios
scribe_config = 'scribe_config.yml'

camera_config = 'cameras.conf'
drivers_config = 'drivers.conf'
camera_subsys = 'camera-config.conf'

# db file location
db_file = 'scribe_books.db'

app_working_dir = os.path.dirname(__file__)

# Required version of capture_action_bindings.json
capture_action_bindings_version = 1

# File which contains keyboard scancode to capture action bindings.
# End-user can edit this file.
capture_action_bindings = join(os.path.expanduser(config_dir),
                               'capture_action_bindings.json')

# Default bindings defined by developers and copied by app to
# capture_action_bindings if capture_action_bindings does not exists or
# versions does not match
default_capture_action_bindings = join(app_working_dir, 'ia_scribe', 'capture',
                                       'capture_action_bindings.json')

# scancenter
# this folder contains the metadata.xml file where are set
# the scan center's metadata, and the default collection
#
# for test we are using the collection 'booksgrouptest'
#
#  <metadata>
#      <language>eng</language>
#      <scanningcenter>yourCity</scanningcenter>
#      <scanner>ttScribeName</scanner>
#      <sponsor>Internet Archive</sponsor>
#      <operator>operatorName</operator>
#      <collection>booksgrouptest</collection>
#  </metadata>
#
scancender_metadata =   '~/.scribe'
#'/usr/local/scribett/metadata/'
scancenter_metadata = "metadata.xml"
scancenter_metadata_collections = "collections_metadata.xml"


# ScribeException
#_________________________________________________________________________________________
class ScribeException(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr(self.msg)
