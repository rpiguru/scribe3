#!/bin/sh

# This script create the folder and the metadata.xml file for the ttScribe
#
# Author: Giovanni Damiola <gio@archive.org>
#   Date: 23-06-2016
#

SCANNING_CENTER_DIR='/usr/local/scribett/metadata'
METADATA_FILE="$SCANNING_CENTER_DIR/metadata.xml"

DEF_SCANCENTER='San Fancisco'
DEF_LANG='eng'
DEF_SCANNER='ttScribeDev'
DEF_SPONSOR='Internet Archive'
DEF_OPERATOR='operatorName'
DEF_COLLECTION='booksgrouptest'

printf "\n\n"

## funtion to compile and save the metadata.xml
write_xml () {
	read -p "Scanning Center [$DEF_SCANCENTER]: " scancenter
	if [ -z $scancenter ]; then scancenter=$DEF_SCANCENTER; fi

	read -p "Language: [$DEF_LANG]" lang
	if [ -z $lang ]; then lang=$DEF_LANG; fi

	read -p "Scanner: [$DEF_SCANNER]" scanner
	if [ -z $scanner ]; then scanner=$DEF_SCANNER; fi

	read -p "Sponsor: [$DEF_SPONSOR]" sponsor
	if [ -z $sponsor ]; then sponsor=$DEF_SPONSOR; fi

	read -p "Operator: [$DEF_OPERATOR]" operator
	if [ -z $operator ]; then operator=$DEF_OPERATOR; fi

	read -p "Collection: [$DEF_COLLECTION]" collection
	if [ -z $collection ]; then collection=$DEF_COLLECTION; fi

	CONTENT="<metadata>\n\t<language>$lang</language>\n\t<scanningcenter>$scancenter</scanningcenter>\n\t<scanner>$scanner</scanner>\n\t<sponsor>$sponsor</sponsor>\n\t<operator>$operator</operator>\n\t<collection>$collection</collection>\n</metadata>\n"

	printf "$CONTENT"
	read -p "Do you want to save it? [y,n]" answ1

	if [ "$answ1" = "y" ]; then
		# create backup file if a metadata already exists
		if [ -e $METADATA_FILE ]; then
			/bin/cp $METADATA_FILE "$METADATA_FILE.bck"
		fi
		# save the new metadata.xml
		printf "$CONTENT" > $METADATA_FILE
	elif [ "$answ1" = "n" ]; then
		exit 0
	elif [ -z "$answ1" ]; then
		exit 0
	fi

}


# this scrib should be run as ROOT
if [ "$(id -u)" != "0" ]
  then echo "Please, run this script as root" 1>&2
  exit 1
fi

# create default directory
if [ -d $SCANNING_CENTER_DIR ]; then
	echo "The directory $SCANNING_CENTER_DIR already exists"
else
	echo "Creating DIR: $SCANNING_CENTER_DIR"
	mkdir -p $SCANNING_CENTER_DIR
fi

# check if the configuration file already exists
if [ -e $METADATA_FILE ]; then
	printf "\n"
        read -p "The file $METADATA_FILE already exists, overwrite it? [y/n]: " answ
	if [ "$answ" = "y" ]; then
		write_xml		
		exit 0
	elif [ "$answ" = "n" ]; then
		exit 0
	elif [ -z "$answ" ]; then
		exit 0
	fi
fi

printf "\n"
write_xml

