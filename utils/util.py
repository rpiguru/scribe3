import requests
import subprocess
from internetarchive.cli import ia
import os
import sys
import re
import scribe_globals
from enum import Enum
from kivy.logger import Logger


class UpdateStatus(Enum):
    up_to_date = 100
    update_available = 200
    error = 800
    unknown = 900

update_status = UpdateStatus.unknown.value


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


def restart_app():
    # before leaving, remove the current pid lock.
    config_dir = os.path.expanduser(scribe_globals.config_dir)
    path = os.path.join(config_dir, 'scribe_pid')
    if os.path.exists(path):
        f = open(path)
        old_pid = f.read().strip()
        f.close()
        pid_dir = os.path.join('/proc', old_pid)
        if os.path.exists(pid_dir):
            print("Got the pid file at " + str(pid_dir) + ". Now unlinking before relaunch...")
            os.unlink(path)
    python = sys.executable
    os.execl(python, python, *sys.argv)


def check_software_update():
    Logger.info('Update:: Begin')

    pkg_name = "ia-scribe"
    global update_status

    def apt_cache(*args):
        try:
            out = subprocess.check_output(args)
            m = re.findall("Candidate:.*|Installed:.*", out)
            return m if m else "No match"
        except subprocess.CalledProcessError as er:
            return er.output

    def parse(m):
        installed = m[0].split("Installed: ")[1]
        candidate = m[1].split("Candidate: ")[1]
        return installed, candidate

    try:
        result = apt_cache("apt-cache", "policy", pkg_name)
        if result != 'No match':
            i, c = parse(result)
            Logger.info('Update:: Local release -> {0}| Remote Release -> {1}'.format(i, c))
            if i == c:
                update_status = UpdateStatus.up_to_date.value
                Logger.info('Update:: Up to date!')
                return i, c, None
            elif i != c:
                update_status = UpdateStatus.update_available.value
                Logger.info('Update:: Updated version {0} available!'.format(c))
                return i, c, None
        else:
            update_status = UpdateStatus.error.value
            Logger.info('Update:: Scribe3 Debian package (ia-scribe) not found.' )
            return None, None, None

    except Exception as e:
        print e
        return None, None, e


def download_annotated_scandata_from_republisher(book):
    Logger.debug('ScanData Puller: Downloading book updates from cluster republisher for {0}'.format(book['path']))
    try:
        item = ia.get_item(book['identifier'], config={'general': {'secure': False}})
        datanode = item.d1
        md_url = "https://" + datanode + "/RePublisher/RePublisher-viewScanData.php?id=" + book['identifier']
        Logger.info('correction_scandata.xml -> {0}'.format(md_url))
        md = requests.get(md_url, verify=False)
        f = open(os.path.join(book['path'], "correction_scandata.xml"), 'w+')
        f.write(md.content)
        # tree = ET.fromstring(md.content)
        # book['status'] = UploadStatus.scribing_corrections.value
        return True
    except Exception as e:
        Logger.exception("ScanData Puller: Error {0}".format(e))
        raise scribe_globals.ScribeException('Could not query archive.org for repub_state!')
