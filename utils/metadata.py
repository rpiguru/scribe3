import copy

from kivy.logger import Logger

import os
import xml.etree.ElementTree as et

import scribe_globals


def get_metadata(meta_dir, file_name='metadata.xml'):
    md = {}
    meta_dir_expanded = os.path.expanduser(meta_dir)
    meta_file = os.path.join(meta_dir_expanded, file_name)
    if os.path.exists(meta_file):
        tree = et.parse(meta_file)
        root = tree.getroot()
        collections = []
        catalogs = []
        for key in root:
            if key.tag == "collection" and key.text is not None:
                collections.append(key.text)
            elif key.tag == "catalog" and key.text is not None:
                catalogs.append(key.text)
            else:
                md[key.tag] = key.text
        if collections:
            md['collection'] = collections
        if catalogs:
            md['catalog'] = catalogs

    return md


# set_metadata()
def set_metadata(md, meta_dir, file_name='metadata.xml', root_element='metadata'):
    print md
    print meta_dir
    meta_dir_expanded = os.path.expanduser(meta_dir)
    meta_file = os.path.join(meta_dir_expanded, file_name)
    root = et.Element(root_element)
    for key, value in md.iteritems():
        child = et.SubElement(root, key)
        if key in ["collection", "catalog"]:
            # Collections and catalogs are always passed as lists
            for item in value:
                subchild = et.SubElement(root, key)
                subchild.text = item
        else:
            child.text = value

    tree = et.ElementTree(root)
    tree.write(meta_file)


def get_collections_metadata(meta_dir, file_name='collections_metadata.xml'):
    def dictify(r, root=True):
        from copy import copy
        if root:
            return {r.tag: dictify(r, False)}
        d = copy(r.attrib)
        if r.text:
            d["_text"] = r.text
        for x in r.findall("./*"):
            if x.tag not in d:
                d[x.tag] = []
            d[x.tag].append(dictify(x, False))
        return d

    meta_dir_expanded = os.path.expanduser(meta_dir)
    meta_file = os.path.join(meta_dir_expanded, file_name)

    if os.path.exists(meta_file):
        a = dictify(et.parse(meta_file).getroot())
        return a


def set_collections_metadata(tree, meta_dir, file_name='collections_metadata.xml'):
    meta_dir_expanded = os.path.expanduser(meta_dir)
    meta_file = os.path.join(meta_dir_expanded, file_name)
    tree.write(meta_file, method='xml')


def get_sc_metadata():
    return get_metadata(scribe_globals.scancender_metadata)


def get_catalogs_from_metadata():
    """
    Retrieve catalogs from catalogs_metadata.xml.
    :return:
    """
    meta_dir_expanded = os.path.expanduser(scribe_globals.config_dir)
    conf_file = os.path.join(meta_dir_expanded, 'catalogs_metadata.xml')
    if not os.path.isfile(conf_file):
        top = et.Element('catalogs')
        tree = et.ElementTree(top)
        tree.write(conf_file)
        return []

    root = et.parse(conf_file).getroot()
    cats = []
    for child_of_root in root:
        if child_of_root.tag == 'catalog':
            cats.append(child_of_root.text)
    return cats


def get_collections_from_metadata():
    """
        Get number of collection sets
    :return:
    """
    meta_dir_expanded = os.path.expanduser(scribe_globals.config_dir)
    conf_file = os.path.join(meta_dir_expanded, 'collections_metadata.xml')
    if not os.path.isfile(conf_file):
        top = et.Element('collections')
        tree = et.ElementTree(top)
        tree.write(conf_file)
        return []

    root = et.parse(conf_file).getroot()
    cols = []
    for child_of_root in root:
        if child_of_root.tag == 'set':
            cols.append(child_of_root.text)
    return cols


def set_sc_metadata(md):
    # TODO: Try catch, dependency on figuring out what the best course of action would be
    config = get_metadata(scribe_globals.scancender_metadata)
    # Make a copy of it
    new_config = copy.deepcopy(config)
    # Load all textboxs in the interface in a dict
    # For each of them, get the value in the textbox  and assign it to the new copy of the dict
    for k, v in md.iteritems():
        Logger.debug('Scan center metadata ::{0} -> {1}'.format(k, v))
        new_config[k] = v

    # if the two dics are different, set the new one as default
    if cmp(config, new_config) != 0:
        set_metadata(new_config, scribe_globals.scancender_metadata)
        # print "New metadata:", new_config
    else:
        pass
    return get_metadata(scribe_globals.scancender_metadata)
