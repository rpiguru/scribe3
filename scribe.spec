# -*- mode: python -*-
from kivy.tools.packaging.pyinstaller_hooks import install_hooks
install_hooks(globals())

from PyInstaller.hooks.hookutils import collect_data_files
# Get the cacert.pem
datas = collect_data_files('requests')

def filter_binaries(all_binaries):
    '''Exclude binaries provided by system packages, and rely on .deb dependencies
    to ensure these binaries are available on the target machine.

    We need to remove OpenGL-related libraries so we can distribute the executable
    to other linux machines that might have different graphics hardware. If you
    bundle system libraries, your application might crash when run on a different
    machine with the following error during kivy startup:

    Fatal Python Error: (pygame parachute) Segmentation Fault

    If we strip all libraries, then PIL might not be able to find the correct _imaging
    module, even if the `python-image` package has been installed on the system. The
    easy way to fix this is to not filter binaries from the python-imaging package.

    We will strip out all binaries, except libpython2.7, which is required for the
    pyinstaller-frozen executable to work, and any of the python-* packages.
    '''
    print('Excluding system libraries:')
    import subprocess
    excluded_pkgs  = set()
    excluded_files = set()
    whitelist_prefixes = ('libpython2.7', 'python-')
    for b in all_binaries:
        if b[-1] == 'EXTENSION':
            continue
        try:
            output = subprocess.check_output(['dpkg', '-S', b[1]], stderr=open('/dev/null'))
            items = output.split(':')
            package = items[0]
            path = items[-1].strip()
            if not package.startswith(whitelist_prefixes):
                excluded_pkgs.add(package)
                excluded_files.add(b[0])
                print('Excluding {} from package {}'.format(b[0], package))
        except Exception:
            pass
    print('Exe will depend on the following packages:')
    print(','.join(excluded_pkgs))
    print('')
    binaries = [x for x in all_binaries if x[0] not in excluded_files]
    return binaries


a = Analysis(['scribe.py'],
             pathex=['.'],
             hiddenimports=[],
            )
pyz = PYZ(a.pure)

binaries = filter_binaries(a.binaries)

exe = EXE(pyz,
          [('scribe.kv', 'scribe.kv', 'DATA')],
          [('FreeSansTTScribe.ttf', 'FreeSansTTScribe.ttf', 'DATA')],
          [('CHANGELOG.md','CHANGELOG.md', 'DATA')],
          [('cacert.pem','cacert.pem', 'DATA')],
          [('camera_shoot.wav','camera_shoot.wav', 'DATA')],
          [('build_number', 'build_number', 'DATA')],
          [('DejaVuSansMono.ttf','utils/barcode/DejaVuSansMono.ttf', 'DATA')],
          a.scripts,
          Tree('ia_scribe', prefix='ia_scribe'),
          Tree('optics', prefix='optics'),
          Tree('fastcap', prefix='fastcap'),
          Tree('utils', prefix='utils'),
          Tree('images', prefix='images'),
          Tree('gphoto/bin', prefix='gphoto/bin'),
          Tree('gphoto/lib', prefix='gphoto/lib'),
          Tree('gphoto_2000/bin', prefix='gphoto_2000/bin'),
          Tree('gphoto_2000/lib', prefix='gphoto_2000/lib'),
          Tree('gphoto_2510/bin', prefix='gphoto_2510/bin'),
          Tree('gphoto_2510/lib', prefix='gphoto_2510/lib'),
          Tree('gphoto_canon/bin', prefix='gphoto_canon/bin'),
          Tree('gphoto_canon/lib', prefix='gphoto_canon/lib'),
          Tree('kakadu', prefix='kakadu'),
          #Tree('piggyphoto'),
          binaries, #a.binaries,
          a.zipfiles,
          a.datas,
          name='ia-scribe',
          debug=False,
          strip=None,
          upx=True,
          console=True )