import os, time, shutil, logging, sys
import subprocess
import scribe_globals
from optics.config import ScribeCameraConfig
from ttsservices.iabdash import push_event

fake_cameras = scribe_globals.fake_cameras
camera_lib = "gphoto_2000"
scribe_camera_config = ScribeCameraConfig()

DEBUG = True

## initializing logger
log = logging.getLogger("cameras3")
fh = logging.StreamHandler(sys.stdout)
if DEBUG:
    log.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)
    fh.setLevel(logging.INFO)
formatter=logging.Formatter('[%(levelname)s] [%(name)s] %(message)s (%(asctime)s)')
fh.setFormatter(formatter)
log.addHandler(fh)

'''
Camera subsystem

This is the 1.0 version, which only serves as a manager of camera_ports objects and 
does not contain camera threads. This will happen after 1.50-Release, as scribe.py
is gutted and widgets moved into place in ia_scribe, and we will need to have containerized
camera contexts (for camera autopilot). For now a Cameras object serves as an interface for 
CaptureScreen and SingleCaptureScreen to have a shoot() method that hides the following 
responsibilities:

- Determining what cameras are connected
- Determining what camera goes on what side basing on (in descending order of importance)
    - already known state (is there an exact match in the config?)
    - camera models (two identical camera models belong on left and right side)
    - usb ids (they change all the time)
    - device type  (ttscribe, full frame scribe, fodlout station)
    
a Cameras() object is basically an editor for camera_ports dictionaries, the format of whose
are inherited from earlier Scribe3 versions. In order to fill in the camera_ports dict
with the correct usb port per side, it also takes care of loading the camera config from the
drive, as well as juggling built-in driver and camera configs. As such, it also takes
responsablity for saving the config to file when the user re-calibrates, as well as determining
what to do if a camera falls off the bus, changes id, stalls or is added.

'''

# this will substitute the camera_ports dictionary in scribe_widget
# this is an editor of ScribeCameraConfig objects
class Cameras():
    camera_ports = {'left':None, 'right':None, 'foldout':None}

    def __init__(self, camera_ports = {'left':None, 'right':None, 'foldout':None}, **kwargs):
        #super(Camera, self).__init__(**kwargs)
        log.info( "Cameras::Init Called cameras object with: {0}".format(camera_ports))
        self.sides = scribe_camera_config.get_camera_list() #or ['left', 'right', 'foldout']
        log.info(  "Cameras::Init Sides available from config are {0}. Initializing cameras...".format(self.sides)) 
        self.get_cameras()
        log.info("Cameras::Init Finished init, sides {0}. At end of creation, camera ports is {1}".format(self.sides, self.camera_ports))
        
    def num_cameras(self):
        ret = len([x for x in self.camera_ports if self.camera_ports[x] is not None])
        return ret

    def get_raw_config(self):
        return scribe_camera_config.get_as_dict()

    def load_raw_config(self):
        for side in self.camera_ports:
            try:
                cam = scribe_camera_config.get_camera_config(side)
                self.camera_ports[side] = cam['port']
                log.info("load_raw_config: Got port {0} on {1} side ".format(port, side))  
            except:
                log.info("load_raw_config: ignoring {0} side ".format(side))    
        log.info("load_raw_config: At this point, system vector is {0} ".format(self.camera_ports))


    def set_camera(self, side, port):
        # Setting cameras
        #elf.camera_ports[side] = port
        log.info("set_cameras: Assigning cameras {0} to sides {1} and storing in config.".format(side, port, ))
        log.info("set_cameras: At this point, system vector is {0} ".format(self.camera_ports))
        if port is not None:
            camera_temp = Camera(port, side)
            camera_temp.set_port(port)
            self.camera_ports[side] = port
            log.info("set_cameras: At this point, system vector is {0} ".format(self.camera_ports))
        #save in config

    def set_cameras(self, raw_models, raw_ports):
        log.debug("set_cameras::init. raw_models = {0}, raw_ports= {1}".format(raw_models, raw_ports))
        log.debug("set_cameras: At this point, system vector is {0} ".format(self.camera_ports))
        for i, port in enumerate(raw_ports):
            target_side = scribe_camera_config.get_camera_by_port(port)
            log.debug("set_cameras: Processing port: {0}, target side is {1} ".format(port, target_side))

            if target_side == 'foldout':
                if len(raw_ports) == 2:
                    log.warning("We had a foldout camera, but we have fallen back into two-cameras mode.")
                    avsides = ['left', 'right']
                    used_sides = [c_side for c_side,c_port in self.camera_ports.items() if (port is not None and c_port != port)]
                    if list(set(avsides) - set(used_sides)) != []:
                        target_side = list(set(avsides) - set(used_sides))[0]
                    else:
                        scribe_camera_config.flush()
                        target_side = 'left'
                elif len(raw_ports) == 1:
                    log.warning("Foldout port changed")
                    #available_sides = []
                elif len(raw_ports) == 3:
                    log.warning("Full triple-camera mode")
                    


            elif target_side is None:
                used_sides = scribe_camera_config.get_camera_list()
                avsides = ['left', 'right', 'foldout']
                if len(raw_ports) == 1:
                    avsides = ['foldout']
                elif len(raw_ports) == 2:
                    avsides = ['left', 'right']
                available_sides = list(set(avsides) - set(used_sides))
                
                if available_sides == []:
                    # None of the ports listed in the config include this camera
                    # Flush the config and start afresh

                    scribe_camera_config.flush()

                    if len(raw_ports) == 1:
                        # If we have only one camera, we assign it to the foldout side
                        available_sides = ['foldout']
                    elif len(raw_ports) ==2 :
                        # this is a default ttscribe/scribe station
                        available_sides = ['left', 'right']
                    elif len(raw_ports) ==3:
                        # ttscribes can also have a LIC
                        available_sides =['foldout', 'left', 'right']


                scribe_camera_config.set_camera_config_param(available_sides[0], 'port', port)
                scribe_camera_config.set_camera_config_param(available_sides[0], 'model', raw_models[i])
                target_side = scribe_camera_config.get_camera_by_port(port)
                log.warn("set_cameras: Target side now is {0}".format(target_side))

            self.camera_ports[target_side] = port
        log.info("set_cameras: At this point, system vector is {0} ".format(self.camera_ports))
        return

    def get_cameras(self, force= False):
        # External API, this is called by the main widget and returns a models, ports tuple 
        log.info("get_cameras::init")
        log.info("get_cameras: At this point, system vector is {0} ".format(self.camera_ports))
        #if True:
        try:

            # get actual cameras info
            gmodel, gports = [], []
            try:
                gmodel, gports = self._get_cameras_gphoto()
            except Exception as e:
                log.error("Error calling gphoto: " +  str(e))

            log.info("get_cameras:: Got {0}, {1} from USB- Checking against config: Do we have a side for this port?".format(gmodel, gports, ))
            log.info( ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            # First check that port is in ext config at all. If not, add it. 
            try:
                for port in gports:
                    cport = scribe_camera_config.get_camera_by_port(port)
                    log.info("get_cameras:: is port {0} associated to a side already? {1}".format(port, cport))
            except:
                log.info("get_cameras:: No. Saving!".format())
                save_cameras(gmodel, gports)

            # Check if camera info is already loaded in runtime config
            if scribe_camera_config.is_vector_current(self.camera_ports):
                log.info("Camera config current and loaded.")
            else:
                log.info("No, setting from config... models: {0}, ports: {1} ".format(gmodel, gports))
                self.set_cameras(gmodel, gports)
        except Exception as e:
            log.error("Failed to get cameras with error {0}".format(e))
            raise e
        
        log.info( "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        payload = {'runtime_configuration': self.camera_ports, 'stored_configuration': scribe_camera_config.get_as_dict(), }# 'camera_config_file': camera_config}
        push_event('tts-cameras-initialized', payload )

        return self.camera_ports.items(), self.camera_ports.values()

    def _get_cameras_gphoto(self):
        log.info("_get_cameras_gphoto::init")
        if fake_cameras == 3:
            return ['Nikon J3', 'Nikon J3', 'Nikon J3'], ['USB:001', 'USB:002', 'USB:003']
        elif fake_cameras == 2:
            #return range(random.randint(0, 3))
            return ['Nikon J3', 'Nikon J3'], ['USB:001', 'USB:002',]
        elif fake_cameras == 1:
            #return range(random.randint(0, 3))
            return ['Nikon J3',], ['USB:010', ]
        elif fake_cameras == 4:
            #return range(random.randint(0, 3))
            return ['Nikon J3', 'Sony A6300 (Control)'], ['USB:003', 'USB:012',  ]
        else:
            log.info("GETTING CAMERAS. First selecting polling driver.")
            # We ephemerally create camera() objects here just to write to the config file
            # get the current default driver 
            driver_name = 'gphoto-sony-2510'
            default_driver = scribe_camera_config.get_general_config_value('default_driver')
            if default_driver:
                driver_name = default_driver

            driver = scribe_camera_config.get_driver_by_name(driver_name)
            log.info("_get_camera_gphoto: Selected driver {1} ({0})".format(driver_name, driver))

            if driver['needs_environment']:
                log.info("_get_camera_gphoto: Driver specifies environment constants")
                for env_setting in ['camlibs', 'iolibs', 'ld_library_path']:
                    if env_setting in driver:
                        os.environ[env_setting.upper()] = resource_path(os.path.join(driver['path'], driver[env_setting]))
                        log.debug("_get_camera_gphoto: Set {0} to {1}".format( env_setting , os.environ[env_setting.upper()]))

            
            # call gphoto
            # parse output
            # create camera objects
            # thereby saving to  config if all is well
            # return to main thread

            command = resource_path(os.path.join(driver['path'], driver['command']))
            log.info("_get_camera_gphoto: Calling command {0} ".format(command))
            print subprocess.check_output([command, '--version', ])

            output = subprocess.check_output([command, '--auto-detect', ])

            lines = output.split('\n')
            ports = []
            models = []
            for line in lines[2:]:
                line = line.strip()
                if '' == line:
                    continue
                parts = line.split()
                if parts[-1].startswith('usb:'):
                    ports.append(parts[-1])
                    models.append(' '.join(parts[:-1]))

            
            log.info("_get_camera_gphoto: Cameras = {0}, ports = {1}".format(models, ports))
            # if camera model is sony, do this again.
            '''
             for side in camera_ports:
                    camera_ports[side] = 
                        print "Initialized cameras object with:", self.camera_ports
            '''

            if 'sony' in driver_name:
                time.sleep(1)
                for port in ports:
                    output = subprocess.check_output([command, '--list-config', '--port', port])

            return models, ports


    def save_cameras(self, models, ports,):
        '''
        This method contains the state machine that decides how to treat attached cameras. 
        
        
        as reasonable, and use the config as a general reference of we may find attached. If there's
        an exact (camera port, camera model) matche with a value in the config 
        
        :param models: 
        :param ports: 
        :return: 
        '''

        log.info("save_cameras: Assigning cameras to sides | #ports = {0}".format(len(ports)))
        log.info("models: {0} + ports: {1} -> {2}".format(models, ports, self.camera_ports.keys()))
        #check num sides!

        for i in range(0,len(ports)):
            # create object
            log.info("save_cameras: i = {0}".format(i))
            port = ports[i]
            model = models[i]
            side = scribe_camera_config.get_camera_by_port(port)
            log.info("port={0}, model={1}, side={2}".format(port, model, side))

            # this writes to config
            if side is None:
                print "camera has changed USB ID. Reassigning."
                try:
                    log.info("save_cameras: Trying to get side ")
                    side = self.sides[i]
                    log.info("save_cameras: done ")
                except:
                    side = ['left', 'right', 'foldout'][i]
                    log.info("save_cameras: Trying to get side failed, assuming".format(side))
            # At this point, side must have a correct value
            # If the value is none, y
            if  scribe_camera_config.get_camera_port(side) == None:
                print "disagreement, saving new port"
                scribe_camera_config.set_camera_config_param(side, 'port', port )
                scribe_camera_config.set_camera_config_param(side, 'model', model )

            if  scribe_camera_config.get_camera_port(side) != port:
                print "disagreement, saving new port"
                scribe_camera_config.set_camera_config_param(side, 'port', port )
                scribe_camera_config.set_camera_config_param(side, 'model', model )

            # this saves it to the local dict that scribewidget uses to reference cameras
            self.camera_ports[side] = port
            log.info("save_cameras: Saved camera on port {0} to side {1}".format(port,side))
            
        self.sides = scribe_camera_config.get_camera_list()
        log.info("save_cameras: Sides after backfill are {0}".format(self.sides))


    def swap(self,):
        # Update configs
        log.info("swap: Swapping left and right side")
        lconf, rconf = scribe_camera_config.get_camera_config("left"),  scribe_camera_config.get_camera_config("right"), 
        log.debug("swap: Right config -> {0} | Left config -> {1}".format(rconf, lconf))
        scribe_camera_config.set_camera_config('right', lconf)
        scribe_camera_config.set_camera_config('left', rconf)
        # reload vector
        self.load_raw_config()
        return

class Camera():
    # possibly add the thread in here?
    local_config = None
    port = None
    side = None

    def __init__(self, port, side, **kwargs):
        #super(Camera, self).__init__(**kwargs)
        log.info("Camera: DRIVER port={0}, side={1}".format(port,side))
        self.side = side
        self.local_config = scribe_camera_config.get_camera_config(side)
        self.driver = scribe_camera_config.get_camera_driver(side)
        # if the provided port is different from that in the config
        self.port = port
        log.info("Camera: -INIT END- Config -> {0} | Ephemeral port -> {1}".format(self.local_config, self.port))

    def _driver(self, path):
        log.info( "_driver: Called _driver dispatcher. Do I have to set environment variables to use this driver?")
        if self.driver['needs_environment']:
            #print "----> driver meeds env!"
            log.info( "_driver: Yes.")
            for env_setting in ['camlibs', 'iolibs', 'ld_library_path']:
                if env_setting in self.driver:
                    #print "found", env_setting
                    os.environ[env_setting.upper()] = resource_path(os.path.join(self.driver['path'], self.driver[env_setting]))
                    log.info("Camera _driver: Setting env var {0} -> {1}".format(env_setting , os.environ[env_setting.upper()]))
        else:
            log.info("_driver: No.")
        log.info("Camera _driver: Shooting {0} side".format(self.side))
        if not fake_cameras:
            command = resource_path(os.path.join(self.driver['path'], self.driver['command']))
            try:
                result = subprocess.check_call([command, '--capture-image-and-download', '--port', self.port, '--filename', path, '--force-overwrite', '--quiet',])       
            except Exception as e:
                log.error("Error calling {0} camera: {1}".format(self.side, e))
        else:
            log.warn("Not shooting for real, using {0} fake cameras.".format(fake_cameras))
            src = resource_path('images/fake.jpg')
            shutil.copyfile(src, path)
            result = path
        #print self.side, "camera model", self.local_config['model'], 'called with driver', self.driver
        log.info("{0} camera, using {1} -> {2}".format(self.side, self.driver, result))
        return result

    def shoot(self, path):
        log.info("CAMERA SHOOT: Destination path: {0}".format(path))
        
        try:
            result = self._driver(path)
        except subprocess.CalledProcessError as e:
            err = 'gphoto error: {r}'.format(r=e.returncode)
            #print "Checking if this is a -11 error and retrying capture"
            # in case of error gphoto -11
            if e.returncode == -11:
                return shoot(path)
            else:
                raise e

        return (True, result)

    def set_port(self, port):
        log.info( "Saving camera config, port={0}, side={1}.".format(port,self.side))
        try:
            scribe_camera_config.set_camera_config_param(self.side, 'port', port )
            self.local_config = scribe_camera_config.get_camera_config(side)
            return True
        except:
            return False


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


def parse_gphoto_output(g_output):
    value = ''
    for line in g_output.split('\n'):
        if line.startswith('Current:'):
            value = line[8:].strip()
            break
    return value


