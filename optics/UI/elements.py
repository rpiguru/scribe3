from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.switch import Switch
from kivy.uix.textinput import TextInput


class MetadataTextInput(TextInput):
    metadata_key = StringProperty(None)
    key_input = ObjectProperty(None)
    pass


# MetadataSwitch
#  _________________________________________________________________________________________
class MetadataSwitch(Switch):
    metadata_key = StringProperty(None)
    pass
