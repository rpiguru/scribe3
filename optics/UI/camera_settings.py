from kivy.uix.screenmanager import Screen
from kivy.properties import BooleanProperty, ObjectProperty, NumericProperty, ListProperty, StringProperty, DictProperty
import scribe_config
from kivy.cache import Cache
import os
from os.path import join, dirname
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.core.window import Window
from optics.config import ScribeCameraConfig, DriverConfig
from elements import *

Builder.load_file(join(dirname(__file__), 'camera_settings.kv'))


class CameraSettingsScreen(Screen):
    vector = StringProperty()
    about_screen = ObjectProperty(None)
    config = ObjectProperty(None)
    scribe_widget = ObjectProperty(None)

    is_added = False

    # __init__()
    def __init__(self, **kwargs):
        super(CameraSettingsScreen, self).__init__(**kwargs)
        self.camera_config = ScribeCameraConfig()

    def on_enter(self):
        if self.is_added:
            return
        # read overall camera configuration
        self.config = self.camera_config.get_config()
        # Create main widget
        cfilew = ConfigFileWidget()
        
        # For each, initialize
        for each_section in self.config.sections():
            section_root = ConfigFileSectionWidget(each_section)
            for (each_key, each_val) in self.config.items(each_section):
                config_widget = ConfigFileSectionValueWidget(each_key, each_val)
                section_root.ids['_insert'].add_widget(config_widget)

            cfilew.ids['_insert'].add_widget(section_root)

        self.ids['_conf_emerge'].add_widget(cfilew)
        # fastcap = False
        # try:
        #     fastcap = self.camera_config.get_setting('general', 'fastcap')
        # except Exception:
        #     print "No fastcap setting in general"
        #
        # self.ids['_fastcap_toggle'].active = bool(fastcap)

        try:
            cameras = self.scribe_widget.cameras.camera_ports
            #if self.scribe_widget.cameras.num_cameras() == 1:
                #del cameras['right']
                # cameras['single'] = cameras['left']
                #del cameras['left']
            cfilew = ConfigFileWidget()
            section_root = ConfigFileSectionWidget('vector')
            for (each_key, each_val) in cameras.items():
                config_widget = ConfigFileSectionValueWidget(each_key, each_val)
                section_root.ids['_insert'].add_widget(config_widget)

            cfilew.ids['_insert'].add_widget(section_root)
            self.ids['bl_vector'].add_widget(cfilew)
            self.is_added = True
        except Exception as e:
            print("Error in initializing vector : {0}".format(e))

    def save_config(self):
        print "Saving settings"
        self.camera_config.update_setting('general', 'fastcap', self.ids['_fastcap_toggle'].active)


class ConfigFileWidget(GridLayout):

    def __init__(self, **kwargs):
        super(ConfigFileWidget, self).__init__(**kwargs)


class ConfigFileSectionWidget(GridLayout):

    value = ObjectProperty(None)

    def __init__(self, value, **kwargs):
        self.value = str(value)
        super(ConfigFileSectionWidget, self).__init__(**kwargs)


class ConfigFileSectionValueWidget(GridLayout):

    key = ObjectProperty(None)
    value = ObjectProperty(None)

    def __init__(self, key, value, **kwargs):
        self.value = str(value)
        self.key = str(key)
        super(ConfigFileSectionValueWidget, self).__init__(**kwargs)
