# I/O module that reads and writes the camera configuration file

import logging,sys
import ConfigParser
import ast, os
import scribe_globals


DEBUG = False

## initializing logger
log = logging.getLogger("configs3")
fh = logging.StreamHandler(sys.stdout)
if DEBUG:
    log.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)
    fh.setLevel(logging.INFO)
formatter=logging.Formatter('[%(levelname)s] [%(name)s] %(message)s (%(asctime)s)')
fh.setFormatter(formatter)
log.addHandler(fh)

class AbstractConfig(object):     
    path = None

    def __init__(self, **kwargs):
        ConfigParser.ConfigParser.__init__(self)
        try:
            log.info("Init config")
            self.load_config()
            log.info("Loaded config: {0}".format(self.config))
        except:
            print "couldn't load configuration"
            pass

    def load_config(self):
        """
        Returns the config object
        """
        log.info("Loading configuration file at {0}".format(self.path))
        if os.path.exists(self.path):
        #create_config(self.path)
            log.info("File exists!")
        else:
            log.warn("No config file found. Creating one...")
            if type(self) is ScribeCameraConfig:
                try:
                    with open(self.path, "wb+") as config_file:
                        self.config = ConfigParser.ConfigParser()
                        self.config.add_section('general')
                        self.config.set('general','model','tts')
                        #self.config.set('general','default_driver','gphoto-sony')
                        self.config.write(config_file)
                        config_file.close()
                        log.info("Created config {0}".format(self.config))
                        return self.config
                except Exception as a:
                    log.error("Failed to load scribe camera config. Error: {0}".format(a))
                    
        self.config = ConfigParser.ConfigParser()
        self.config.read(self.path)    
        return self.config
    
    def print_config(self):
        for each_section in self.config.sections():
            for (each_key, each_val) in self.config.items(each_section):
                print each_key, ':', each_val
        return

    def get_config(self):
        try:
            return self.config
        except:
            return self.load_config()

    def get_sections(self):
        return self.config.sections()

    def get_section(self, section):
        return dict(self.config.items(section))

    def get_setting(self, section, setting):
        """
        Print out a setting
        """
        config = self.get_config()
        value = config.get(section, setting)
        print "{section} {setting} is {value}".format(
            section=section, setting=setting, value=value)
        return value
     
     
    def update_setting(self, section, setting, value):
        """
        Update a setting
        """
        self.config = self.get_config()
        self.config.set(section, setting, value)
        with open(self.path, "wb") as config_file:
            self.config.write(config_file)
     
     
    def delete_setting(self, section, setting):
        """
        Delete a setting
        """
        self.config = self.get_config()
        self.config.remove_option(section, setting)
        with open(self.path, "wb") as config_file:
            self.config.write(config_file)

    def delete_section(self, section):
        """
        Delete a setting
        """
        self.config = self.get_config()
        self.config.remove_section(section)
        with open(self.path, "wb") as config_file:
            self.config.write(config_file)

# list of supported camera models
class CamerasConfig(AbstractConfig):
    def __init__(self, **kwargs):
        #super(CameraConfig, self).__init__(**kwargs)
        #self.path = os.path.join(scribe_globals.config_dir, scribe_globals.camera_config)
        self.path = os.path.join(os.getcwd(), 'optics', 'confs', 'camera-models.conf')
        self.get_config()

# List of drivers, commands, bindings and env vars
class DriverConfig(AbstractConfig):
    def __init__(self, **kwargs):
        #super(CameraConfig, self).__init__(**kwargs)
        #self.path = os.path.join(scribe_globals.config_dir, scribe_globals.camera_config)
        self.path = os.path.join(os.getcwd(), 'optics', 'confs', 'drivers.conf')
        self.get_config()

# this ojbect contains the configuration for the scribe
class ScribeCameraConfig(AbstractConfig):
    cameras_config = CamerasConfig()
    driver_config = DriverConfig()

    def __init__(self, **kwargs):
        log.info("ScribeCameraConfig: Loading")
        self.path = os.path.expanduser(os.path.join(scribe_globals.config_dir, scribe_globals.camera_subsys))
        self.get_config()
        # only cameras in config + general
        self.num_cameras = len(self.get_sections()) -1 
        #self.sides_available = [x for x in self.get_sections() if x != 'general']
        log.info("ScribeCameraConfig: Loaded number of cameras: {0}, sections!=general {1}, path: {2})".format(self.num_cameras, self.get_camera_list(), self.path))
        #super(AbstractConfig, self).__init__(**kwargs)
        # sono solo dizionarietti

    def get_camera_config(self, side):
        #check ppi!
        return dict(self.get_section(side))

    def flush_side(self, side):
        print "Flushing side", side,
        for setting in self.get_section(side):
            self.delete_setting(side, setting)
        print "... done!"

    def flush(self):
        for side in self.get_camera_list():
            self.delete_section(side)
            print "flushed", side,
        print "All done!"

    def get_as_dict(self):
        ret = {}
        for each_section in self.config.sections():
            section = {}
            for (each_key, each_val) in self.config.items(each_section):
                section[each_key] = each_val
            ret[each_section] = section
        return ret

    def set_camera_config(self, side, temp_config = {}):
        log.info("ScribeCameraConfig::set_camera_config: setting {0} on {1}side".format(temp_config, side))
        for key, value in temp_config.iteritems():
            self.update_setting(side, key, value)
        return self.get_camera_config(side)
        #check ppi!
        #return dict(self.get_section(side))

    def set_camera_config_param(self, side, param, value):
        print "Updating {0} to {1} in {2} camera config".format(param, value, side)
        try:
            self.update_setting(side, param, value)
        except ConfigParser.NoSectionError as e:
            print "Section not found in file, creating..."
            self.config.add_section(side)
            print "added section", side
            self.set_camera_config_param(side, param, value)
            print "-> end exception block in set_camera_config_param"
        #check ppi!
        #return dict(self.get_section(side))

    def get_camera_driver(self, side):
        log.debug("get_camera_driver: init - {0} side".format(side))
        c = self.get_camera_config(side)
        log.debug("get_camera_driver: loaded config {0}".format(c))
        try:
            log.debug("get_camera_driver: Is fastcap to be used? {0}".format(self.get_general_config_value('fastcap')))
            if self.get_general_config_value('fastcap') == True:
                log.warn("Using fastcap!")
                ret = self.driver_config.get_section('fastcap')
                log.info("get_camera_driver: Got {0}) on {1} side and camera {2}".format(ret, side, c['model'] ))
                return ret
            else:
                pass
        except:
            log.info("Fastcap is not enabled. Looking for driver.")
            pass
        try:
            driver = self.cameras_config.get_section(c['model'])
            log.debug("get_camera_driver: {1} matches driver {0} on side {2}".format(driver, c['model'], side))
        except:
            log.debug("No driver was specified. Educately guessing...")
            from difflib import SequenceMatcher as SM
            models = self.cameras_config.get_sections()
            scores = {}
            for supported_model in models:
                full_name = self.cameras_config.get_section(supported_model)['full-name']
                log.debug("get_camera_driver: Checking supported model {0} ({1})".format(full_name, supported_model))
                scores[supported_model] = SM(None, full_name, c['model'] ).ratio()
            ss = sorted(scores, key= scores.get)
            log.debug("get_camera_driver: Scores: {0}".format(ss))
            driver = self.cameras_config.get_section(ss.pop())
            log.debug("get_camera_driver: And the winner is ({0})".format(driver))

        ret = self.driver_config.get_section(driver['driver'])
        log.info("get_camera_driver: Got {0}) on {1} side and camera {2}".format(ret, side, c['model'] ))
        return ret

    def get_camera_list(self):
        ret= [x for x in self.get_sections() if x != 'general' and not self.is_section_empty(x)]
        return ret

    def is_section_empty(self, section):
        ret = True
        for setting in self.get_section(section):
            if setting:
                ret = False
        return ret


    def is_vector_current(self, vector):
        log.debug("is_vector_current:: Checking vector {0}".format(vector))
        flag = True
        try:
            for vector_side in vector:
                #log.info("is_vector_current:: Checking {0} side".format(vector_side))
                vector_port = vector[vector_side]
                config_cam_side = self.get_camera_by_port(vector_port)
                log.debug("is_vector_current:: Checking that cam at {0}, configured to be on the {1} side is in fact there...".format(vector_port, vector_side))
                if config_cam_side == vector_side:
                    flag = True
                else:
                    flag = False
                log.debug("is_vector_current:: Flag is {0} for {1} and {2}".format(flag, config_cam_side, vector_side))
        except Exception as e:
            log.error("is_vector_current:: Error while comparing: {0})".format(e))
            flag = False
        
        log.info("is_vector_current:: VERDICT --> {0})".format(flag))
        return flag

    def get_camera_by_port(self, port):
        log.debug("get_camera_by_port:: Port {0}".format(port))
        try:
            for side in self.get_camera_list():
                c = self.get_camera_config(side)
                log.debug("get_camera_by_port:: c= {0}, side= {1} --> port{2}".format(c, side, c['port']))
                if port == c['port']:
                    log.debug("get_camera_by_port:: returning {0}".format(side))
                    return side
        except Exception as e:
            return None

    def get_camera_port(self, side):
        try:
            c = self.get_camera_config(side)
            return c['port']
        except Exception as e:
            log.error("ScribeCameraConfig::get_camera_port: Error {0}".format(e))
            return False


    def get_driver_by_name(self, driver_name):
        available_drivers = [x.split('gphoto-')[1] for x in self.driver_config.get_sections() if x != "fastcap"]
        log.info("ScribeCameraConfig::get_driver_by_name: Looking for {0} in {1})".format(driver_name, available_drivers))
        for vendor in available_drivers:        
            if vendor in driver_name:
                log.info("ScribeCameraConfig::get_driver_by_name: found {0})".format(vendor))
                return self.driver_config.get_section(driver_name)
        log.info("ScribeCameraConfig::get_driver_by_name: No match found.)")
        return False

    def get_general_config_value(self, value):
        ret = {}
        log.info("get_general_config_value:: looking for '{0}'".format(value))
        general_section = self.get_section('general')
        log.info("get_general_config_value:: general_section={0}".format(general_section))
        if value in general_section:
            return general_section[value]
        else:
            return False




